#ifndef NET_CONFIG_H
#define NET_CONFIG_H

#include <nuttx/wireless/sx127x.h>

struct lora_net_config_st
{
    struct sx127x_modem_s modem;
    uint32_t freq;
    int spi_fd;
    uint16_t node_address;
    uint16_t node_num;
    void (*sys_msg_callback)(int);
    void (*user_msg_callback)(int);
};

bool net_config_is_update(void);
void set_config_update(bool b);

struct lora_net_config_st *get_net_config(void);
void load_config(struct lora_net_config_st *config);
void apply_config(void);
int get_config_fd(void);
uint16_t get_config_nodeaddress(void);
void set_config_nodeaddress(uint16_t node);
void set_RSSI(void);
uint8_t get_RSSI(void);

void lora_set_bandwidth(const uint8_t bandwidth);
void lora_set_spreadfactor(const uint8_t spreadfactor);

void lora_set_para(const int lora_fd, const uint8_t band_width,
                   const uint8_t spread_factor, const uint32_t freq, const uint8_t node_add);

int lora_net_init(const char *dev, const uint8_t band_width,
                  const uint8_t spread_factor, const uint32_t freq, const uint8_t node_add);
#endif