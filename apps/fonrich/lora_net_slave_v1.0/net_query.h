#ifndef NET_QUERY_H
#define NET_QUERY_H

#include "node.h"
#include <stdbool.h>
#include <stdint.h>

int pack_query(pack_base_st **pbuf, uint8_t dotid);
bool alldot_query(void);

#endif