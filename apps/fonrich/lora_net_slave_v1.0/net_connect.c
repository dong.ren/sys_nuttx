
#include "net_connect.h"
#include "counter.h"
#include "m_timer.h"
#include "mshare.h"
#include "mshare.h"
#include "net_config.h"
#include "net_query.h"
#include "net_sync.h"
#include "net_time.h"
#include "node.h"
#include "receive.h"
#include "sysqueue.h"
#include "userqueue.h"
#include <errno.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>

#define RETRY_MAX 5
#define SLAVE_ACK_OVERTIME 3

void send_pack(pack_base_st *pack);
uint8_t get_pack_id(pack_base_st *pack);

pack_base_st *send_sys_context(pack_context_em pc);
void send_user_context(void);

void slave_ack_overtime_handle(void);

mode_em lora_mode;

static struct receive_manage_st re_manage;
static struct send_manage_st send_manage;

static void connect_init(void)
{
    send_manage.retry = 0;
    send_manage.g_query_id = 1; //start from node 1
    re_manage.re_overtime_cnt = 0;

    userqueue_clr();
    sysqueue_clr();
    node_set_offline();
    node_init();
    cnt_reset_interval(LAST_HOST_PACK);
}

void *connect_task(void)
{

    int ret;
    uint8_t recvd[MAX_PACK_LEN];
    uint8_t usersendbuf[MAX_PACK_LEN];
    int function = 0;

    pack_base_st *pcontext = NULL;

    lora_mode = INIT;
    sysqueue_init();
    userqueue_init();
    softTimerInit();

    while (1)
    {
        switch (lora_mode)
        {
        case INIT:
            mDebug("init\n");
            connect_init();
            lora_mode = RECEIVE_INIT;
            break;
        case RECEIVE_INIT:
            apply_config();
            lora_mode = RECEIVEMODE;
            break;
        case RECEIVEMODE:
            if (net_config_is_update())
            {
                lora_mode = INIT;
            }

            ret = read(get_config_fd(), recvd, MAX_PACK_LEN);

            if (ret > 0)
            {
                set_RSSI();

                function = depack(recvd);
                switch (function)
                {
                case FUNCTION_QUERY:
                    mDebug("query\n");
                    send_manage.pack_context = PACK_QUERY;
                    lora_mode = SEND_CONTEXT;
                    break;
                case FUNCTION_SYNC:
                    mDebug("sync\n");
                    break;
                case FUNCTION_SYNCTIME:
                    break;
                case FUNCTION_USER:
                    if (BROADCAST_ADDRESS != get_pack_id((pack_base_st *)recvd))
                    {
                        lora_mode = SEND_USER;
                    }
                    else
                    {
                        lora_mode = RECEIVEMODE;
                    }
                    break;
                default:
                    break;
                }
            }
            else //no re
            {
            }
            if (cnt_get_interval_off(LAST_HOST_PACK) > NO_HOST_PACK_OFFLINE_TIME)
            {
                node_set_offline();
            }
            break;
        case SEND_USER:
            if (userqueue_get(usersendbuf)) //user
            {
                pcontext = (pack_base_st *)usersendbuf;
                lora_mode = SEND_INIT;
            }
            else
            {
                usleep(1000);
            }
            break;
        case SEND_CONTEXT:
            //sys
            pcontext = send_sys_context(send_manage.pack_context);

            lora_mode = SEND_INIT;

            break;
        case SEND_INIT:
            lora_mode = SENDMODE;
            break;
        case SENDMODE:
            if (NULL != pcontext)
            {
                send_pack(pcontext);
            }
            lora_mode = RECEIVEMODE;
            break;
        default:
            break;
        }
    }
}

pack_base_st *send_sys_context(pack_context_em pc)
{
    pack_base_st *ppack = NULL;

    switch (pc)
    {
    case PACK_QUERY:
        pack_query(&ppack, get_config_nodeaddress());
        break;
    case PACK_ALLOW_REGISTER:
        break;
    default:
        break;
    }
    return ppack;
}

uint8_t get_pack_id(pack_base_st *pack)
{
    return pack->dest_addr;
}

void send_pack(pack_base_st *pack)
{
    usleep(10000);
    write(get_config_fd(), pack, pack->length);
}

void send_user_context(void)
{
}
