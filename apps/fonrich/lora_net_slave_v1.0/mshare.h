#ifndef M_SHARE_H
#define M_SHARE_H

#include <stdio.h>
#include <unistd.h>

// #define CONFIG_MY_DEBUG
#ifdef CONFIG_MY_DEBUG
#define mDebug(format, ...) printf(format, ##__VA_ARGS__)
#else
#define mDebug(x...)
#endif

#endif