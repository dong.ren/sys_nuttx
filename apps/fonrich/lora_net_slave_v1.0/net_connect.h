#ifndef NET_CONNECT_H
#define NET_CONNECT_H

#include <stdbool.h>
#include <stdint.h>

typedef enum { INIT,
               RECEIVE_INIT,
               RECEIVEMODE,
               SEND_CONTEXT,
               SEND_USER,
               SEND_INIT,
               SENDMODE,
} mode_em;

typedef enum {
    PACK_TIME,
    PACK_SYNC,
    PACK_QUERY,
    PACK_ALLOW_REGISTER,
} pack_context_em;

struct receive_manage_st
{
    uint8_t re_overtime_cnt;
};

struct send_manage_st
{
    int retry;
    int g_query_id;
    pack_context_em pack_context;
};

void *connect_task(void);

#endif