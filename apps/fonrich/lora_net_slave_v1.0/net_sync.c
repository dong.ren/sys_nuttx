#include "net_sync.h"
#include "node.h"
#include <stdio.h>
#include <string.h>

static SyncPack_st sp;
int repack_sync(pack_base_st *pbuf)
{
    memcpy(&sp, pbuf, pbuf->length);
    return pbuf->length;
}

uint8_t *get_sync_data(void)
{
    return sp.data;
}

void clr_sync_data(void)
{
    memset(sp.data, 0, (MAX_NODE + 7) >> 3);
}