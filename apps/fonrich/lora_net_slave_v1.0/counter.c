#include "counter.h"
#include "m_timer.h"
#include "mshare.h"
#include "sys/time.h"
#include <stdint.h>
#include <stdio.h>

#define MAX_UINT16_VALUE 65535

static uint32_t Counter[MAXCOUNT];
static uint16_t CounterTimes[MAX_TIMES];
// static uint32_t gettimeoff(void);

void cnt_reset_interval(CounterInterval_em em)
{
    // Counter[em] = gettimeoff();
    Counter[em] = getTimerCnt();
}

uint32_t cnt_get_interval_off(CounterInterval_em em)
{
    uint32_t no_sec = getTimerCnt();
    return no_sec - Counter[em];
}
/*
uint32_t gettimeoff(void)
{
    struct timeval curtime;
    int ret;
    ret = gettimeofday(&curtime, NULL);
    if (ret < 0)
    {
        mDebug("gettimeofday fail..\n");
    }
    return curtime.tv_sec;
}*/

uint32_t cnt_measure_time_ms(void)
{
    struct timeval curtime;
    int ret;
    ret = gettimeofday(&curtime, NULL);
    if (ret < 0)
    {
        mDebug("gettimeofday fail..\n");
    }
    return curtime.tv_sec * 1000 + curtime.tv_usec / 1000;
}

void cnt_reset_times(CounterTimes_em em)
{
    CounterTimes[em] = 0;
}

void cnt_set_times(CounterTimes_em em, uint16_t times)
{
    CounterTimes[em] = times;
}

void cnt_times_up(CounterTimes_em em)
{
    if (CounterTimes[em] < MAX_UINT16_VALUE)
        CounterTimes[em]++;
}

uint16_t cnt_get_times(CounterTimes_em em)
{
    return CounterTimes[em];
}