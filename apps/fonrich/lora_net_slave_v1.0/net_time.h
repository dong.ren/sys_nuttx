#ifndef NET_TIME_H
#define NET_TIME_H

#include "node.h"

typedef struct
{
    pack_base_st st;
    uint32_t time;
} SyncTime_st;

int pack_synctime(pack_base_st **pbuf);

#endif