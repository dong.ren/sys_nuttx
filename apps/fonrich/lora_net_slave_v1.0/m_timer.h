#ifndef M_TIMER_H
#define M_TIMER_H

#include <stdint.h>

void softTimerInit(void);
uint32_t getTimerCnt(void);

#endif
