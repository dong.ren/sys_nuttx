#ifndef NODE_H
#define NODE_H
#include <stdbool.h>
#include <stdint.h>

#define MAX_NODE 100
#define MAX_USER_PACK_LEN 150
#define MAX_PACK_LEN (sizeof(pack_net_st))

#define BROADCAST_ADDRESS 0xFF
#define HOST_ADDRESS 0x00
// #define CHECK_SUM_LEN 5

#define FUNCTION_SYNC 20
#define FUNCTION_QUERY 21
#define FUNCTION_SYNCTIME 22
#define FUNCTION_ALLOW_REGISTER 23

#define FUNCTION_USER 128

#define NO_HOST_PACK_OFFLINE_TIME 60

#define clr_bit(arr, pos) (arr[(pos) >> 3] &= (~(0x80 >> ((pos) % 8))))
#define set_bit(arr, pos) (arr[(pos) >> 3] |= (0x80 >> ((pos) % 8)))
#define get_bit(arr, pos) (arr[(pos) >> 3] & (0x80 >> ((pos) % 8)))

typedef struct
{
    uint8_t source_addr;
    uint8_t dest_addr;
    uint8_t function;
    uint8_t bridge;
    uint8_t length;
    uint8_t checksum;
} pack_base_st;

typedef struct
{
    pack_base_st pack;
    uint8_t data[MAX_USER_PACK_LEN];
} pack_net_st;

void user_data(uint8_t *p, uint8_t id, uint8_t size);
uint8_t calc_checksum(uint8_t *sour, uint8_t n);

int lora_write(int fd, uint16_t node, const void *buf, uint8_t size);
int lora_read(int fd, uint16_t node, void *buf, uint8_t size);

int get_node_address(void);

void dispack(pack_base_st *base);
bool node_is_online(void);
void node_set_offline(void);

void node_init(void);

void set_msg_flag(void);
void clr_msg_flag(void);
bool get_msg_flag(void);

#endif