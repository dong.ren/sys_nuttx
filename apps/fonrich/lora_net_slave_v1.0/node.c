#include "node.h"
#include "mshare.h"
#include "net_config.h"
#include "net_sync.h"
#include "userqueue.h"
#include <nuttx/wireless/sx127x.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// static void set_dotonlineflag(uint8_t id);
// static void clr_dotonlineflag(uint8_t id);
static bool get_dotonlineflag(uint8_t id);
// uint8_t calc_checksum(uint8_t *sour, uint8_t n);

static pack_net_st node_info[2];
static uint8_t msg_flag = 0;

void node_init(void)
{
    clr_sync_data();
    msg_flag = 0;
}

void user_data(uint8_t *p, uint8_t id, uint8_t size)
{
    memcpy(&node_info[1], p, size);
    set_msg_flag();
}

int lora_write(int fd, uint16_t node, const void *buf, uint8_t size)
{
    if (size > MAX_USER_PACK_LEN)
    {
        return -1;
    }

    if (node <= MAX_NODE || BROADCAST_ADDRESS == node)
    {
        node_info[0].pack.bridge = 0;
        node_info[0].pack.function = FUNCTION_USER;
        node_info[0].pack.dest_addr = node;
        node_info[0].pack.length = sizeof(pack_base_st) + size;
        node_info[0].pack.source_addr = get_config_nodeaddress();
        node_info[0].pack.checksum = calc_checksum((uint8_t *)(&node_info[0]), node_info[0].pack.length);

        memcpy(node_info[0].data, buf, size);
        userqueue_put((uint8_t *)&node_info[0], node_info[0].pack.length);
        return node_info[0].pack.length;
    }
    return -1;
}

int lora_read(int fd, uint16_t node, void *buf, uint8_t size)
{
    int len = -1;
    if (size > MAX_USER_PACK_LEN)
    {
        size = MAX_USER_PACK_LEN;
    }

    if (node <= MAX_NODE)
    {
        len = node_info[1].pack.length - sizeof(pack_base_st);
        if (len > size)
        {
            len = size;
        }
        memcpy(buf, node_info[1].data, len);
    }
    clr_msg_flag();
    return len;
}

uint8_t calc_checksum(uint8_t *sour, uint8_t n)
{
    uint8_t cnt = 0;
    uint8_t sum = 0;
    for (cnt = 0; cnt < n; cnt++)
    {
        sum = sum + sour[cnt];
    }
    return sum;
}

void node_set_offline(void)
{
    clr_sync_data();
}

bool node_is_online(void)
{
    return get_dotonlineflag(get_config_nodeaddress());
}

// void set_dotonlineflag(uint8_t id)
// {
//     if (id > 0 && id <= MAX_NODE) //ID 1--40
//     {
//         set_bit(get_sync_data(), id - 1);
//     }
// }
// void clr_dotonlineflag(uint8_t id)
// {
//     if (id > 0 && id <= MAX_NODE)
//     {
//         clr_bit(get_sync_data(), id - 1);
//     }
// }
bool get_dotonlineflag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE)
    {
        if (get_bit(get_sync_data(), id - 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

void dispack(pack_base_st *base)
{
    mDebug("sour:%d,dest:%d,func:%d,bri:%d,len:%d,cs:%d\n", base->source_addr,
           base->dest_addr, base->function, base->bridge, base->length, base->checksum);
}

void set_msg_flag(void)
{
    msg_flag = 1;
}
void clr_msg_flag(void)
{
    msg_flag = 0;
}
bool get_msg_flag(void)
{
    if (msg_flag)
        return true;
    else
        return false;
}
