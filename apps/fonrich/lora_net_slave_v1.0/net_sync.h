#include "node.h"
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

typedef struct
{
    pack_base_st st;
    uint8_t data[(MAX_NODE + 7) >> 3];
} SyncPack_st;

int repack_sync(pack_base_st *pbuf);
uint8_t *get_sync_data(void);
void clr_sync_data(void);