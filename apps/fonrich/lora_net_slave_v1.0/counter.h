#ifndef COUNTER_H
#define COUNTER_H

#include <stdint.h>

#define ACK_TIME_OVERTIME 12

typedef enum {
    LAST_HOST_PACK,
    RSSI_UPDATE,
    UART485_SAMPLE,
    SD_SAVE,
    MAXCOUNT
} CounterInterval_em;

typedef enum {
    MAX_TIMES
} CounterTimes_em;

uint32_t cnt_get_interval_off(CounterInterval_em em);
void cnt_reset_interval(CounterInterval_em em);
void cnt_reset_times(CounterTimes_em em);
void cnt_set_times(CounterTimes_em em, uint16_t times);
void cnt_times_up(CounterTimes_em em);
uint16_t cnt_get_times(CounterTimes_em em);
uint32_t cnt_measure_time_ms(void);
#endif