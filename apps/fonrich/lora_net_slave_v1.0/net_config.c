#include "net_config.h"
#include "counter.h"
#include "net_connect.h"
#include <fcntl.h>
#include <nuttx/wireless/sx127x.h>
#include <pthread.h>
#include <sys/ioctl.h>

const uint8_t SX127X_MODEM_BANDWITH_7_8K_CONST = 0b0000;
const uint8_t SX127X_MODEM_BANDWITH_10_4K_CONST = 0b0001;
const uint8_t SX127X_MODEM_BANDWITH_15_6K_CONST = 0b0010;
const uint8_t SX127X_MODEM_BANDWITH_20_8K_CONST = 0b0011;
const uint8_t SX127X_MODEM_BANDWITH_31_25K_CONST = 0b0100;
const uint8_t SX127X_MODEM_BANDWITH_41_7K_CONST = 0b0101;
const uint8_t SX127X_MODEM_BANDWITH_62_5K_CONST = 0b0110;
const uint8_t SX127X_MODEM_BANDWITH_125K_CONST = 0b0111;
const uint8_t SX127X_MODEM_BANDWITH_250K_CONST = 0b1000;
const uint8_t SX127X_MODEM_BANDWITH_500K_CONST = 0b1001;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_7_CONST = 0x07;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_8_CONST = 0x08;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_9_CONST = 0x09;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_10_CONST = 0x0A;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_11_CONST = 0x0B;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_12_CONST = 0x0C;

static struct lora_net_config_st lora_net_config = {
    {SX127X_MODEM_IMPLICIT_HEADER_OFF,
     SX127X_MODEM_CODING_RATE_4_5,
     SX127X_MODEM_BANDWITH_62_5K,
     1, //rx_timeout_msg
     SX127X_MODEM_RX_PAYLOAD_CRC_ON,
     0, //tx_continux
     SX127X_MODEM_SPREADING_FACTOR_10},
    424000000, //freq
    0,         //spi_fd
    1,         //node_address
    30,        //node_num
    NULL,      //sys callback
    NULL,      //user callback
};
static bool config_update_flag = true;

void apply_config(void)
{
    bool low_data_optimize = true;

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ, &low_data_optimize);

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_CARRIER_FREQ, &lora_net_config.freq);

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_MODEM, &lora_net_config.modem);

    set_config_update(false);
}

struct lora_net_config_st *get_net_config(void)
{
    return &lora_net_config;
}

void load_config(struct lora_net_config_st *config)
{
    struct lora_net_config_st *pself = &lora_net_config;
    if (pself != config)
    {
        lora_net_config = *config;
    }
    config_update_flag = true;
}

bool net_config_is_update(void)
{
    return config_update_flag;
}
void set_config_update(bool b)
{
    config_update_flag = b;
}

int get_config_fd(void)
{
    return lora_net_config.spi_fd;
}

uint16_t get_config_nodeaddress(void)
{
    return lora_net_config.node_address;
}

void set_config_nodeaddress(uint16_t node)
{
    lora_net_config.node_address = node;
}

static uint32_t rssi = 0;
void set_RSSI(void)
{
    cnt_reset_interval(RSSI_UPDATE);
    ioctl(lora_net_config.spi_fd, SX127X_IOC_GET_RSSI, &rssi);
}
uint8_t get_RSSI(void)
{
    if (cnt_get_interval_off(RSSI_UPDATE) < 60)
    {
        return (uint8_t)rssi;
    }
    else
    {
        return 0;
    }
}

int lora_net_init(const char *dev, const uint8_t band_width,
                  const uint8_t spread_factor, const uint32_t freq, const uint8_t node_add)
{
    int fd = open(dev, O_RDWR);
    pthread_t p_thread;

    if (fd > 0)
    {
        lora_set_para(fd, band_width, spread_factor, freq, node_add);
        pthread_create(&p_thread, NULL, (void *)connect_task, NULL);
    }
    return fd;
}

void lora_set_para(const int lora_fd, const uint8_t band_width,
                   const uint8_t spread_factor, const uint32_t freq, const uint8_t node_add)
{
    lora_net_config.modem.band_width = band_width;
    lora_net_config.modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
    lora_net_config.modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
    lora_net_config.modem.spreading_factor = spread_factor;
    lora_net_config.modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
    lora_net_config.modem.rx_timeout_msb = 1;
    lora_net_config.modem.tx_continus = 0;

    lora_net_config.spi_fd = lora_fd;
    lora_net_config.freq = freq;
    lora_net_config.node_address = node_add;
    lora_net_config.sys_msg_callback = NULL;
    lora_net_config.user_msg_callback = NULL;

    config_update_flag = true;
}

void lora_set_freq(const uint32_t freq)
{
    lora_net_config.freq = freq;
    config_update_flag = true;
}

void lora_set_bandwidth(const uint8_t bandwidth)
{
    lora_net_config.modem.band_width = bandwidth;
    config_update_flag = true;
}

void lora_set_spreadfactor(const uint8_t spreadfactor)
{
    lora_net_config.modem.spreading_factor = spreadfactor;
    config_update_flag = true;
}
