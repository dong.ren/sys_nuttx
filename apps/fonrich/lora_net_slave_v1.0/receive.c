#include "receive.h"
#include "counter.h"
#include "mshare.h"
#include "net_config.h"
#include "net_sync.h"
#include "node.h"
#include <stdio.h>
#include <string.h>

static void sys_pack(pack_base_st *pbs);
static void user_pack(pack_base_st *pbs);

int depack(uint8_t *p)
{
    pack_base_st *pbs = (pack_base_st *)p;
    struct lora_net_config_st *pconfig = NULL;

    dispack(pbs);
    if (HOST_ADDRESS == pbs->source_addr)
    {
        cnt_reset_interval(LAST_HOST_PACK);
    }

    pconfig = get_net_config();

    if (get_config_nodeaddress() == pbs->dest_addr || BROADCAST_ADDRESS == pbs->dest_addr)
    {
        if (FUNCTION_USER == pbs->function)
        {
            user_pack(pbs);
            if (NULL != pconfig->user_msg_callback)
            {
                mDebug("user callback\n");
                pconfig->user_msg_callback(pbs->dest_addr);
            }
        }
        else
        {
            sys_pack(pbs);
            if (NULL != pconfig->sys_msg_callback)
            {
                // mDebug("sys callback\n");
                pconfig->sys_msg_callback(pbs->dest_addr);
            }
        }
        return pbs->function;
    }
    return -1;
}

void sys_pack(pack_base_st *pbs)
{
    switch (pbs->function)
    {
    case FUNCTION_QUERY:
        break;
    case FUNCTION_SYNC:
        // mDebug("sync len %d\n", pbs->length);
        repack_sync(pbs);
        break;
    case FUNCTION_ALLOW_REGISTER:
        break;
    default:
        break;
    }
}

void user_pack(pack_base_st *pbs)
{
    user_data((uint8_t *)pbs, pbs->dest_addr, pbs->length);
}
