#include "m_timer.h"
#include "mshare.h"
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <time.h>

#define MY_TIMER_SIGNAL 17
#define SIGVALUE_INT 42

#ifndef OK
#define OK (0)
#endif

typedef void (*timerTimeout)(int signo, siginfo_t* info, void* ucontext);

timer_t mSeekTimer;
timer_t mPrepareAsyncTimer;
volatile static uint32_t timerCnt_Sec = 0;

void seekTimerout(int signo, siginfo_t* info, void* ucontext)
{
    timerCnt_Sec++;
}

uint32_t getTimerCnt(void)
{
    return timerCnt_Sec;
}

void createTimer(timer_t* timerId, timerTimeout func)
{
    struct sigevent notify;
    struct sigaction act;
    struct sigaction oact;
    sigset_t set;
    int status;

    (void)sigemptyset(&set);
    (void)sigaddset(&set, MY_TIMER_SIGNAL);
    status = sigprocmask(SIG_UNBLOCK, &set, NULL);
    if (status != OK) {
        mDebug("signal sigprocmask failed,status=%d\n", status);
    }
    act.sa_flags = SA_SIGINFO;
    act.sa_sigaction = func;

    (void)sigfillset(&act.sa_mask);
    (void)sigdelset(&act.sa_mask, MY_TIMER_SIGNAL);

    status = sigaction(MY_TIMER_SIGNAL, &act, &oact);
    if (status != OK) {
        mDebug("Timer:sigaction error %d\n", status);
    }

    notify.sigev_notify = SIGEV_SIGNAL;
    notify.sigev_signo = MY_TIMER_SIGNAL;
    notify.sigev_value.sival_int = SIGVALUE_INT;

    status = timer_create(CLOCK_REALTIME, &notify, timerId);

    if (status != OK) {
        mDebug("timer_create failed,erron=%d\n", errno);
        return;
    }
}

void setTimer(timer_t timerId, int timeMSec)
{
    struct itimerspec its;

    its.it_interval.tv_sec = 1;
    its.it_interval.tv_nsec = 0;
    its.it_value.tv_sec = timeMSec / 1000;
    its.it_value.tv_nsec = (timeMSec % 1000) * 1000000;

    if (timer_settime(timerId, 0, &its, NULL) == -1) {
        mDebug("timer settime error\n");
    }
    mDebug("set time down");
    return;
}

void softTimerInit(void)
{
    createTimer(&mSeekTimer, seekTimerout);
    setTimer(mSeekTimer, 1000);
}