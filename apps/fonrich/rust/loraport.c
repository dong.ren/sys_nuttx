#include "fcntl.h"
#include "nuttx/wireless/sx127x.h"
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>

int16_t lora_open(const char *dev)
{
    int16_t fd = open(dev, O_RDWR);
    return fd;
}

void lora_set_para(const int16_t lora_fd, const uint8_t band_width,
                   const uint8_t spread_factor, const uint32_t freq)
{
    struct sx127x_modem_s modem;
    bool low_data_optimize = true;
    uint32_t temp;

    modem.band_width = band_width;
    modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
    modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
    modem.spreading_factor = spread_factor;
    modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
    modem.rx_timeout_msb = 3;
    modem.tx_continus = 0;

    ioctl(lora_fd, SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ, &low_data_optimize);
    ioctl(lora_fd, SX127X_IOC_SET_MODEM, &modem);
    ioctl(lora_fd, SX127X_IOC_SET_CARRIER_FREQ, &freq);

    ioctl(lora_fd, SX127X_IOC_GET_CARRIER_FREQ, &temp);
}

void lora_set_bw_sf(const int16_t lora_fd, const uint8_t band_width, const uint8_t spread_factor)
{
    struct sx127x_modem_s modem;
    bool low_data_optimize = true;

    modem.band_width = band_width;
    modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
    modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
    modem.spreading_factor = spread_factor;
    modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
    modem.rx_timeout_msb = 3;
    modem.tx_continus = 0;

    ioctl(lora_fd, SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ, &low_data_optimize);
    ioctl(lora_fd, SX127X_IOC_SET_MODEM, &modem);
}

void lora_set_freq(const int16_t lora_fd, const uint32_t freq)
{
    uint32_t temp = freq;
    ioctl(lora_fd, SX127X_IOC_SET_CARRIER_FREQ, &temp);
}

uint32_t lora_get_freq(const int16_t lora_fd)
{
    uint32_t temp;
    ioctl(lora_fd, SX127X_IOC_GET_CARRIER_FREQ, &temp);
    return temp;
}

int16_t lora_raw_write_c(const int16_t fd, uint16_t size, uint8_t *buf)
{
    return write(fd, buf, size);
}

int16_t lora_raw_read_c(const int16_t fd, uint16_t size, uint8_t *buf)
{
    return read(fd, buf, size);
}

uint8_t lora_get_rssi(const int16_t fd)
{
    uint32_t rssi;
    ioctl(fd, SX127X_IOC_GET_RSSI, &rssi);
    return (uint8_t)rssi;
}

int32_t lora_get_errno(void)
{
    return errno;
}