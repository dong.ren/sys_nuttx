#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <nuttx/config.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *port_mutex_create(void)
{
    pthread_mutex_t *mutex = malloc(sizeof(pthread_mutex_t));
    if (NULL == mutex)
    {
        return mutex;
    }
    pthread_mutex_init(mutex, NULL);
    return mutex;
}

void port_set_blocking(int32_t fd, bool block)
{
    int32_t flags = fcntl(fd, F_GETFL);
    if (block)
    {
        fcntl(fd, F_SETFL, flags & ~O_NONBLOCK);
    }
    else
    {
        fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    }
}

int port_mutex_lock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_lock(mutex);
}

int port_mutex_try_lock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_trylock(mutex);
}
int port_mutex_unlock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_unlock(mutex);
}

int port_mutex_destroy(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    int ret = pthread_mutex_destroy(mutex);
    if (ret == 0)
    {
        free(mutex);
    }
    return ret;
}

pthread_attr_t *port_set_stack_size(int size)
{
    pthread_attr_t *attr = malloc(sizeof(pthread_attr_t));
    pthread_attr_init(attr);
    pthread_attr_setstacksize(attr, size);
    return attr;
}

void port_destory_stack_attr(pthread_attr_t *attr)
{
    pthread_attr_destroy(attr);
    free(attr);
}

int port_open_wr(const char *path) { return open(path, O_RDWR, 0666); }

int port_open_wr_c(const char *path)
{
    return open(path, O_RDWR | O_CREAT, 0666);
}

int port_open_r(const char *path) { return open(path, O_RDONLY, 0666); }

mqd_t port_mq_open(int siz, char *path_buf)
{
    static unsigned int seed = 666;
    int i;
    srand(seed++);
    i = rand();
    snprintf(path_buf, 11, "/mq_%d", i);
    struct mq_attr attr;
    attr.mq_curmsgs = 0;
    attr.mq_flags = O_RDWR | O_CREAT;
    attr.mq_maxmsg = 8;
    attr.mq_msgsize = siz;
    return mq_open(path_buf, O_RDWR | O_CREAT, 0666, &attr);
}

int port_mq_close(mqd_t id) { return mq_close(id); }

int port_mq_receive(mqd_t id, char *buf, size_t msg_len)
{
    return mq_receive(id, buf, msg_len, (void *)NULL);
}

int port_mq_unlink(const char *path) { return mq_unlink(path); }

int port_mq_send(mqd_t id, const char *buf, size_t msg_len)
{
    return mq_send(id, buf, msg_len, (unsigned int)0);
}

uint8_t *get_stdout(void) { return (uint8_t *)stdout; }

sem_t *port_sem_create(size_t n)
{
    sem_t *sig = malloc(sizeof(sem_t));
    sem_init(sig, 0, n);
    return sig;
}

#ifdef CONFIG_NET

#include <arpa/inet.h>
#include <netinet/in.h>
#include <nuttx/net/tcp.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define SA struct sockaddr
struct sockaddr_in seraddr;
/*
int tcp_connect(const char *addrs)
{
    int port = 0;
    int i = 0, j = 0, k = 0, flag = 0;
    char ch;
    char ip_addr[20];
    char port_addr[10];

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("fail to tcp_socket");
        return -1;
    }

    for (i = 0; i < 20; i++)
    {
        ch = *(addrs + i);

        if (ch == ':')
        {
            flag = 1;
        }
        if (ch != ' ' && ch != '\0' && flag != 1)
        {
            ip_addr[j] = ch;
            j++;
        }
        if (ch == '\0')
        {
            break;
        }
        if (flag && ch != ':')
        {
            port_addr[k] = ch;
            k++;
        }
    }

    ip_addr[j] = '\0';
    port_addr[k] = '\0';

    for (i = 0; i < k; i++)
    {
        port = port * 10 + (port_addr[i] - 48);
    }

    seraddr.sin_family = AF_INET;
    seraddr.sin_port = htons(port);
    seraddr.sin_addr.s_addr = inet_addr(ip_addr);

    int ret_connect = connect(sockfd, (SA *)&seraddr, sizeof(seraddr));
    if (ret_connect < 0)
    {
        perror("fail to tcp_connect");
        return -1;
    }

    return sockfd;
}
*/
int tcp_recv(int sockfd, void *buf, size_t len)
{
    int ret_recv = recv(sockfd, buf, len, 0);
    if (ret_recv < 0)
    {
        perror("fail to tcp_recv");
        return -1;
    }
    return ret_recv;
}
/*
int tcp_send(int sockfd, const void *buf, size_t len)
{
    int ret_send = send(sockfd, buf, len, 0);
    if (ret_send < 0)
    {
        perror("fail to tcp_send");
        return -1;
    }
    return ret_send;
}
*/
int tcp_setsockopt(int sockfd)
{
    int optval = 1;

    int ret_setskop = setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, (char *)&optval, sizeof(unsigned int));
    if (ret_setskop < 0)
    {
        perror("fail to tcp_setsockopt_1");
        return -1;
    }
    // ret_setskop = setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char *)&optval, sizeof(unsigned int));
    // if (ret_setskop < 0)
    // {
    //     perror("fail to tcp_setsockopt_2");
    //     return -1;
    // }

    return ret_setskop;
}

#endif
