#include "net_query.h"
#include "net_config.h"
#include "node.h"
#include "sysqueue.h"

static pack_base_st query;

int pack_query(pack_base_st **pbuf, uint8_t dotid)
{
    int len = 0;
    query.source_addr = HOST_ADDRESS;
    query.dest_addr = dotid;
    query.function = FUNCTION_QUERY;
    query.bridge = 0;
    query.length = sizeof(pack_base_st);
    len = query.length;
    query.checksum = 0;
    query.checksum = calc_checksum((uint8_t *)&query, len);
    *pbuf = &query;
    return len;
}
void alldot_query(void)
{
    int i;
    for (i = 1; i <= get_config_node_total(); i++)
    {
        sysqueue_put(i);
    }
}
