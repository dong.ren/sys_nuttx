#include "node.h"
#include "mshare.h"
#include "net_config.h"
#include "net_connect.h"
#include "userqueue.h"
#include <nuttx/wireless/sx127x.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>

uint8_t calc_checksum(uint8_t *sour, uint8_t n);

static pack_net_st node_info[2];//0 send buf,1 re buf

static uint8_t dotonline[(MAX_NODE + 7) >> 3] = {0};
static uint8_t msg_flag[(MAX_NODE + 7) >> 3] = {0};
node_state_st node_state[MAX_NODE + 1];

void node_init(void)
{
    memset(dotonline, 0, sizeof(dotonline));
    memset(msg_flag, 0, sizeof(msg_flag));
    memset(node_state, 0, sizeof(node_state));
}

void user_data(uint8_t *p, uint8_t id, uint8_t size)
{
    memcpy(&node_info[1], p, size);
    set_msg_flag(id);
}

int lora_write(int fd, uint16_t node, const void *buf, uint8_t size)
{
    if (size > MAX_USER_PACK_LEN)
    {
        return -1;
    }

    if (((node <= MAX_NODE) || (BROADCAST_ADDRESS == node)) && (size < MAX_USER_DATA_LEN))
    {
        node_info[0].pack.bridge = 0;
        node_info[0].pack.function = FUNCTION_USER;
        node_info[0].pack.dest_addr = node;
        node_info[0].pack.length = sizeof(pack_base_st) + size;
        node_info[0].pack.source_addr = HOST_ADDRESS;
        node_info[0].pack.checksum = 0;
        memcpy(node_info[0].data, buf, size);

        node_info[0].pack.checksum = calc_checksum((uint8_t *)(&node_info[0]), node_info[0].pack.length);

        userqueue_put((uint8_t *)&node_info[0], node_info[0].pack.length);
        set_user_send_state(USER_MSG_WAIT);
        return node_info[0].pack.length;
    }
    return -1;
}

int lora_read(int fd, uint16_t node, void *buf, uint8_t size)
{
    int len;
    if (size > MAX_USER_PACK_LEN)
    {
        size = MAX_USER_PACK_LEN;
    }

    if (get_msg_flag(node) && node <= MAX_NODE)
    {
        len = node_info[1].pack.length - sizeof(pack_base_st);
        if (len > size)
        {
            len = size;
        }
        memcpy(buf, node_info[1].data, len);
        clr_msg_flag(node);
        return len;
    }
    else
    {
        return -1;
    }
}

uint8_t calc_checksum(uint8_t *sour, uint8_t n)
{
    uint8_t cnt = 0;
    uint8_t sum = 0;
    for (cnt = 0; cnt < n; cnt++)
    {
        sum = sum + sour[cnt];
    }
    return sum;
}

void set_msg_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE) //ID 1--40
    {
        set_bit(msg_flag, id - 1);
    }
    // mDebug("%x,%x,%x,%x,%x\n", dotonline[0], dotonline[1], dotonline[2], dotonline[3], dotonline[4]);
}
void clr_msg_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE)
    {
        clr_bit(msg_flag, id - 1);
    }
}
bool get_msg_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE)
    {
        if (get_bit(msg_flag, id - 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

void set_dot_online_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE) //ID 1--40
    {
        set_bit(dotonline, id - 1);
    }
    // mDebug("%x,%x,%x,%x,%x\n", dotonline[0], dotonline[1], dotonline[2], dotonline[3], dotonline[4]);
}
void clr_dot_online_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE)
    {
        clr_bit(dotonline, id - 1);
    }
}
bool get_dot_online_flag(uint8_t id)
{
    if (id > 0 && id <= MAX_NODE)
    {
        if (get_bit(dotonline, id - 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

uint8_t *GetDotOnLineInfo(void)
{
    return dotonline;
}

void set_node_rssi(uint8_t node, uint8_t rssi)
{
    if (node <= MAX_NODE)
    {
        node_state[node].rssi = rssi;
    }
}

uint8_t get_node_rssi(uint8_t node)
{
    if (node <= MAX_NODE)
    {
        return node_state[node].rssi;
    }
    return 0;
}

uint8_t get_lorarssi(void)
{
    uint32_t rssi = 0;
    ioctl(get_config_fd(), SX127X_IOC_GET_RSSI, &rssi);
    return rssi;
}

void dispack(pack_base_st *base)
{
    mDebug("sour:%d,dest:%d,func:%d,bri:%d,len:%d,cs:%d\n", base->source_addr,
           base->dest_addr, base->function, base->bridge, base->length, base->checksum);
}