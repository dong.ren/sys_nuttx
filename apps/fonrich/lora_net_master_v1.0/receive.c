#include "receive.h"
#include "net_connect.h"
#include "node.h"
#include <string.h>

static void user_pack(pack_base_st *pbs);

int depack(uint8_t *p)
{
    pack_base_st *pbs = (pack_base_st *)p;
    dispack(pbs);

    switch (pbs->function)
    {
    case FUNCTION_USER:
        user_pack(pbs);
        set_dot_online_flag(pbs->source_addr);
        break;
    case FUNCTION_SYNC:
        break;
    case FUNCTION_QUERY:
        set_node_rssi(pbs->source_addr, get_lorarssi());
        set_dot_online_flag(pbs->source_addr);
        break;
    default:
        set_user_send_state(USER_MSG_OVERTIME);
        break;
    }
    return pbs->function;
}
void user_pack(pack_base_st *pbs)
{
    if (pbs->source_addr <= MAX_NODE)
    {
        set_user_send_state(USER_MSG_COMPLISH);
        user_data((uint8_t *)pbs, pbs->source_addr, pbs->length);
    }
    else
    {
        set_user_send_state(USER_MSG_OVERTIME);
    }
}
