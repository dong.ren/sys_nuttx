
#include "net_connect.h"
#include "mshare.h"
#include "net_config.h"
#include "net_query.h"
#include "net_register.h"
#include "net_sync.h"
#include "net_time.h"
#include "node.h"
#include "receive.h"
#include "sysqueue.h"
#include "userqueue.h"
#include <errno.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>

#define RETRY_MAX 5
#define SLAVE_ACK_OVERTIME 0

const int USER_MSG_IDLE_CONST = 0;
const int USER_MSG_WAIT_CONST = 1;
const int USER_MSG_SENDING_CONST = 2;
const int USER_MSG_COMPLISH_CONST = 3;
const int USER_MSG_OVERTIME_CONST = 4;

static uint8_t get_pack_id(pack_base_st *pack);
static void send_pack(pack_base_st *pack);
int get_current_query_id(void);
int get_next_query_id(bool *b);

pack_base_st *send_sys_context(pack_context_em pc);
void send_user_context(void);

void slave_ack_overtime_handle(void);

static mode_em lora_mode;

static struct receive_manage_st re_manage;
static struct send_manage_st send_manage;

static void connect_init(void)
{
    re_manage.ack = ACK_OTHER;
    send_manage.sys_retry = 0;
    send_manage.user_retry = 0;
    send_manage.pack_context = PACK_SYNC;
    send_manage.last_pack_context = PACK_SYNC;
    send_manage.g_query_id = 1; //start from node 1
    re_manage.re_overtime_cnt = 0;

    set_user_send_state(USER_MSG_IDLE);
    sysqueue_clr();
    userqueue_clr();
    set_user_send_state(USER_MSG_OVERTIME);
    node_init();
}

void connect_task(void)
{

    int ret;
    uint8_t recvd[MAX_PACK_LEN];
    uint8_t usersendbuf[MAX_PACK_LEN];

    bool bl;
    pack_base_st *pcontext = NULL;
    static uint8_t user_sys_send_token = 0;

    lora_mode = INIT;
    sysqueue_init();
    userqueue_init();

    while (1)
    {
        switch (lora_mode)
        {
        case INIT:

            mDebug("net init\n");
            connect_init();
            alldot_query();

            apply_config();

            mDebug("net init over\n");

            lora_mode = SEND_CONTEXT;
            break;
        case RECEIVEMODE:

            ret = read(get_config_fd(), recvd, MAX_PACK_LEN);
            if (ret > 0)
            {
                lora_mode = SEND_CONTEXT;
                depack(recvd);
            }
            else if (EBADE == errno)
            {
                lora_mode = SENDMODE;
            }
            else
            {
                slave_ack_overtime_handle();
            }

            if (net_config_is_update())
            {
                lora_mode = INIT;
            }

            break;
        case SEND_CONTEXT:
            if (userqueue_cnt() && user_sys_send_token) //user
            {
                userqueue_get(usersendbuf);
                pcontext = (pack_base_st *)usersendbuf;
                user_sys_send_token = 0;
                send_manage.last_pack_context = send_manage.pack_context;
                send_manage.pack_context = PACK_USER;

                if (BROADCAST_ADDRESS != pcontext->dest_addr)
                {
                    set_user_send_state(USER_MSG_SENDING);
                }
                else
                {
                    set_user_send_state(USER_MSG_COMPLISH);
                }
            }
            else //sys
            {
                user_sys_send_token = 1;
                if (PACK_USER == send_manage.pack_context)
                {
                    send_manage.pack_context = send_manage.last_pack_context;
                }
                if (PACK_QUERY == send_manage.pack_context)
                {
                    get_next_query_id(&bl);
                    send_manage.sys_retry = 0;
                    if (bl)
                    {
                    }
                    else
                    {
                        alldot_query();
                        send_manage.pack_context = PACK_SYNC;
                    }
                }
                pcontext = send_sys_context(send_manage.pack_context);
            }

            lora_mode = SENDMODE;

            break;
        case SENDMODE:
            // mDebug("send mode\n");
            if (NULL != pcontext)
            {
                send_pack(pcontext);
            }
            switch (send_manage.pack_context)
            {
            case PACK_TIME:
                send_manage.pack_context = PACK_SYNC;
                lora_mode = SEND_CONTEXT;
                break;
            case PACK_SYNC:
                send_manage.pack_context = PACK_QUERY;
                lora_mode = SEND_CONTEXT;
                break;
            case PACK_QUERY:

                lora_mode = RECEIVEMODE;
                break;
            case PACK_USER:
                if (BROADCAST_ADDRESS != get_pack_id(pcontext))
                {
                    lora_mode = RECEIVEMODE;
                }
                else
                {
                    lora_mode = SEND_CONTEXT;
                }
                break;
            case PACK_ALLOW_REGISTER:
                lora_mode = RECEIVEMODE;
                break;
            default:
                lora_mode = RECEIVEMODE;
                break;
            }
            break;
        default:
            break;
        }
    }
}

void set_user_send_state(user_send_state_em sta)
{
    send_manage.user_send_state = sta;
}

int get_user_send_state(void)
{
    return (int)send_manage.user_send_state;
}

pack_base_st *send_sys_context(pack_context_em pc)
{
    pack_base_st *ppack = NULL;

    switch (pc)
    {
    case PACK_TIME:
        mDebug("pack time\n");
        pack_synctime(&ppack);
        break;
    case PACK_SYNC:
        mDebug("pack sync\n");
        pack_sync(&ppack);
        break;
    case PACK_QUERY:
        mDebug("pack %d\n", send_manage.g_query_id);
        re_manage.re_overtime_cnt = 0;
        pack_query(&ppack, get_current_query_id());
        break;
    case PACK_ALLOW_REGISTER:
        mDebug("pack allow register\n");
        pack_allow_register(&ppack);
        break;
    default:
        break;
    }
    return ppack;
}

void send_pack(pack_base_st *pack)
{

    dispack(pack);
    usleep(10000);
    write(get_config_fd(), pack, pack->length);
}

uint8_t get_pack_id(pack_base_st *pack)
{
    return pack->dest_addr;
}

void slave_ack_overtime_handle(void)
{
    re_manage.re_overtime_cnt++;
    if (re_manage.re_overtime_cnt > SLAVE_ACK_OVERTIME) //start counting if send completion
    {
        re_manage.re_overtime_cnt = 0;
        switch (send_manage.pack_context)
        {
        case PACK_QUERY:

            send_manage.sys_retry++;
            if (send_manage.sys_retry < RETRY_MAX)
            {
                lora_mode = SENDMODE;
                mDebug("resend\n");
            }
            else
            {
                send_manage.sys_retry = 0;
                lora_mode = SEND_CONTEXT;
                clr_dot_online_flag(get_current_query_id());
            }

            break;
        case PACK_USER:

            send_manage.user_retry++;
            if (send_manage.user_retry < lora_get_retry())
            {
                lora_mode = SENDMODE;
                mDebug("resend\n");
            }
            else
            {
                send_manage.user_retry = 0;
                lora_mode = SEND_CONTEXT;
                set_user_send_state(USER_MSG_OVERTIME);
            }

            break;
        case PACK_TIME:
            break;
        default:
            break;
        }
    }
}

int get_current_query_id(void)
{
    return send_manage.g_query_id;
}

int get_next_query_id(bool *b)
{
    if (!sysqueue_isempty())
    {
        sysqueue_get(&send_manage.g_query_id);
        *b = true;
    }
    else
    {
        *b = false;
    }
    return send_manage.g_query_id;
}

void send_user_context(void)
{
}
