#include "userqueue.h"
#include "mshare.h"
#include "stdio.h"
#include "string.h"
#include <pthread.h>

static user_queue_st queue;
static pthread_mutex_t mutex;

void userqueue_init(void)
{
    queue.head = 0;
    queue.tail = 0;
    queue.datacnt = 0;
    pthread_mutex_init(&mutex, NULL);
}

void userqueue_clr(void)
{
    queue.head = 0;
    queue.tail = 0;
    queue.datacnt = 0;
}

bool userqueue_put(uint8_t *pdata, uint8_t size)
{
    pthread_mutex_lock(&mutex);
    if (queue.datacnt < MAX_USER_QUEUE)
    {
        // mDebug("size %d,head %d\n", size, queue.head);
        memcpy(queue.data[queue.head], pdata, size);
        queue.head++;
        if (queue.head == MAX_USER_QUEUE)
        {
            queue.head = 0;
        }
        queue.datacnt++;
        pthread_mutex_unlock(&mutex);
        return true;
    }
    pthread_mutex_unlock(&mutex);
    return false;
}

//p buf size must bigger MAX_USER_DATA_LEN
bool userqueue_get(uint8_t *p)
{
    if (queue.datacnt > 0)
    {
        memcpy(p, queue.data[queue.tail], MAX_USER_DATA_LEN);
        queue.datacnt--;
        queue.tail++;
        if (queue.tail == MAX_USER_QUEUE)
        {
            queue.tail = 0;
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool userqueue_isempty(void)
{
    return queue.datacnt == 0;
}
bool userqueue_isfull(void)
{
    return queue.datacnt == MAX_USER_QUEUE;
}

uint8_t userqueue_cnt(void)
{
    return queue.datacnt;
}