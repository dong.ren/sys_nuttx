#include "net_sync.h"
#include "node.h"
#include <stdio.h>
#include <string.h>

static SyncPack_st sp;
int pack_sync(pack_base_st **pbuf)
{
    int len = 0;

    sp.st.dest_addr = BROADCAST_ADDRESS;
    sp.st.source_addr = HOST_ADDRESS;
    sp.st.bridge = 0;
    sp.st.function = FUNCTION_SYNC;
    sp.st.length = sizeof(SyncPack_st);
    len = sp.st.length;
    memcpy(sp.data, GetDotOnLineInfo(), sizeof(sp.data));
    sp.st.checksum = 0;
    sp.st.checksum = calc_checksum((uint8_t *)&sp, len);
    *pbuf = (pack_base_st *)&sp;
    return len;
}
