#ifndef NET_CONNECT_H
#define NET_CONNECT_H

#include <stdbool.h>
#include <stdint.h>

typedef enum { INIT,
               RECEIVEMODE,
               SEND_CONTEXT,
               SENDMODE,
} mode_em;

typedef enum {
    ACK_OK,
    ACK_CRC_ERR,
    ACK_OVER_TIME,
    ACK_OTHER,
} re_ack_em;

typedef enum {
    PACK_TIME,
    PACK_SYNC,
    PACK_QUERY,
    PACK_USER,
    PACK_ALLOW_REGISTER,
} pack_context_em;

typedef enum {
    USER_MSG_IDLE,
    USER_MSG_WAIT,
    USER_MSG_SENDING,
    USER_MSG_COMPLISH,
    USER_MSG_OVERTIME,
} user_send_state_em;

struct receive_manage_st
{
    re_ack_em ack;
    uint8_t re_overtime_cnt;
};

struct send_manage_st
{
    uint16_t sys_retry;
    uint16_t user_retry;
    user_send_state_em user_send_state;
    int g_query_id;
    pack_context_em pack_context;
    pack_context_em last_pack_context;
};

void connect_task(void);
void set_user_send_state(user_send_state_em sta);
int get_user_send_state(void);

#endif