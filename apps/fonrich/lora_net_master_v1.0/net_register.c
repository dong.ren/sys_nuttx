#include "net_register.h"
#include "node.h"

static pack_base_st arst;
int pack_allow_register(pack_base_st **pbuf)
{
    int len = 0;

    arst.dest_addr = BROADCAST_ADDRESS;
    arst.source_addr = HOST_ADDRESS;
    arst.bridge = 0;
    arst.function = FUNCTION_ALLOW_REGISTER;
    arst.length = sizeof(pack_base_st);
    len = arst.length;
    arst.checksum = 0;
    arst.checksum = calc_checksum((uint8_t *)&arst, len);
    *pbuf = &arst;

    return len;
}