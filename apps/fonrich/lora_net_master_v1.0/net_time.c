#include "net_time.h"

static SyncTime_st stime;

int pack_synctime(pack_base_st **pbuf)
{
    int len = 0;

    stime.st.dest_addr = BROADCAST_ADDRESS;
    stime.st.source_addr = HOST_ADDRESS;
    stime.st.bridge = 0;
    stime.st.function = FUNCTION_SYNCTIME;
    stime.time = 1234;
    stime.st.length = sizeof(SyncTime_st);
    len = stime.st.length;
    stime.st.checksum = calc_checksum((uint8_t *)&stime, len);
    *pbuf = (pack_base_st *)&stime;
    return len;
}
