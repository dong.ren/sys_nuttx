#include "sysqueue.h"
#include "string.h"
#include <pthread.h>

static queue_st queue;
static pthread_mutex_t sys_mutex;

void sysqueue_init(void)
{
    queue.head = 0;
    queue.tail = 0;
    queue.datacnt = 0;
    pthread_mutex_init(&sys_mutex, NULL);
}

void sysqueue_clr(void)
{
    queue.head = 0;
    queue.tail = 0;
    queue.datacnt = 0;
}

bool sysqueue_put(int id)
{
    pthread_mutex_lock(&sys_mutex);
    if (queue.datacnt < MAX_QUEUE)
    {
        queue.id[queue.head] = id;
        queue.head++;
        if (queue.head == MAX_QUEUE)
        {
            queue.head = 0;
        }
        queue.datacnt++;
        pthread_mutex_unlock(&sys_mutex);
        return true;
    }
    pthread_mutex_unlock(&sys_mutex);
    return false;
}

bool sysqueue_get(int *p)
{
    if (queue.datacnt > 0)
    {
        *p = queue.id[queue.tail];
        queue.datacnt--;
        queue.tail++;
        if (queue.tail == MAX_QUEUE)
        {
            queue.tail = 0;
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool sysqueue_isempty(void)
{
    return queue.datacnt == 0;
}
bool sysqueue_isfull(void)
{
    return queue.datacnt == MAX_QUEUE;
}

uint8_t sysqueue_cnt(void)
{
    return queue.datacnt;
}