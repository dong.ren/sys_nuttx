#ifndef NET_REGISTER_H
#define NET_REGISTER_H

#include "node.h"
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

int pack_allow_register(pack_base_st **pbuf);

#endif