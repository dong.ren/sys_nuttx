#include "net_config.h"
#include "mshare.h"
#include "net_connect.h"
#include "node.h"
#include "stdio.h"
#include <fcntl.h>
#include <nuttx/wireless/sx127x.h>
#include <sys/ioctl.h>

const uint8_t SX127X_MODEM_BANDWITH_7_8K_CONST = 0b0000;
const uint8_t SX127X_MODEM_BANDWITH_10_4K_CONST = 0b0001;
const uint8_t SX127X_MODEM_BANDWITH_15_6K_CONST = 0b0010;
const uint8_t SX127X_MODEM_BANDWITH_20_8K_CONST = 0b0011;
const uint8_t SX127X_MODEM_BANDWITH_31_25K_CONST = 0b0100;
const uint8_t SX127X_MODEM_BANDWITH_41_7K_CONST = 0b0101;
const uint8_t SX127X_MODEM_BANDWITH_62_5K_CONST = 0b0110;
const uint8_t SX127X_MODEM_BANDWITH_125K_CONST = 0b0111;
const uint8_t SX127X_MODEM_BANDWITH_250K_CONST = 0b1000;
const uint8_t SX127X_MODEM_BANDWITH_500K_CONST = 0b1001;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_7_CONST = 0x07;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_8_CONST = 0x08;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_9_CONST = 0x09;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_10_CONST = 0x0A;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_11_CONST = 0x0B;
const uint8_t SX127X_MODEM_SPREADING_FACTOR_12_CONST = 0x0C;

static struct lora_net_config_st lora_net_config;
static bool config_update_flag = true;

void dis_config(struct sx127x_modem_s modem);

void default_config(void)
{
    lora_net_config.modem.band_width = SX127X_MODEM_BANDWITH_62_5K;
    lora_net_config.modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
    lora_net_config.modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
    lora_net_config.modem.spreading_factor = SX127X_MODEM_SPREADING_FACTOR_10;
    lora_net_config.modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
    lora_net_config.modem.rx_timeout_msb = 1;
    lora_net_config.modem.tx_continus = 0;

    lora_net_config.freq = 424000000;
}

void apply_config(void)
{
    bool low_data_optimize = true;
    uint32_t freq;

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ, &low_data_optimize);

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_CARRIER_FREQ, &lora_net_config.freq);

    ioctl(lora_net_config.spi_fd, SX127X_IOC_SET_MODEM, &lora_net_config.modem);

    ioctl(lora_net_config.spi_fd, SX127X_IOC_GET_CARRIER_FREQ, &freq);

    mDebug("freq %ld\n", freq);

    set_config_update(false);
}

struct lora_net_config_st *get_net_config(void)
{
    return &lora_net_config;
}

void load_config(struct lora_net_config_st *config)
{
    struct lora_net_config_st *pself = &lora_net_config;
    if (pself != config)
    {
        lora_net_config = *config;
    }
    if (lora_net_config.node_total > MAX_NODE)
    {
        lora_net_config.node_total = MAX_NODE;
    }

    config_update_flag = true;
}

bool net_config_is_update(void)
{
    return config_update_flag;
}
void set_config_update(bool b)
{
    config_update_flag = b;
}

int get_config_fd(void)
{
    return lora_net_config.spi_fd;
}

uint16_t get_config_node_total(void)
{
    return lora_net_config.node_total;
}

void dis_config(struct sx127x_modem_s modem)
{
    // mDebug("spi_fd:%d\n", modem.spi_fd);
    mDebug("bandwidth:%d\n", modem.band_width);
    mDebug("code rate %d\n", modem.coding_rate);
    mDebug("crc:%d\n", modem.rx_payload_crc);
    mDebug("header %d\n", modem.implicit_header);
    mDebug("timeout %d\n", modem.rx_timeout_msb);
    mDebug("spreadingfactor:%d\n", modem.spreading_factor);
}

int lora_net_init(const char *dev, uint16_t retry, const uint8_t band_width,
                  const uint8_t spread_factor, const uint32_t freq, const uint8_t node_total)
{
    int fd = open(dev, O_RDWR);
    pthread_t p_thread;

    if (fd > 0)
    {
        lora_set_para(fd, retry, band_width, spread_factor, freq, node_total);
        pthread_create(&p_thread, NULL, (void *)connect_task, NULL);
    }
    return fd;
}

/***
*lora_fd:来自open，如fd = open("/dev/sx0", O_RDWR)
*band_width,带宽
*spread_factor,扩頻因子
*freq:频率
*node_total:节点总数
*/
void lora_set_para(const int lora_fd, uint16_t retry, const uint8_t band_width,
                   const uint8_t spread_factor, const uint32_t freq, const uint8_t node_total)
{

    lora_net_config.modem.band_width = band_width;
    lora_net_config.modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
    lora_net_config.modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
    lora_net_config.modem.spreading_factor = spread_factor;
    lora_net_config.modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
    lora_net_config.modem.rx_timeout_msb = 1;
    lora_net_config.modem.tx_continus = 0;
    lora_net_config.retry = retry;

    lora_net_config.spi_fd = lora_fd;
    lora_net_config.freq = freq;
    if (node_total > MAX_NODE)
    {
        lora_net_config.node_total = MAX_NODE;
    }
    else
    {
        lora_net_config.node_total = node_total; //less than MAX_NODE
    }
    config_update_flag = true;
}

uint16_t lora_get_retry(void)
{
    return lora_net_config.retry;
}

void lora_set_freq(const uint32_t freq)
{
    lora_net_config.freq = freq;
    config_update_flag = true;
}

void lora_set_node_total(uint8_t node_total)
{
    lora_net_config.node_total = node_total;
    config_update_flag = true;
}

void lora_set_bandwidth(const uint8_t bandwidth)
{
    lora_net_config.modem.band_width = bandwidth;
    config_update_flag = true;
}

void lora_set_spreadfactor(const uint8_t spreadfactor)
{
    lora_net_config.modem.spreading_factor = spreadfactor;
    config_update_flag = true;
}
