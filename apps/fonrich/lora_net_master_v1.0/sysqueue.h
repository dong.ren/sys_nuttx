#ifndef SYS_QUEUE_H
#define SYS_QUEUE_H

#include "stdbool.h"
#include "unistd.h"
#include <stdint.h>

#define MAX_DATA_LEN 20
#define MAX_QUEUE 45

typedef struct
{
    uint8_t head;
    uint8_t tail;
    uint8_t datacnt;
    int id[MAX_QUEUE];
} queue_st;

void sysqueue_init(void);
bool sysqueue_put(int id);
bool sysqueue_get(int *p);
bool sysqueue_isempty(void);
bool sysqueue_isfull(void);
void sysqueue_clr(void);
uint8_t sysqueue_cnt(void);
#endif