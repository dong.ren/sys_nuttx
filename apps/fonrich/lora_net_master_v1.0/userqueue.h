#ifndef USER_QUEUE_H
#define USER_QUEUE_H

#include "node.h"
#include "stdbool.h"
#include "unistd.h"
#include <stdint.h>

#define MAX_USER_DATA_LEN MAX_PACK_LEN
#define MAX_USER_QUEUE 2

typedef struct
{
    uint8_t head;
    uint8_t tail;
    uint8_t datacnt;
    uint8_t data[MAX_USER_QUEUE][MAX_USER_DATA_LEN];
} user_queue_st;

void userqueue_init(void);
bool userqueue_put(uint8_t *pdata, uint8_t size);
bool userqueue_get(uint8_t *p);
bool userqueue_isempty(void);
bool userqueue_isfull(void);
void userqueue_clr(void);
uint8_t userqueue_cnt(void);
#endif