

typedef signed char _int8_t;
typedef unsigned char _uint8_t;

typedef signed short _int16_t;
typedef unsigned short _uint16_t;

typedef signed int _int32_t;
typedef unsigned int _uint32_t;

typedef signed long long _int64_t;
typedef unsigned long long _uint64_t;




typedef signed int _intptr_t;
typedef unsigned int _uintptr_t;
typedef unsigned int irqstate_t;
typedef _int8_t int8_t;
typedef _uint8_t uint8_t;

typedef _int16_t int16_t;
typedef _uint16_t uint16_t;






typedef _int32_t int32_t;
typedef _uint32_t uint32_t;


typedef _int64_t int64_t;
typedef _uint64_t uint64_t;




typedef _int8_t int_least8_t;
typedef _uint8_t uint_least8_t;

typedef _int16_t int_least16_t;
typedef _uint16_t uint_least16_t;





typedef _int32_t int_least24_t;
typedef _uint32_t uint_least24_t;


typedef _int32_t int_least32_t;
typedef _uint32_t uint_least32_t;


typedef _int64_t int_least64_t;
typedef _uint64_t uint_least64_t;




typedef _int8_t int_fast8_t;
typedef _uint8_t uint_fast8_t;

typedef int int_fast16_t;
typedef unsigned int uint_fast16_t;





typedef _int32_t int_fast24_t;
typedef _uint32_t uint_fast24_t;


typedef _int32_t int_fast32_t;
typedef _uint32_t uint_fast32_t;


typedef _int64_t int_fast64_t;
typedef _uint64_t uint_fast64_t;




typedef _intptr_t intptr_t;
typedef _uintptr_t uintptr_t;
typedef _int64_t intmax_t;
typedef _uint64_t uintmax_t;
typedef uint8_t _Bool8;

typedef int16_t b8_t;
typedef uint16_t ub8_t;
typedef int32_t b16_t;
typedef uint32_t ub16_t;

typedef int64_t b32_t;
typedef uint64_t ub32_t;
b16_t b16sin(b16_t rad);
b16_t b16cos(b16_t rad);
b16_t b16atan2(b16_t y, b16_t x);


typedef float float32;




typedef double double_t;
typedef double float64;
typedef unsigned int mode_t;
typedef uintptr_t size_t;
typedef intptr_t ssize_t;
typedef uintptr_t rsize_t;







typedef int16_t uid_t;
typedef int16_t gid_t;



typedef uint16_t dev_t;



typedef uint16_t ino_t;





typedef int16_t pid_t;





typedef int16_t id_t;






typedef int16_t key_t;



typedef intptr_t ptrdiff_t;
typedef uint16_t wchar_t;
typedef uint32_t blkcnt_t;
typedef int32_t off_t;
typedef off_t fpos_t;




typedef int64_t off64_t;
typedef int64_t fpos64_t;




typedef int16_t blksize_t;



typedef unsigned int socklen_t;
typedef uint16_t sa_family_t;



typedef uint32_t clock_t;







typedef uint32_t useconds_t;
typedef int32_t suseconds_t;
typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;



typedef unsigned char unchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef signed char s_char;
typedef char *caddr_t;



typedef int (*main_t)(int argc, char *argv[]);

typedef uint16_t fb_coord_t;



struct fb_videoinfo_s
{
 uint8_t fmt;
 fb_coord_t xres;
 fb_coord_t yres;
 uint8_t nplanes;
};





struct fb_planeinfo_s
{
  void *fbmem;
  uint32_t fblen;
  fb_coord_t stride;
  uint8_t display;
  uint8_t bpp;
};
struct fb_vtable_s
{




  int (*getvideoinfo)( struct fb_vtable_s *vtable,
                      struct fb_videoinfo_s *vinfo);
  int (*getplaneinfo)( struct fb_vtable_s *vtable, int planeno,
                      struct fb_planeinfo_s *pinfo);
};
int up_fbinitialize(int display);
 struct fb_vtable_s *up_fbgetvplane(int display, int vplane);
void up_fbuninitialize(int display);
struct lcd_planeinfo_s
{
  int (*putrun)(fb_coord_t row, fb_coord_t col, const uint8_t *buffer,
                size_t npixels);
  int (*getrun)(fb_coord_t row, fb_coord_t col, uint8_t *buffer,
                size_t npixels);
  uint8_t *buffer;






  uint8_t bpp;
};



struct lcd_dev_s
{





  int (*getvideoinfo)( struct lcd_dev_s *dev,
         struct fb_videoinfo_s *vinfo);
  int (*getplaneinfo)( struct lcd_dev_s *dev, unsigned int planeno,
         struct lcd_planeinfo_s *pinfo);
  int (*getpower)(struct lcd_dev_s *dev);






  int (*setpower)(struct lcd_dev_s *dev, int power);



  int (*getcontrast)(struct lcd_dev_s *dev);



  int (*setcontrast)(struct lcd_dev_s *dev, unsigned int contrast);
};
typedef uint8_t nxgl_mxpixel_t;
typedef int16_t nxgl_coord_t;



struct nxgl_point_s
{
  nxgl_coord_t x;
  nxgl_coord_t y;
};



struct nxgl_size_s
{
  nxgl_coord_t w;
  nxgl_coord_t h;
};



struct nxgl_rect_s
{
  struct nxgl_point_s pt1;
  struct nxgl_point_s pt2;
};



struct nxgl_vector_s
{
  struct nxgl_point_s pt1;
  struct nxgl_point_s pt2;
};






struct nxgl_run_s
{
  b16_t x1;
  b16_t x2;
  nxgl_coord_t y;
};





struct nxgl_trapezoid_s
{
  struct nxgl_run_s top;
  struct nxgl_run_s bot;
};
void nxgl_rgb2yuv(uint8_t r, uint8_t g, uint8_t b,
                  uint8_t *y, uint8_t *u, uint8_t *v);
void nxgl_yuv2rgb(uint8_t y, uint8_t u, uint8_t v,
                  uint8_t *r, uint8_t *g, uint8_t *b);
void nxgl_setpixel_1bpp( struct lcd_planeinfo_s *pinfo,
                        const struct nxgl_point_s *pos, uint8_t color);
void nxgl_setpixel_2bpp( struct lcd_planeinfo_s *pinfo,
                        const struct nxgl_point_s *pos, uint8_t color);
void nxgl_setpixel_4bpp( struct lcd_planeinfo_s *pinfo,
                        const struct nxgl_point_s *pos, uint8_t color);
void nxgl_setpixel_8bpp( struct lcd_planeinfo_s *pinfo,
                        const struct nxgl_point_s *pos, uint8_t color);
void nxgl_setpixel_16bpp( struct lcd_planeinfo_s *pinfo,
                         const struct nxgl_point_s *pos, uint16_t color);
void nxgl_setpixel_24bpp( struct lcd_planeinfo_s *pinfo,
                         const struct nxgl_point_s *pos, uint32_t color);
void nxgl_setpixel_32bpp( struct lcd_planeinfo_s *pinfo,
                         const struct nxgl_point_s *pos, uint32_t color);
void nxgl_fillrectangle_1bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             uint8_t color);
void nxgl_fillrectangle_2bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             uint8_t color);
void nxgl_fillrectangle_4bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             uint8_t color);
void nxgl_fillrectangle_8bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             uint8_t color);
void nxgl_fillrectangle_16bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              uint16_t color);
void nxgl_fillrectangle_24bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              uint32_t color);
void nxgl_fillrectangle_32bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              uint32_t color);
void nxgl_getrectangle_1bpp( struct lcd_planeinfo_s *pinfo,
                            const struct nxgl_rect_s *rect,
                            void *dest, unsigned int deststride);
void nxgl_getrectangle_2bpp( struct lcd_planeinfo_s *pinfo,
                            const struct nxgl_rect_s *rect,
                            void *dest, unsigned int deststride);
void nxgl_getrectangle_4bpp( struct lcd_planeinfo_s *pinfo,
                            const struct nxgl_rect_s *rect,
                            void *dest, unsigned int deststride);
void nxgl_getrectangle_8bpp( struct lcd_planeinfo_s *pinfo,
                            const struct nxgl_rect_s *rect,
                            void *dest, unsigned int deststride);
void nxgl_getrectangle_16bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             void *dest, unsigned int deststride);
void nxgl_getrectangle_24bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             void *dest, unsigned int deststride);
void nxgl_getrectangle_32bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             void *dest, unsigned int deststride);
void nxgl_filltrapezoid_1bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_trapezoid_s *trap,
                             const struct nxgl_rect_s *bounds,
                             uint8_t color);
void nxgl_filltrapezoid_2bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_trapezoid_s *trap,
                             const struct nxgl_rect_s *bounds,
                             uint8_t color);
void nxgl_filltrapezoid_4bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_trapezoid_s *trap,
                             const struct nxgl_rect_s *bounds,
                             uint8_t color);
void nxgl_filltrapezoid_8bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_trapezoid_s *trap,
                             const struct nxgl_rect_s *bounds,
                             uint8_t color);
void nxgl_filltrapezoid_16bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_trapezoid_s *trap,
                              const struct nxgl_rect_s *bounds,
                              uint16_t color);
void nxgl_filltrapezoid_24bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_trapezoid_s *trap,
                              const struct nxgl_rect_s *bounds,
                              uint32_t color);
void nxgl_filltrapezoid_32bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_trapezoid_s *trap,
                              const struct nxgl_rect_s *bounds,
                              uint32_t color);
void nxgl_moverectangle_1bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             struct nxgl_point_s *offset);
void nxgl_moverectangle_2bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             struct nxgl_point_s *offset);
void nxgl_moverectangle_4bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             struct nxgl_point_s *offset);
void nxgl_moverectangle_8bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *rect,
                             struct nxgl_point_s *offset);
void nxgl_moverectangle_16bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              struct nxgl_point_s *offset);
void nxgl_moverectangle_24bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              struct nxgl_point_s *offset);
void nxgl_moverectangle_32bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *rect,
                              struct nxgl_point_s *offset);
void nxgl_copyrectangle_1bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *dest,
                             const void *src,
                             const struct nxgl_point_s *origin,
                             unsigned int srcstride);
void nxgl_copyrectangle_2bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *dest,
                             const void *src,
                             const struct nxgl_point_s *origin,
                             unsigned int srcstride);
void nxgl_copyrectangle_4bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *dest,
                             const void *src,
                             const struct nxgl_point_s *origin,
                             unsigned int srcstride);
void nxgl_copyrectangle_8bpp( struct lcd_planeinfo_s *pinfo,
                             const struct nxgl_rect_s *dest,
                             const void *src,
                             const struct nxgl_point_s *origin,
                             unsigned int srcstride);
void nxgl_copyrectangle_16bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *dest,
                              const void *src,
                              const struct nxgl_point_s *origin,
                              unsigned int srcstride);
void nxgl_copyrectangle_24bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *dest,
                              const void *src,
                              const struct nxgl_point_s *origin,
                              unsigned int srcstride);
void nxgl_copyrectangle_32bpp( struct lcd_planeinfo_s *pinfo,
                              const struct nxgl_rect_s *dest,
                              const void *src,
                              const struct nxgl_point_s *origin,
                              unsigned int srcstride);
void nxgl_rectcopy( struct nxgl_rect_s *dest,
                   const struct nxgl_rect_s *src);
void nxgl_rectoffset( struct nxgl_rect_s *dest,
                     const struct nxgl_rect_s *src,
                     nxgl_coord_t dx, nxgl_coord_t dy);
void nxgl_vectoradd( struct nxgl_point_s *dest,
                    const struct nxgl_point_s *v1,
                    const struct nxgl_point_s *v2);
void nxgl_vectsubtract( struct nxgl_point_s *dest,
                       const struct nxgl_point_s *v1,
                       const struct nxgl_point_s *v2);
void nxgl_rectintersect( struct nxgl_rect_s *dest,
                        const struct nxgl_rect_s *src1,
                        const struct nxgl_rect_s *src2);
_Bool8 nxgl_intersecting( const struct nxgl_rect_s *rect1,
                       const struct nxgl_rect_s *rect2);
void nxgl_rectadd( struct nxgl_rect_s *dest,
                  const struct nxgl_rect_s *src1,
                  const struct nxgl_rect_s *src2);
void nxgl_rectunion( struct nxgl_rect_s *dest,
                    const struct nxgl_rect_s *src1,
                    const struct nxgl_rect_s *src2);
void nxgl_nonintersecting( struct nxgl_rect_s result[4],
                          const struct nxgl_rect_s *rect1,
                          const struct nxgl_rect_s *rect2);
_Bool8 nxgl_rectoverlap( struct nxgl_rect_s *rect1,
                      struct nxgl_rect_s *rect2);
_Bool8 nxgl_rectinside( const struct nxgl_rect_s *rect,
                     const struct nxgl_point_s *pt);
void nxgl_rectsize( struct nxgl_size_s *size,
                   const struct nxgl_rect_s *rect);
_Bool8 nxgl_nullrect( const struct nxgl_rect_s *rect);
void nxgl_runoffset( struct nxgl_run_s *dest,
                    const struct nxgl_run_s *src,
                    nxgl_coord_t dx, nxgl_coord_t dy);
void nxgl_runcopy( struct nxgl_run_s *dest,
                  const struct nxgl_run_s *src);
void nxgl_trapoffset( struct nxgl_trapezoid_s *dest,
                     const struct nxgl_trapezoid_s *src,
                     nxgl_coord_t dx, nxgl_coord_t dy);
void nxgl_trapcopy( struct nxgl_trapezoid_s *dest,
                   const struct nxgl_trapezoid_s *src);
int nxgl_splitline( struct nxgl_vector_s *vector,
                   struct nxgl_trapezoid_s *traps,
                   struct nxgl_rect_s *rect,
                   nxgl_coord_t linewidth);
void nxgl_circlepts( const struct nxgl_point_s *center,
                    nxgl_coord_t radius,
                    struct nxgl_point_s *circle);
void nxgl_circletraps( const struct nxgl_point_s *center,
                      nxgl_coord_t radius,
                      struct nxgl_trapezoid_s *circle);
uint32_t nxglib_rgb24_blend(uint32_t color1, uint32_t color2, ub16_t frac1);
uint16_t nxglib_rgb565_blend(uint16_t color1, uint16_t color2, ub16_t frac1);
typedef void *NXHANDLE;



typedef void *NXWINDOW;
struct nx_callback_s
{
  void (*redraw)(NXWINDOW hwnd, const struct nxgl_rect_s *rect,
                 _Bool8 more, void *arg);
  void (*position)(NXWINDOW hwnd, const struct nxgl_size_s *size,
                   const struct nxgl_point_s *pos,
                   const struct nxgl_rect_s *bounds,
                   void *arg);
};
NXHANDLE nx_open( struct lcd_dev_s *dev);
void nx_close(NXHANDLE handle);
NXWINDOW nx_openwindow(NXHANDLE handle, const struct nx_callback_s *cb,
                       void *arg);
int nx_closewindow(NXWINDOW hwnd);
int nx_requestbkgd(NXHANDLE handle, const struct nx_callback_s *cb,
                   void *arg);
int nx_releasebkgd(NXWINDOW hwnd);
int nx_getposition(NXWINDOW hwnd);
int nx_setposition(NXWINDOW hwnd, const struct nxgl_point_s *pos);
int nx_setsize(NXWINDOW hwnd, const struct nxgl_size_s *size);
int nx_raise(NXWINDOW hwnd);
int nx_lower(NXWINDOW hwnd);
int nx_setpixel(NXWINDOW hwnd, const struct nxgl_point_s *pos,
                nxgl_mxpixel_t color[1]);
int nx_fill(NXWINDOW hwnd, const struct nxgl_rect_s *rect,
            nxgl_mxpixel_t color[1]);
int nx_getrectangle(NXWINDOW hwnd, const struct nxgl_rect_s *rect,
                    unsigned int plane, uint8_t *dest,
                    unsigned int deststride);
int nx_filltrapezoid(NXWINDOW hwnd, const struct nxgl_rect_s *clip,
                     const struct nxgl_trapezoid_s *trap,
                     nxgl_mxpixel_t color[1]);
int nx_drawline(NXWINDOW hwnd, struct nxgl_vector_s *vector,
                nxgl_coord_t width, nxgl_mxpixel_t color[1],
                uint8_t caps);
int nx_drawcircle(NXWINDOW hwnd, const struct nxgl_point_s *center,
                  nxgl_coord_t radius, nxgl_coord_t width,
                  nxgl_mxpixel_t color[1]);
int nx_fillcircle(NXWINDOW hwnd, const struct nxgl_point_s *center,
                  nxgl_coord_t radius,
                  nxgl_mxpixel_t color[1]);
int nx_setbgcolor(NXHANDLE handle, nxgl_mxpixel_t color[1]);
int nx_move(NXWINDOW hwnd, const struct nxgl_rect_s *rect,
            const struct nxgl_point_s *offset);
int nx_bitmap(NXWINDOW hwnd, const struct nxgl_rect_s *dest,
              const void *src[1],
              const struct nxgl_point_s *origin, unsigned int stride);
void nx_redrawreq(NXWINDOW hwnd, const struct nxgl_rect_s *rect);
int nx_constructwindow(NXHANDLE handle, NXWINDOW hwnd,
                       const struct nx_callback_s *cb, void *arg);
enum nx_fontid_e
{
  FONTID_DEFAULT = 0




  , FONTID_MONO5X8 = 18





  , FONTID_SANS17X22 = 14



  , FONTID_SANS20X26 = 15



  , FONTID_SANS23X27 = 1



  , FONTID_SANS22X29 = 2



  , FONTID_SANS28X37 = 3



  , FONTID_SANS39X48 = 4
  , FONTID_SERIF22X29 = 8



  , FONTID_SERIF29X37 = 9



  , FONTID_SERIF38X48 = 10
  , FONTID_TOM_THUMB_4X6 = 43

};



struct nx_fontmetric_s
{
  uint32_t stride : 3;
  uint32_t width : 6;
  uint32_t height : 6;
  uint32_t xoffset : 6;
  uint32_t yoffset : 6;
  uint32_t unused : 5;
};



struct nx_fontbitmap_s
{
  struct nx_fontmetric_s metric;
  const uint8_t *bitmap;
};






struct nx_fontset_s
{
  uint8_t first;
  uint8_t nchars;
  const struct nx_fontbitmap_s *bitmap;
};



struct nx_font_s
{
  uint8_t mxheight;
  uint8_t mxwidth;
  uint8_t mxbits;
  uint8_t spwidth;
};



struct nx_fontpackage_s
{
  uint8_t id;
  const struct nx_font_s metrics;
  const struct nx_fontset_s font7;



};




typedef void *FCACHE;



struct nxfonts_glyph_s
{
  struct nxfonts_glyph_s *flink;
  uint8_t code;
  uint8_t height;
  uint8_t width;
  uint8_t stride;
  uint8_t bitmap[1];
};
NXHANDLE nxf_getfonthandle(enum nx_fontid_e fontid);
 const struct nx_font_s *nxf_getfontset(NXHANDLE handle);
 const struct nx_fontbitmap_s *nxf_getbitmap(NXHANDLE handle, uint16_t ch);
int nxf_convert_1bpp( uint8_t *dest, uint16_t height,
                     uint16_t width, uint16_t stride,
                     const struct nx_fontbitmap_s *bm,
                     nxgl_mxpixel_t color);
int nxf_convert_2bpp( uint8_t *dest, uint16_t height,
                     uint16_t width, uint16_t stride,
                     const struct nx_fontbitmap_s *bm,
                     nxgl_mxpixel_t color);
int nxf_convert_4bpp( uint8_t *dest, uint16_t height,
                     uint16_t width, uint16_t stride,
                     const struct nx_fontbitmap_s *bm,
                     nxgl_mxpixel_t color);
int nxf_convert_8bpp( uint8_t *dest, uint16_t height,
                     uint16_t width, uint16_t stride,
                     const struct nx_fontbitmap_s *bm,
                     nxgl_mxpixel_t color);
int nxf_convert_16bpp( uint16_t *dest, uint16_t height,
                      uint16_t width, uint16_t stride,
                      const struct nx_fontbitmap_s *bm,
                      nxgl_mxpixel_t color);
int nxf_convert_24bpp( uint32_t *dest, uint16_t height,
                      uint16_t width, uint16_t stride,
                      const struct nx_fontbitmap_s *bm,
                      nxgl_mxpixel_t color);
int nxf_convert_32bpp( uint32_t *dest, uint16_t height,
                      uint16_t width, uint16_t stride,
                      const struct nx_fontbitmap_s *bm,
                      nxgl_mxpixel_t color);
FCACHE nxf_cache_connect(enum nx_fontid_e fontid,
                         nxgl_mxpixel_t fgcolor, nxgl_mxpixel_t bgcolor,
                         int bpp, int maxglyph);
void nxf_cache_disconnect(FCACHE fhandle);
NXHANDLE nxf_cache_getfonthandle(FCACHE fhandle);
 const struct nxfonts_glyph_s *nxf_cache_getglyph(FCACHE fhandle, uint8_t ch);


extern struct nx_fontset_s g_7bitfonts;



extern struct nx_font_s g_fonts;
static const uint8_t g_bitmap_33[] = {0x80, 0x80, 0x80, 0x0, 0x80};


static const uint8_t g_bitmap_34[] = {0xa0, 0xa0};


static const uint8_t g_bitmap_35[] = {0xa0, 0xe0, 0xa0, 0xe0, 0xa0};


static const uint8_t g_bitmap_36[] = {0x60, 0xc0, 0x60, 0xc0, 0x40};


static const uint8_t g_bitmap_37[] = {0x80, 0x20, 0x40, 0x80, 0x20};


static const uint8_t g_bitmap_38[] = {0xc0, 0xc0, 0xe0, 0xa0, 0x60};


static const uint8_t g_bitmap_39[] = {0x80, 0x80};


static const uint8_t g_bitmap_40[] = {0x40, 0x80, 0x80, 0x80, 0x40};


static const uint8_t g_bitmap_41[] = {0x80, 0x40, 0x40, 0x40, 0x80};


static const uint8_t g_bitmap_42[] = {0xa0, 0x40, 0xa0};


static const uint8_t g_bitmap_43[] = {0x40, 0xe0, 0x40};


static const uint8_t g_bitmap_44[] = {0x40, 0x80};


static const uint8_t g_bitmap_45[] = {0xe0};


static const uint8_t g_bitmap_46[] = {0x80};


static const uint8_t g_bitmap_47[] = {0x20, 0x20, 0x40, 0x80, 0x80};


static const uint8_t g_bitmap_48[] = {0x60, 0xa0, 0xa0, 0xa0, 0xc0};


static const uint8_t g_bitmap_49[] = {0x40, 0xc0, 0x40, 0x40, 0x40};


static const uint8_t g_bitmap_50[] = {0xc0, 0x20, 0x40, 0x80, 0xe0};


static const uint8_t g_bitmap_51[] = {0xc0, 0x20, 0x40, 0x20, 0xc0};


static const uint8_t g_bitmap_52[] = {0xa0, 0xa0, 0xe0, 0x20, 0x20};


static const uint8_t g_bitmap_53[] = {0xe0, 0x80, 0xc0, 0x20, 0xc0};


static const uint8_t g_bitmap_54[] = {0x60, 0x80, 0xe0, 0xa0, 0xe0};


static const uint8_t g_bitmap_55[] = {0xe0, 0x20, 0x40, 0x80, 0x80};


static const uint8_t g_bitmap_56[] = {0xe0, 0xa0, 0xe0, 0xa0, 0xe0};


static const uint8_t g_bitmap_57[] = {0xe0, 0xa0, 0xe0, 0x20, 0xc0};


static const uint8_t g_bitmap_58[] = {0x80, 0x0, 0x80};


static const uint8_t g_bitmap_59[] = {0x40, 0x0, 0x40, 0x80};


static const uint8_t g_bitmap_60[] = {0x20, 0x40, 0x80, 0x40, 0x20};


static const uint8_t g_bitmap_61[] = {0xe0, 0x0, 0xe0};


static const uint8_t g_bitmap_62[] = {0x80, 0x40, 0x20, 0x40, 0x80};


static const uint8_t g_bitmap_63[] = {0xe0, 0x20, 0x40, 0x0, 0x40};


static const uint8_t g_bitmap_64[] = {0x40, 0xa0, 0xe0, 0x80, 0x60};


static const uint8_t g_bitmap_65[] = {0x40, 0xa0, 0xe0, 0xa0, 0xa0};


static const uint8_t g_bitmap_66[] = {0xc0, 0xa0, 0xc0, 0xa0, 0xc0};


static const uint8_t g_bitmap_67[] = {0x60, 0x80, 0x80, 0x80, 0x60};


static const uint8_t g_bitmap_68[] = {0xc0, 0xa0, 0xa0, 0xa0, 0xc0};


static const uint8_t g_bitmap_69[] = {0xe0, 0x80, 0xe0, 0x80, 0xe0};


static const uint8_t g_bitmap_70[] = {0xe0, 0x80, 0xe0, 0x80, 0x80};


static const uint8_t g_bitmap_71[] = {0x60, 0x80, 0xe0, 0xa0, 0x60};


static const uint8_t g_bitmap_72[] = {0xa0, 0xa0, 0xe0, 0xa0, 0xa0};


static const uint8_t g_bitmap_73[] = {0xe0, 0x40, 0x40, 0x40, 0xe0};


static const uint8_t g_bitmap_74[] = {0x20, 0x20, 0x20, 0xa0, 0x40};


static const uint8_t g_bitmap_75[] = {0xa0, 0xa0, 0xc0, 0xa0, 0xa0};


static const uint8_t g_bitmap_76[] = {0x80, 0x80, 0x80, 0x80, 0xe0};


static const uint8_t g_bitmap_77[] = {0xa0, 0xe0, 0xe0, 0xa0, 0xa0};


static const uint8_t g_bitmap_78[] = {0xa0, 0xe0, 0xe0, 0xe0, 0xa0};


static const uint8_t g_bitmap_79[] = {0x40, 0xa0, 0xa0, 0xa0, 0x40};


static const uint8_t g_bitmap_80[] = {0xc0, 0xa0, 0xc0, 0x80, 0x80};


static const uint8_t g_bitmap_81[] = {0x40, 0xa0, 0xa0, 0xe0, 0x60};


static const uint8_t g_bitmap_82[] = {0xc0, 0xa0, 0xe0, 0xc0, 0xa0};


static const uint8_t g_bitmap_83[] = {0x60, 0x80, 0x40, 0x20, 0xc0};


static const uint8_t g_bitmap_84[] = {0xe0, 0x40, 0x40, 0x40, 0x40};


static const uint8_t g_bitmap_85[] = {0xa0, 0xa0, 0xa0, 0xa0, 0x60};


static const uint8_t g_bitmap_86[] = {0xa0, 0xa0, 0xa0, 0x40, 0x40};


static const uint8_t g_bitmap_87[] = {0xa0, 0xa0, 0xe0, 0xe0, 0xa0};


static const uint8_t g_bitmap_88[] = {0xa0, 0xa0, 0x40, 0xa0, 0xa0};


static const uint8_t g_bitmap_89[] = {0xa0, 0xa0, 0x40, 0x40, 0x40};


static const uint8_t g_bitmap_90[] = {0xe0, 0x20, 0x40, 0x80, 0xe0};


static const uint8_t g_bitmap_91[] = {0xe0, 0x80, 0x80, 0x80, 0xe0};


static const uint8_t g_bitmap_92[] = {0x80, 0x40, 0x20};


static const uint8_t g_bitmap_93[] = {0xe0, 0x20, 0x20, 0x20, 0xe0};


static const uint8_t g_bitmap_94[] = {0x40, 0xa0};


static const uint8_t g_bitmap_95[] = {0xe0};


static const uint8_t g_bitmap_96[] = {0x80, 0x40};


static const uint8_t g_bitmap_97[] = {0xc0, 0x60, 0xa0, 0xe0};


static const uint8_t g_bitmap_98[] = {0x80, 0xc0, 0xa0, 0xa0, 0xc0};


static const uint8_t g_bitmap_99[] = {0x60, 0x80, 0x80, 0x60};


static const uint8_t g_bitmap_100[] = {0x20, 0x60, 0xa0, 0xa0, 0x60};


static const uint8_t g_bitmap_101[] = {0x60, 0xa0, 0xc0, 0x60};


static const uint8_t g_bitmap_102[] = {0x20, 0x40, 0xe0, 0x40, 0x40};


static const uint8_t g_bitmap_103[] = {0x60, 0xa0, 0xe0, 0x20, 0x40};


static const uint8_t g_bitmap_104[] = {0x80, 0xc0, 0xa0, 0xa0, 0xa0};


static const uint8_t g_bitmap_105[] = {0x80, 0x0, 0x80, 0x80, 0x80};


static const uint8_t g_bitmap_106[] = {0x20, 0x0, 0x20, 0x20, 0xa0, 0x40};


static const uint8_t g_bitmap_107[] = {0x80, 0xa0, 0xc0, 0xc0, 0xa0};


static const uint8_t g_bitmap_108[] = {0xc0, 0x40, 0x40, 0x40, 0xe0};


static const uint8_t g_bitmap_109[] = {0xe0, 0xe0, 0xe0, 0xa0};


static const uint8_t g_bitmap_110[] = {0xc0, 0xa0, 0xa0, 0xa0};


static const uint8_t g_bitmap_111[] = {0x40, 0xa0, 0xa0, 0x40};


static const uint8_t g_bitmap_112[] = {0xc0, 0xa0, 0xa0, 0xc0, 0x80};


static const uint8_t g_bitmap_113[] = {0x60, 0xa0, 0xa0, 0x60, 0x20};


static const uint8_t g_bitmap_114[] = {0x60, 0x80, 0x80, 0x80};


static const uint8_t g_bitmap_115[] = {0x60, 0xc0, 0x60, 0xc0};


static const uint8_t g_bitmap_116[] = {0x40, 0xe0, 0x40, 0x40, 0x60};


static const uint8_t g_bitmap_117[] = {0xa0, 0xa0, 0xa0, 0x60};


static const uint8_t g_bitmap_118[] = {0xa0, 0xa0, 0xe0, 0x40};


static const uint8_t g_bitmap_119[] = {0xa0, 0xe0, 0xe0, 0xe0};


static const uint8_t g_bitmap_120[] = {0xa0, 0x40, 0x40, 0xa0};


static const uint8_t g_bitmap_121[] = {0xa0, 0xa0, 0x60, 0x20, 0x40};


static const uint8_t g_bitmap_122[] = {0xe0, 0x60, 0xc0, 0xe0};


static const uint8_t g_bitmap_123[] = {0x60, 0x40, 0x80, 0x40, 0x60};


static const uint8_t g_bitmap_124[] = {0x80, 0x80, 0x0, 0x80, 0x80};


static const uint8_t g_bitmap_125[] = {0xc0, 0x40, 0x20, 0x40, 0xc0};


static const uint8_t g_bitmap_126[] = {0x60, 0xc0};
static const struct nx_fontbitmap_s g_tom_thumb_4x6_7bitmaps[(126 - 33 + 1)] =
{
{ {1, 2, 5, 1, 0, 0}, g_bitmap_33 },


{ {1, 4, 2, 0, 0, 0}, g_bitmap_34 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_35 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_36 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_37 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_38 },


{ {1, 2, 2, 1, 0, 0}, g_bitmap_39 },


{ {1, 3, 5, 1, 0, 0}, g_bitmap_40 },


{ {1, 3, 5, 0, 0, 0}, g_bitmap_41 },


{ {1, 4, 3, 0, 0, 0}, g_bitmap_42 },


{ {1, 4, 3, 0, 1, 0}, g_bitmap_43 },


{ {1, 3, 2, 0, 3, 0}, g_bitmap_44 },


{ {1, 4, 1, 0, 2, 0}, g_bitmap_45 },


{ {1, 2, 1, 1, 4, 0}, g_bitmap_46 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_47 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_48 },


{ {1, 3, 5, 0, 0, 0}, g_bitmap_49 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_50 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_51 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_52 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_53 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_54 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_55 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_56 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_57 },


{ {1, 2, 3, 1, 1, 0}, g_bitmap_58 },


{ {1, 3, 4, 0, 1, 0}, g_bitmap_59 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_60 },


{ {1, 4, 3, 0, 1, 0}, g_bitmap_61 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_62 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_63 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_64 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_65 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_66 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_67 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_68 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_69 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_70 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_71 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_72 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_73 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_74 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_75 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_76 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_77 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_78 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_79 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_80 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_81 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_82 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_83 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_84 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_85 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_86 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_87 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_88 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_89 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_90 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_91 },


{ {1, 4, 3, 0, 1, 0}, g_bitmap_92 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_93 },


{ {1, 4, 2, 0, 0, 0}, g_bitmap_94 },


{ {1, 4, 1, 0, 4, 0}, g_bitmap_95 },


{ {1, 3, 2, 0, 0, 0}, g_bitmap_96 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_97 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_98 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_99 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_100 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_101 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_102 },


{ {1, 4, 5, 0, 1, 0}, g_bitmap_103 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_104 },


{ {1, 2, 5, 1, 0, 0}, g_bitmap_105 },


{ {1, 4, 6, 0, 0, 0}, g_bitmap_106 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_107 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_108 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_109 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_110 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_111 },


{ {1, 4, 5, 0, 1, 0}, g_bitmap_112 },


{ {1, 4, 5, 0, 1, 0}, g_bitmap_113 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_114 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_115 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_116 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_117 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_118 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_119 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_120 },


{ {1, 4, 5, 0, 1, 0}, g_bitmap_121 },


{ {1, 4, 4, 0, 1, 0}, g_bitmap_122 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_123 },


{ {1, 2, 5, 1, 0, 0}, g_bitmap_124 },


{ {1, 4, 5, 0, 0, 0}, g_bitmap_125 },


{ {1, 4, 2, 0, 0, 0}, g_bitmap_126 },




};
const struct nx_fontpackage_s g_tom_thumb_4x6_package =
{
  FONTID_TOM_THUMB_4X6,
  {
    6,
    4,
    
   7
                          ,
    4,
  },
  {
    33,
    (126 - 33 + 1),
    g_tom_thumb_4x6_7bitmaps
  }







};
