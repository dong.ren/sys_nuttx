#include <nuttx/config.h>
#include <stdint.h>
#include <stddef.h>
#include <debug.h>
#include <nuttx/nx/nx.h>
#include <nuttx/nx/nxfonts.h>
#include "nxfonts.h"

#define gdbg(x, ...)

#ifdef CONFIG_NXFONT_MONO5X8
extern const struct nx_fontpackage_s g_mono5x8_package;
#endif

/* SANS */

#ifdef CONFIG_NXFONT_SANS17X22
extern const struct nx_fontpackage_s g_sans17x22_package;
#endif

#ifdef CONFIG_NXFONT_SANS20X26
extern const struct nx_fontpackage_s g_sans20x26_package;
#endif

#ifdef CONFIG_NXFONT_SANS23X27
extern const struct nx_fontpackage_s g_sans23x27_package;
#endif

#ifdef CONFIG_NXFONT_UNICODE16X16
extern const struct nx_fontpackage_s g_unicode16x16_package;
#endif

#ifdef CONFIG_NXFONT_WENPT19X16
extern const struct nx_fontpackage_s g_wenpt19x16_package;
#endif

#ifdef CONFIG_NXFONT_WENPT14X12
extern const struct nx_fontpackage_s g_wenpt14x12_package;
#endif

#ifdef CONFIG_NXFONT_SANS22X29
extern const struct nx_fontpackage_s g_sans22x29_package;
#endif

#ifdef CONFIG_NXFONT_SANS28X37
extern const struct nx_fontpackage_s g_sans28x37_package;
#endif

#ifdef CONFIG_NXFONT_SANS39X48
extern const struct nx_fontpackage_s g_sans39x48_package;
#endif

/* SANS-BOLD */

#ifdef CONFIG_NXFONT_SANS17X23B
extern const struct nx_fontpackage_s g_sans17x23b_package;
#endif

#ifdef CONFIG_NXFONT_SANS20X27B
extern const struct nx_fontpackage_s g_sans20x27b_package;
#endif

#ifdef CONFIG_NXFONT_SANS22X29B
extern const struct nx_fontpackage_s g_sans22x29b_package;
#endif

#ifdef CONFIG_NXFONT_SANS28X37B
extern const struct nx_fontpackage_s g_sans28x37b_package;
#endif

#ifdef CONFIG_NXFONT_SANS40X49B
extern const struct nx_fontpackage_s g_sans40x49b_package;
#endif

/* SERIF */

#ifdef CONFIG_NXFONT_SERIF22X29
extern const struct nx_fontpackage_s g_serif22x29_package;
#endif

#ifdef CONFIG_NXFONT_SERIF29X37
extern const struct nx_fontpackage_s g_serif29x37_package;
#endif

#ifdef CONFIG_NXFONT_SERIF38X48
extern const struct nx_fontpackage_s g_serif38x48_package;
#endif

/* SERIF-BOLD */

#ifdef CONFIG_NXFONT_SERIF22X28B
extern const struct nx_fontpackage_s g_serif22x28b_package;
#endif

#ifdef CONFIG_NXFONT_SERIF27X38B
extern const struct nx_fontpackage_s g_serif27x38b_package;
#endif

#ifdef CONFIG_NXFONT_SERIF38X49
extern const struct nx_fontpackage_s g_serif38x49b_package;
#endif

static FAR const struct nx_fontpackage_s *g_fontpackages[] =
{

/* MONO */

#ifdef CONFIG_NXFONT_MONO5X8
	&g_mono5x8_package,
#endif
  
/* SANS */

#ifdef CONFIG_NXFONT_SANS17X22
	&g_sans17x22_package,
#endif

#ifdef CONFIG_NXFONT_SANS20X26
	&g_sans20x26_package,
#endif

#ifdef CONFIG_NXFONT_SANS23X27
	&g_sans23x27_package,
#endif

#ifdef CONFIG_NXFONT_UNICODE16X16
	&g_unicode16x16_package,
#endif

#ifdef CONFIG_NXFONT_WENPT19X16
	&g_wenpt19x16_package,
#endif

#ifdef CONFIG_NXFONT_WENPT14X12
	&g_wenpt14x12_package,
#endif

#ifdef CONFIG_NXFONT_SANS22X29
	&g_sans22x29_package,
#endif

#ifdef CONFIG_NXFONT_SANS28X37
	&g_sans28x37_package,
#endif

#ifdef CONFIG_NXFONT_SANS39X48
	&g_sans39x48_package,
#endif

/* SANS-BOLD */

#ifdef CONFIG_NXFONT_SANS17X23B
	&g_sans17x23b_package,
#endif

#ifdef CONFIG_NXFONT_SANS20X27B
	&g_sans20x27b_package,
#endif

#ifdef CONFIG_NXFONT_SANS22X29B
	&g_sans22x29b_package,
#endif

#ifdef CONFIG_NXFONT_SANS28X37B
	&g_sans28x37b_package,
#endif

#ifdef CONFIG_NXFONT_SANS40X49B
	&g_sans40x49b_package,
#endif

/* SERIF */

#ifdef CONFIG_NXFONT_SERIF22X29
	&g_serif22x29_package,
#endif

#ifdef CONFIG_NXFONT_SERIF29X37
	&g_serif29x37_package,
#endif

#ifdef CONFIG_NXFONT_SERIF38X48
	&g_serif38x48_package,
#endif

/* SERIF-BOLD */

#ifdef CONFIG_NXFONT_SERIF22X28B
	&g_serif22x28b_package,
#endif

#ifdef CONFIG_NXFONT_SERIF27X38B
	&g_serif27x38b_package,
#endif

#ifdef CONFIG_NXFONT_SERIF38X49B
	&g_serif38x49b_package,
#endif

	NULL
};

static inline FAR const struct nx_fontset_s *
nxf_getglyphset(uint16_t ch, FAR const struct nx_fontpackage_s *package)
{
 FAR const struct nx_fontset_s *fontset;

  /* Select the 7- or 8-bit font set */

  if (ch < 128)
    {
      /* Select the 7-bit font set */

      fontset = &package->font7;
    }
  else if (ch < 256)
    {
#if CONFIG_NXFONTS_CHARBITS >= 8
      /* Select the 8-bit font set */

      fontset = &package->font8;
#else
      gdbg("8-bit font support disabled: %d\n", ch);
      return NULL;
#endif
    }
  else
    {
      /* Someday, perhaps 16-bit fonts will go here */

      gdbg("16-bit font not currently supported\n");
      return NULL;
    }

  /* Then verify that the character actually resides in the font */
  /*  ch >= fontset->first && ch < fontset->first +fontset->nchars*/
  if (ch < fontset->first + 95) //fontset->nchars
    {
      return fontset;
    }

  gdbg("No bitmap for code %02x\n", ch);
  return NULL;
}

static inline FAR const struct nx_fontset16_s  *
qxf_getglyphset(uint16_t ch, FAR const struct nx_fontpackage_s *package)
{
	FAR const struct nx_fontset16_s *fontset_qx;

#if  CONFIG_NXFONTS_CHARBITS <= 16
		fontset_qx = &package->font16;

#else
		gdbg("16-bit font support disabled: %d\n", ch);
		return NULL;
#endif
	
	if(ch < 40895){
		return fontset_qx;
 	}
	gdbg("No bitmap for code %d\n", ch);
	gdbg("No bitmap for code U+%x\n", ch);
	return NULL;
}

NXHANDLE qxf_getfonthandle(enum nx_fontid_e fontid)
{
	FAR const struct nx_fontpackage_s **pkglist;
	FAR const struct nx_fontpackage_s  *package;
	FAR const struct nx_fontpackage_s  *defpkg = NULL;

	if (fontid == FONTID_DEFAULT){
		fontid = NXFONT_DEFAULT;
	}
	for (pkglist = g_fontpackages; *pkglist; pkglist++){
	
		package = *pkglist;
		if (package->id == fontid){
			return (NXHANDLE)package;
		} 
	
		else if (package->id == NXFONT_DEFAULT){
			defpkg = package;
		}
	}
	return (NXHANDLE)defpkg;
}

FAR const struct nx_font_s *qxf_getfontset(NXHANDLE handle)
{
	FAR const struct nx_fontpackage_s *package =
		(FAR const struct nx_fontpackage_s *)handle;
	if (package){
		return &package->metrics;
	}
	return NULL;
}


FAR int qxf_getbitmap(NXHANDLE handle, const uint16_t ch,struct nx_fontbitmap_s *font)
{
	FAR const struct nx_fontpackage_s *package =
		(FAR const struct nx_fontpackage_s *)handle;
	FAR const struct nx_fontset16_s  *fontset16;
	FAR const struct nx_fontset_s *fontset;
	uint16_t left, right, now;
	const struct nx_fontbitmap16_s *fontbitmap;
	FAR const struct nx_fontbitmap_s *bm ;
	if(package){
		if(ch < 0x100){
			fontset = nxf_getglyphset(ch, package);
			if (fontset && ch >= fontset->first && ch < fontset->first +95)// fontset->nchars
			{
				bm = &fontset->bitmap[ch - fontset->first];
				font->metric = bm->metric;
				font->bitmap = bm->bitmap;
				return OK;
			}
			fontset16 = qxf_getglyphset(ch, package);
			font->metric = fontset16->metric;
			font->bitmap = fontset16->bitmap->bitmap;
			return ERROR;
		}
		
		fontset16 = qxf_getglyphset(ch, package);
		font->metric = fontset16->metric;
		left = now = 0;
		right = fontset16->size - 1;
		fontbitmap = fontset16->bitmap;
		while((fontbitmap->unicode != ch)){
			if(left ==right){
				font->bitmap = fontset16->bitmap->bitmap;
				return ERROR;
			}
			if(fontbitmap->unicode < ch){
				left = now;
			}else{
				right = now;
			}
			now  = (left + right)>>1;
			if(now == left) { 
				left = now = right;
			}
			fontbitmap = fontset16->bitmap + now;
		}
		font->bitmap = fontbitmap->bitmap;
		return OK;
	}
	return ERROR;
}











