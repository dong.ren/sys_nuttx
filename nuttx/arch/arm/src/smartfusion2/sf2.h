

#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_H


#include <nuttx/config.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>

#include "up_internal.h"

#include "chip.h"
#include "sf2_clock.h"
#include "sf2_uart.h"
#include "sf2_lowputc.h"
#include "sf2_eth.h"
#include "sf2_gpio.h"
#include "sf2_i2c.h"
#include "sf2_spi.h"
#include "sf2_timer.h"
#include "sf2_can.h"
#include "sf2_waste.h"
 
#endif /* __ARCH_ARM_SRC_SMARTFUSION2_SF2_H */
