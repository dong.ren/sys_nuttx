
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_TIMER_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_TIMER_H
 

#include <nuttx/config.h>

#include "chip.h"
#include "chip/sf2_timer.h" 
#include <nuttx/irq.h>

#if defined(CONFIG_SF2_TIM1) || defined(CONFIG_SF2_TIM2)
#  define HAVE_TIMER 1
#endif

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif 

#ifdef HAVE_TIMER 
extern void sf2_timebus_initialize(void);
#endif

#ifdef CONFIG_SF2_TIM1
void clear_tim1_irq(void);
void set_tim1_us(uint64_t uSecond);
void tim1_start(xcpt_t isr);
void tim1_stop(void);
uint32_t get_tim1_value(void);
#endif
#ifdef CONFIG_SF2_TIM2
void clear_tim2_irq(void);
void set_tim2_us(uint64_t uSecond);
void tim2_start(xcpt_t isr);
void tim2_stop(void);
uint32_t get_tim2_value(void);
#endif

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __ARCH_ARM_STC_SMARTFUSION2_SF2_TIMER_H */
