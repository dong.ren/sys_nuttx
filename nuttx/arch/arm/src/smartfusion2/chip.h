


#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_H

#include <nuttx/config.h>
#include <arch/smartfusion2/chip.h>

#include "chip/sf2_memorymap.h"
#include "chip/sf2_sysreg.h"



#endif /* __ARCH_ARM_SRC_SF2_CHIP_H */
