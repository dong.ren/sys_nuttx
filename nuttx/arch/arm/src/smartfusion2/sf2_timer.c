#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>
#include <sys/types.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/config.h>
#include <nuttx/semaphore.h>
#include <arch/board/board.h>
#include "up_arch.h"
#include "chip.h"
#include "sf2_timer.h"
#include "sf2.h"
#include "up_internal.h"
 
void sf2_timebus_initialize(void)
{
#ifdef HAVE_TIMER
    irqstate_t flags = enter_critical_section();
    modifyreg32(SF2_SYSREG_SOFT_RST_CR, SYSREG_SOFTRESET_TIMER, 0);
    modifyreg32(SF2_TIM64_MODE, TIMx_MODE_64, 0);
#ifdef CONFIG_SF2_TIM1
	{
		modifyreg32(SF2_TIM1_CTRL, TIMx_ENABLE, 0);
		modifyreg32(SF2_TIM1_CTRL, TIMx_INTEN, 0);
		modifyreg32(SF2_TIM1_CTRL, TIMx_MODE, 0);
#ifdef CONFIG_TIM1_MODE_ONE_SHOT
		modifyreg32(SF2_TIM1_CTRL, 0, TIMx_MODE);
#endif
		modifyreg32(SF2_TIM1_RIS, 0, 1);
	} 
#endif
#ifdef CONFIG_SF2_TIM2
	{
		modifyreg32(SF2_TIM2_CTRL, TIMx_ENABLE, 0);
		modifyreg32(SF2_TIM2_CTRL, TIMx_INTEN, 0);
		modifyreg32(SF2_TIM2_CTRL, TIMx_MODE, 0);
#ifdef CONFIG_TIM2_MODE_ONE_SHOT
		modifyreg32(SF2_TIM2_CTRL, 0, TIMx_MODE);
#endif
		modifyreg32(SF2_TIM2_RIS, 0, 1);
	}
#endif

#endif
	leave_critical_section(flags);
}
#ifdef CONFIG_SF2_TIM1

void set_tim1_us(uint64_t uSecond)
{
	  uint32_t count = (g_frequencypclk0 / 1000000 * uSecond) - 1;
#ifndef CONFIG_TIM2_MODE_ONE_SHOT
	  putreg32(count, SF2_TIM1_BGLOADVAL);
#else
	  putreg32(count, SF2_TIM1_LOADVAL);
#endif
}

void clear_tim1_irq(void)
{
		modifyreg32(SF2_TIM1_RIS, 0, 1);
}

void tim1_start(xcpt_t isr)
{
	irq_attach(SF2_IRQ_TIMER1, isr, NULL);
	modifyreg32(SF2_TIM1_CTRL, 0, TIMx_INTEN);
	up_enable_irq(SF2_IRQ_TIMER1);
	modifyreg32(SF2_TIM1_CTRL,0 ,TIMx_ENABLE);
}

void tim1_stop(void)
{
	modifyreg32(SF2_TIM1_CTRL, TIMx_ENABLE, 0);
	up_disable_irq(SF2_IRQ_TIMER1);
	
}

uint32_t get_tim1_value(void)
{
	return getreg32(SF2_TIM1_VAL);
}
#endif

#ifdef CONFIG_SF2_TIM2
void set_tim2_us(uint64_t uSecond)
{
	  uint32_t count = (g_frequencypclk0 / 1000000 * uSecond) - 1;
#ifndef CONFIG_TIM2_MODE_ONE_SHOT
	  putreg32(count, SF2_TIM2_BGLOADVAL);
#else
	  putreg32(count, SF2_TIM2_LOADVAL);
#endif
}

void clear_tim2_irq(void)
{
		modifyreg32(SF2_TIM2_RIS, 0, 1);
}

void tim2_start(xcpt_t isr)
{
	irq_attach(SF2_IRQ_TIMER2, isr, NULL);
	modifyreg32(SF2_TIM2_CTRL, 0, TIMx_INTEN);
	up_enable_irq(SF2_IRQ_TIMER2);
	modifyreg32(SF2_TIM2_CTRL,0 ,TIMx_ENABLE);
}

void tim2_stop(void)
{
	modifyreg32(SF2_TIM2_CTRL, TIMx_ENABLE, 0);
	up_disable_irq(SF2_IRQ_TIMER2);
}

uint32_t get_tim2_value(void)
{
	return getreg32(SF2_TIM2_VAL);
}
#endif
 
