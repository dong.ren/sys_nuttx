#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_CLOCK_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_CLOCK_H

#include <nuttx/config.h>
#include "chip.h"

//#if CONFIG_M3_CLK_FREQ > 330
//#error  "m3 clk freq error."
//#endif

#define g_systemcoreclock CONFIG_M3_CLK_FREQ
#define g_frequencypclk0  CONFIG_APB0_CLK_FREQ
#define g_frequencypclk1  CONFIG_APB1_CLK_FREQ
#define g_frequencypclk2  CONFIG_APB2_CLK_FREQ
#define g_frequencyfic0   CONFIG_FIC0_CLK_FREQ
#define g_frequencyfic1   CONFIG_FIC1_CLK_FREQ
#define g_frequencyfic64  CONFIG_FIC64_CLK_FREQ

#endif /* __ARCH_ARM_SRC_SF2_SF2_CLOCK_H */






