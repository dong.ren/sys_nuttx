
#include <nuttx/config.h>

#include <debug.h>
#include <errno.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

#include <arch/board/board.h>
#include <nuttx/arch.h>
#include <nuttx/drivers/can.h>

#include "chip.h"
#include "nvic.h"
#include "sf2.h"
#include "sf2_can.h"
#include "up_arch.h"
#include "up_internal.h"

#if defined(CONFIG_CAN) && defined(CONFIG_SF2_CAN)

#ifdef CONFIG_DEBUG_CAN
#define candbg dbg
#define canvdbg vdbg
#define canlldbg lldbg
#define canllvdbg llvdbg
#else
#define candbg(x...)
#define canvdbg(x...)
#define canlldbg(x...)
#define canllvdbg(x...)
#endif

#if !defined(CONFIG_DEBUG) || !defined(CONFIG_DEBUG_CAN)
#undef CONFIG_CAN_REGDEBUG
#endif

struct sf2_can_s {
	uint8_t irqno;
	uint8_t filter;
	uint32_t base;
	uint32_t baud;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* CAN Register access */

static uint32_t can_getreg(struct sf2_can_s *priv, int offset);
static uint32_t can_getfreg(struct sf2_can_s *priv, int offset);
static void can_putreg(struct sf2_can_s *priv, int offset, uint32_t value);
static void can_putfreg(struct sf2_can_s *priv, int offset, uint32_t value);

/* CAN driver methods */

static void sf2_can_reset(FAR struct can_dev_s *dev);
static int sf2_can_setup(FAR struct can_dev_s *dev);
static void sf2_can_shutdown(FAR struct can_dev_s *dev);
static void sf2_can_rxint(FAR struct can_dev_s *dev, bool enable);
static void sf2_can_txint(FAR struct can_dev_s *dev, bool enable);
static int sf2_can_ioctl(FAR struct can_dev_s *dev, int cmd, unsigned long arg);
static int sf2_can_remoterequest(FAR struct can_dev_s *dev, uint16_t id);
static int sf2_can_send(FAR struct can_dev_s *dev, FAR struct can_msg_s *msg);
static bool sf2_can_txready(FAR struct can_dev_s *dev);
static bool sf2_can_txempty(FAR struct can_dev_s *dev);

/* CAN interrupt handling */

static int can_rx0interrupt(int irq, void *context);
static int sf2_can_txinterrupt(int irq, void *context);

/* Initialization */

// static int can_bittiming(struct sf2_can_s *priv);
// static int can_cellinit(struct sf2_can_s *priv);
// static int can_filterinit(struct sf2_can_s *priv);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct can_ops_s g_canops = {
	.co_reset = sf2_can_reset,
	.co_setup = sf2_can_setup,
	.co_shutdown = sf2_can_shutdown,
	.co_rxint = sf2_can_rxint,
	.co_txint = sf2_can_txint,
	.co_ioctl = sf2_can_ioctl,
	.co_remoterequest = sf2_can_remoterequest,
	.co_send = sf2_can_send,
	.co_txready = sf2_can_txready,
	.co_txempty = sf2_can_txempty,
};

static struct sf2_can_s g_canpriv = {
	.irqno = SF2_IRQ_CAN,
	.filter = 0,
	.base = SF2_CAN_BASE,
	.baud = CONFIG_CAN_BAUD,
};

static struct can_dev_s g_candev = {
	.cd_ops = &g_canops, .cd_priv = &g_canpriv,
};

static uint32_t can_getreg(struct sf2_can_s *priv, int offset) {
	return getreg32(priv->base + offset);
}

static void can_putreg(struct sf2_can_s *priv, int offset, uint32_t value) {
	putreg32(value, priv->base + offset);
}

/****************************************************************************
 * Name: sf2_can_reset
 *
 * Description:
 *   Reset the CAN device.  Called early to initialize the hardware. This
 *   function is called, before sf2_can_setup() and on error conditions.
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *  None
 *
 ****************************************************************************/

static void sf2_can_reset(FAR struct can_dev_s *dev) {
	uint32_t regval;
	irqstate_t flags;

	flags = enter_critical_section();

	regval = getreg32(SF2_SYSREG_SOFT_RST_CR);
	regval |= SYSREG_SOFTRESET_CAN;
	putreg32(regval, SF2_SYSREG_SOFT_RST_CR);

	regval &= ~SYSREG_SOFTRESET_CAN;
	putreg32(regval, SF2_SYSREG_SOFT_RST_CR);

	leave_critical_section(flags);
}

/****************************************************************************
 * Name: sf2_can_setup
 *
 * Description:
 *   Configure the CAN. This method is called the first time that the CAN
 *   device is opened.  This will occur when the port is first opened.
 *   This setup includes configuring and attaching CAN interrupts.
 *   All CAN interrupts are disabled upon return.
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *   Zero on success; a negated errno on failure
 *
 ****************************************************************************/
static int can_interrupt(int irq, void *context);

static int sf2_can_setup(FAR struct can_dev_s *dev) {
	uint32_t regval;
	int32_t i;

	for (i = 0; i < 32; i++) {
		putreg32(0, SF2_CAN_RXMSG_ID(i));
		putreg32(0, SF2_CAN_RXMSG_DATA_LOW(i));
		putreg32(0, SF2_CAN_RXMSG_DATA_HIGH(i));
		putreg32(0, SF2_CAN_RXMSG_AMR(i));
		putreg32(0, SF2_CAN_RXMSG_ACR(i));
		putreg32(0, SF2_CAN_RXMSG_AMR_DATA(i));
		putreg32(0, SF2_CAN_RXMSG_ACR_DATA(i));
		putreg32(CAN_RXMSG_CTRL_CMD_WPNL | CAN_RXMSG_CTRL_CMD_WPNH,
				SF2_CAN_RXMSG_CTRL_CMD(i));
	}

	regval = getreg32(SF2_CAN_CONFIG);
	regval &= ~(CAN_CFG_BITRATE_MASK);
	regval |= (3 << CAN_CFG_BITRATE_SHIFT);
	regval &= ~(CAN_CFG_TSEG1_MASK);
	regval |= (CONFIG_CAN_TSEG1 << CAN_CFG_TSEG1_SHIFT);
	regval &= ~(CAN_CFG_TSEG2_MASK);
	regval |= (CONFIG_CAN_TSEG2 << CAN_CFG_TSEG2_SHIFT);
	regval |= CAN_CFG_AUTO_RESTART;
	regval |= CAN_CFG_SWAP_ENDIAN;
	putreg32(regval, SF2_CAN_CONFIG);

	/* interrupt disable */
	putreg32(0, SF2_CAN_INT_ENABLE);

	/* normal mode */
#ifdef CONFIG_CAN_LOOPBACK
	putreg32(0x06u, SF2_CAN_COMMAND);
#else
	putreg32(0x00u, SF2_CAN_COMMAND);
#endif

	for (i = 0; i < 32; i++) {
		putreg32(0xffffffff, SF2_CAN_RXMSG_AMR(i));
		putreg32(0xffffffff, SF2_CAN_RXMSG_ACR(i));
		putreg32(0xffff, SF2_CAN_RXMSG_AMR_DATA(i));
		putreg32(0xffff, SF2_CAN_RXMSG_ACR_DATA(i));
		putreg32(CAN_RXMSG_CTRL_CMD_WPNL | CAN_RXMSG_CTRL_CMD_WPNH |
				CAN_RXMSG_CTRL_CMD_TXBUFEBI | CAN_RXMSG_CTRL_CMD_RXINTEBI |
				CAN_RXMSG_CTRL_CMD_LF,
				SF2_CAN_RXMSG_CTRL_CMD(i));
	}

	putreg32(0, SF2_CAN_INT_STATUS);
	putreg32(CAN_INT_ENABLE_GLOBAL, SF2_CAN_INT_ENABLE);
	irq_attach(SF2_IRQ_CAN, can_interrupt, NULL);
	regval = getreg32(SF2_CAN_COMMAND);
	regval |= 0x1;
	putreg32(regval, SF2_CAN_COMMAND);
	//	irq_attach
	up_enable_irq(SF2_IRQ_CAN);

	return OK;
}

/****************************************************************************
 * Name: sf2_can_shutdown
 *
 * Description:
 *   Disable the CAN.  This method is called when the CAN device is closed.
 *   This method reverses the operation the setup method.
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

static void sf2_can_shutdown(FAR struct can_dev_s *dev) {
	up_disable_irq(SF2_IRQ_CAN);
	irq_detach(SF2_IRQ_CAN);
	sf2_can_reset(dev);
}

/****************************************************************************
 * Name: sf2_can_rxint
 *
 * Description:
 *   Call to enable or disable RX interrupts.
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

static void sf2_can_rxint(FAR struct can_dev_s *dev, bool enable) {
	uint32_t regval = getreg32(SF2_CAN_INT_ENABLE);

	if (enable) {
		regval |= CAN_INT_ENABLE_RXMSG;
	} else {
		regval &= ~CAN_INT_ENABLE_RXMSG;
	}

	putreg32(regval, SF2_CAN_INT_ENABLE);
}

static void sf2_can_txint(FAR struct can_dev_s *dev, bool enable) {
	if (!enable) {
		/* Support only disabling the transmit mailbox interrupt */

		uint32_t regval = getreg32(SF2_CAN_INT_ENABLE);
		regval &= ~CAN_INT_ENABLE_TXMSG;
		putreg32(regval, SF2_CAN_INT_ENABLE);
	}
}

static int sf2_can_ioctl(FAR struct can_dev_s *dev, int cmd,
		unsigned long arg) {
	/* No CAN ioctls are supported */

	return -ENOTTY;
}

static int sf2_can_remoterequest(FAR struct can_dev_s *dev, uint16_t id) {
#warning "Remote request not implemented"
	return -ENOSYS;
}

static int sf2_can_send(FAR struct can_dev_s *dev, FAR struct can_msg_s *msg) {
	uint32_t regval;
	int32_t i;
	int32_t tmp;
	int32_t dlc;
	uint8_t *ptr;

	for (i = 0; i < 32; i++) {
		if ((getreg32(SF2_CAN_TX_BUF_STATUS) & (1 << i)) == 0) {

			dlc = msg->cm_hdr.ch_dlc;
			ptr = msg->cm_data;

			/* data low */
			regval = *(uint32_t *)ptr;
			putreg32(regval, SF2_CAN_TXMSG_DATA_HIGH(i));

			/* data high */
			ptr += 4;
			regval = *(uint32_t *)ptr;
			putreg32(regval, SF2_CAN_TXMSG_DATA_LOW(i));

			/* send interrupt */
			regval = getreg32(SF2_CAN_INT_ENABLE);
			regval |= CAN_INT_ENABLE_TXMSG;
			putreg32(regval, SF2_CAN_INT_ENABLE);

			/* send req */
			regval = 0;
			regval |= CAN_TXMSG_CTRL_CMD_WPN16;
			regval |= CAN_TXMSG_CTRL_CMD_WPN2;
			regval |= CAN_TXMSG_CTRL_CMD_TXINTEBI;
			regval &= ~CAN_TXMSG_CTRL_CMD_DLC_MASK;
			regval |= dlc << CAN_TXMSG_CTRL_CMD_DLC_SHIFT;
			regval |= CAN_TXMSG_CTRL_CMD_TXREQ;
#ifdef CONFIG_CAN_EXTID
			if (msg->cm_hdr.ch_extid) {
				putreg32((uint32_t)msg->cm_hdr.ch_id << CAN_TXMSG_EXID_SHIFT,
						SF2_CAN_TXMSG_ID(i));
				regval |= CAN_TXMSG_CTRL_CMD_IDE;
			} else {
				putreg32((uint32_t)msg->cm_hdr.ch_id << CAN_TXMSG_STID_SHIFT,
						SF2_CAN_TXMSG_ID(i));
			}
#else
			putreg32((uint32_t)msg->cm_hdr.ch_id << CAN_TXMSG_STID_SHIFT,
					SF2_CAN_TXMSG_ID(i));
#endif
			putreg32(regval, SF2_CAN_TXMSG_CTRL_CMD(i));

			break;
		}
	}

	return OK;
}

/****************************************************************************
 * Name: sf2_can_txready
 *
 * Description:
 *   Return true if the CAN hardware can accept another TX message.
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *   True if the CAN hardware is ready to accept another TX message.
 *
 ****************************************************************************/

static bool sf2_can_txready(FAR struct can_dev_s *dev) {
	return (getreg32(SF2_CAN_TX_BUF_STATUS) != 0xffffffff);
}

/****************************************************************************
 * Name: sf2_can_txempty
 *
 * Description:
 *   Return true if all message have been sent.  If for example, the CAN
 *   hardware implements FIFOs, then this would mean the transmit FIFO is
 *   empty.  This method is called when the driver needs to make sure that
 *   all characters are "drained" from the TX hardware before calling
 *   co_shutdown().
 *
 * Input Parameters:
 *   dev - An instance of the "upper half" can driver state structure.
 *
 * Returned Value:
 *   True if there are no pending TX transfers in the CAN hardware.
 *
 ****************************************************************************/

static bool sf2_can_txempty(FAR struct can_dev_s *dev) {
	int regval = getreg32(SF2_CAN_TX_BUF_STATUS);
	return (regval == 0);
}

static int can_interrupt(int irq, void *context) {
	struct can_dev_s *dev = &g_candev;
	uint32_t regval;
	uint8_t data[CAN_MAXDATALEN];

	regval = getreg32(SF2_CAN_INT_STATUS);

	if (regval & CAN_INT_STATUS_TXMSG) {
		(void)can_txdone(dev);
		putreg32(CAN_INT_STATUS_TXMSG, SF2_CAN_INT_STATUS);
	}

	if (regval & CAN_INT_STATUS_RXMSG) {
		struct can_hdr_s hdr;
		int32_t i;

		for (i = 0; i < 32; i++) {
			regval = getreg32(SF2_CAN_RXMSG_CTRL_CMD(i));
			if (regval & CAN_RXMSG_CTRL_CMD_MSGAV) {
				uint32_t id_reg;
				regval = getreg32(SF2_CAN_RXMSG_CTRL_CMD(i));
				id_reg = getreg32(SF2_CAN_RXMSG_ID(i));
#ifdef CONFIG_CAN_EXTID
				if (regval & CAN_RXMSG_CTRL_CMD_IDE) {
					hdr.ch_id = id_reg >> CAN_RXMSG_STID_SHIFT;
					hdr.ch_extid = true;
				} else {
					hdr.ch_id = id_reg >> CAN_RXMSG_EXID_SHIFT;
					hdr.ch_extid = false;
				}
#else
				hdr.ch_id = (uint16_t)(id_reg >> CAN_RXMSG_STID_SHIFT);
#endif
				hdr.ch_rtr = (regval & CAN_RXMSG_CTRL_CMD_RTR) ? true : false;
				hdr.ch_dlc = (regval & CAN_RXMSG_CTRL_CMD_DLC_MASK) >>
					CAN_RXMSG_CTRL_CMD_DLC_SHIFT;

				regval = getreg32(SF2_CAN_RXMSG_DATA_HIGH(i));
				data[0] = (uint8_t)(regval & 0xff);
				data[1] = (uint8_t)((regval >> 8) & 0xff);
				data[2] = (uint8_t)((regval >> 16) & 0xff);
				data[3] = (uint8_t)((regval >> 24) & 0xff);

				regval = getreg32(SF2_CAN_RXMSG_DATA_LOW(i));
				data[4] = (uint8_t)(regval & 0xff);
				data[5] = (uint8_t)((regval >> 8) & 0xff);
				data[6] = (uint8_t)((regval >> 16) & 0xff);
				data[7] = (uint8_t)((regval >> 24) & 0xff);

				putreg32(CAN_RXMSG_CTRL_CMD_MSGAV, SF2_CAN_RXMSG_CTRL_CMD(i));

				(void)can_receive(dev, &hdr, data);
			}
		}
		putreg32(CAN_INT_STATUS_RXMSG, SF2_CAN_INT_STATUS);
	}
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_caninitialize
 *
 * Description:
 *   Initialize the selected CAN port
 *
 * Input Parameter:
 *   Port number (for hardware that has mutiple CAN interfaces)
 *
 * Returned Value:
 *   Valid CAN device structure reference on succcess; a NULL on failure
 *
 ****************************************************************************/

FAR struct can_dev_s *sf2_caninitialize(int port) {
	(void)port;
	return &g_candev;
}

#endif /* CONFIG_CAN && CONFIG_SF2_CAN */
