
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_LOWPUTC_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_LOWPUTC_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Name: sf2_lowsetup
 *
 * Description:
 *   Called at the very beginning of _start.  Performs low level initialization
 *   of serial console.
 *
 ************************************************************************************/

EXTERN void sf2_lowsetup(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __ARCH_ARM_SRC_SMARTFUSION2_SF2_LOWPUTC_H */

