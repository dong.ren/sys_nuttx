#include <assert.h>
#include <debug.h>
#include <nuttx/config.h>
#include <stdint.h>

#include <arch/board/board.h>
#include <nuttx/init.h>

#include "up_arch.h"
#include "up_internal.h"

#include "sf2.h"
#include "sf2_waste.h"

#ifdef CONFIG_DEBUG
#define showprogress(c) up_lowputc(c)
#else
#define showprogress(c)
#endif


#define HEAP_BASE  ((uintptr_t)&_ebss+CONFIG_IDLETHREAD_STACKSIZE)
const uintptr_t g_idle_topstack = HEAP_BASE;

void __start(void) {

  const uint32_t *src;
  uint32_t *dest;

  /* Configure the uart so that we can get debug output as soon as possible */
  sf2_lowsetup();
  showprogress('A');

  /* Clear .bss.  We'll do this inline (vs. calling memset) just to be
   * certain that there are no issues with the state of global variables.
   */
  for (dest = &_sbss; dest < &_ebss;) {
    *dest++ = 0;
  }

  showprogress('B');

  /* Move the initialized data section from his temporary holding spot in
   * FLASH into the correct place in SRAM.  The correct place in SRAM is
   * give by _sdata and _edata.  The temporary location is in FLASH at the
   * end of all of the other read-only data (.text, .rodata) at _eronly.
   */
  for (src = &_eronly, dest = &_sdata; dest < &_edata;) {
    *dest++ = *src++;
  }

  showprogress('C');

/* Perform early serial initialization */

#ifdef USE_EARLYSERIALINIT
  up_earlyserialinit();
#endif
  showprogress('D');

/* For the case of the separate user-/kernel-space build, perform whatever
 * platform specific initialization of the user memory is required.
 * Normally this just means initializing the user space .data and .bss
 * segments.
 */

#ifdef CONFIG_NUTTX_KERNEL
  //	stm32_userspace();
  showprogress('E');
#endif

  /* Initialize onboard resources */

  sf2_boardinitialize();
  showprogress('F');

  /* Then start NuttX */

  showprogress('\r');
  showprogress('\n');

#ifdef CONFIG_DEBUG_STACK
  /* Set the IDLE stack to the coloration value and jump into os_start() */

  go_os_start((FAR void *)&_ebss, CONFIG_IDLETHREAD_STACKSIZE);
#else
  /* Call os_start() */

  os_start();

  /* Shoulnd't get here */

  for (;;)
    ;
#endif
}
