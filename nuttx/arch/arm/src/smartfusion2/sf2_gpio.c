#include <nuttx/config.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>
#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "sf2.h"
#include "sf2_gpio.h"

uint32_t gpio_config_reg[SF2_NGPIO ] =
{
	GPIO_0,
	GPIO_1,
	GPIO_2,
	GPIO_3,
	GPIO_4,
	GPIO_5,
	GPIO_6,
	GPIO_7,
	GPIO_8,
	GPIO_9,
	GPIO_10,
	GPIO_11,
	GPIO_12,
	GPIO_13,
	GPIO_14,
	GPIO_15,
	GPIO_16,
	GPIO_17,
	GPIO_18,
	GPIO_19,
	GPIO_20,
	GPIO_21,
	GPIO_22,
	GPIO_23,
	GPIO_24,
	GPIO_25,
	GPIO_26,
	GPIO_27,
	GPIO_28,
	GPIO_29,
	GPIO_30,
	GPIO_31,
};

/*------------------------------------------------------------------------
 * sf2_gpio_Config
 *------------------------------------------------------------------------*/
void sf2_gpio_config(uint8_t port_id, uint32_t config)
{
    uint32_t gpio_idx = (uint32_t)port_id;
    
    if(gpio_idx < SF2_NGPIO)
    {
		putreg32(config, gpio_config_reg[gpio_idx]);
    }
}


/**************************************************************
 * sf2__gpio_write
 *
 * Description:
 *   Write 1 or 0 to the selected GPIO pin
 *
 *************************************************************/

void sf2_gpio_write(uint8_t port_id, uint8_t value)
{
    uint32_t reg;
    uint8_t gpio_idx = port_id;
    
    if(gpio_idx < SF2_NGPIO)
    {
		reg = getreg32(GPIO_OUT);
		reg &= ~(1 << gpio_idx);
		reg |= (value << gpio_idx);
		putreg32(reg, GPIO_OUT);
    }
}

/************************************************************
 * Name: sf2_gpio_read
 *
 * Description:
 *   Read one or zero from the selected GPIO pin
 *
 ************************************************************/

uint8_t sf2_gpio_read(uint32_t port_id)
{
    uint8_t flag_gpio_in;

    uint32_t gpio_idx = (uint32_t)port_id;
	flag_gpio_in = ((getreg32(GPIO_IN) >> gpio_idx)&0x01u);
    if(gpio_idx < SF2_NGPIO)
    {
		//return ((GPIO_IN>>gpio_idx)&0x01u);
		return flag_gpio_in;
    }
    
	return -1;
}
