/*******************************************************************************
 *arch/arm/src/smartfusion2/sf2_exti_gpio.c
 *
 *  Author: peichun.wang  
 *  function : the external interrupt of GPIO  , the gpio5 <--->externl rtc.MFP
 *******************************************************************************/
#include <nuttx/config.h>
#include <nuttx/irq.h>  // for xcpt_t  
#include <nuttx/arch.h>

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>

#include <arch/irq.h>

#include "up_arch.h"
#include "chip.h"
#include "sf2_gpio.h"
#include "sf2_exti_gpio.h"

void sf2_gpio_enable_irq(int irq)
{
	
}

void sf2_gpio_clear_irq(int irq)
{
	/* irq =  0~31  , the gpio number */
	modifyreg32(GPIO_IRQ,irq,0);	
}


