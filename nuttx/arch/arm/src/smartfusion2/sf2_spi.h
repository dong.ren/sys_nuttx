
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_SPI_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_SPI_H


#include <nuttx/config.h>

#include "chip.h"
#include "chip/sf2_spi.h"


#if defined(CONFIG_SF2_SPI0) || defined(CONFIG_SF2_SPI1)
#  define HAVE_SPI 1
#endif 

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

#ifdef HAVE_SPI 
extern struct spi_dev_s *sf2_spibus_initialize(int bus);
#endif

#ifdef CONFIG_SF2_SPI0
extern void sf2_spi0select(struct spi_dev_s *dev, uint32_t devid, bool selected);
extern uint8_t sf2_spi0status(struct spi_dev_s *dev, uint32_t devid);
extern int sf2_spi0cmddata(struct spi_dev_s *dev, uint32_t devid, bool cmd);
#endif

#ifdef CONFIG_SF2_SPI1
extern void sf2_spi1select(struct spi_dev_s *dev, uint32_t devid, bool selected);
extern uint8_t sf2_spi1status(struct spi_dev_s *dev, uint32_t devid);
extern int sf2_spi1cmddata(struct spi_dev_s *dev, uint32_t devid, bool cmd);
#endif

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __ARCH_ARM_STC_SMARTFUSION2_SF2_SPI_H */
