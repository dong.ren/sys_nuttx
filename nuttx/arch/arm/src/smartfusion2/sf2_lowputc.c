
/**************************************************************************
 * Included Files
 **************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <arch/board/board.h>

#include "up_arch.h"
#include "up_internal.h"

#include "chip.h"
#include "nvic.h"
#include "sf2.h"
#include "sf2_clock.h"
#include "sf2_uart.h"

/**************************************************************************
 * Private Definitions
 **************************************************************************/

#ifdef HAVE_CONSOLE
#if defined(CONFIG_UART0_SERIAL_CONSOLE)
#define SF2_CONSOLE_BASE SF2_UART0_BASE
#define SF2_APBCLOCK g_frequencypclk0
#define SF2_CONSOLE_BAUD CONFIG_UART0_BAUD
#define SF2_CONSOLE_BITS CONFIG_UART0_BITS
#define SF2_CONSOLE_PARITY CONFIG_UART0_PARITY
#define SF2_CONSOLE_2STOP CONFIG_UART0_2STOP
#elif defined(CONFIG_UART1_SERIAL_CONSOLE)
#define SF2_CONSOLE_BASE SF2_UART1_BASE
#define SF2_APBCLOCK g_frequencypclk1
#define SF2_CONSOLE_BAUD CONFIG_UART1_BAUD
#define SF2_CONSOLE_BITS CONFIG_UART1_BITS
#define SF2_CONSOLE_PARITY CONFIG_UART1_PARITY
#define SF2_CONSOLE_2STOP CONFIG_UART1_2STOP
#endif

#if SF2_CONSOLE_BITS == 5
#define UART_LCR_WLS_VALUE UART_LCR_WLS_5
#elif SF2_CONSOLE_BITS == 6
#define UART_LCR_WLS_VALUE UART_LCR_WLS_6
#elif SF2_CONSOLE_BITS == 7
#define UART_LCR_WLS_VALUE UART_LCR_WLS_7
#else
#define UART_LCR_WLS_VALUE UART_LCR_WLS_8
#endif

#if SF2_CONSOLE_PARITY == 1
#define UART_LCR_PARITY_VALUE (UART_LCR_PEN)
#elif SF2_CONSOLE_PARITY == 2
#define UART_LCR_PARITY_VALUE (UART_LCR_PEN | UART_LCR_EPS)
#else
#define UART_LCR_PARITY_VALUE 0
#endif

#if SF2_CONSOLE_2STOP != 0
#define UART_LCR_STOP2_VALUE 1
#else
#define UART_LCR_STOP2_VALUE 0
#endif

#define UART_LCR_SETBITS                                                       \
  (UART_LCR_WLS_VALUE | UART_LCR_PARITY_VALUE | UART_LCR_STOP2_VALUE)

#endif /* HAVE_CONSOLE */

#ifdef HAVE_CORE_CONSOLE
#if defined(CONFIG_CORE_UART0_SERIAL_CONSOLE)
#define SF2_CONSOLE_BASE SF2_CORE_UART0_BASE
#define SF2_CONSOLE_BAUD CONFIG_CORE_UART0_BAUD
#define SF2_CONSOLE_BITS CONFIG_CORE_UART0_BITS
#define SF2_CONSOLE_PARITY CONFIG_CORE_UART0_PARITY
#endif
#endif
/**************************************************************************
 * Name: up_lowputc
 *
 * Description:
 *   Output one byte on the serial console
 *
 **************************************************************************/

void up_lowputc(char ch) {

#ifdef HAVE_CONSOLE
  /* Wait until the TX data register is empty */
  while ((getreg32(SF2_CONSOLE_BASE + SF2_UART_LSR_OFFSET) & UART_LSR_THRE) ==
         0)
    ;

  /* Then send the character */
  putreg32((uint32_t)ch, SF2_CONSOLE_BASE + SF2_UART_THR_OFFSET);

#endif /* HAVE_CONSOLE */

#ifdef HAVE_CORE_CONSOLE
  /* Wait until the TX data register is empty */
  while ((getreg32(SF2_CONSOLE_BASE + SF2_CORE_UART_STATUS_OFFSET) & CORE_UART_STATUS_TXRDY) ==
         0)
    ;

  /* Then send the character */
  putreg32((uint32_t)ch, SF2_CONSOLE_BASE + SF2_CORE_UART_TXDATA_OFFSET);
#endif /* HAVE_CORE_CONSOLE */

}

/**************************************************************************
 * Name: sf2_lowsetup
 *
 * Description:
 *   This performs basic initialization of the UART used for the serial
 *   console.  Its purpose is to get the console output availabe as soon
 *   as possible.
 *
 **************************************************************************/

void sf2_lowsetup(void) {
  uint32_t pll_locked;
  do {
    pll_locked = getreg32(SF2_SYSREG_MSSDDR_PLL_STATUS);
    pll_locked &= 0x1;
  } while (!pll_locked);

  pll_locked = getreg32(SF2_SYSREG_MSSDDR_PLL_STATUS_HIGH_CR);
  pll_locked &= ~0x1;
  putreg32(pll_locked, SF2_SYSREG_MSSDDR_PLL_STATUS_HIGH_CR);

  do {
    pll_locked = getreg32(SF2_SYSREG_MSSDDR_PLL_STATUS);
    pll_locked &= 0x2;
  } while (!pll_locked);

  pll_locked = getreg32(SF2_SYSREG_MSSDDR_FACC1_CR);
  pll_locked &= ~0x1000;
  putreg32(pll_locked, SF2_SYSREG_MSSDDR_FACC1_CR);

  pll_locked = getreg32(SF2_SYSREG_SOFT_RST_CR);
  pll_locked &= ~SYSREG_SOFTRESET_FPGA;
  putreg32(pll_locked, SF2_SYSREG_SOFT_RST_CR);

  pll_locked = getreg32(NVIC_CFGCON);
  pll_locked |= 0x20;
  putreg32(pll_locked, NVIC_CFGCON);

  putreg32(0, SF2_SYSREG_WDOG_CR);

#if defined(HAVE_UART)
#if defined(HAVE_CONSOLE) && !defined(CONFIG_SUPPRESS_UART_CONFIG)

  uint8_t regval;
  uint32_t baud_value;
  uint32_t baud_value_by_64;
  uint32_t baud_value_by_128;
  uint32_t fractional_baud_value;

  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_MM0_OFFSET);
  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_MM1_OFFSET);
  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_MM2_OFFSET);
  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_IIR_OFFSET);

  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);
  putreg8(UART_LCR_SETBITS, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);

  baud_value_by_128 = (8u * SF2_APBCLOCK) / (SF2_CONSOLE_BAUD);
  baud_value_by_64 = baud_value_by_128 / 2;
  baud_value = baud_value_by_64 / 64;
  fractional_baud_value = baud_value_by_64 - (baud_value * 64);
  fractional_baud_value +=
      (baud_value_by_128 - (baud_value * 128)) - (fractional_baud_value * 2);

  if (baud_value > 1) {
    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);
    regval |= UART_LCR_DLAB;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);

    putreg8((uint8_t)(baud_value >> 8), SF2_CONSOLE_BASE + SF2_UART_DMR_OFFSET);
    putreg8((uint8_t)baud_value, SF2_CONSOLE_BASE + SF2_UART_DLR_OFFSET);

    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);
    regval &= ~UART_LCR_DLAB;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);

    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_MM0_OFFSET);
    regval |= UART_MM0_EFBR;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_MM0_OFFSET);

    putreg8((uint8_t)fractional_baud_value,
            SF2_CONSOLE_BASE + SF2_UART_DFR_OFFSET);
  } else {
    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);
    regval |= UART_LCR_DLAB;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);

    putreg8((uint8_t)(baud_value >> 8), SF2_CONSOLE_BASE + SF2_UART_DMR_OFFSET);
    putreg8((uint8_t)baud_value, SF2_CONSOLE_BASE + SF2_UART_DLR_OFFSET);

    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);
    regval &= ~UART_LCR_DLAB;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_LCR_OFFSET);

    regval = getreg8(SF2_CONSOLE_BASE + SF2_UART_MM0_OFFSET);
    regval &= ~UART_MM0_EFBR;
    putreg8(regval, SF2_CONSOLE_BASE + SF2_UART_MM0_OFFSET);
  }

  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_IER_OFFSET);

  putreg8(0, SF2_CONSOLE_BASE + SF2_UART_FCR_OFFSET);
  putreg8(UART_FCR_ENABLE_TX_RX_FIFO | UART_FCR_CLEAR_TX_FIFO |
              UART_FCR_CLEAR_RX_FIFO,
          SF2_CONSOLE_BASE + SF2_UART_FCR_OFFSET);
#endif /* HAVE_CONSOLE && !CONFIG_SUPPRESS_UART_CONFIG */

#if defined(HAVE_CORE_CONSOLE) && !defined(CONFIG_SUPPRESS_UART_CONFIG)
  uint8_t regval;
  
  uint8_t buad_value0_7 = CORE_UART_CTRL1_B9600;
	uint8_t buad_value8_13 = CORE_UART_CTRL2_B9600;

	if(SF2_CONSOLE_BAUD == 9600)
	{
		buad_value0_7 = CORE_UART_CTRL1_B9600;
		buad_value8_13 = CORE_UART_CTRL2_B9600;
	}else if(SF2_CONSOLE_BAUD == 19200)
	{
		buad_value0_7 = CORE_UART_CTRL1_B19200;
		buad_value8_13 = CORE_UART_CTRL2_B19200;
	}else if(SF2_CONSOLE_BAUD == 38400)
	{
		buad_value0_7 = CORE_UART_CTRL1_B38400;
		buad_value8_13 = CORE_UART_CTRL2_B38400;
	}else if(SF2_CONSOLE_BAUD == 57600)
	{
		buad_value0_7 = CORE_UART_CTRL1_B57600;
		buad_value8_13 = CORE_UART_CTRL2_B57600;
	}else if(SF2_CONSOLE_BAUD == 115200)
	{
		buad_value0_7 = CORE_UART_CTRL1_B19200;
		buad_value8_13 = CORE_UART_CTRL2_B19200;
	}
	putreg8(SF2_CONSOLE_BASE + SF2_CORE_UART_CTRL1_OFFSET, buad_value0_7);

	regval = 0;
	switch(SF2_CONSOLE_BITS){
	case 7: regval &= ~CORE_UART_CTRL2_BIT8; break;	
	case 8: regval |= CORE_UART_CTRL2_BIT8; break;	
	default: regval |= CORE_UART_CTRL2_BIT8; break;
	}
	regval |= SF2_CONSOLE_PARITY << 1;
	regval |= buad_value8_13;
	putreg8(SF2_CONSOLE_BASE + SF2_CORE_UART_CTRL2_OFFSET, regval);

#endif /* HAVE_CORE_CONSOLE && !CONFIG_SUPPRESS_UART_CONFIG */

#endif /* HAVE_UART */


}
