/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <nuttx/serial/serial.h>

#ifdef CONFIG_SERIAL_TERMIOS
#  include <termios.h>
#endif

#include <arch/serial.h>
#include <arch/board/board.h>

#include "sf2.h"
#include "chip.h"
#include "sf2_uart.h"
#include "up_arch.h"
#include "up_internal.h"

/****************************************************************************
 * Definitions
 ****************************************************************************/

#define UART_IER_USED_INTS (UART_IER_ELSI | UART_IER_ETBEI | UART_IER_ERBFI)


#ifdef USE_SERIALDRIVER
#ifdef HAVE_UART

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct up_dev_s
{
	struct uart_dev_s dev;       /* Generic UART device */
	uint16_t          ie;        /* Saved interrupt mask bits value */
	uint16_t          sr;        /* Saved status bits */

	/* If termios are supported, then the following fields may vary at
	 * runtime.
	 */

#ifdef CONFIG_SERIAL_TERMIOS
	uint8_t           parity;    /* 0=none, 1=odd, 2=even */
	uint8_t           bits;      /* Number of bits (7 or 8) */
	bool              stopbits2; /* True: Configure with 2 stop bits instead of 1 */
	uint32_t          baud;      /* Configured baud */
#else
	const uint8_t     parity;    /* 0=none, 1=odd, 2=even */
	const uint8_t     bits;      /* Number of bits (7 or 8) */
	const bool        stopbits2; /* True: Configure with 2 stop bits instead of 1 */
	const uint32_t    baud;      /* Configured baud */
#endif

	const uint8_t     irq;       /* IRQ associated with this UART */
	const uint32_t    apbclock;  /* PCLK 1 or 2 frequency */
	const uint32_t    uartbase;  /* Base address of UART registers */

	int (* const vector)(int irq, void *context); /* Interrupt handler */

	uint32_t          rst_mask;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static void up_set_format(struct uart_dev_s *dev);
static int  up_setup(struct uart_dev_s *dev);
static void up_shutdown(struct uart_dev_s *dev);
static int  up_attach(struct uart_dev_s *dev);
static void up_detach(struct uart_dev_s *dev);
static int  up_interrupt_common(struct up_dev_s *dev);
static int  up_ioctl(struct file *filep, int cmd, unsigned long arg);

static int  up_receive(struct uart_dev_s *dev, uint32_t *status);
static void up_rxint(struct uart_dev_s *dev, bool enable);
static bool up_rxavailable(struct uart_dev_s *dev);

static void up_send(struct uart_dev_s *dev, int ch);
static void up_txint(struct uart_dev_s *dev, bool enable);
static bool up_txready(struct uart_dev_s *dev);

#ifdef CONFIG_SF2_UART0
static int up_interrupt_uart0(int irq, void *context);
#endif
#ifdef CONFIG_SF2_UART1
static int up_interrupt_uart1(int irq, void *context);
#endif
#ifdef CONFIG_SF2_CORE_UART0
static int  core_up_setup(struct uart_dev_s *dev);
static int  core_up_interrupt_common(struct up_dev_s *dev);
static int  core_up_ioctl(struct file *filep, int cmd, unsigned long arg);

static int  core_up_receive(struct uart_dev_s *dev, uint32_t *status);
static void core_up_rxint(struct uart_dev_s *dev, bool enable);
static bool core_up_rxavailable(struct uart_dev_s *dev);

static void core_up_send(struct uart_dev_s *dev, int ch);
static void core_up_txint(struct uart_dev_s *dev, bool enable);
static bool core_up_txready(struct uart_dev_s *dev);
static int up_interrupt_core_uart0(int irq, void *context);
#endif
/****************************************************************************
 * Private Variables
 ****************************************************************************/


static const struct uart_ops_s g_uart_ops =
{
	.setup          = up_setup,
	.shutdown       = up_shutdown,
	.attach         = up_attach,
	.detach         = up_detach,
	.ioctl          = up_ioctl,
	.receive        = up_receive,
	.rxint          = up_rxint,
	.rxavailable    = up_rxavailable,
	.send           = up_send,
	.txint          = up_txint,
	.txready        = up_txready,
	.txempty        = up_txready,
};

static const struct uart_ops_s g_core_uart_ops =
{
	.setup          = core_up_setup,
	.shutdown       = up_shutdown,
	.attach         = up_attach,
	.detach         = up_detach,
	.ioctl          = core_up_ioctl,
	.receive        = core_up_receive,
	.rxint          = core_up_rxint,
	.rxavailable    = core_up_rxavailable,
	.send           = core_up_send,
	.txint          = core_up_txint,
	.txready        = core_up_txready,
	.txempty        = core_up_txready,
};

/* I/O buffers */

#ifdef CONFIG_SF2_UART0
static char g_uart0rxbuffer[CONFIG_UART0_RXBUFSIZE];
static char g_uart0txbuffer[CONFIG_UART0_TXBUFSIZE];
#endif

#ifdef CONFIG_SF2_UART1
static char g_uart1rxbuffer[CONFIG_UART1_RXBUFSIZE];
static char g_uart1txbuffer[CONFIG_UART1_TXBUFSIZE];
#endif

#ifdef CONFIG_SF2_CORE_UART0
static char g_core_uart0rxbuffer[CONFIG_CORE_UART0_RXBUFSIZE];
static char g_core_uart0txbuffer[CONFIG_CORE_UART0_TXBUFSIZE];
#endif

/* This describes the state of the SF2 UART0 ports. */

#ifdef CONFIG_SF2_UART0
static struct up_dev_s g_uart0priv =
{
	.dev =
	{
#if CONSOLE_UART == 0
		.isconsole = true,
#endif
		.recv      =
		{
			.size    = CONFIG_UART0_RXBUFSIZE,
			.buffer  = g_uart0rxbuffer,
		},
		.xmit      =
		{
			.size    = CONFIG_UART0_TXBUFSIZE,
			.buffer  = g_uart0txbuffer,
		},

		.ops       = &g_uart_ops,

		.priv      = &g_uart0priv,
	},

	.irq           = SF2_IRQ_UART0,
	.parity        = CONFIG_UART0_PARITY,
	.bits          = CONFIG_UART0_BITS,
	.stopbits2     = CONFIG_UART0_2STOP,
	.baud          = CONFIG_UART0_BAUD,
	.apbclock      = g_frequencypclk0,
	.uartbase      = SF2_UART0_BASE,

	.vector        = up_interrupt_uart0,

	.rst_mask      = SYSREG_SOFTRESET_MMUART0,

};
#endif

/* This describes the state of the SF2 UART1 port. */

#ifdef CONFIG_SF2_UART1
static struct up_dev_s g_uart1priv =
{
	.dev =
	{
#if CONSOLE_UART == 1
		.isconsole = true,
#endif
		.recv      =
		{
			.size    = CONFIG_UART1_RXBUFSIZE,
			.buffer  = g_uart1rxbuffer,
		},
		.xmit      =
		{
			.size    = CONFIG_UART1_TXBUFSIZE,
			.buffer  = g_uart1txbuffer,
		},

		.ops       = &g_uart_ops,

		.priv      = &g_uart1priv,
	},

	.irq           = SF2_IRQ_UART1,
	.parity        = CONFIG_UART1_PARITY,
	.bits          = CONFIG_UART1_BITS,
	.stopbits2     = CONFIG_UART1_2STOP,
	.baud          = CONFIG_UART1_BAUD,
	.apbclock      = g_frequencypclk1,
	.uartbase      = SF2_UART1_BASE,

	.vector        = up_interrupt_uart1,

	.rst_mask      = SYSREG_SOFTRESET_MMUART1,
};

#endif

/* This describes the state of the SF2 CORE UART port. */
static struct up_dev_s g_core_uart0priv =
{
	.dev =
	{
#if CONSOLE_UART == 2
		.isconsole = true,
#endif
		.recv      =
		{
			.size    = CONFIG_CORE_UART0_RXBUFSIZE,
			.buffer  = g_core_uart0rxbuffer,
		},
		.xmit      =
		{
			.size    = CONFIG_CORE_UART0_TXBUFSIZE,
			.buffer  = g_core_uart0txbuffer,
		},

		.ops       = &g_core_uart_ops,

		.priv      = &g_core_uart0priv,
	},

	.irq           = CONFIG_CORE_UART0_IRQ,
	.parity        = CONFIG_CORE_UART0_PARITY,
	.bits          = CONFIG_CORE_UART0_BITS,
	.stopbits2     = CONFIG_CORE_UART0_2STOP,
	.baud          = CONFIG_CORE_UART0_BAUD,
	.apbclock      = g_frequencypclk2,
	.uartbase      = SF2_CORE_UART0_BASE,

	.vector        = up_interrupt_core_uart0,

	.rst_mask      = CONFIG_CORE_UART0_IRQ_RESET_MASK,

};
/* This table lets us iterate over the configured UARTs */

static struct up_dev_s *uart_devs[SF2_NUART] =
{
#ifdef CONFIG_SF2_UART0
	[0] = &g_uart0priv,
#endif
#ifdef CONFIG_SF2_UART1
	[1] = &g_uart1priv,
#endif
#ifdef CONFIG_SF2_CORE_UART0
	[2] = &g_core_uart0priv,
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_serialin
 ****************************************************************************/

static inline uint8_t up_serialin(struct up_dev_s *priv, int offset){

	return getreg8(priv->uartbase + offset);
}

/****************************************************************************
 * Name: up_serialout
 ****************************************************************************/

static inline void up_serialout(struct up_dev_s *priv, int offset, uint8_t value){

	putreg8(value, priv->uartbase + offset);
}

/****************************************************************************
 * Name: up_restoreuartint
 ****************************************************************************/

static void up_restoreuartint(struct up_dev_s *priv, uint16_t ie){

	/* Save the interrupt mask */	
	priv->ie = ie;

	/* And restore the interrupt state */
	up_serialout(priv, SF2_UART_IER_OFFSET, 0);
	up_serialout(priv, SF2_UART_IER_OFFSET, ie & UART_IER_USED_INTS);
}

/****************************************************************************
 * Name: up_disableuartint
 ****************************************************************************/

static inline void up_disableuartint(struct up_dev_s *priv, uint16_t *ie)
{

	if(ie){
		uint8_t ier;

		ier = up_serialin(priv, SF2_UART_IER_OFFSET);

		*ie = ier & UART_IER_USED_INTS;
	}

	up_restoreuartint(priv, 0);
}

/****************************************************************************
 * Name: up_set_format
 *
 * Description:
 *   Set the serial line format and speed.
 *
 ****************************************************************************/

#ifndef CONFIG_SUPPRESS_UART_CONFIG
static void up_set_format(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	uint8_t regval;

	uint32_t baud_value;
	uint32_t baud_value_by_64;
	uint32_t baud_value_by_128;
	uint32_t fractional_baud_value;
	uint32_t pclk_freq;

	pclk_freq = priv->apbclock;

	baud_value_by_128 = (8 * pclk_freq) / (priv->baud);
	baud_value_by_64 = baud_value_by_128 / 2;
	baud_value = baud_value_by_64 / 64;
	fractional_baud_value = baud_value_by_64 - (baud_value * 64);
	fractional_baud_value += (baud_value_by_128 - (baud_value * 128)) - (fractional_baud_value * 2);

	if(baud_value > 1u){
		regval = up_serialin(priv, SF2_UART_LCR_OFFSET);
		regval |= UART_LCR_DLAB;
		up_serialout(priv, SF2_UART_LCR_OFFSET, regval);

		up_serialout(priv, SF2_UART_DMR_OFFSET, (uint8_t)(baud_value >> 8));
		up_serialout(priv, SF2_UART_DLR_OFFSET, (uint8_t)baud_value);

		regval = up_serialin(priv, SF2_UART_LCR_OFFSET);
		regval &= ~UART_LCR_DLAB;
		up_serialout(priv, SF2_UART_LCR_OFFSET, regval);

		regval = up_serialin(priv, SF2_UART_MM0_OFFSET);
		regval |= UART_MM0_EFBR;
		up_serialout(priv, SF2_UART_MM0_OFFSET, regval);

		up_serialout(priv, SF2_UART_DFR_OFFSET, (uint8_t)fractional_baud_value);
	}else{
		regval = up_serialin(priv, SF2_UART_LCR_OFFSET);
		regval |= UART_LCR_DLAB;
		up_serialout(priv, SF2_UART_LCR_OFFSET, regval);

		up_serialout(priv, SF2_UART_DMR_OFFSET, (uint8_t)(baud_value >> 8));
		up_serialout(priv, SF2_UART_DLR_OFFSET, (uint8_t)baud_value);

		regval = up_serialin(priv, SF2_UART_LCR_OFFSET);
		regval &= ~UART_LCR_DLAB;
		up_serialout(priv, SF2_UART_LCR_OFFSET, regval);

		regval = up_serialin(priv, SF2_UART_MM0_OFFSET);
		regval &= ~UART_MM0_EFBR;
		up_serialout(priv, SF2_UART_MM0_OFFSET, regval);
	}

	/* Configure parity mode and STOP bits*/

	regval  = up_serialin(priv, SF2_UART_LCR_OFFSET);
	regval &= ~(UART_LCR_PEN | UART_LCR_EPS);

	if (priv->parity == 1){      /* Odd parity */
		regval |= UART_LCR_PEN;
	}else if (priv->parity == 2){  /* Even parity */
		regval |= (UART_LCR_PEN | UART_LCR_EPS_EVEN);
	}

	regval &= ~(UART_LCR_STB);

	if(priv->stopbits2){
		regval |= UART_LCR_STB;
	}

	up_serialout(priv, SF2_UART_LCR_OFFSET, regval);
}
#endif /* CONFIG_SUPPRESS_UART_CONFIG */

/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Configure the UART baud, bits, parity, etc. This method is called the
 *   first time that the serial port is opened.
 *
 ****************************************************************************/

static int up_setup(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;

#ifndef CONFIG_SUPPRESS_UART_CONFIG
	uint32_t regval = 0;

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval |= priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	up_disable_irq(priv->irq);
	regval = 30;
	while(regval--);

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval &= ~priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	up_serialout(priv, SF2_UART_IER_OFFSET, 0);

	up_serialout(priv, SF2_UART_FCR_OFFSET, 0);
	regval = UART_FCR_CLEAR_RX_FIFO | UART_FCR_CLEAR_TX_FIFO | UART_FCR_ENABLE_TXRDY_RXRDY | 1;
	up_serialout(priv, SF2_UART_FCR_OFFSET, regval);

	up_serialout(priv, SF2_UART_MCR_OFFSET, 0);
	up_serialout(priv, SF2_UART_MM1_OFFSET, 0);
	up_serialout(priv, SF2_UART_MM2_OFFSET, 0);
	up_serialout(priv, SF2_UART_MM0_OFFSET, 0);
	up_serialout(priv, SF2_UART_IIR_OFFSET, 0);
	up_serialout(priv, SF2_UART_GFR_OFFSET, 0);
	up_serialout(priv, SF2_UART_TTG_OFFSET, 0);
	up_serialout(priv, SF2_UART_RTO_OFFSET, 0);

	regval = up_serialin(priv, SF2_UART_LCR_OFFSET);
	regval &= ~UART_LCR_WLS_MASK;
	switch(priv->bits){
	case 5: regval |= UART_LCR_WLS_5; break;
	case 6: regval |= UART_LCR_WLS_6; break;
	case 7: regval |= UART_LCR_WLS_7; break;	
	case 8: regval |= UART_LCR_WLS_8; break;	
	default: regval |= UART_LCR_WLS_8; break;
	}
	up_serialout(priv, SF2_UART_LCR_OFFSET, regval);

	up_set_format(dev);

#endif  /* CONFIG_SUPPRESS_UART_CONFIG */
	priv->ie    = 0;
	return OK;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Disable the UART.  This method is called when the serial
 *   port is closed
 *
 ****************************************************************************/

static void up_shutdown(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	irqstate_t flags;
	uint32_t regval = 0;

	flags = enter_critical_section();

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval |= priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	up_disable_irq(priv->irq);

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval &= ~priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	leave_critical_section(flags);
}

/****************************************************************************
 * Name: up_attach
 *
 * Description:
 *   Configure the UART to operation in interrupt driven mode.  This method is
 *   called when the serial port is opened.  Normally, this is just after the
 *   the setup() method is called, however, the serial console may operate in
 *   a non-interrupt driven mode during the boot phase.
 *
 *   RX and TX interrupts are not enabled when by the attach method (unless the
 *   hardware supports multiple levels of interrupt enabling).  The RX and TX
 *   interrupts are not enabled until the txint() and rxint() methods are called.
 *
 ****************************************************************************/

static int up_attach(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	int ret;

	/* Attach and enable the IRQ */
	ret = irq_attach(priv->irq, (xcpt_t)priv->vector, NULL);
	if (ret == OK){

		/* Enable the interrupt (RX and TX interrupts are still disabled
		 * in the UART
		 */
		up_enable_irq(priv->irq);
	}
	return ret;
}

/****************************************************************************
 * Name: up_detach
 *
 * Description:
 *   Detach UART interrupts.  This method is called when the serial port is
 *   closed normally just before the shutdown method is called.  The exception
 *   is the serial console which is never shutdown.
 *
 ****************************************************************************/

static void up_detach(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	up_disable_irq(priv->irq);
	irq_detach(priv->irq);
}


static int up_interrupt_common(struct up_dev_s *priv){

	int  passes;
	bool handled;

	/* Loop until there are no characters to be transferred or,
	 * until we have been looping for a long time.
	 */


	/* interrupt Enable Register(IER):
	 *
	 * Enable             Status          Meaning                         Usage
	 * ------------------ --------------- ------------------------------- ----------
	 *ELSI                 R/W    Receiver line status interrupt enable
	 *ETBEI                R/W    Transmitter holding register empty interrupt enable 
	 *ERBFI                R/W    Enable receiver data available interrupt
	 *
	 *interrupts identification Register(IIR):
	 *
	 * Enable             Status          Meaning                         Usage
	 * ------------------ --------------- ------------------------------- ----------
	 *[3:0] IIR            R              interrupt identification bits.
	 * 0b0010 : Transmit holding register empty interrupt
	 *
	 *Line Status Register(LSR):
	 *
	 * Enable             Status      reset value     Meaning 
	 * ------------------ --------------- ------------------------------- ----------
	 * TEMT                R             1   Transmit empty(TEMT).This bit is set to 1 
	 *                    when both the transmitter FIFO and shift registers are empty.
	 *
	 *THRE                 R             1   Transmitter holding register empty(THRE).
	 *                    indicates that   UARTx is ready to transmit a new data byte.
	 *                    or transmiter complete.
	 *
	 *DR                   R            0   Data ready(DR). indicates when a data byte is 
	 *                     received and stored in the receive buffer or the FIFO.
	 */

	handled = true;
	for (passes = 0; passes < 256 && handled; passes++){

		handled = false;

		priv->sr = up_serialin(priv, SF2_UART_LSR_OFFSET);

		if((priv->sr & UART_LSR_DR) != 0 && (priv->ie & UART_IER_ERBFI) != 0){
			uart_recvchars(&priv->dev);
			handled = true;
		}else if((priv->sr & (UART_LSR_OE | UART_LSR_PE | UART_LSR_BI | UART_LSR_FE)) != 0){
			(void)up_serialin(priv, SF2_UART_LSR_OFFSET);
		}

		if((priv->sr & UART_LSR_THRE) != 0 && (priv->ie & UART_IER_ETBEI) != 0){

			(void)up_serialin(priv, SF2_UART_IIR_OFFSET);
			uart_xmitchars(&priv->dev);
			handled = true;
		}


	}
	return OK;
}



/****************************************************************************
 * Name: up_ioctl
 *
 * Description:
 *   All ioctl calls will be routed through this method
 *
 ****************************************************************************/

static int up_ioctl(struct file *filep, int cmd, unsigned long arg){

#if defined(CONFIG_SERIAL_TERMIOS) || defined(CONFIG_SERIAL_TIOCSERGSTRUCT)
	struct inode      *inode = filep->f_inode;
	struct uart_dev_s *dev   = inode->i_private;
#endif
#ifdef CONFIG_SERIAL_TERMIOS
	struct up_dev_s   *priv  = (struct up_dev_s*)dev->priv;
#endif
	int                ret    = OK;

	switch (cmd){
#ifdef CONFIG_SERIAL_TIOCSERGSTRUCT
	case TIOCSERGSTRUCT:
		{
			struct up_dev_s *user = (struct up_dev_s*)arg;
			if (!user){
				ret = -EINVAL;
			}else{
				memcpy(user, dev, sizeof(struct up_dev_s));
			}
		}
		break;
#endif

#ifdef CONFIG_SERIAL_TERMIOS
	case TCGETS:
		{
			struct termios *termiosp = (struct termios*)arg;

			if (!termiosp){
				ret = -EINVAL;
				break;
			}

			cfsetispeed(termiosp, priv->baud);

			/* Note that since we only support 8/9 bit modes and
			 * there is no way to report 9-bit mode, we always claim 8.
			 */

			termiosp->c_cflag =
				((priv->parity != 0) ? PARENB : 0) |
				((priv->parity == 1) ? PARODD : 0) |
				((priv->stopbits2) ? CSTOPB : 0) |
				CS8;
		}
		break;

	case TCSETS:
		{
			struct termios *termiosp = (struct termios*)arg;

			if (!termiosp){
				ret = -EINVAL;
				break;
			}

			/* Perform some sanity checks before accepting any changes */

			if (((termiosp->c_cflag & CSIZE) != CS8)){
				ret = -EINVAL;
				break;
			}

			if (termiosp->c_cflag & PARENB){
				priv->parity = (termiosp->c_cflag & PARODD) ? 1 : 2;
			}else{
				priv->parity = 0;
			}

			priv->stopbits2 = (termiosp->c_cflag & CSTOPB) != 0;

			/* Note that since there is no way to request 9-bit mode
			 * and no way to support 5/6/7-bit modes, we ignore them
			 * all here.
			 */

			/* Note that only cfgetispeed is used because we have knowledge
			 * that only one speed is supported.
			 */

			priv->baud = cfgetispeed(termiosp);

			/* effect the changes immediately - note that we do not implement
			 * TCSADRAIN / TCSAFLUSH
			 */

			up_set_format(dev);
		}
		break;
#endif /* CONFIG_SERIAL_TERMIOS */

	default:
		ret = -ENOTTY;
		break;
	}

	return ret;
}

/****************************************************************************
 * Name: up_receive
 *
 * Description:
 *   Called (usually) from the interrupt level to receive one
 *   character from the UART.  Error bits associated with the
 *   receipt are provided in the return 'status'.
 *
 ****************************************************************************/

static int up_receive(struct uart_dev_s *dev, uint32_t *status){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	uint32_t rbr;

	/* Get the Rx byte */
	rbr      = (uint32_t)up_serialin(priv, SF2_UART_RBR_OFFSET);

	/* Get the Rx byte plux error information.  Return those in status */
	*status  = priv->sr << 16 | rbr;
	priv->sr = 0;

	/* Then return the actual received byte */
	return rbr & 0xff;
}

/****************************************************************************
 * Name: up_rxint
 *
 * Description:
 *   Call to enable or disable RX interrupts
 *
 ****************************************************************************/

static void up_rxint(struct uart_dev_s *dev, bool enable){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	irqstate_t flags;
	uint16_t ie;

	flags = enter_critical_section();
	ie = priv->ie;

	if(enable){

#ifndef CONFIG_SUPPRESS_SERIAL_INTS
#ifdef CONFIG_UART_ERRINTS
		ie |= (UART_IER_ERBFI | UART_IER_ELSI);
#else
		ie |= UART_IER_ERBFI;
#endif
#endif

	}else{
		ie &= ~(UART_IER_ERBFI | UART_IER_ELSI);
	}

	up_restoreuartint(priv, ie);
	leave_critical_section(flags);
}

/****************************************************************************
 * Name: up_rxavailable
 *
 * Description:
 *   Return true if the receive register is not empty
 *
 ****************************************************************************/

static bool up_rxavailable(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	return ((up_serialin(priv, SF2_UART_LSR_OFFSET) & UART_LSR_DR) != 0);
}

/****************************************************************************
 * Name: up_send
 *
 * Description:
 *   This method will send one byte on the UART
 *
 ****************************************************************************/

static void up_send(struct uart_dev_s *dev, int ch){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	up_serialout(priv, SF2_UART_THR_OFFSET, (uint8_t)ch);

}

/****************************************************************************
 * Name: up_txint
 *
 * Description:
 *   Call to enable or disable TX interrupts
 *
 ****************************************************************************/

static void up_txint(struct uart_dev_s *dev, bool enable){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	irqstate_t flags;

	flags = enter_critical_section();

	if(enable){
#ifndef CONFIG_SUPPRESS_SERIAL_INTS
		uint16_t ie = priv->ie | UART_IER_ETBEI;

		up_restoreuartint(priv, ie);

		uart_xmitchars(dev);
#endif
	}else{
		up_restoreuartint(priv, priv->ie & ~UART_IER_ETBEI);
	}

	leave_critical_section(flags);
}

/****************************************************************************
 * Name: up_txready
 *
 * Description:
 *   Return true if the tranmsit data register is empty
 *
 ****************************************************************************/

static bool up_txready(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	return ((up_serialin(priv, SF2_UART_LSR_OFFSET) & UART_LSR_THRE) != 0);
}

/* CORE UART0 */
#ifdef CONFIG_SF2_CORE_UART0

static int core_up_setup(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;

#ifndef CONFIG_SUPPRESS_UART_CONFIG
	uint32_t regval = 0;

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval |= priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	up_disable_irq(priv->irq);
	regval = 30;
	while(regval--);

	regval = getreg32(SF2_SYSREG_SOFT_IRQ_CR);
	regval &= ~priv->rst_mask;
	putreg32(regval, SF2_SYSREG_SOFT_IRQ_CR);

	uint8_t buad_value0_7 = CORE_UART_CTRL1_B9600;
	uint8_t buad_value8_13 = CORE_UART_CTRL2_B9600;

	if(priv->baud == 9600)
	{
		buad_value0_7 = CORE_UART_CTRL1_B9600;
		buad_value8_13 = CORE_UART_CTRL2_B9600;
	}else if(priv->baud == 19200)
	{
		buad_value0_7 = CORE_UART_CTRL1_B19200;
		buad_value8_13 = CORE_UART_CTRL2_B19200;
	}else if(priv->baud == 38400)
	{
		buad_value0_7 = CORE_UART_CTRL1_B38400;
		buad_value8_13 = CORE_UART_CTRL2_B38400;
	}else if(priv->baud == 57600)
	{
		buad_value0_7 = CORE_UART_CTRL1_B57600;
		buad_value8_13 = CORE_UART_CTRL2_B57600;
	}else if(priv->baud == 115200)
	{
		buad_value0_7 = CORE_UART_CTRL1_B19200;
		buad_value8_13 = CORE_UART_CTRL2_B19200;
	}
	up_serialout(priv, SF2_CORE_UART_CTRL1_OFFSET, buad_value0_7);

	regval = 0;
	switch(priv->bits){
	case 7: regval &= ~CORE_UART_CTRL2_BIT8; break;	
	case 8: regval |= CORE_UART_CTRL2_BIT8; break;	
	default: regval |= CORE_UART_CTRL2_BIT8; break;
	}
	regval |= priv->parity << 1;
	regval |= buad_value8_13;
	up_serialout(priv, SF2_CORE_UART_CTRL2_OFFSET, regval);

#endif  /* CONFIG_SUPPRESS_UART_CONFIG */
	priv->ie    = 0;
	return OK;
}


static int core_up_interrupt_common(struct up_dev_s *priv){

	int  passes;
	bool handled;

	handled = true;
	for (passes = 0; passes < 256 && handled; passes++){

		handled = false;

		priv->sr = up_serialin(priv, SF2_CORE_UART_STATUS_OFFSET);

		if((priv->sr & CORE_UART_STATUS_RXRDY) != 0){
			uart_recvchars(&priv->dev);
			handled = true;
		}else if((priv->sr & (CORE_UART_STATUS_PARITY_ERR | CORE_UART_STATUS_OVERFLOW | CORE_UART_STATUS_FRAMING_ERR)) != 0){
			(void)up_serialin(priv, SF2_CORE_UART0_RXDATA);
		}

		if((priv->sr & CORE_UART_STATUS_TXRDY) != 0){
			uart_xmitchars(&priv->dev);
			handled = true;
		}


	}
	return OK;
}

static int core_up_ioctl(struct file *filep, int cmd, unsigned long arg){
	return -ENOTTY;
}

static int core_up_receive(struct uart_dev_s *dev, uint32_t *status){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	uint32_t rbr;

	/* Get the Rx byte */
	rbr      = (uint32_t)up_serialin(priv, SF2_CORE_UART_RXDATA_OFFSET);

	/* Get the Rx byte plux error information.  Return those in status */
	*status  = priv->sr << 16 | rbr;
	priv->sr = 0;

	/* Then return the actual received byte */
	return rbr & 0xff;
}

static void core_up_rxint(struct uart_dev_s *dev, bool enable){

}
static bool core_up_rxavailable(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	return ((up_serialin(priv, SF2_CORE_UART_STATUS_OFFSET) & CORE_UART_STATUS_RXRDY) != 0);
}

static void core_up_send(struct uart_dev_s *dev, int ch){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	up_serialout(priv, SF2_CORE_UART_TXDATA_OFFSET, (uint8_t)ch);

}

static void core_up_txint(struct uart_dev_s *dev, bool enable){

}

static bool core_up_txready(struct uart_dev_s *dev){

	struct up_dev_s *priv = (struct up_dev_s*)dev->priv;
	return ((up_serialin(priv, SF2_CORE_UART_STATUS_OFFSET) & CORE_UART_STATUS_TXRDY) != 0);

}
#endif
/****************************************************************************
 * Name: up_interrupt_u[s]art[n]
 *
 * Description:
 *   Interrupt handlers for U[S]ART[n] where n=1,..,6.
 *
 ****************************************************************************/

#ifdef CONFIG_SF2_UART0
static int up_interrupt_uart0(int irq, void *context)
{
	return up_interrupt_common(&g_uart0priv);
}
#endif

#ifdef CONFIG_SF2_UART1
static int up_interrupt_uart1(int irq, void *context)
{
	return up_interrupt_common(&g_uart1priv);
}
#endif

#ifdef CONFIG_SF2_CORE_UART0
static int up_interrupt_core_uart0(int irq, void *context)
{
	return core_up_interrupt_common(&g_core_uart0priv);
}
#endif
#endif /* HAVE UART */

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_earlyserialinit
 *
 * Description:
 *   Performs the low level UART initialization early in debug so that the
 *   serial console will be available during bootup.  This must be called
 *   before up_serialinit.
 *
 ****************************************************************************/

void up_earlyserialinit(void)
{
#ifdef HAVE_UART
	unsigned i;
	for(i = 0; i < 2; i++){
		if(uart_devs[i]){
			up_disableuartint(uart_devs[i], NULL);
		}
	}
#ifdef HAVE_CONSOLE 
	up_setup(&uart_devs[CONSOLE_UART]->dev);
#endif
#ifdef HAVE_CORE_CONSOLE
	core_up_setup(&uart_devs[CONSOLE_UART]->dev);
#endif
#endif
}

/****************************************************************************
 * Name: up_serialinit
 *
 * Description:
 *   Register serial console and serial ports.  This assumes
 *   that up_earlyserialinit was called previously.
 *
 ****************************************************************************/

void up_serialinit(void){

#ifdef HAVE_UART
	char devname[16];
	unsigned i;
	unsigned minor = 0;

	/* Register the console */

#if CONSOLE_UART >= 0
	(void)uart_register("/dev/console", &uart_devs[CONSOLE_UART]->dev);

#ifndef CONFIG_SERIAL_DISABLE_REORDERING
	/* If not disabled, register the console UART to ttyS0 and exclude
	 * it from initializing it further down
	 */

	(void)uart_register("/dev/ttyS0", &uart_devs[CONSOLE_UART]->dev);
	minor = 1;

#endif /* CONFIG_SERIAL_DISABLE_REORDERING not defined */

#endif /* CONSOLE_UART >= 0 */

	/* Register all remaining UARTs */

	strcpy(devname, "/dev/ttySx");

	for (i = 0; i < SF2_NUART; i++){
		/* Don't create a device for non-configured ports. */
		if (uart_devs[i] == 0){
			continue;
		}

#ifndef CONFIG_SERIAL_DISABLE_REORDERING
		/* Don't create a device for the console - we did that above */

		if (uart_devs[i]->dev.isconsole){
			continue;
		}
#endif

		/* Register UARTs as devices in increasing order */

		devname[9] = '0' + minor++;
		(void)uart_register(devname, &uart_devs[i]->dev);
	}
#endif /* HAVE UART */
}

/****************************************************************************
 * Name: up_putc
 *
 * Description:
 *   Provide priority, low-level access to support OS debug  writes
 *
 ****************************************************************************/

int up_putc(int ch){

#if CONSOLE_UART >= 0
	struct up_dev_s *priv = uart_devs[CONSOLE_UART];
	uint16_t ie;
#ifdef HAVE_CONSOLE
	up_disableuartint(priv, &ie);
#endif
	/* Check for LF */

	if (ch == '\n'){
		/* Add CR */

		up_lowputc('\r');
	}

	up_lowputc(ch);

#ifdef HAVE_CONSOLE    
	up_restoreuartint(priv, ie);
#endif
#endif
	return ch;
}

#else /* USE_SERIALDRIVER */

/****************************************************************************
 * Name: up_putc
 *
 * Description:
 *   Provide priority, low-level access to support OS debug writes
 *
 ****************************************************************************/

int up_putc(int ch){

#if CONSOLE_UART >= 0
	/* Check for LF */

	if (ch == '\n'){
		/* Add CR */

		up_lowputc('\r');
	}

	up_lowputc(ch);
#endif
	return ch;
}

#endif /* USE_SERIALDRIVER */
