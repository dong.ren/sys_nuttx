#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>
#include <sys/types.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/config.h>
#include <nuttx/semaphore.h>
#include <nuttx/spi/spi.h>
#include <arch/board/board.h>
#include "up_arch.h"
#include "sf2_spi.h"

struct sf2_spidev_s
{
	struct spi_dev_s spidev;
	uint32_t regbase;
	sem_t exclsem;
	enum spi_mode_e mode;
}; 

#if defined(CONFIG_SF2_SPI0) || defined(CONFIG_SF2_SPI1)

static int spi_lock(FAR struct spi_dev_s *dev, bool lock)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;

	if (lock)
	{
		/* Take the semaphore (perhaps waiting) */

		while (sem_wait(&priv->exclsem) != 0)
		{
			/* The only case that an error should occur here is if the wait was awakened
			 * by a signal.
			 */

			ASSERT(errno == EINTR);
		}
	}
	else
	{
		(void)sem_post(&priv->exclsem);
	}

	return OK;
}

static void spi_setmode(FAR struct spi_dev_s *dev, enum spi_mode_e mode)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;

	/* Has the mode changed? */

	if (mode != priv->mode)
	{
		/* Yes... Set CR1 appropriately */

		switch (mode)
		{
		case SPIDEV_MODE0: /* CPOL=0; CPHA=0 */
			break;

		case SPIDEV_MODE1: /* CPOL=0; CPHA=1 */
			break;

		case SPIDEV_MODE2: /* CPOL=1; CPHA=0 */
			break;

		case SPIDEV_MODE3: /* CPOL=1; CPHA=1 */
			break;

		default:
			return;
		}
		/* Save the mode so that subsequent re-configurations will be faster */
		priv->mode = mode;
	}
}

// void set_frames_count(uint8_t count)
// {
//     modifyreg32(SF2_SPI0_CONTROL, 0, count << 8);
// }

static uint16_t spi_send(FAR struct spi_dev_s *dev, uint16_t wd)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;
	// uint32_t regval;
	// uint16_t ret;

	putreg32(wd, (void *)priv->regbase + SF2_SPI_TX_DATA_OFFSET);

	while ((getreg32((void *)priv->regbase + SF2_SPI_STATUS_OFFSET) & SPI_STATUS_TXFIFOEMP) == 0)
	{
	}

	while ((getreg32((void *)priv->regbase + SF2_SPI_STATUS_OFFSET) & SPI_STATUS_RXFIFOEMP))
	{
	}

	return getreg32((void *)priv->regbase + SF2_SPI_RX_DATA_OFFSET);
}

static void spi_exchange(FAR struct spi_dev_s *dev, FAR const void *txbuffer,
		FAR void *rxbuffer, size_t nwords)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;
	DEBUGASSERT(priv && priv->spibase);
	UNUSED(priv);

	/* 8-bit mode */

	const uint8_t *src = (const uint8_t *)txbuffer;
	uint8_t *dest = (uint8_t *)rxbuffer;
	uint8_t word;

	while (nwords-- > 0)
	{
		/* Get the next word to write.  Is there a source buffer? */

		if (src)
		{
			word = *src++;
		}
		else
		{
			word = 0xff;
		}

		/* Exchange one word */

		word = (uint8_t)spi_send(dev, (uint16_t)word);

		/* Is there a buffer to receive the return value? */
		if (dest)
		{
			*dest++ = word;
		}
	}
}

static void spi_sndblock(FAR struct spi_dev_s *dev, FAR const void *txbuffer, size_t nwords)
{
	spiinfo("txbuffer=%p nwords=%d\n", txbuffer, nwords);
	return spi_exchange(dev, txbuffer, NULL, nwords);
}

static void spi_recvblock(FAR struct spi_dev_s *dev, FAR void *rxbuffer, size_t nwords)
{
	spiinfo("rxbuffer=%p nwords=%d\n", rxbuffer, nwords);
	return spi_exchange(dev, NULL, rxbuffer, nwords);
}

static void spi_setbits(FAR struct spi_dev_s *dev, int nbits)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;

	modifyreg32(priv->regbase + SF2_SPI_CONTROL_OFFSET, 0, 1 << 31);
	putreg32(nbits, (void *)priv->regbase + SF2_SPI_TXRXDF_SIZE_OFFSET);
	modifyreg32(priv->regbase + SF2_SPI_CONTROL_OFFSET, 1 << 31, 0);
}

#ifdef CONFIG_SPI_HWFEATURES
static int spi_hwfeatures(FAR struct spi_dev_s *dev, spi_hwfeatures_t features)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;
	return -ENOSYS;
}
#endif

static uint32_t spi_setfrequency(FAR struct spi_dev_s *dev, uint32_t frequency)
{
	FAR struct sf2_spidev_s *priv = (FAR struct sf2_spidev_s *)dev;
	UNUSED(priv);
	return frequency;
}

#ifdef CONFIG_SF2_SPI0
static const struct spi_ops_s g_spi0ops =
{
	.lock = spi_lock,
	.select = sf2_spi0select,
	.setfrequency = spi_setfrequency,
	.setmode = spi_setmode,
	.setbits = spi_setbits,
#ifdef CONFIG_SPI_HWFEATURES
	.hwfeatures = spi_hwfeatures,
#endif
	.status = sf2_spi0status,
#ifdef CONFIG_SPI_CMDDATA
	.cmddata = sf2_spi0cmddata,
#endif
	.send = spi_send,
#ifdef CONFIG_SPI_EXCHANGE
	.exchange = spi_exchange,
#else
	.sndblock = spi_sndblock,
	.recvblock = spi_recvblock,
#endif
#ifdef CONFIG_SPI_CALLBACK
	.registercallback = 0, /* Provided externally */
#else
	.registercallback = 0, /* Not implemented */
#endif
};

static struct sf2_spidev_s g_spi0dev =
{
	.spidev = {&g_spi0ops},
	.regbase = SF2_SPI0_BASE,
};
#endif

#ifdef CONFIG_SF2_SPI1
static const struct spi_ops_s g_spi1ops =
{
	.lock = spi_lock,
	.select = sf2_spi1select,
	.setfrequency = spi_setfrequency,
	.setmode = spi_setmode,
	.setbits = spi_setbits,
#ifdef CONFIG_SPI_HWFEATURES
	.hwfeatures = spi_hwfeatures,
#endif
	.status = sf2_spi1status,
#ifdef CONFIG_SPI_CMDDATA
	.cmddata = sf2_spi1cmddata,
#endif
	.send = spi_send,
#ifdef CONFIG_SPI_EXCHANGE
	.exchange = spi_exchange,
#else
	.sndblock = spi_sndblock,
	.recvblock = spi_recvblock,
#endif
#ifdef CONFIG_SPI_CALLBACK
	.registercallback = 0, /* Provided externally */
#else
	.registercallback = 0, /* Not implemented */
#endif
};

static struct sf2_spidev_s g_spi1dev =
{
	.spidev = {&g_spi1ops},
	.regbase = SF2_SPI1_BASE,
};
#endif

static void spibus_init(struct sf2_spidev_s *dev)
{
	sem_init(&dev->exclsem, 0, 1);
	putreg32(8, (void *)dev->regbase + SF2_SPI_TXRXDF_SIZE_OFFSET);
	modifyreg32(dev->regbase + SF2_SPI_COMMAND_OFFSET, 0, 1 << 2 | 1 << 3);
	modifyreg32(dev->regbase + SF2_SPI_COMMAND_OFFSET, 1 << 2 | 1 << 3, 0);
	modifyreg32(dev->regbase + SF2_SPI_CONTROL_OFFSET, 1 << 31, 0);
	modifyreg32(dev->regbase + SF2_SPI_CONTROL_OFFSET, 0, 1 << 1 | 3 << 8 | 1 << 26 | 3 << 28);
	modifyreg32(dev->regbase + SF2_SPI_CLK_GEN_OFFSET, 0, 1 << 0);    
	modifyreg32(dev->regbase + SF2_SPI_CONTROL_OFFSET, 0, 1 << 0);
}

struct spi_dev_s *sf2_spibus_initialize(int bus)
{
	irqstate_t flags = enter_critical_section();
#ifdef CONFIG_SF2_SPI0
	if (bus == 0)
	{ 
		modifyreg32(SF2_SYSREG_SOFT_RST_CR, SYSREG_SOFTRESET_SPI0, 0);
		spibus_init(&g_spi0dev);
		leave_critical_section(flags);
		return (void *)&g_spi0dev;
	}
#endif
#ifdef CONFIG_SF2_SPI1
	if (bus == 1)
	{
		modifyreg32(SF2_SYSREG_SOFT_RST_CR, SYSREG_SOFTRESET_SPI1, 0);
		spibus_init(&g_spi1dev);
		leave_critical_section(flags);
		return (void *)&g_spi1dev;
	}
#endif
	leave_critical_section(flags);
	return 0;
}

#endif
