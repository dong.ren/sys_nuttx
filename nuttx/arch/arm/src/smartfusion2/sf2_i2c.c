#include <nuttx/config.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>
#include <errno.h>
#include <debug.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/kmalloc.h>
#include <nuttx/clock.h>
#include <arch/board/board.h>
#include "up_arch.h"
#include "sf2_i2c.h"

/* At least one I2C peripheral must be enabled */

#if defined(CONFIG_SF2_I2C0) || defined(CONFIG_SF2_I2C1) 

/*--------------------for   the para: options and bus_status-----------------------------------------------------
  The SF2_I2C_RELEASE_BUS constant is used to specify the options parameter to
  functions sf2_i2c_read(), sf2_i2c_write() to indicate   that a STOP bit must be
  generated at the end of the I2C transaction  to release   the bus.
  */
#define SF2_I2C_RELEASE_BUS     0x00u

/*-------------------------------------------------------------------------
  The SF2_I2C_HOLD_BUS constant is used to specify the options parameter to
  functions  sf2_i2c_read(),  sf2_i2c_write()  to  indicate that a STOP bit 
  must not be generated at the end of the I2C  transaction in order to retain 
  the bus ownership.  This causes the next transaction to begin with a repeated
  START bit and no STOP bit between the  transactions.
  */
#define SF2_I2C_HOLD_BUS        0x01u

/*-------------------------------------------------------------------------
  The SF2_I2C_NO_TIMEOUT constant is used as parameter to the sf2_i2c_wait_complete()
  function to indicate that the wait for completion of the transaction should
  not time out.
  */
#define SF2_I2C_NO_TIMEOUT  0u


/*-------------------------------------------------------------------------*//**
																			   The mss_i2c_status_t type is used to report the status of I2C transactions.
																			   for the para :  master_status  
																			   */

typedef enum sf2_i2c_status
{
	SF2_I2C_SUCCESS = 0u,
	SF2_I2C_IN_PROGRESS,
	SF2_I2C_FAILED,
	SF2_I2C_TIMED_OUT
} sf2_i2c_status_t;

/* I2C transaction direction.*/
#define WRITE_DIR    0u
#define READ_DIR     1u

/* -- TRANSACTIONS TYPES --   for   para:  trasaction and pending_trasaction    */
#define NO_TRANSACTION                      0u
#define MASTER_WRITE_TRANSACTION            1u
#define MASTER_READ_TRANSACTION             2u
#define MASTER_RANDOM_READ_TRANSACTION      3u
//#define WRITE_SLAVE_TRANSACTION             4u
//#define READ_SLAVE_TRANSACTION              5u

/* -- MASTER STATES -- */
#define ST_START              	0x08u                   	/* ST :STATES    start condition sent */
#define ST_RESTART              0x10u                	  /* repeated start */
#define ST_SLAW_ACK            	0x18u               	/* SLA+W sent, ack received */
#define ST_SLAW_NACK          	0x20u              	/* SLA+W sent, nack received */
#define ST_TX_DATA_ACK      	0x28u            		/* Data sent, ACK'ed */
#define ST_TX_DATA_NACK     	0x30u           		/* Data sent, NACK'ed */
#define ST_LOST_ARB         	0x38u           		/* Master lost arbitration */
#define ST_SLAR_ACK         	0x40u           		/* SLA+R sent, ACK'ed */
#define ST_SLAR_NACK        	0x48u           		/* SLA+R sent, NACK'ed */
#define ST_RX_DATA_ACK      	0x50u           		/* Data received, ACK sent */
#define ST_RX_DATA_NACK    	    0x58u           		/* Data received, NACK sent */
#define ST_RESET_ACTIVATED      0xD0u           	/* Master reset is activated */

typedef enum sf2_i2c_clock_divider {
	SF2_I2C_PCLK_DIV_256 = 0u,
	SF2_I2C_PCLK_DIV_224,
	SF2_I2C_PCLK_DIV_192,
	SF2_I2C_PCLK_DIV_160,
	SF2_I2C_PCLK_DIV_960,
	SF2_I2C_PCLK_DIV_120,
	SF2_I2C_PCLK_DIV_60,
	SF2_I2C_BCLK_DIV_8
} sf2_i2c_clock_divider_t;

/* I2C Device hardware configuration */

struct sf2_i2c_config_s
{
	uint32_t base;              /* I2C base address */ 
	int (*isr)(int, void *);    /* Interrupt handler */
	uint32_t ev_irq;            /* Event IRQ */
};

/* I2C Device Private Data */
struct sf2_i2c_priv_s
{
	const struct i2c_ops_s *ops;
	const struct sf2_i2c_config_s *config; /* Port configuration */
	int refs;                              /* Referernce count */
	sem_t sem_excl;                       /* Mutual exclusion semaphore */
	uint8_t msgc;                         /* Message count */
	uint8_t transaction;
	struct i2c_msg_s *msgv;               /* Message list */
	uint8_t *ptr;                         /* Current message buffer */
	uint32_t frequency;
	int dcnt;                             /* Current message length */
	uint16_t flags;                       /* Current message flags */
	uint_fast8_t dir;
	/*SF2_I2C_SUCCESS ,  SF2_I2C_PROGRESS , SF2_I2C_FAILED  SF2_I2C_TIMED_OUT*/ 
	volatile sf2_i2c_status_t master_status; 
	/*SF2_I2C_RELEASE_BUS  ,SF2_I2C_HOLD_BUS   SF2_I2C_NO_TIMEOUT  */	
	uint8_t  bus_status; 
	/*SF2_I2C_RELEASE_BUS  ,SF2_I2C_HOLD_BUS   SF2_I2C_NO_TIMEOUT  */
	uint8_t  options;    
};



/* I2C Device, Instance */

struct sf2_i2c_inst_s
{
	const struct i2c_ops_s  *ops;  /* Standard I2C operations */
	struct sf2_i2c_priv_s *priv; /* Common driver private data structure */
	uint32_t    frequency;      /* Frequency used in this instantiation */
	int         address;         /* Address used in this instantiation */
	uint16_t    flags;              /* Flags used in this instantiation */
};

static inline uint16_t sf2_i2c_getreg(FAR struct sf2_i2c_priv_s *priv,
		uint8_t offset);
static inline void sf2_i2c_putreg(FAR struct sf2_i2c_priv_s *priv, uint8_t offset,
		uint16_t value);
static inline void sf2_i2c_modifyreg(FAR struct sf2_i2c_priv_s *priv,
		uint8_t offset, uint16_t clearbits,
		uint16_t setbits);

static inline void sf2_i2c_sem_init(FAR struct i2c_master_s *dev);
static inline void sf2_i2c_sem_destroy(FAR struct i2c_master_s *dev);
static inline void sf2_i2c_sem_wait(FAR struct i2c_master_s *dev);
static inline void sf2_i2c_sem_post(FAR struct i2c_master_s *dev);
static inline uint8_t  sf2_i2c_getstatus(FAR struct sf2_i2c_priv_s *priv);
static inline sf2_i2c_status_t  sf2_i2c_wait_complete(FAR struct sf2_i2c_priv_s *priv,uint32_t  timeout);
static int sf2_i2c_isr(struct sf2_i2c_priv_s * priv);

#ifdef CONFIG_SF2_I2C0
static int sf2_i2c0_isr(int irq, void *context);
#endif

#ifdef CONFIG_SF2_I2C1
static int sf2_i2c1_isr(int irq, void *context);
#endif


static int sf2_i2c_init(uint8_t port, FAR struct sf2_i2c_priv_s *priv);
static int sf2_i2c_deinit(FAR struct sf2_i2c_priv_s *priv);
static int sf2_i2c_process(FAR struct i2c_master_s *dev, FAR struct i2c_msg_s *msgs,
		int count);
static uint32_t sf2_i2c_setfrequency(FAR struct i2c_master_s *dev,uint32_t frequency);                 
static int sf2_i2c_setaddress(FAR struct i2c_master_s *dev, int addr, int nbits);
static int sf2_i2c_write(FAR struct i2c_master_s *dev, uint8_t flags, const uint8_t *buffer,int buflen);
static int sf2_i2c_read(FAR struct i2c_master_s *dev, uint8_t flags, uint8_t *buffer, int buflen);
static int sf2_i2c_transfer(FAR struct i2c_master_s *dev, FAR struct i2c_msg_s *msgs, int count);

static const struct i2c_ops_s sf2_i2c_ops = {
	.transfer = sf2_i2c_transfer
#ifdef CONFIG_I2C_RESET
		, .reset = sf2_i2c_reset
#endif
};

#ifdef CONFIG_SF2_I2C0
static const struct sf2_i2c_config_s sf2_i2c0_config =
{
	.base       = I2C0_BASE,
	.isr        = sf2_i2c0_isr,
	.ev_irq     = I2C0_IRQn
};

static struct sf2_i2c_priv_s sf2_i2c0_priv =
{
	.ops      = &sf2_i2c_ops,
	.config     = &sf2_i2c0_config,
	.refs       = 0,
	.msgc       = 0,
	.msgv       = NULL,
	.ptr        = NULL,
	.dcnt       = 0,
	.flags      = 0,
	.dir         =WRITE_DIR,
	.master_status=SF2_I2C_SUCCESS,
	.bus_status=SF2_I2C_RELEASE_BUS, 
	.options=SF2_I2C_RELEASE_BUS    
};
#endif

#ifdef CONFIG_SF2_I2C1
static const struct sf2_i2c_config_s sf2_i2c1_config =
{
	.base       = I2C1_BASE,
	.isr        = sf2_i2c1_isr,
	.ev_irq     = I2C1_IRQn
};

static struct sf2_i2c_priv_s sf2_i2c1_priv =
{
	.ops        = &sf2_i2c_ops,
	.config     = &sf2_i2c1_config,
	.refs       = 0,
	.msgc       = 0,
	.msgv       = NULL,
	.ptr        = NULL,
	.dcnt       = 0,
	.flags      = 0,
	.dir         =WRITE_DIR,
	.master_status=SF2_I2C_SUCCESS,
	.bus_status=SF2_I2C_RELEASE_BUS, 
	.options=SF2_I2C_RELEASE_BUS    
};
#endif

static inline uint16_t sf2_i2c_getreg(FAR struct sf2_i2c_priv_s *priv, uint8_t offset)
{
	return getreg16(priv->config->base + offset);
}

static inline void sf2_i2c_putreg(FAR struct sf2_i2c_priv_s *priv, uint8_t offset,
		uint16_t value)
{
	putreg16(value, priv->config->base + offset);
}

static inline void sf2_i2c_modifyreg(FAR struct sf2_i2c_priv_s *priv,
		uint8_t offset, uint16_t clearbits,
		uint16_t setbits)
{
	modifyreg16(priv->config->base + offset, clearbits, setbits);
}

static inline void sf2_i2c_sem_wait(FAR struct i2c_master_s *dev)
{
	while (sem_wait(&((struct sf2_i2c_inst_s *)dev)->priv->sem_excl) != 0)
	{
		ASSERT(errno == EINTR);
	}
}

static inline void sf2_i2c_sem_post(FAR struct i2c_master_s *dev)
{
	sem_post(&((struct sf2_i2c_inst_s *)dev)->priv->sem_excl);
}


static inline void sf2_i2c_sem_init(FAR struct i2c_master_s *dev)
{
	sem_init(&((struct sf2_i2c_inst_s *)dev)->priv->sem_excl, 0, 1);
}

static inline void sf2_i2c_sem_destroy(FAR struct i2c_master_s *dev)
{
	sem_destroy(&((struct sf2_i2c_inst_s *)dev)->priv->sem_excl);
}

static inline uint8_t sf2_i2c_getstatus(FAR struct sf2_i2c_priv_s *priv)
{
	uint8_t status = (sf2_i2c_getreg(priv, SF2_I2C_STATUS_OFFSET)&0xff);
	return status;
}

static int  sf2_i2c_isr(struct sf2_i2c_priv_s  *priv )
{
	volatile uint8_t status;
	uint8_t hold_bus;
	uint8_t clear_irq = 1u;

	status = sf2_i2c_getstatus(priv);

	switch(status){
		/************** MASTER TRANSMITTER / RECEIVER *******************/
	case ST_START: /* start has been xmt'd */      
	case ST_RESTART: /* repeated start has been xmt'd */  
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,I2C_CTRL_STA, 0 );          
		/* Get run-time data */
		priv->ptr   = priv->msgv->buffer;
		priv->dcnt  = priv->msgv->length;
		priv->flags = priv->msgv->flags;	  

		/* Send address byte and define addressing mode */
		// sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,((priv->msgv->addr << 1) | (priv->flags & I2C_M_READ)));             
		sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,((priv->msgv->addr) |
					(priv->flags & I2C_M_READ)));             
		/* Increment to next pointer and decrement message count */
		priv->msgc--;  
		if(priv->msgc >0){
			priv->msgv++;
		}
		break;

	case ST_LOST_ARB:
		/* Set start bit.  Let's keep trying!  Don't give up! */         
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_STA );
		break;

		/******************* MASTER TRANSMITTER *************************/
	case ST_SLAW_NACK:
		/* SLA+W has been transmitted; not ACK has been received - let's stop. */
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO  );
		priv->master_status = SF2_I2C_FAILED;       
		break;

	case ST_SLAW_ACK:
		if (priv->dcnt > 0){
			/* Send a byte */ 
			sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,*priv->ptr++);  
			priv->dcnt--;
		}        
		break;
	case ST_TX_DATA_ACK:  
		/* data byte has been xmt'd with ACK, time to send stop bit or repeated start. */
		if(priv->dir == READ_DIR){
			priv ->msgv ->flags  = I2C_M_READ;
			// priv->flags = priv->msgv->flags;
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STA );
		}			    	
		else if ((priv->dcnt > 0)&&(priv->dir == WRITE_DIR))
		{
			/* Send a byte */
			if(priv->dcnt > 1)
			{
				sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,*priv->ptr++);  
			}
			else 
			{
				sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,*priv->ptr);  
			}
			priv->dcnt--;

		}   		

#if 1   
		else if ( priv->transaction == MASTER_RANDOM_READ_TRANSACTION )
		{
			/* We are finished sending the address offset part of a random read transaction.
			 * It is is time to send a restart in order to change direction. */
			priv->dir = READ_DIR;   
			priv ->msgv ->flags  = I2C_M_READ;  //bug  ????????????????????
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STA );
		}
#endif 	
		else if(( priv ->dcnt ==0)&&(priv ->msgc >0))
		{

			/* Get run-time data */
			priv->ptr   = priv->msgv->buffer;
			priv->dcnt  = priv->msgv->length;
			priv->flags = priv->msgv->flags;	  

			/* Send a byte */ 
			sf2_i2c_putreg(priv, SF2_I2C_DATA_OFFSET,*priv->ptr++);  
			priv->dcnt--;            

			/* Increment to next pointer and decrement message count */
			priv->msgc--;  
			if(priv -> msgc > 0){
				priv->msgv++;
			}		
		}
		else  /* done sending. let's stop */
		{
			/*
			 * Set the transaction back to NO_TRANSACTION to allow user to do further
			 * transaction
			 */

			hold_bus = priv->options & SF2_I2C_HOLD_BUS;

			/* Store the information of current I2C bus status in the bus_status*/
			priv->bus_status  = hold_bus;
			if (hold_bus == 0u){ 
				sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO );
				//    enable_slave_if_required(priv);
			}
			else
			{
				up_disable_irq(priv->config->ev_irq);
				clear_irq = 0u;
			}
			priv->master_status = SF2_I2C_SUCCESS;
		}
		break;

	case ST_TX_DATA_NACK:
		/* data byte SENT, ACK to be received
		 * In fact, this means we've received a NACK (This may not be 
		 * obvious, but if we've rec'd an ACK then we would be in state 
		 * 0x28!) hence, let's send a stop bit
		 */
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO );
		priv->master_status = SF2_I2C_FAILED;

		/*
		 * Set the transaction back to NO_TRANSACTION to allow user to do further
		 * transaction
		 */

		// enable_slave_if_required(priv);
		break;

		/********************* MASTER (or slave?) RECEIVER *************************/

		/* STATUS codes 08H, 10H, 38H are all covered in MTX mode */
	case ST_SLAR_ACK: /* SLA+R tx'ed. */
		/* Let's make sure we ACK the first data byte received (set AA bit in CTRL) unless
		 * the next byte is the last byte of the read transaction.
		 */
		if(priv->dcnt> 1u){              
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_AA);
		}
		else if(1u == priv->dcnt){
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,I2C_CTRL_AA,0);
		}
		else /* priv->dcnt == 0u */
		{
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_AA);
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO);
			priv->master_status = SF2_I2C_SUCCESS;            
		}
		break;

	case ST_SLAR_NACK: /* SLA+R tx'ed; let's release the bus (send a stop condition) */
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO);
		priv->master_status = SF2_I2C_FAILED;

		// enable_slave_if_required(priv);
		break;

	case ST_RX_DATA_ACK: /* Data byte received, ACK returned */
		/* First, get the data */
		*priv->ptr++ = sf2_i2c_getreg(priv, SF2_I2C_DATA_OFFSET);
		/* Disable acknowledge when last byte is to be received */
		priv->dcnt--;
		if (priv->dcnt == 1){
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,I2C_CTRL_AA,0);
		}
		break;

	case ST_RX_DATA_NACK: /* Data byte received, NACK returned */
		/* Get the data, then send a stop condition */
		*priv->ptr++ = sf2_i2c_getreg(priv, SF2_I2C_DATA_OFFSET);
		hold_bus = priv->options &  SF2_I2C_HOLD_BUS;       

		/* Store the information of current I2C bus status in the bus_status*/
		priv->bus_status  = hold_bus;
		if ( hold_bus == 0u ){ 
			sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_STO);  /*xmt stop condition */
			/* Bus is released, now we can start listening to bus, if it is slave */
			//enable_slave_if_required(priv);
		}
		else
		{
			up_disable_irq(priv->config->ev_irq);
			clear_irq = 0u;
		}

		priv->master_status = SF2_I2C_SUCCESS;
		break;

	default:
		/* Some undefined state has encountered. Clear Start bit to make sure, next good transaction happen */

		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,I2C_CTRL_STA,0);
		priv->master_status = SF2_I2C_FAILED;

		/* 
		 * We need this because we end up in an unexpected state when we are
		 * the taget of read quick command. We need to reset the slave_tx_idx
		 * here to ensure the slave tx buffer is transmited from the beginning
		 * in subsequent transactions.
		 */
		priv->dcnt= 0u;
		break;
	}

	if(clear_irq){
		/* clear interrupt. */
		sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,I2C_CTRL_SI,0);
	}

	/*
	   Read the status register to ensure the last I2C registers write 
	   took place  in a system built around a bus making use of posted writes.
	   */
	status =  sf2_i2c_getstatus(priv);
	return 0;
}


#ifdef CONFIG_SF2_I2C0
static int sf2_i2c0_isr(int irq, void *context)
{
	return sf2_i2c_isr(&sf2_i2c0_priv);
}
#endif


#ifdef CONFIG_SF2_I2C1
static int sf2_i2c1_isr(int irq, void *context)
{
	return sf2_i2c_isr(&sf2_i2c1_priv);
}
#endif

static inline void  ClearPendingInterrupt(FAR struct sf2_i2c_priv_s *priv)
{  
	sf2_i2c_modifyreg(priv,SF2_I2C_CTRL_OFFSET,0,I2C_CTRL_SI );
}


static int sf2_i2c_init(uint8_t port, FAR struct sf2_i2c_priv_s *priv)
{

#ifdef CONFIG_SF2_I2C0         
	if (port == 0) {
		/* reset I2C0 */
		modifyreg16(SYSREG_BASE+SOFT_RST_CR_OFFSET,0,I2C0_SOFTRESET);        
		/* Clear any previously pended I2C0 interrupt */
		ClearPendingInterrupt(priv);
		/* Take I2C0 out of reset. */
		modifyreg16(SYSREG_BASE+SOFT_RST_CR_OFFSET,I2C0_SOFTRESET ,0);
	}
#endif


#ifdef CONFIG_SF2_I2C1    
	if (port == 1) {
		sf2_i2c_modifyreg(priv,SOFT_RST_CR_OFFSET ,0,I2C1_SOFTRESET);

		ClearPendingInterrupt(( struct i2c_dev_s *)priv);
		/* Take I2C1 out of reset. */
		sf2_i2c_modifyreg(priv,SOFT_RST_CR_OFFSET ,I2C1_SOFTRESET,0);
	}
#endif

	/* Attach ISRs */
	irq_attach(priv->config->ev_irq, priv->config->isr); 
	up_enable_irq(priv->config->ev_irq); //firstly open the interrupt and then open the IIC 
	/* Enable I2C */
	//  sf2_i2c_modifyreg(priv,  SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_ENS1 );
	return OK;
}

static int sf2_i2c_deinit(FAR struct sf2_i2c_priv_s *priv)
{
	/* Disable I2C */
	sf2_i2c_putreg(priv, SF2_I2C_CTRL_OFFSET , 0);
	/* Disable and detach interrupts */
	up_disable_irq(priv->config->ev_irq);
	irq_detach(priv->config->ev_irq);
	/* Disable clocking */
	// modifyreg32(STM32_RCC_APB1ENR, priv->config->clk_bit, 0);
	return OK;
}

static uint32_t sf2_i2c_setfrequency(FAR struct i2c_master_s  *dev, uint32_t frequency)
{    
	int CR0,CR1,CR2;
	uint_fast16_t clock_speed = (uint_fast16_t)frequency;  
	struct  sf2_i2c_inst_s  *inst=(struct  sf2_i2c_inst_s *)dev ;	

	sf2_i2c_sem_wait(dev);     
	//inst->frequency =frequency;   //bug::divider
	/*
	   CR2 = ((clock_speed >> 2u) & 0x01u);
	   CR1 = ((clock_speed >> 1u) & 0x01u);
	   CR0 = (clock_speed & 0x01u);
	   */

	CR2 = 1;
	CR1 = 0;
	CR0 = 0;

	if(CR2){
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_CR2 );
	}else
	{
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,I2C_CTRL_CR2, 0 );
	}

	if(CR1){
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_CR1 );
	}
	else{
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,I2C_CTRL_CR1, 0 );
	}

	if(CR0){
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_CR0 );
	}
	else{  
		sf2_i2c_modifyreg(inst->priv,  SF2_I2C_CTRL_OFFSET,I2C_CTRL_CR0, 0 );
	}

	sf2_i2c_sem_post(dev);	 
	return inst->frequency;
}

static int sf2_i2c_setaddress(FAR struct i2c_master_s *dev, int addr, int nbits)
{
	struct  sf2_i2c_inst_s  *inst=(struct sf2_i2c_inst_s *)dev ;

	sf2_i2c_sem_wait(dev); 
	inst->address   = addr;    
	sf2_i2c_putreg( inst->priv,SF2_I2C_SLAVE0_ADR_OFFSET, (uint_fast8_t)(inst->address));
	//	sf2_i2c_putreg( inst->priv,SF2_I2C_SLAVE0_ADR_OFFSET, (uint_fast8_t)(inst->address<< 1u));
	sf2_i2c_sem_post(dev);

	return OK;
}



	sf2_i2c_status_t sf2_i2c_wait_complete
(
 FAR struct sf2_i2c_priv_s *priv,
 uint32_t timeout
 )
{
	uint32_t start;
	uint32_t elapsed;
	sf2_i2c_status_t   i2c_status;  

	start = clock_systimer();

	do{
		elapsed = clock_systimer() - start;
		i2c_status = priv->master_status;
		if(elapsed >= timeout){
			priv->master_status = SF2_I2C_TIMED_OUT;
			i2c_status = SF2_I2C_TIMED_OUT;
			break;
		}    
	}
	/* Run the loop until state returns time out or I2C_SUCESS*/
	while ((SF2_I2C_IN_PROGRESS == i2c_status)&&( elapsed < timeout));
	return i2c_status;

}

static int sf2_i2c_process(FAR struct i2c_master_s *dev, FAR struct i2c_msg_s *msgs,
		int count)
{
	struct sf2_i2c_inst_s     *inst = (struct sf2_i2c_inst_s *)dev;
	FAR struct sf2_i2c_priv_s *priv = inst->priv; 
	sf2_i2c_status_t  the_result_status;  

	priv->msgv = msgs;
	priv->msgc = count;  
	sf2_i2c_modifyreg(inst ->priv,SF2_I2C_CTRL_OFFSET, 0, I2C_CTRL_STA);
	/* Wait for an ISR, if there was a timeout, return ERROR flag  */
	the_result_status=  sf2_i2c_wait_complete( priv,20);

	if(the_result_status ==SF2_I2C_TIMED_OUT ){    
		sf2_i2c_sem_post(dev);
		return -ETIME;
	}else if(the_result_status ==SF2_I2C_FAILED){/*SF2_I2C_FAILED*/
		sf2_i2c_sem_post(dev);
		return   -EBUSY;
	}

	sf2_i2c_sem_post(dev);
	return SF2_I2C_SUCCESS;
}

static int sf2_i2c_write(FAR struct i2c_master_s *dev,uint8_t flags, const uint8_t *buffer,int buflen)	
{
	struct  sf2_i2c_inst_s  *inst=(struct  sf2_i2c_inst_s *)dev ;
	sf2_i2c_sem_wait(dev);   /* ensure that address or flags don't change meanwhile */

	struct i2c_msg_s msgv1 =
	{
		.addr   = ((struct sf2_i2c_inst_s *)dev)->address,
		.flags  = flags,
		.buffer = (uint8_t *)buffer,
		.length = buflen
	};

	inst ->priv->dir = WRITE_DIR;
	/* Set I2C status in progress */
	inst ->priv->master_status = SF2_I2C_IN_PROGRESS;

	//	sf2_i2c_modifyreg(inst ->priv,SF2_I2C_CTRL_OFFSET, 0, I2C_CTRL_STA);

	/*
	   Clear interrupts if required (depends on repeated starts).
	   Since the Bus is on hold, only then prior status needs to
	   be cleared.
	   */
	if(SF2_I2C_HOLD_BUS ==inst ->priv->bus_status){
		sf2_i2c_modifyreg(inst ->priv,SF2_I2C_CTRL_OFFSET, I2C_CTRL_SI , 0);
		// stat_ctrl = this_i2c->hw_reg->STATUS;
		// stat_ctrl = stat_ctrl;  /* Avoids Lint warning */
		//  NVIC_ClearPendingIRQ( this_i2c->irqn ); 
	}

	/* Enable the interrupt. ( Re-enable) */
	//up_enable_irq( inst->priv->config ->ev_irq); 
	return sf2_i2c_process(dev, &msgv1, 1);
}

static int sf2_i2c_read(FAR struct i2c_master_s *dev, uint8_t flags, uint8_t *buffer,int buflen)
{
	struct  sf2_i2c_inst_s  *inst=(struct  sf2_i2c_inst_s *)dev ;	
	sf2_i2c_sem_wait(dev);   /* ensure that address or flags don't change meanwhile */

	struct i2c_msg_s msgv2 =
	{
		.addr   = ((struct sf2_i2c_inst_s *)dev)->address,
		// .flags  = ((struct sf2_i2c_inst_s *)dev)->flags | I2C_M_READ,
		.flags  = flags , 
		.buffer = (uint8_t *)buffer,
		.length = buflen
	};

	inst ->priv->dir = READ_DIR;
	/* Set I2C status in progress */
	inst ->priv->master_status = SF2_I2C_IN_PROGRESS;
	//	sf2_i2c_modifyreg(inst ->priv,SF2_I2C_CTRL_OFFSET, 0, I2C_CTRL_STA);

	/*
	 * Clear interrupts if required (depends on repeated starts).
	 * Since the Bus is on hold, only then prior status needs to
	 * be cleared.
	 */
	if(SF2_I2C_HOLD_BUS == inst ->priv->bus_status){
		sf2_i2c_modifyreg(inst ->priv,SF2_I2C_CTRL_OFFSET, I2C_CTRL_SI , 0);
		//  stat_ctrl = this_i2c->hw_reg->STATUS;
		//  stat_ctrl = stat_ctrl;  /* Avoids Lint warning */
		//  NVIC_ClearPendingIRQ( this_i2c->irqn );
	}

	/* Enable the interrupt. ( Re-enable) */
	//  up_enable_irq( inst->priv->config ->ev_irq);
	return sf2_i2c_process(dev, &msgv2, 1);
}

static int sf2_i2c_transfer(FAR struct i2c_master_s *dev,
		FAR struct i2c_msg_s *msgs, int count)
{
	uint8_t flag = msgs->flags;
	int ret;
	sf2_i2c_setaddress(dev, msgs->addr, 7);
	/* set frequency */
	sf2_i2c_setfrequency(dev, 1000000);
	/* write */
	if (msgs->flags == 0) {
		ret = sf2_i2c_write(dev, msgs->flags, msgs->buffer, msgs->length);
		//sf2_i2c_write(dev, msgs);
	}
	/* read */
	if (msgs->flags == I2C_M_READ) {
		ret = sf2_i2c_read(dev, msgs->flags, msgs->buffer, msgs->length);
		//sf2_i2c_read(dev, msgs);
	}

	return ret;
}

FAR struct i2c_master_s *sf2_i2cbus_initialize(int port)
{
	struct sf2_i2c_priv_s * priv = NULL;  /* Private data of device with multiple instances */
	struct sf2_i2c_inst_s * inst = NULL;  /* Device, single instance */
	irqstate_t flags;
	int count =port;

	/* Get I2C private structure */
	switch(count){
	case 0:
		priv = (struct sf2_i2c_priv_s *)&sf2_i2c0_priv;  
		break;
#if 1
#ifdef CONFIG_SF2_I2C1
	case 1:
		priv = (struct sf2_i2c_priv_s *)&sf2_i2c1_priv;
		break;
#endif
#endif 
	default:
		return NULL;
	}
	/* Allocate instance */
	if(!(inst = kmm_malloc(sizeof(struct sf2_i2c_inst_s)))){
		return NULL;
	}
	/* Initialize instance */
	inst->ops       = &sf2_i2c_ops;
	inst->priv      = priv;
	inst->frequency = 100000; 
	inst->address   = 0;      
	inst->flags     = 0;

	/* Initialize private data for the first time, increment reference count,
	 * power-up hardware and configure GPIOs.
	 */
	flags = enter_critical_section();

	if((volatile int)priv->refs++ == 0){
		sf2_i2c_sem_init((struct i2c_master_s *)inst);
		sf2_i2c_init(port, priv);
		//sf2_i2c_setfrequency((struct i2c_dev_s *)inst, inst->frequency);   
		//sf2_i2c_setaddress((struct i2c_dev_s *)inst,inst ->address,0); 
	}

	sf2_i2c_modifyreg(priv,  SF2_I2C_CTRL_OFFSET,0, I2C_CTRL_ENS1 );
	leave_critical_section(flags);
	return (struct i2c_master_s *)inst;
}

int sf2_i2cbus_uninitialize(FAR struct i2c_master_s * dev)
{
	irqstate_t flags;
	ASSERT(dev);
	/* Decrement reference count and check for underflow */
	if(((struct sf2_i2c_inst_s *)dev)->priv->refs == 0){
		return ERROR;
	}
	flags = enter_critical_section();
	if(--((struct sf2_i2c_inst_s *)dev)->priv->refs){
		leave_critical_section(flags);
		kmm_free(dev);
		return OK;
	}

	leave_critical_section(flags);

	/* Disable power and other HW resource (GPIO's) */
	sf2_i2c_deinit(((struct sf2_i2c_inst_s *)dev)->priv);
	/* Release unused resources */
	sf2_i2c_sem_destroy((struct i2c_master_s *)dev);
	kmm_free(dev);
	return OK;
}

#ifdef CONFIG_I2C_RESET
int sf2_i2c_reset(FAR struct i2c_master_s * dev)
{
	struct sf2_i2c_priv_s * priv;
	unsigned int clock_count;
	unsigned int stretch_count;
	uint32_t scl_gpio;
	uint32_t sda_gpio;
	int ret = ERROR;

	ASSERT(dev);
	/* Get I2C private structure */
	priv = ((struct sf2_i2c_inst_s *)dev)->priv;

	/* Our caller must own a ref */
	ASSERT(priv->refs > 0);

	/* Lock out other clients */
	sf2_i2c_sem_wait(dev);

	/* De-init the port */
	sf2_i2c_deinit(priv);

	sf2_i2c_init(priv);
	sf2_i2c_sem_post(dev);
	return ret;
}
#endif /* CONFIG_I2C_RESET */

#endif /* CONFIG_SF2_I2C0 || CONFIG_SF2_I2C1 */
