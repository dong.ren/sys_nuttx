#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_I2C_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_I2C_H

#include <nuttx/config.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/i2c/i2c_slave.h>
//#include "chip.h"

#  include "chip/sf2_i2c.h"

/****************************************************************************
 * Name: sf2_i2cbus_initialize
 *
 * Description:
 *   Initialize the selected I2C port. And return a unique instance of struct
 *   struct i2c_master_s.  This function may be called to obtain multiple
 *   instances of the interface, each of which may be set up with a
 *   different frequency and slave address.
 *
 * Input Parameter:
 *   Port number (for hardware that has multiple I2C interfaces)
 *
 * Returned Value:
 *   Valid I2C device structure reference on succcess; a NULL on failure
 *
 ****************************************************************************/

FAR struct i2c_master_s *sf2_i2cbus_initialize(int port);

/****************************************************************************
 * Name: sf2_i2cbus_uninitialize
 *
 * Description:
 *   De-initialize the selected I2C port, and power down the device.
 *
 * Input Parameter:
 *   Device structure as returned by the stm32_i2cbus_initialize()
 *
 * Returned Value:
 *   OK on success, ERROR when internal reference count mismatch or dev
 *   points to invalid hardware device.
 *
 ****************************************************************************/

int sf2_i2cbus_uninitialize(FAR struct i2c_master_s *dev);

#endif /* __ARCH_ARM_SRC_SMARTFUSION2_SF2_I2C_H */

