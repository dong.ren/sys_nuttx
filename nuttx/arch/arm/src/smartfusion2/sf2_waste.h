#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

void up_waste(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /*__ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H */

