
#ifndef __ARCH_ARM_STC_SMARTFUSION2_SF2_UART_H
#define __ARCH_ARM_STC_SMARTFUSION2_SF2_UART_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"
#include "chip/sf2_uart.h"
#include "chip/sf2_core_uart.h"

/*********************************************************************************
 * Pre-processor Definitions
 ********************************************************************************/

/* Is there a UART enabled? */

#if defined(CONFIG_SF2_UART0) || defined(CONFIG_SF2_UART1) || defined(CONFIG_SF2_CORE_UART0)
#  define HAVE_UART 1
#endif

/* Is there a serial console? */

#if defined(CONFIG_UART0_SERIAL_CONSOLE) && defined(CONFIG_SF2_UART0)
#  undef CONFIG_UART1_SERIAL_CONSOLE
#  undef CONFIG_CORE_UART0_SERIAL_CONSOLE
#  define CONSOLE_UART 0
#  define HAVE_CONSOLE 1
#elif defined(CONFIG_UART1_SERIAL_CONSOLE) && defined(CONFIG_SF2_UART1)
#  undef CONFIG_UART0_SERIAL_CONSOLE
#  undef CONFIG_CORE_UART0_SERIAL_CONSOLE
#  define CONSOLE_UART 1
#  define HAVE_CONSOLE 1
#elif defined(CONFIG_CORE_UART0_SERIAL_CONSOLE) && defined(CONFIG_SF2_CORE_UART0) 
#  undef CONFIG_UART0_SERIAL_CONSOLE
#  undef CONFIG_UART1_SERIAL_CONSOLE
#  define CONSOLE_UART 2
#  undef HAVE_CONSOLE
#  define HAVE_CORE_CONSOLE 1
#else
#  undef CONFIG_UART0_SERIAL_CONSOLE
#  undef CONFIG_UART1_SERIAL_CONSOLE
#  undef CONFIG_CORE_UART0_SERIAL_CONSOLE
#  define CONSOLE_UART -1
#  undef HAVE_CONSOLE
#  undef HAVE_CORE_CONSOLE
#endif


#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif


#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __ARCH_ARM_STC_SMARTFUSION2_SF2_UART_H */
