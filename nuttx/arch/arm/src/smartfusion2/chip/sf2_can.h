
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CAN_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CAN_H

#include <nuttx/config.h>
#include "chip.h"

#define SF2_CAN_BASE      0x40015000

/* Register Offsets ***********************************************************/

#define SF2_CAN_CONFIG             (SF2_CAN_BASE + 0x018)
#define SF2_CAN_COMMAND            (SF2_CAN_BASE + 0x014)

#define SF2_CAN_TXMSG_CTRL_CMD(n)  (SF2_CAN_BASE + 0x020 + ((n) << 4) + 0x00)
#define SF2_CAN_TXMSG_ID(n)        (SF2_CAN_BASE + 0x020 + ((n) << 4) + 0x04)
#define SF2_CAN_TXMSG_DATA_HIGH(n) (SF2_CAN_BASE + 0x020 + ((n) << 4) + 0x08)
#define SF2_CAN_TXMSG_DATA_LOW(n)  (SF2_CAN_BASE + 0x020 + ((n) << 4) + 0x0c)
#define SF2_CAN_TX_BUF_STATUS      (SF2_CAN_BASE + 0x00c)

#define SF2_CAN_RXMSG_CTRL_CMD(n)  (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x00)
#define SF2_CAN_RXMSG_ID(n)        (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x04)
#define SF2_CAN_RXMSG_DATA_HIGH(n) (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x08)
#define SF2_CAN_RXMSG_DATA_LOW(n)  (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x0c)
#define SF2_CAN_RXMSG_AMR(n)       (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x10)
#define SF2_CAN_RXMSG_ACR(n)       (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x14)
#define SF2_CAN_RXMSG_AMR_DATA(n)  (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x18)
#define SF2_CAN_RXMSG_ACR_DATA(n)  (SF2_CAN_BASE + 0x220 + ((n) << 5) + 0x1c)
#define SF2_CAN_RX_BUF_STATUS      (SF2_CAN_BASE + 0x008)

#define SF2_CAN_ECR                (SF2_CAN_BASE + 0x01c)
#define SF2_CAN_ERROR_STATUS       (SF2_CAN_BASE + 0x010)
#define SF2_CAN_INT_ENABLE         (SF2_CAN_BASE + 0x004)
#define SF2_CAN_INT_STATUS         (SF2_CAN_BASE + 0x000)

/* Register Addresses *********************************************************/

/* CAN_CONFIG register */

#define CAN_CFG_EDGE_MODE      (1 << 0)
#define CAN_CFG_SAMPLING_MODE  (1 << 1)
#define CAN_CFG_SJW_SHIFT      (2)
#define CAN_CFG_SJW_MASK       (0x3 << CAN_CFG_SJW_SHIFT)
#define CAN_CFG_AUTO_RESTART   (1 << 4)
#define CAN_CFG_TSEG2_SHIFT    (5)
#define CAN_CFG_TSEG2_MASK     (0x7 << CAN_CFG_TSEG2_SHIFT)
#  define CAN_CFG_TSEG2_MAX    (0x7)
#define CAN_CFG_TSEG1_SHIFT    (8)
#define CAN_CFG_TSEG1_MASK     (0xf << CAN_CFG_TSEG1_SHIFT)
#  define CAN_CFG_TSEG1_MAX    (0xf)
#define CAN_CFG_ARBITER        (1 << 12)
#define CAN_CFG_SWAP_ENDIAN    (1 << 13)
#define CAN_CFG_ECR_MODE       (1 << 14)
#define CAN_CFG_BITRATE_SHIFT  (16)
#define CAN_CFG_BITRATE_MASK   (0x7fff << CAN_CFG_BITRATE_SHIFT)

/* CAN_COMMAND register */

#define CAN_COMMAND_RUNSTOP_MODE    (1 << 0)
#define CAN_COMMAND_LISTENONLY_MODE (1 << 1)
#define CAN_COMMAND_LOOPBACK_MODE   (1 << 2)
#define CAN_COMMAND_SRAM_MODE       (1 << 3)

#define CAN_COMMAND_REVISION_CONTROL_SHIFT (16)
#define CAN_COMMAND_REVISION_CONTROL_MASK  \
	(0xffff << CAN_COMMAND_REVISION_CONTROL_SHIFT)

/* TX_MSG_CTRL_CMD */
#define CAN_TXMSG_CTRL_CMD_TXREQ     (1 << 0)
#define CAN_TXMSG_CTRL_CMD_TXABORT   (1 << 1)
#define CAN_TXMSG_CTRL_CMD_TXINTEBI  (1 << 2)
#define CAN_TXMSG_CTRL_CMD_WPN2      (1 << 3)
#define CAN_TXMSG_CTRL_CMD_DLC_SHIFT (16)
#define CAN_TXMSG_CTRL_CMD_DLC_MASK  (0xf << CAN_TXMSG_CTRL_CMD_DLC_SHIFT)
#define CAN_TXMSG_CTRL_CMD_IDE       (1 << 20)
#define CAN_TXMSG_CTRL_CMD_RTR       (1 << 21)
#define CAN_TXMSG_CTRL_CMD_WPN16     (1 << 22)

/* TX_MSG_ID */
#define CAN_TXMSG_STID_SHIFT (3)
#define CAN_TXMSG_EXID_SHIFT (3)

/* TX_BUF_STATUS */
#define CAN_STATUS_TXMESSAGE(n)  (1 << n)


/* RXMSG_CTRL_CMD */
#define CAN_RXMSG_CTRL_CMD_MSGAV  (1 << 0)
#define CAN_RXMSG_CTRL_CMD_RTRP   (1 << 1)
#define CAN_RXMSG_CTRL_CMD_RTRABORT (1 << 2)
#define CAN_RXMSG_CTRL_CMD_TXBUFEBI (1 << 3)
#define CAN_RXMSG_CTRL_CMD_RTRREPLY (1 << 4)
#define CAN_RXMSG_CTRL_CMD_RXINTEBI (1 << 5)
#define CAN_RXMSG_CTRL_CMD_LF       (1 << 6)
#define CAN_RXMSG_CTRL_CMD_WPNL     (1 << 7)
#define CAN_RXMSG_CTRL_CMD_DLC_SHIFT (16)
#define CAN_RXMSG_CTRL_CMD_DLC_MASK  (0xf << CAN_RXMSG_CTRL_CMD_DLC_SHIFT)
#define CAN_RXMSG_CTRL_CMD_IDE      (1 << 20)
#define CAN_RXMSG_CTRL_CMD_RTR      (1 << 21)
#define CAN_RXMSG_CTRL_CMD_WPNH     (1 << 23)

/* RX_MSG_ID */
#define CAN_RXMSG_STID_SHIFT (3)
#define CAN_RXMSG_EXID_SHIFT (28)

/* RX_BUF_STATUS */
#define CAN_STATUS_RXMESSAGE(n)  (1 << n)

/* CAN_ECR */
/* CAN_ERROR_STATUS */

/* CAN_INT_ENABLE */
#define CAN_INT_ENABLE_GLOBAL      (1 << 0)
#define CAN_INT_ENABLE_ARB_LOSS    (1 << 2)
#define CAN_INT_ENABLE_OVR_LOAD    (1 << 3)
#define CAN_INT_ENABLE_BIT_ERR     (1 << 4)
#define CAN_INT_ENABLE_STUFF_ERR   (1 << 5)
#define CAN_INT_ENABLE_ACK_ERR     (1 << 6)
#define CAN_INT_ENABLE_FORM_ERR    (1 << 7)
#define CAN_INT_ENABLE_CRC_ERR     (1 << 8)
#define CAN_INT_ENABLE_BUS_OFF     (1 << 9)
#define CAN_INT_ENABLE_RXMSG_LOSS  (1 << 10)
#define CAN_INT_ENABLE_TXMSG       (1 << 11)
#define CAN_INT_ENABLE_RXMSG       (1 << 12)
#define CAN_INT_ENABLE_RTRMSG      (1 << 13)
#define CAN_INT_ENABLE_STUCK_AT0   (1 << 14)
#define CAN_INT_ENABLE_SST_FAILURE (1 << 15)

/* CAN_INT_STATUS */
#define CAN_INT_STATUS_ARB_LOSS    (1 << 2)
#define CAN_INT_STATUS_OVR_LOAD    (1 << 3)
#define CAN_INT_STATUS_BIT_ERR     (1 << 4)
#define CAN_INT_STATUS_STUFF_ERR   (1 << 5)
#define CAN_INT_STATUS_ACK_ERR     (1 << 6)
#define CAN_INT_STATUS_FORM_ERR    (1 << 7)
#define CAN_INT_STATUS_CRC_ERR     (1 << 8)
#define CAN_INT_STATUS_BUS_OFF     (1 << 9)
#define CAN_INT_STATUS_RXMSG_LOSS  (1 << 10)
#define CAN_INT_STATUS_TXMSG       (1 << 11)
#define CAN_INT_STATUS_RXMSG       (1 << 12)
#define CAN_INT_STATUS_RTRMSG      (1 << 13)
#define CAN_INT_STATUS_STUCK_AT0   (1 << 14)
#define CAN_INT_STATUS_SST_FAILURE (1 << 15)


/* Register Bitfield Definitions **********************************************/



#endif /* __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CAN_H */
