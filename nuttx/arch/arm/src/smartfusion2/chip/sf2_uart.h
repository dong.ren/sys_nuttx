
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_UART_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_UART_H


#include <nuttx/config.h>
#include "chip.h"

/* Register Offsets ***********************************************************/

#define SF2_UART_RBR_OFFSET   0x0000
#define SF2_UART_THR_OFFSET   0x0000
#define SF2_UART_DLR_OFFSET   0x0000
#define SF2_UART_DMR_OFFSET   0x0004
#define SF2_UART_DFR_OFFSET   0x003c
#define SF2_UART_IER_OFFSET   0x0004
#define SF2_UART_IEM_OFFSET   0x0024
#define SF2_UART_IIR_OFFSET   0x0008
#define SF2_UART_IIM_OFFSET   0x0028
#define SF2_UART_FCR_OFFSET   0x0008
#define SF2_UART_LCR_OFFSET   0x000c
#define SF2_UART_MCR_OFFSET   0x0010
#define SF2_UART_LSR_OFFSET   0x0014
#define SF2_UART_MSR_OFFSET   0x0018
#define SF2_UART_SR_OFFSET    0x001c
#define SF2_UART_MM0_OFFSET   0x0030
#define SF2_UART_MM1_OFFSET   0x0034
#define SF2_UART_MM2_OFFSET   0x0038
#define SF2_UART_GFR_OFFSET   0x0044
#define SF2_UART_TTG_OFFSET   0x0048
#define SF2_UART_RTO_OFFSET   0x004c
#define SF2_UART_ADR_OFFSET   0x0050

/* Register Addresses *********************************************************/

#define SF2_UART0_RBR  (SF2_UART0_BASE + SF2_UART_RBR_OFFSET)
#define SF2_UART0_THR  (SF2_UART0_BASE + SF2_UART_THR_OFFSET)
#define SF2_UART0_DLR  (SF2_UART0_BASE + SF2_UART_DLR_OFFSET)
#define SF2_UART0_DMR  (SF2_UART0_BASE + SF2_UART_DMR_OFFSET)
#define SF2_UART0_DFR  (SF2_UART0_BASE + SF2_UART_DFR_OFFSET)
#define SF2_UART0_IER  (SF2_UART0_BASE + SF2_UART_IER_OFFSET)
#define SF2_UART0_IEM  (SF2_UART0_BASE + SF2_UART_IEM_OFFSET)
#define SF2_UART0_IIR  (SF2_UART0_BASE + SF2_UART_IIR_OFFSET)
#define SF2_UART0_IIM  (SF2_UART0_BASE + SF2_UART_IIM_OFFSET)
#define SF2_UART0_FCR  (SF2_UART0_BASE + SF2_UART_FCR_OFFSET)
#define SF2_UART0_LCR  (SF2_UART0_BASE + SF2_UART_LCR_OFFSET)
#define SF2_UART0_MCR  (SF2_UART0_BASE + SF2_UART_MCR_OFFSET)
#define SF2_UART0_LSR  (SF2_UART0_BASE + SF2_UART_LSR_OFFSET)
#define SF2_UART0_MSR  (SF2_UART0_BASE + SF2_UART_MSR_OFFSET)
#define SF2_UART0_SR   (SF2_UART0_BASE + SF2_UART_SR_OFFSET)
#define SF2_UART0_MM0  (SF2_UART0_BASE + SF2_UART_MM0_OFFSET)
#define SF2_UART0_MM1  (SF2_UART0_BASE + SF2_UART_MM1_OFFSET)
#define SF2_UART0_MM2  (SF2_UART0_BASE + SF2_UART_MM2_OFFSET)
#define SF2_UART0_GFR  (SF2_UART0_BASE + SF2_UART_GFR_OFFSET)
#define SF2_UART0_TTG  (SF2_UART0_BASE + SF2_UART_TTG_OFFSET)
#define SF2_UART0_RTO  (SF2_UART0_BASE + SF2_UART_RTO_OFFSET)
#define SF2_UART0_ADR  (SF2_UART0_BASE + SF2_UART_ADR_OFFSET)

#define SF2_UART1_RBR  (SF2_UART1_BASE + SF2_UART_RBR_OFFSET)
#define SF2_UART1_THR  (SF2_UART1_BASE + SF2_UART_THR_OFFSET)
#define SF2_UART1_DLR  (SF2_UART1_BASE + SF2_UART_DLR_OFFSET)
#define SF2_UART1_DMR  (SF2_UART1_BASE + SF2_UART_DMR_OFFSET)
#define SF2_UART1_DFR  (SF2_UART1_BASE + SF2_UART_DFR_OFFSET)
#define SF2_UART1_IER  (SF2_UART1_BASE + SF2_UART_IER_OFFSET)
#define SF2_UART1_IEM  (SF2_UART1_BASE + SF2_UART_IEM_OFFSET)
#define SF2_UART1_IIR  (SF2_UART1_BASE + SF2_UART_IIR_OFFSET)
#define SF2_UART1_IIM  (SF2_UART1_BASE + SF2_UART_IIM_OFFSET)
#define SF2_UART1_FCR  (SF2_UART1_BASE + SF2_UART_FCR_OFFSET)
#define SF2_UART1_LCR  (SF2_UART1_BASE + SF2_UART_LCR_OFFSET)
#define SF2_UART1_MCR  (SF2_UART1_BASE + SF2_UART_MCR_OFFSET)
#define SF2_UART1_LSR  (SF2_UART1_BASE + SF2_UART_LSR_OFFSET)
#define SF2_UART1_MSR  (SF2_UART1_BASE + SF2_UART_MSR_OFFSET)
#define SF2_UART1_SR   (SF2_UART1_BASE + SF2_UART_SR_OFFSET)
#define SF2_UART1_MM0  (SF2_UART1_BASE + SF2_UART_MM0_OFFSET)
#define SF2_UART1_MM1  (SF2_UART1_BASE + SF2_UART_MM1_OFFSET)
#define SF2_UART1_MM2  (SF2_UART1_BASE + SF2_UART_MM2_OFFSET)
#define SF2_UART1_GFR  (SF2_UART1_BASE + SF2_UART_GFR_OFFSET)
#define SF2_UART1_TTG  (SF2_UART1_BASE + SF2_UART_TTG_OFFSET)
#define SF2_UART1_RTO  (SF2_UART1_BASE + SF2_UART_RTO_OFFSET)
#define SF2_UART1_ADR  (SF2_UART1_BASE + SF2_UART_ADR_OFFSET)

/* Register Bitfield Definitions **********************************************/

/* FIFO Control Register (FCR) */

#define UART_FCR_ENABLE_TX_RX_FIFO  (1 << 0)
#define UART_FCR_CLEAR_RX_FIFO      (1 << 1)
#define UART_FCR_CLEAR_TX_FIFO      (1 << 2)
#define UART_FCR_ENABLE_TXRDY_RXRDY (1 << 3)
#define UART_FCR_RX_TRIG_SHIFT      (6)
#define UART_FCR_RX_TRIG_MASK       (3 << UART_FCR_RX_TRIG_SHIFT)
#  define UART_FCR_RX_TRIG_1        (0 << UART_FCR_RX_TRIG_SHIFT)
#  define UART_FCR_RX_TRIG_4        (1 << UART_FCR_RX_TRIG_SHIFT)
#  define UART_FCR_RX_TRIG_8        (2 << UART_FCR_RX_TRIG_SHIFT)
#  define UART_FCR_RX_TRIG_14       (3 << UART_FCR_RX_TRIG_SHIFT)

/* Interrupt Enable Register (IER) */

#define UART_IER_ERBFI (1 << 0)
#define UART_IER_ETBEI (1 << 1)
#define UART_IER_ELSI  (1 << 2)
#define UART_IER_EDSSI (1 << 3)

/* Multi-Mode Interrupt Enable Register (IEM) */

#define UART_IEM_ERTOI    (1 << 0)
#define UART_IEM_ENACKI   (1 << 1)
#define UART_IEM_EPID_PEI (1 << 2)
#define UART_IEM_ELINBI   (1 << 3)
#define UART_IEM_ELINSI   (1 << 4)

/* Interrupt Identification Register (IIR) */

#define UART_IIR_MASK             (0x0f)
#  define UART_IIR_MODEM_STATUS   (0x00)
#  define UART_IIR_THRE           (0x02)
#  define UART_IIR_MMI            (0x03)
#  define UART_IIR_RX_DATA        (0x04)
#  define UART_IIR_RX_LINE_STATUS (0x06)
#  define UART_IIR_DATA_TIMEOUT   (0x0c)

/* Multi-Mode Interrupt Identification Register (IIM) */

#define UART_IIM_RTOII   (1 << 0)
#define UART_IIM_NACKI   (1 << 1)
#define UART_IIM_PID_PEI (1 << 2)
#define UART_IIM_LINBI   (1 << 3)
#define UART_IIM_LINSI   (1 << 4)

/* Line Control Register (LCR) */

#define UART_LCR_WLS_MASK (3)
#  define UART_LCR_WLS_5  (0)
#  define UART_LCR_WLS_6  (1)
#  define UART_LCR_WLS_7  (2)
#  define UART_LCR_WLS_8  (3)

#define UART_LCR_STB       (1 << 2)
#  define UART_LCR_STB_1   (0 << 2)
#  define UART_LCR_STB_1p5 (1 << 2)
#  define UART_LCR_STB_2   (1 << 2)

#define UART_LCR_PEN       (1 << 3)

#define UART_LCR_EPS        (1 << 4)
#  define UART_LCR_EPS_ODD  (0 << 4)
#  define UART_LCR_EPS_EVEN (1 << 4)

#define UART_LCR_SP     (1 << 5)
#define UART_LCR_SB     (1 << 6)
#define UART_LCR_DLAB   (1 << 7)

/* Modem Control Registar (MCR) */

#define UART_MCR_DTR    (1 << 0)
#define UART_MCR_RTS    (1 << 1)
#define UART_MCR_OUT1   (1 << 2)
#define UART_MCR_OUT2   (1 << 3)

#define UART_MCR_LOOP   (1 << 4)

#define UART_MCR_RLOOP_SHIFT     (5)
#define UART_MCR_RLOOP_MASK      (3 << UART_MCR_RLOOP_SHIFT)
#  define UART_MCR_RLOOP_DISABLE (0 << UART_MCR_RLOOP_SHIFT)
#  define UART_MCR_RLOOP_REMOTE  (1 << UART_MCR_RLOOP_SHIFT)
#  define UART_MCR_RLOOP_ECHO    (2 << UART_MCR_RLOOP_SHIFT)

/* Line Status Register (LSR) */

#define UART_LSR_DR   (1 << 0)
#define UART_LSR_OE   (1 << 1)
#define UART_LSR_PE   (1 << 2)
#define UART_LSR_FE   (1 << 3)
#define UART_LSR_BI   (1 << 4)
#define UART_LSR_THRE (1 << 5)
#define UART_LSR_TEMT (1 << 6)
#define UART_LSR_FIER (1 << 7)

/* Modem Status Register (MSR) */

#define UART_MSR_DCTS (1 << 0)
#define UART_MSR_DDSR (1 << 1)
#define UART_MSR_TERI (1 << 2)
#define UART_MSR_DDCD (1 << 3)
#define UART_MSR_CTS  (1 << 4)
#define UART_MSR_DSR  (1 << 5)
#define UART_MSR_RI   (1 << 6)
#define UART_MSR_DCR  (1 << 7)

/* Multi-Mode Control Register 0 (MM0) */

#define UART_MM0_ESYN_MASK   (0x7)
#  define UART_MM0_ESYN_ASY  (0x0)
#  define UART_MM0_ESYN_SSP  (0x1)
#  define UART_MM0_ESYN_SSN  (0x2)
#  define UART_MM0_ESYN_SMP  (0x3)
#  define UART_MM0_ESYN_SMN  (0x4)

#define UART_MM0_ELIN (1 << 3)
#define UART_MM0_ETTG (1 << 5)
#define UART_MM0_ERTO (1 << 6)
#define UART_MM0_EFBR (1 << 7)

/* Multi-Mode Control Register 1 (MM1) */

#define UART_MM1_E_MSB_RX (1 << 0)
#define UART_MM1_E_MSB_TX (1 << 1)
#define UART_MM1_EIRD     (1 << 2)
#define UART_MM1_EIRX     (1 << 3)
#define UART_MM1_EITX     (1 << 4)
#define UART_MM1_EITP     (1 << 5)

/* Multi-Mode Control Register 2 (MM2) */

#define UART_MM2_EERR     (1 << 0)
#define UART_MM2_EAFM     (1 << 1)
#define UART_MM2_EAFC     (1 << 2)
#define UART_MM2_ESWM     (1 << 3)

/* Glitch Filter Register (GFR) */

#define UART_GFR_GLR_MASK (0x7)




#endif /* __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_UART_H */
