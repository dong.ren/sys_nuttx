
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_ETH_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_ETH_H

#include <nuttx/config.h>
#include "chip.h"

/* Register Offsets ***********************************************************/

/* M-AHB Registers*/

#if SF2_NETHERNET > 0

#define SF2_ETH_DMA_TX_CTRL_OFFSET      0x0180 /* transmit control register */
#define SF2_ETH_DMA_TX_DESC_OFFSET      0x0184 /* pointer to transmit descriptor */
#define SF2_ETH_DMA_TX_STATUS_OFFSET    0x0188 /* transmit status register */
#define SF2_ETH_DMA_RX_CTRL_OFFSET      0x018c /* receive control register */
#define SF2_ETH_DMA_RX_DESC_OFFSET      0x0190 /* pointer to receive descriptor */
#define SF2_ETH_DMA_RX_STATUS_OFFSET    0x0194 /* receive status register */
#define SF2_ETH_DMA_IRQ_MASK_OFFSET     0x0198 /* interrupt mask register */
#define SF2_ETH_DMA_IRQ_OFFSET          0x019c /* interrupt register */

/* PE-MCXMAC Registers */

#define SF2_ETH_CFG1_OFFSET             0x0000 /* MAC configuration register */
#define SF2_ETH_CFG2_OFFSET             0x0004 /* MAC configuration register */
#define SF2_ETH_IFG_OFFSET              0x0008 /* inter packet gap and inter frame gap register */
#define SF2_ETH_HALF_DUPLEX_OFFSET      0x000c /* definition of half duplex register */
#define SF2_ETH_MAX_FRAME_LENGTH_OFFSET 0x0010 
#define SF2_ETH_TEST_OFFSET             0x001c
#define SF2_ETH_MII_CONFIG_OFFSET       0x0020 
#define SF2_ETH_MII_COMMAND_OFFSET      0x0024
#define SF2_ETH_MII_ADDRESS_OFFSET      0x0028
#define SF2_ETH_MII_CTRL_OFFSET         0x002c
#define SF2_ETH_MII_STATUS_OFFSET       0x0030
#define SF2_ETH_MII_INDICATORS_OFFSET   0x0034
#define SF2_ETH_INTERFACE_CTRL_OFFSET   0x0038
#define SF2_ETH_INTERFACE_STATUS_OFFSET 0x003c
#define SF2_ETH_STATION_ADDRESS1_OFFSET 0x0040
#define SF2_ETH_STATION_ADDRESS2_OFFSET 0x0044

/* A-MCXFIFO */

#define SF2_ETH_FIFO_CFG0_OFFSET        0x0048
#define SF2_ETH_FIFO_CFG1_OFFSET        0x004c
#define SF2_ETH_FIFO_CFG2_OFFSET        0x0050
#define SF2_ETH_FIFO_CFG3_OFFSET        0x0054
#define SF2_ETH_FIFO_CFG4_OFFSET        0x0058
#define SF2_ETH_FIFO_CFG5_OFFSET        0x005c
#define SF2_ETH_FIFO_RAM_ACCESS0_OFFSET 0x0060
#define SF2_ETH_FIFO_RAM_ACCESS1_OFFSET 0x0064
#define SF2_ETH_FIFO_RAM_ACCESS2_OFFSET 0x0068
#define SF2_ETH_FIFO_RAM_ACCESS3_OFFSET 0x006c
#define SF2_ETH_FIFO_RAM_ACCESS4_OFFSET 0x0070
#define SF2_ETH_FIFO_RAM_ACCESS5_OFFSET 0x0074
#define SF2_ETH_FIFO_RAM_ACCESS6_OFFSET 0x0078
#define SF2_ETH_FIFO_RAM_ACCESS7_OFFSET 0x007c

/* PE-MSTAT */

#define SF2_ETH_TR64_OFFSET             0x0080
#define SF2_ETH_TR127_OFFSET            0x0084
#define SF2_ETH_TR255_OFFSET            0x0088
#define SF2_ETH_TR511_OFFSET            0x008c
#define SF2_ETH_TR1K_OFFSET             0x0090
#define SF2_ETH_TRMAX_OFFSET            0x0094
#define SF2_ETH_TRMGV_OFFSET            0x0098
#define SF2_ETH_RBYT_OFFSET             0x009c
#define SF2_ETH_RPKT_OFFSET             0x00a0
#define SF2_ETH_RFCS_OFFSET             0x00a4
#define SF2_ETH_RMCA_OFFSET             0x00a8
#define SF2_ETH_RBCA_OFFSET             0x00ac
#define SF2_ETH_RXCF_OFFSET             0x00b0
#define SF2_ETH_RXPF_OFFSET             0x00b4
#define SF2_ETH_RXUO_OFFSET             0x00b8
#define SF2_ETH_RALN_OFFSET             0x00bc
#define SF2_ETH_RFLR_OFFSET             0x00c0
#define SF2_ETH_RCDE_OFFSET             0x00c4
#define SF2_ETH_RCSE_OFFSET             0x00c8
#define SF2_ETH_RUND_OFFSET             0x00cc
#define SF2_ETH_ROVR_OFFSET             0x00d0
#define SF2_ETH_RFRG_OFFSET             0x00d4
#define SF2_ETH_RJBR_OFFSET             0x00d8
#define SF2_ETH_RDRP_OFFSET             0x00dc
#define SF2_ETH_TBYT_OFFSET             0x00e0
#define SF2_ETH_TPKT_OFFSET             0x00e4
#define SF2_ETH_TMCA_OFFSET             0x00e8
#define SF2_ETH_TBCA_OFFSET             0x00ec
#define SF2_ETH_TXPF_OFFSET             0x00f0
#define SF2_ETH_TDFR_OFFSET             0x00f4
#define SF2_ETH_TEDF_OFFSET             0x00f8
#define SF2_ETH_TSCL_OFFSET             0x00fc
#define SF2_ETH_TMCL_OFFSET             0x0100
#define SF2_ETH_TLCL_OFFSET             0x0104
#define SF2_ETH_TXCL_OFFSET             0x0108
#define SF2_ETH_TNCL_OFFSET             0x010c
#define SF2_ETH_TPFH_OFFSET             0x0110
#define SF2_ETH_TDRP_OFFSET             0x0114
#define SF2_ETH_TJBR_OFFSET             0x0118
#define SF2_ETH_TFCS_OFFSET             0x011c
#define SF2_ETH_TXCF_OFFSET             0x0120
#define SF2_ETH_TOVR_OFFSET             0x0124
#define SF2_ETH_TUND_OFFSET             0x0128
#define SF2_ETH_TFRG_OFFSET             0x012c
#define SF2_ETH_CAR1_OFFSET             0x0130
#define SF2_ETH_CAR2_OFFSET             0x0134
#define SF2_ETH_CAM1_OFFSET             0x0138
#define SF2_ETH_CAM2_OFFSET             0x013c

/* Register Base Address ******************************************************/

/* M-AHB Registers */

#define SF2_ETH_DMA_TX_CTRL             (SF2_ETHERNET_BASE + SF2_ETH_DMA_TX_CTRL_OFFSET)
#define SF2_ETH_DMA_TX_DESC             (SF2_ETHERNET_BASE + SF2_ETH_DMA_TX_DESC_OFFSET)
#define SF2_ETH_DMA_TX_STATUS           (SF2_ETHERNET_BASE + SF2_ETH_DMA_TX_STATUS_OFFSET)
#define SF2_ETH_DMA_RX_CTRL             (SF2_ETHERNET_BASE + SF2_ETH_DMA_RX_CTRL_OFFSET)
#define SF2_ETH_DMA_RX_DESC             (SF2_ETHERNET_BASE + SF2_ETH_DMA_RX_DESC_OFFSET)
#define SF2_ETH_DMA_RX_STATUS           (SF2_ETHERNET_BASE + SF2_ETH_DMA_RX_STATUS_OFFSET)
#define SF2_ETH_DMA_IRQ_MASK            (SF2_ETHERNET_BASE + SF2_ETH_DMA_IRQ_MASK_OFFSET)
#define SF2_ETH_DMA_IRQ                 (SF2_ETHERNET_BASE + SF2_ETH_DMA_IRQ_OFFSET)

/* PE-MCXMAC */

#define SF2_ETH_CFG1                    (SF2_ETHERNET_BASE + SF2_ETH_CFG1_OFFSET)
#define SF2_ETH_CFG2                    (SF2_ETHERNET_BASE + SF2_ETH_CFG2_OFFSET)
#define SF2_ETH_IFG                     (SF2_ETHERNET_BASE + SF2_ETH_IFG_OFFSET)
#define SF2_ETH_HALF_DUPLEX             (SF2_ETHERNET_BASE + SF2_ETH_HALF_DUPLEX_OFFSET)
#define SF2_ETH_MAX_FRAME_LENGTH        (SF2_ETHERNET_BASE + SF2_ETH_MAX_FRAME_LENGTH_OFFSET)
#define SF2_ETH_TEST                    (SF2_ETHERNET_BASE + SF2_ETH_TEST_OFFSET)
#define SF2_ETH_MII_CONFIG              (SF2_ETHERNET_BASE + SF2_ETH_MII_CONFIG_OFFSET)
#define SF2_ETH_MII_COMMAND             (SF2_ETHERNET_BASE + SF2_ETH_MII_COMMAND_OFFSET)
#define SF2_ETH_MII_ADDRESS             (SF2_ETHERNET_BASE + SF2_ETH_MII_ADDRESS_OFFSET)
#define SF2_ETH_MII_CTRL                (SF2_ETHERNET_BASE + SF2_ETH_MII_CTRL_OFFSET)
#define SF2_ETH_MII_STATUS              (SF2_ETHERNET_BASE + SF2_ETH_MII_STATUS_OFFSET)
#define SF2_ETH_MII_INDICATORS          (SF2_ETHERNET_BASE + SF2_ETH_MII_INDICATORS_OFFSET)
#define SF2_ETH_INTERFACE_CTRL          (SF2_ETHERNET_BASE + SF2_ETH_INTERFACE_CTRL_OFFSET)
#define SF2_ETH_INTERFACE_STATUS        (SF2_ETHERNET_BASE + SF2_ETH_INTERFACE_STATUS_OFFSET)
#define SF2_ETH_STATION_ADDRESS1        (SF2_ETHERNET_BASE + SF2_ETH_STATION_ADDRESS1_OFFSET)
#define SF2_ETH_STATION_ADDRESS2        (SF2_ETHERNET_BASE + SF2_ETH_STATION_ADDRESS2_OFFSET)

/* PE-MCXFIFO */

#define SF2_ETH_FIFO_CFG0               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG0_OFFSET)
#define SF2_ETH_FIFO_CFG1               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG1_OFFSET)
#define SF2_ETH_FIFO_CFG2               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG2_OFFSET)
#define SF2_ETH_FIFO_CFG3               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG3_OFFSET)
#define SF2_ETH_FIFO_CFG4               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG4_OFFSET)
#define SF2_ETH_FIFO_CFG5               (SF2_ETHERNET_BASE + SF2_ETH_FIFO_CFG5_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS0        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS0_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS1        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS1_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS2        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS2_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS3        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS3_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS4        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS4_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS5        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS5_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS6        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS6_OFFSET)
#define SF2_ETH_FIFO_RAM_ACCESS7        (SF2_ETHERNET_BASE + SF2_ETH_FIFO_RAM_ACCESS7_OFFSET)

/* PE-MSTAT */

#define SF2_ETH_TR64                    (SF2_ETHERNET_BASE + SF2_ETH_TR64_OFFSET)
#define SF2_ETH_TR127                   (SF2_ETHERNET_BASE + SF2_ETH_TR127_OFFSET)
#define SF2_ETH_TR255                   (SF2_ETHERNET_BASE + SF2_ETH_TR255_OFFSET)
#define SF2_ETH_TR511                   (SF2_ETHERNET_BASE + SF2_ETH_TR511_OFFSET)
#define SF2_ETH_TR1K                    (SF2_ETHERNET_BASE + SF2_ETH_TR1K_OFFSET)
#define SF2_ETH_TRMAX                   (SF2_ETHERNET_BASE + SF2_ETH_TRMAX_OFFSET)
#define SF2_ETH_TRMGV                   (SF2_ETHERNET_BASE + SF2_ETH_TRMGV_OFFSET)
#define SF2_ETH_RBYT                    (SF2_ETHERNET_BASE + SF2_ETH_RBYT_OFFSET)
#define SF2_ETH_RPKT                    (SF2_ETHERNET_BASE + SF2_ETH_RPKT_OFFSET)
#define SF2_ETH_RFCS                    (SF2_ETHERNET_BASE + SF2_ETH_RFCS_OFFSET)
#define SF2_ETH_RMCA                    (SF2_ETHERNET_BASE + SF2_ETH_RMCA_OFFSET)
#define SF2_ETH_RBCA                    (SF2_ETHERNET_BASE + SF2_ETH_RBCA_OFFSET)
#define SF2_ETH_RXCF                    (SF2_ETHERNET_BASE + SF2_ETH_RXCF_OFFSET)
#define SF2_ETH_RXPF                    (SF2_ETHERNET_BASE + SF2_ETH_RXPF_OFFSET)
#define SF2_ETH_RXUO                    (SF2_ETHERNET_BASE + SF2_ETH_RXUO_OFFSET)
#define SF2_ETH_RALN                    (SF2_ETHERNET_BASE + SF2_ETH_RALN_OFFSET)
#define SF2_ETH_RFLR                    (SF2_ETHERNET_BASE + SF2_ETH_RFLR_OFFSET)
#define SF2_ETH_RCDE                    (SF2_ETHERNET_BASE + SF2_ETH_RCDE_OFFSET)
#define SF2_ETH_RCSE                    (SF2_ETHERNET_BASE + SF2_ETH_RCSE_OFFSET)
#define SF2_ETH_RUND                    (SF2_ETHERNET_BASE + SF2_ETH_RUND_OFFSET)
#define SF2_ETH_ROVR                    (SF2_ETHERNET_BASE + SF2_ETH_ROVR_OFFSET)
#define SF2_ETH_RFRG                    (SF2_ETHERNET_BASE + SF2_ETH_RFRG_OFFSET)
#define SF2_ETH_RJBR                    (SF2_ETHERNET_BASE + SF2_ETH_RJBR_OFFSET)
#define SF2_ETH_RDRP                    (SF2_ETHERNET_BASE + SF2_ETH_RDRP_OFFSET)
#define SF2_ETH_TBYT                    (SF2_ETHERNET_BASE + SF2_ETH_TBYT_OFFSET)
#define SF2_ETH_TPKT                    (SF2_ETHERNET_BASE + SF2_ETH_TPKT_OFFSET)
#define SF2_ETH_TMCA                    (SF2_ETHERNET_BASE + SF2_ETH_TMCA_OFFSET)
#define SF2_ETH_TBCA                    (SF2_ETHERNET_BASE + SF2_ETH_TBCA_OFFSET)
#define SF2_ETH_TXPF                    (SF2_ETHERNET_BASE + SF2_ETH_TXPF_OFFSET)
#define SF2_ETH_TDFR                    (SF2_ETHERNET_BASE + SF2_ETH_TDFR_OFFSET)
#define SF2_ETH_TEDF                    (SF2_ETHERNET_BASE + SF2_ETH_TEDF_OFFSET)
#define SF2_ETH_TSCL                    (SF2_ETHERNET_BASE + SF2_ETH_TSCL_OFFSET)
#define SF2_ETH_TMCL                    (SF2_ETHERNET_BASE + SF2_ETH_TMCL_OFFSET)
#define SF2_ETH_TLCL                    (SF2_ETHERNET_BASE + SF2_ETH_TLCL_OFFSET)
#define SF2_ETH_TXCL                    (SF2_ETHERNET_BASE + SF2_ETH_TXCL_OFFSET)
#define SF2_ETH_TNCL                    (SF2_ETHERNET_BASE + SF2_ETH_TNCL_OFFSET)
#define SF2_ETH_TPFH                    (SF2_ETHERNET_BASE + SF2_ETH_TPFH_OFFSET)
#define SF2_ETH_TDRP                    (SF2_ETHERNET_BASE + SF2_ETH_TDRP_OFFSET)
#define SF2_ETH_TJBR                    (SF2_ETHERNET_BASE + SF2_ETH_TJBR_OFFSET)
#define SF2_ETH_TFCS                    (SF2_ETHERNET_BASE + SF2_ETH_TFCS_OFFSET)
#define SF2_ETH_TXCF                    (SF2_ETHERNET_BASE + SF2_ETH_TXCF_OFFSET)
#define SF2_ETH_TOVR                    (SF2_ETHERNET_BASE + SF2_ETH_TOVR_OFFSET)
#define SF2_ETH_TUND                    (SF2_ETHERNET_BASE + SF2_ETH_TUND_OFFSET)
#define SF2_ETH_TFRG                    (SF2_ETHERNET_BASE + SF2_ETH_TFRG_OFFSET)
#define SF2_ETH_CAR1                    (SF2_ETHERNET_BASE + SF2_ETH_CAR1_OFFSET)
#define SF2_ETH_CAR2                    (SF2_ETHERNET_BASE + SF2_ETH_CAR2_OFFSET)
#define SF2_ETH_CAM1                    (SF2_ETHERNET_BASE + SF2_ETH_CAM1_OFFSET)
#define SF2_ETH_CAM2                    (SF2_ETHERNET_BASE + SF2_ETH_CAM2_OFFSET)

/* Register Bit-Field Definitions *********************************************/

/* DMA_TX_CTRL */

#define ETH_DMA_TX_CTRL_TXENABLE        (1 << 0)

/* DMA_TX_STATUS */

#define ETH_DMA_TX_STATUS_TXPKTSENT     (1 << 0)
#define ETH_DMA_TX_STATUS_TXUNDERRUN    (1 << 1)
#define ETH_DMA_TX_STATUS_BUSERROR      (1 << 3)
#define ETH_DMA_TX_STATUS_TXPKTCOUNT_SHIFT  (16)
#define ETH_DMA_TX_STATUS_TXPKTCOUNT_MASK   (255 << ETH_DMA_TX_STATUS_TXPKTCOUNT_SHIFT)

/* DMA_RX_CTRL */

#define ETH_DMA_RX_CTRL_RXENABLE        (1 << 0)

/* DMA_RX_STATUS */

#define ETH_DMA_RX_STATUS_RXPKTRECEIVED (1 << 0)
#define ETH_DMA_RX_STATUS_RXOVERFLOW    (1 << 1)
#define ETH_DMA_RX_STATUS_BUSERROR      (1 << 3)
#define ETH_DMA_RX_STATUS_RXPKTCOUNT_SHIFT  (16)
#define ETH_DMA_RX_STATUS_RXPKTCOUNT_MASK   (255 << ETH_DMA_RX_STATUS_RXPKTCOUNT_SHIFT)

/* DMA_IRQ_MASK */

#define ETH_DMA_IRQ_MASK_TXPKTSENT      (1 << 0)
#define ETH_DMA_IRQ_MASK_TXUNDERRUN     (1 << 1)
#define ETH_DMA_IRQ_MASK_TXBUSERROR     (1 << 3)
#define ETH_DMA_IRQ_MASK_RXPKTRECEIVED  (1 << 4)
#define ETH_DMA_IRQ_MASK_RXOVERFLOW     (1 << 6)
#define ETH_DMA_IRQ_MASK_RXBUSERROR     (1 << 7)

/* DMA_IRQ */

#define ETH_DMA_IRQ_TXPKTSENT           (1 << 0)
#define ETH_DMA_IRQ_TXUNDERRUN          (1 << 1)
#define ETH_DMA_IRQ_TXBUSERROR          (1 << 3)
#define ETH_DMA_IRQ_RXPKTRECEIVED       (1 << 4)
#define ETH_DMA_IRQ_RXOVERFLOW          (1 << 6)
#define ETH_DMA_IRQ_RXBUSERROR          (1 << 7)

/* CFG1 */

#define ETH_CFG1_TX_EN                   (1 << 0)
#define ETH_CFG1_TXSYNC_EN               (1 << 1)
#define ETH_CFG1_RX_EN                   (1 << 2)
#define ETH_CFG1_RXSYNC_EN               (1 << 3)
#define ETH_CFG1_TXFLOWCTRL_EN           (1 << 4)
#define ETH_CFG1_RXFLOWCTRL_EN           (1 << 5)
#define ETH_CFG1_LOOPBACK                (1 << 8)
#define ETH_CFG1_TXFUNC_RESET            (1 << 16)
#define ETH_CFG1_RXFUNC_RESET            (1 << 17)
#define ETH_CFG1_TXMACCTRL_RESET         (1 << 18)
#define ETH_CFG1_RXMACCTRL_RESET         (1 << 19)
#define ETH_CFG1_SIMULA_RESET            (1 << 30)
#define ETH_CFG1_SOFT_RESET              (1 << 31)

/* CFG2 */

#define ETH_CFG2_FULL_DUPLEX             (1 << 0)
#define ETH_CFG2_CRC_ENABLE              (1 << 1)
#define ETH_CFG2_PADCRC_ENABLE           (1 << 2)
#define ETH_CFG2_LENGTH_CHECKING         (1 << 3)
#define ETH_CFG2_HUGE_FRAME_EN           (1 << 4)
#define ETH_CFG2_INT_MODE_SHIFT          (8)
#define ETH_CFG2_INT_MODE_MASK           (3 << ETH_CFG2_INT_MODE_SHIFT)
#  define ETH_CFG2_INT_MODE_NIBBLE       (1 << ETH_CFG2_INT_MODE_SHIFT)
#  define ETH_CFG2_INT_MODE_BYTE         (2 << ETH_CFG2_INT_MODE_SHIFT)
#define ETH_CFG2_PREAMBLE_LENGTH_SHIFT   (12)
#define ETH_CFG2_PREAMBLE_LENGTH_MASK    (15 << ETH_CFG2_PREAMBLE_LENGTH_SHIFT)

/* IFG */

#define ETH_IFG_BTBIRG_SHIFT             (0)
#define ETH_IFG_BTBIRG_MASK              (0x7f)
#define ETH_IFG_MIE_SHIFT                (8)
#define ETH_IFG_MIE_MASK                 (0xff << ETH_IFG_MIE_SHIFT)
#define ETH_IFG_IPGR2_SHIFT              (16)
#define ETH_IFG_IPGR2_MASK               (0x7f << ETH_IFG_IPGR2_SHIFT)
#define ETH_IFG_IPGR1_SHIFT              (24)
#define ETH_IFG_IPGR1_MASK               (0x7f << ETH_IFG_IPGR1_SHIFT)

/* HALF_DUPLEX */

#define ETH_HALF_DUPLEX_CW_SHIFT         (0)
#define ETH_HALF_DUPLEX_CW_MASK          (0x3ff)
#define ETH_HALF_DUPLEX_RM_SHIFT         (12)
#define ETH_HALF_DUPLEX_RM_MASK          (0xf << ETH_HALF_DUPLEX_RM_SHIFT)
#define ETH_HALF_DUPLEX_ED               (1 << 16)
#define ETH_HALF_DUPLEX_NB               (1 << 17)
#define ETH_HALF_DUPLEX_BNB              (1 << 18)
#define ETH_HALF_DUPLEX_ABEBE            (1 << 19)

/* TEST */

#define ETH_TEST_SST                     (1 << 0)
#define ETH_TEST_TP                      (1 << 1)
#define ETH_TEST_RTFE                    (1 << 2)
#define ETH_TEST_MFL                     (1 << 3)

/* MII_CONFIG */

#define ETH_MII_CFG_CLOCK_SELECT_SHIFT   (0)
#define ETH_MII_CFG_CLOCK_SELECT_MASK    (7)
#define ETH_MII_CFG_PS                   (1 << 4)
#define ETH_MII_CFG_SAI                  (1 << 5)
#define ETH_MII_CFG_RESET_MII            (1 << 31)

/* MII_COMMAND */

#define ETH_MII_CMD_READ_CYCLE           (1 << 0)
#define ETH_MII_CMD_SCAN_CYCLE           (1 << 1)

/* MII_ADDRESS */

#define ETH_MII_ADDR_REGADDR_SHIFT       (0)
#define ETH_MII_ADDR_REGADDR_MASK        (0x1f)
#define ETH_MII_ADDR_PHYADDR_SHIFT       (8)
#define ETH_MII_ADDR_PHYADDR_MASK        (0x1f << ETH_MII_ADDR_PHYADDR_SHIFT)

/* MII_INDICATORS */

#define ETH_MII_IND_BUSY                 (1 << 0)
#define ETH_MII_IND_SCANNING             (1 << 1)
#define ETH_MII_IND_NOT_VALID            (1 << 2)

/* INTERFACE_CTRL */

#define ETH_INT_CTRL_EN_JABBER_PRO       (1 << 0)
#define ETH_INT_CTRL_DIS_LINK_FAIL       (1 << 8)
#define ETH_INT_CTRL_NO_CIPHER           (1 << 9)
#define ETH_INT_CTRL_FORCE_QUIET         (1 << 10)
#define ETH_INT_CTRL_RESET_PE100X        (1 << 15)
#define ETH_INT_CTRL_SPEED               (1 << 16)
#define ETH_INT_CTRL_RESET_PREMII        (1 << 23)
#define ETH_INT_CTRL_PHYMODE             (1 << 24)
#define ETH_INT_CTRL_LHDMODE             (1 << 25)
#define ETH_INT_CTRL_GHDMODE             (1 << 26)
#define ETH_INT_CTRL_TBIMODE             (1 << 27)
#define ETH_INT_CTRL_RESET_INT           (1 << 31)

/* INTERFACE_STATUS */

#define ETH_INT_STATUS_LINK_FAIL         (1 << 3)
#define ETH_INT_STATUS_SPEED             (1 << 4)
#define ETH_INT_STATUS_FULL_DUPLEX       (1 << 5)
#define ETH_INT_STATUS_LINK_OK           (1 << 6)
#define ETH_INT_STATUS_JABBER            (1 << 7)
#define ETH_INT_STATUS_CLASH             (1 << 8)
#define ETH_INT_STATUS_EXCESS_DEFER      (1 << 9)

/* FIFO_CFG0 */

#define ETH_FIFO_CFG0_HSTRSTWT           (1 << 0)
#define ETH_FIFO_CFG0_HSTRSTSR           (1 << 1)
#define ETH_FIFO_CFG0_HSTRSTFR           (1 << 2)
#define ETH_FIFO_CFG0_HSTRSTST           (1 << 3)
#define ETH_FIFO_CFG0_HSTRSTFT           (1 << 4)
#define ETH_FIFO_CFG0_WTMENREQ           (1 << 8)
#define ETH_FIFO_CFG0_SRFENREQ           (1 << 9)
#define ETH_FIFO_CFG0_FRFENREQ           (1 << 10)
#define ETH_FIFO_CFG0_STFENREQ           (1 << 11)
#define ETH_FIFO_CFG0_FTFENREQ           (1 << 12)
#define ETH_FIFO_CFG0_WTMENRPLY          (1 << 16)
#define ETH_FIFO_CFG0_SRFENRPLY          (1 << 17)
#define ETH_FIFO_CFG0_FRFENRPLY          (1 << 18)
#define ETH_FIFO_CFG0_STFENRPLY          (1 << 19)
#define ETH_FIFO_CFG0_FTFENRPLY          (1 << 20) 


/* FIFO_CFG1 */

#define ETH_FIFO_CFG1_CFGXOFFRTX_SHIFT   (0)
#define ETH_FIFO_CFG1_CFGXOFFRTX_MASK    (0xffff)
#define ETH_FIFO_CFG1_CFGSRTH_SHIFT      (16)
#define ETH_FIFO_CFG1_CFGSRTH_MASK       (0xfff << ETH_FIFO_CFG1_CFGSRTH_SHIFT)

/* FIFO_CFG2 */

#define ETH_FIFO_CFG2_CFGLWM_SHIFT       (0)
#define ETH_FIFO_CFG2_CFGLWM_MASK        (0x1fff)
#define ETH_FIFO_CFG2_CFGHWM_SHIFT       (16)
#define ETH_FIFO_CFG2_CFGHWM_MASK        (0x1fff << ETH_FIFO_CFG2_CFGHWM_SHIFT)

/* FIFO_CFG3 */

#define ETH_FIFO_CFG3_CFGFTTH_SHIFT      (0)
#define ETH_FIFO_CFG3_CFGFTTH_MASK       (0xfff)
#define ETH_FIFO_CFG3_CFGHWMFT_SHIFT     (16)
#define ETH_FIFO_CFG3_CFGHWMFT_MASK      (0xfff << ETH_FIFO_CFG3_CFGHWMFT_SHIFT)

/* FIFO_CFG4 */

/* FIFO_CFG5 */

#define ETH_FIFO_CFG5_HSTFLTRFRMDC_SHIFT (0)
#define ETH_FIFO_CFG5_HSTFLTRFRMDC_MASK  (0x3ffff << ETH_FIFO_CFG5_HSTFLTRFRMDC_SHIFT)

#define ETH_FIFO_CFG5_HSTDRPLT64         (1 << 18)
#define ETH_FIFO_CFG5_CFGBYTMODE         (1 << 19)
#define ETH_FIFO_CFG5_HSTSRFULLCLR       (1 << 20)
#define ETF_FIFO_CFG5_FRFULL             (1 << 21)
#define ETH_FIFO_CFG5_CFGHDPLX           (1 << 22)

/* FIFO_RAM_ACCESS0 */

#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWADX_SHIFT   (0)
#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWADX_MASK    (0x1fff << ETH_FIFO_RAM_ACCESS0_HSTTRAMWADX_SHIFT)
#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWDAT_SHIFT   (16)
#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWDAT_MASK    (0xff << ETH_FIFO_RAM_ACCESS0_HSTTRAMWDAT_SHIFT)
#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWACK         (1 << 30)
#define ETH_FIFO_RAM_ACCESS0_HSTTRAMWREQ         (1 << 31)


/* FIFO_RAM_ACCESS1 */

/* FIFO_RAM_ACCESS2 */

#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRADX_SHIFT   (0)
#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRADX_MASK    (0x1fff << ETH_FIFO_RAM_ACCESS0_HSTTRAMWADX_SHIFT)
#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRDAT_SHIFT   (16)
#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRDAT_MASK    (0xff << ETH_FIFO_RAM_ACCESS0_HSTTRAMWDAT_SHIFT)
#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRACK         (1 << 30)
#define ETH_FIFO_RAM_ACCESS2_HSTTRAMRREQ         (1 << 31)

/* FIFO_RAM_ACCESS3 */

/* FIFO_RAM_ACCESS4 */

#define ETH_FIFO_RAM_ACCESS4_HSTTRAMWADX_SHIFT   (0)
#define ETH_FIFO_RAM_ACCESS4_HSTTRAMWADX_MASK    (0x3fff)
#define ETH_FIFO_RAM_ACCESS4_HSTRRAMWDAT_SHIFT   (16)
#define ETH_FIFO_RAM_ACCESS4_HSTRRAMWDAT_MASK    (0xff << ETH_FIFO_RAM_ACCESS4_HSTRRAMWDAT_SHIFT)
#define ETH_FIFO_RAM_ACCESS4_HSTRRAMWACK         (1 << 30)
#define ETH_FIFO_RAM_ACCESS4_HSTRRAMWREQ         (1 << 31)

/* FIFO_RAM_ACCESS5 */

/* FIFO_RAM_ACCESS6 */

#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRADX_SHIFT   (0)
#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRADX_MASK    (0x3fff)
#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRDAT_SHIFT   (16)
#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRDAT_MASK    (0xff << ETH_FIFO_RAM_ACCESS6_HSTRRAMRDAT_SHIFT)
#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRACK         (1 << 30)
#define ETH_FIFO_RAM_ACCESS6_HSTRRAMRREQ         (1 << 31)

/* CAR1 */

#define ETH_CAR1_C1RDR                  (1 << 0)
#define ETH_CAR1_C1RJB                  (1 << 1)
#define ETH_CAR1_C1RFR                  (1 << 2)
#define ETH_CAR1_C1ROV                  (1 << 3)
#define ETH_CAR1_C1RUN                  (1 << 4)
#define ETH_CAR1_C1RCS                  (1 << 5)
#define ETH_CAR1_C1RCD                  (1 << 6)
#define ETH_CAR1_C1RFL                  (1 << 7)
#define ETH_CAR1_C1RAL                  (1 << 8)
#define ETH_CAR1_C1RXU                  (1 << 9)
#define ETH_CAR1_C1RXP                  (1 << 10)
#define ETH_CAR1_C1RXC                  (1 << 11)
#define ETH_CAR1_C1RBC                  (1 << 12)
#define ETH_CAR1_C1RMC                  (1 << 13)
#define ETH_CAR1_C1RFC                  (1 << 14)
#define ETH_CAR1_C1RPK                  (1 << 15)
#define ETH_CAR1_C1RBY                  (1 << 16)
#define ETH_CAR1_C1MGV                  (1 << 25)
#define ETH_CAR1_C1MAX                  (1 << 26)
#define ETH_CAR1_C11K                   (1 << 27)
#define ETH_CAR1_C1511                  (1 << 28)
#define ETH_CAR1_C1255                  (1 << 29)
#define ETH_CAR1_C1127                  (1 << 30)
#define ETH_CAR1_C164                   (1 << 31)

/* CAR2 */

#define ETH_CAR2_C2TDP                  (1 << 0)
#define ETH_CAR2_C2TPH                  (1 << 1)
#define ETH_CAR2_C2TNC                  (1 << 2)
#define ETH_CAR2_C2TXC                  (1 << 3)
#define ETH_CAR2_C2TLC                  (1 << 4)
#define ETH_CAR2_C2TMA                  (1 << 5)
#define ETH_CAR2_C2TSC                  (1 << 6)
#define ETH_CAR2_C2TED                  (1 << 7)
#define ETH_CAR2_C2TDF                  (1 << 8)
#define ETH_CAR2_C2TPF                  (1 << 9)
#define ETH_CAR2_C2TBC                  (1 << 10)
#define ETH_CAR2_C2TMC                  (1 << 11)
#define ETH_CAR2_C2TPK                  (1 << 12)
#define ETH_CAR2_C2TBY                  (1 << 13)
#define ETH_CAR2_C2TFG                  (1 << 14)
#define ETH_CAR2_C2TUN                  (1 << 15)
#define ETH_CAR2_C2TOV                  (1 << 16)
#define ETH_CAR2_C2TCF                  (1 << 17)
#define ETH_CAR2_C2TFC                  (1 << 18)
#define ETH_CAR2_C2TJB                  (1 << 19)

/* CAM1 */

#define ETH_CAM1_M1RDR                  (1 << 0)
#define ETH_CAM1_M1RJB                  (1 << 1)
#define ETH_CAM1_M1RFR                  (1 << 2)
#define ETH_CAM1_M1ROV                  (1 << 3)
#define ETH_CAM1_M1RUN                  (1 << 4)
#define ETH_CAM1_M1RCS                  (1 << 5)
#define ETH_CAM1_M1RCD                  (1 << 6)
#define ETH_CAM1_M1RFL                  (1 << 7)
#define ETH_CAM1_M1RAL                  (1 << 8)
#define ETH_CAM1_M1RXU                  (1 << 9)
#define ETH_CAM1_M1RXP                  (1 << 10)
#define ETH_CAM1_M1RXC                  (1 << 11)
#define ETH_CAM1_M1RBC                  (1 << 12)
#define ETH_CAM1_M1RMC                  (1 << 13)
#define ETH_CAM1_M1RFC                  (1 << 14)
#define ETH_CAM1_M1RPK                  (1 << 15)
#define ETH_CAM1_M1RBY                  (1 << 16)
#define ETH_CAM1_M1MGV                  (1 << 25)
#define ETH_CAM1_M1MAX                  (1 << 26)
#define ETH_CAM1_M11K                   (1 << 27)
#define ETH_CAM1_M1511                  (1 << 28)
#define ETH_CAM1_M1255                  (1 << 29)
#define ETH_CAM1_M1127                  (1 << 30)
#define ETH_CAM1_M164                   (1 << 31)

/* CAM2 */

#define ETH_CAM2_M2TDP                  (1 << 0)
#define ETH_CAM2_M2TPH                  (1 << 1)
#define ETH_CAM2_M2TNC                  (1 << 2)
#define ETH_CAM2_M2TXC                  (1 << 3)
#define ETH_CAM2_M2TLC                  (1 << 4)
#define ETH_CAM2_M2TMA                  (1 << 5)
#define ETH_CAM2_M2TSC                  (1 << 6)
#define ETH_CAM2_M2TED                  (1 << 7)
#define ETH_CAM2_M2TDF                  (1 << 8)
#define ETH_CAM2_M2TPF                  (1 << 9)
#define ETH_CAM2_M2TBC                  (1 << 10)
#define ETH_CAM2_M2TMC                  (1 << 11)
#define ETH_CAM2_M2TPK                  (1 << 12)
#define ETH_CAM2_M2TBY                  (1 << 13)
#define ETH_CAM2_M2TFG                  (1 << 14)
#define ETH_CAM2_M2TUN                  (1 << 15)
#define ETH_CAM2_M2TOV                  (1 << 16)
#define ETH_CAM2_M2TCF                  (1 << 17)
#define ETH_CAM2_M2TFC                  (1 << 18)
#define ETH_CAM2_M2TJB                  (1 << 19)

/* DMA Descriptor *************************************************************/

#ifndef __ASSEMBLY__

#define ETH_DES_PKTSIZE_PS_SHIFT (0)
#define ETH_DES_PKTSIZE_PS_MASK  (0xfff)
#define ETH_DES_PKTSIZE_FO_SHIFT (16)
#define ETH_DES_PKTSIZE_FO_MASK  (0x1f << ETH_TDES_PKTSIZE_FO_SHIFT)
#define ETH_DES_PKTSIZE_EF       (1 << 31)


struct eth_txdesc_s
{
	volatile uint32_t des_pktstartaddr;
	volatile uint32_t des_pktsize;
	volatile uint32_t des_nextdes;
};

struct eth_rxdesc_s
{
	volatile uint32_t des_pktstartaddr;
	volatile uint32_t des_pktsize;
	volatile uint32_t des_nextdes;
};

#endif  /* __ASSEMBLY__ */
#endif  /* SF2_NETHERNET > 0 */
#endif  /* __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_ETH_H */
