/************************************************************************************
 * arch/arm/src/smartfusion2/chip/sf2_gpio.h
************************************************************************************/

#ifndef __ARCH_ARM_SRC_SMARTFUION2_CHIP_SF2_GPIO_H
#define __ARCH_ARM_SRC_SMARTFUION2_CHIP_SF2_GPIO_H

#include <nuttx/config.h>


#define GPIO_BASE               0x40013000

#define GPIO_CFG_OFFSET(n)      (0x00 + (n << 2))
#define GPIO_0_CFG_OFFSET       0x00   /*  Configuration register for GPIO 0 */
#define GPIO_1_CFG_OFFSET       0x04   /*  Configuration register for GPIO 1 */
#define GPIO_2_CFG_OFFSET       0x08   /*  Configuration register for GPIO 2 */
#define GPIO_3_CFG_OFFSET       0x0C   /*  Configuration register for GPIO 3 */
#define GPIO_4_CFG_OFFSET       0x10   /*  Configuration register for GPIO 4 */
#define GPIO_5_CFG_OFFSET       0x14   /*  Configuration register for GPIO 5 */
#define GPIO_6_CFG_OFFSET       0x18   /*  Configuration register for GPIO 6 */
#define GPIO_7_CFG_OFFSET       0x1C   /*  Configuration register for GPIO 7 */
#define GPIO_8_CFG_OFFSET       0x20   /*  Configuration register for GPIO 8 */
#define GPIO_9_CFG_OFFSET       0x24   /*  Configuration register for GPIO 9 */
#define GPIO_10_CFG_OFFSET      0x28  /*  Configuration register for GPIO 10*/
#define GPIO_11_CFG_OFFSET      0x2C  /*  Configuration register for GPIO 11*/
#define GPIO_12_CFG_OFFSET      0x30  /*  Configuration register for GPIO 12*/
#define GPIO_13_CFG_OFFSET      0x34  /*  Configuration register for GPIO 13*/
#define GPIO_14_CFG_OFFSET      0x38  /*  Configuration register for GPIO 14*/
#define GPIO_15_CFG_OFFSET      0x3C  /*  Configuration register for GPIO 15*/
#define GPIO_16_CFG_OFFSET      0x40  /*  Configuration register for GPIO 16*/
#define GPIO_17_CFG_OFFSET      0x44  /*  Configuration register for GPIO 17*/
#define GPIO_18_CFG_OFFSET      0x48  /*  Configuration register for GPIO 18*/
#define GPIO_19_CFG_OFFSET      0x4C  /*  Configuration register for GPIO 19*/
#define GPIO_20_CFG_OFFSET      0x50  /*  Configuration register for GPIO 20*/
#define GPIO_21_CFG_OFFSET      0x54  /*  Configuration register for GPIO 21*/
#define GPIO_22_CFG_OFFSET      0x58  /*  Configuration register for GPIO 22*/
#define GPIO_23_CFG_OFFSET      0x5C  /*  Configuration register for GPIO 23*/
#define GPIO_24_CFG_OFFSET      0x60  /*  Configuration register for GPIO 24*/
#define GPIO_25_CFG_OFFSET      0x64  /*  Configuration register for GPIO 25*/
#define GPIO_26_CFG_OFFSET      0x68  /*  Configuration register for GPIO 26*/
#define GPIO_27_CFG_OFFSET      0x6C  /*  Configuration register for GPIO 27*/
#define GPIO_28_CFG_OFFSET      0x70  /*  Configuration register for GPIO 28*/
#define GPIO_29_CFG_OFFSET      0x74  /*  Configuration register for GPIO 29*/
#define GPIO_30_CFG_OFFSET      0x78  /*  Configuration register for GPIO 30*/
#define GPIO_31_CFG_OFFSET      0x7c  /*  Configuration register for GPIO 31*/

#define GPIO_IRQ_OFFSET         0x80  /*Read Modify Write per interrupt bit.*/ 
#define GPIO_IN_OFFSET          0x84  /*GPIO input register – Read-only for input  configured Ports.*/          
#define GPIO_OUT_OFFSET         0x88  /*GPIO output register */


#define GPIO_0   (GPIO_BASE+GPIO_0_CFG_OFFSET)
#define GPIO_1   (GPIO_BASE+GPIO_1_CFG_OFFSET)
#define GPIO_2   (GPIO_BASE+GPIO_2_CFG_OFFSET)
#define GPIO_3   (GPIO_BASE+GPIO_3_CFG_OFFSET)
#define GPIO_4   (GPIO_BASE+GPIO_4_CFG_OFFSET)
#define GPIO_5   (GPIO_BASE+GPIO_5_CFG_OFFSET)
#define GPIO_6   (GPIO_BASE+GPIO_6_CFG_OFFSET)
#define GPIO_7   (GPIO_BASE+GPIO_7_CFG_OFFSET)
#define GPIO_8   (GPIO_BASE+GPIO_8_CFG_OFFSET)
#define GPIO_9   (GPIO_BASE+GPIO_9_CFG_OFFSET)
#define GPIO_10  (GPIO_BASE+GPIO_10_CFG_OFFSET)
#define GPIO_11  (GPIO_BASE+GPIO_11_CFG_OFFSET)
#define GPIO_12  (GPIO_BASE+GPIO_12_CFG_OFFSET)
#define GPIO_13  (GPIO_BASE+GPIO_13_CFG_OFFSET)
#define GPIO_14  (GPIO_BASE+GPIO_14_CFG_OFFSET)
#define GPIO_15  (GPIO_BASE+GPIO_15_CFG_OFFSET)
#define GPIO_16  (GPIO_BASE+GPIO_16_CFG_OFFSET)
#define GPIO_17  (GPIO_BASE+GPIO_17_CFG_OFFSET)
#define GPIO_18  (GPIO_BASE+GPIO_18_CFG_OFFSET)
#define GPIO_19  (GPIO_BASE+GPIO_19_CFG_OFFSET)
#define GPIO_20  (GPIO_BASE+GPIO_20_CFG_OFFSET)
#define GPIO_21  (GPIO_BASE+GPIO_21_CFG_OFFSET)
#define GPIO_22  (GPIO_BASE+GPIO_22_CFG_OFFSET)
#define GPIO_23  (GPIO_BASE+GPIO_23_CFG_OFFSET)
#define GPIO_24  (GPIO_BASE+GPIO_24_CFG_OFFSET)
#define GPIO_25  (GPIO_BASE+GPIO_25_CFG_OFFSET)
#define GPIO_26  (GPIO_BASE+GPIO_26_CFG_OFFSET)
#define GPIO_27  (GPIO_BASE+GPIO_27_CFG_OFFSET)
#define GPIO_28  (GPIO_BASE+GPIO_28_CFG_OFFSET)
#define GPIO_29  (GPIO_BASE+GPIO_29_CFG_OFFSET)
#define GPIO_30  (GPIO_BASE+GPIO_30_CFG_OFFSET)
#define GPIO_31  (GPIO_BASE+GPIO_31_CFG_OFFSET)
#define GPIO_IRQ (GPIO_BASE+GPIO_IRQ_OFFSET)
#define GPIO_IN  (GPIO_BASE+GPIO_IN_OFFSET)
#define GPIO_OUT (GPIO_BASE+GPIO_OUT_OFFSET)

/* GPIO config register bits **********************************************/

/* GPIO CFG */
#define GPIO_CFG_OUTPUT_ENABLE   (1 << 0)
#define GPIO_CFG_INPUT_ENABLE    (1 << 1)
#define GPIO_CFG_OUTPUT_BUFFER   (1 << 2)
#define GPIO_CFG_INT_ENABLE      (1 << 3)

#define GPIO_CFG_INT_TYPT_SHIFT  (5)
#define GPIO_CFG_INT_TYPT_MASK   (7 << GPIO_CFG_INT_TYPT_SHIFT)
#define GPIO_CFG_INT_TYPE_LH     (0 << GPIO_CFG_INT_TYPT_SHIFT)
#define GPIO_CFG_INT_TYPE_LL     (1 << GPIO_CFG_INT_TYPT_SHIFT)
#define GPIO_CFG_INT_TYPE_EP     (2 << GPIO_CFG_INT_TYPT_SHIFT)
#define GPIO_CFG_INT_TYPE_EN     (3 << GPIO_CFG_INT_TYPT_SHIFT)
#define GPIO_CFG_INT_TYPE_EB     (4 << GPIO_CFG_INT_TYPT_SHIFT)

#endif /* __ARCH_ARM_SRC_SMARTFUION2_CHIP_SF2_GPIO_H */

