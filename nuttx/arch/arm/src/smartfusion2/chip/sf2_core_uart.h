
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CORE_UART_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CORE_UART_H


#include <nuttx/config.h>
#include "chip.h"

/* Register Offsets ***********************************************************/

#define SF2_CORE_UART_TXDATA_OFFSET   0x0000
#define SF2_CORE_UART_RXDATA_OFFSET   0x0004
#define SF2_CORE_UART_CTRL1_OFFSET    0x0008
#define SF2_CORE_UART_CTRL2_OFFSET    0x000C
#define SF2_CORE_UART_STATUS_OFFSET   0x0010
#define SF2_CORE_UART_CTRL3_OFFSET    0x0014


/* Register Addresses *********************************************************/

#define SF2_CORE_UART0_TXDATA  (SF2_CORE_UART0_BASE + SF2_CORE_UART_TXDATA_OFFSET)
#define SF2_CORE_UART0_RXDATA  (SF2_CORE_UART0_BASE + SF2_CORE_UART_RXDATA_OFFSET)
#define SF2_CORE_UART0_CTRL1   (SF2_CORE_UART0_BASE + SF2_CORE_UART_CTRL1_OFFSET)
#define SF2_CORE_UART0_CTRL2   (SF2_CORE_UART0_BASE + SF2_CORE_UART_CTRL2_OFFSET)
#define SF2_CORE_UART0_STATUS  (SF2_CORE_UART0_BASE + SF2_CORE_UART_STATUS_OFFSET)
#define SF2_CORE_UART0_CTRL3   (SF2_CORE_UART0_BASE + SF2_CORE_UART_CTRL3_OFFSET)


/* Register Bitfield Definitions **********************************************/

/* Buad value */
#define CORE_UART_CTRL1_B9600             (0x0D)
#define CORE_UART_CTRL2_B9600             (0x12 << 3)
#define CORE_UART_CTRL1_B19200            (0x06)
#define CORE_UART_CTRL2_B19200            (0x9 << 3)
#define CORE_UART_CTRL1_B38400            (0x82)
#define CORE_UART_CTRL2_B38400            (0x4 << 3)
#define CORE_UART_CTRL1_B57600            (0x01)
#define CORE_UART_CTRL2_B57600            (0x3 << 3)
#define CORE_UART_CTRL1_B115200           (0x80)
#define CORE_UART_CTRL2_B115200           (0x1 << 3)

/* Control Register 2 (CR2) */
#define CORE_UART_CTRL2_BIT8             (1 << 0)
#define CORE_UART_CTRL2_PARITY_NONE      (0 << 1)
#define CORE_UART_CTRL2_PARITY_EVEN      (1 << 1)
#define CORE_UART_CTRL2_PARITY_ODD       (2 << 1)

/* Control Register 3 (CR3) */
#define CORE_UART_CTRL3_B0_0             (0)
#define CORE_UART_CTRL3_B0_125           (1)
#define CORE_UART_CTRL3_B0_25            (2)
#define CORE_UART_CTRL3_B0_375           (3)
#define CORE_UART_CTRL3_B0_5             (4)
#define CORE_UART_CTRL3_B0_625           (5)
#define CORE_UART_CTRL3_B0_75            (6)
#define CORE_UART_CTRL3_B0_875           (7)

/* Status Register (SR) */
#define CORE_UART_STATUS_TXRDY           (1 << 0)
#define CORE_UART_STATUS_RXRDY           (1 << 1)
#define CORE_UART_STATUS_PARITY_ERR      (1 << 2)
#define CORE_UART_STATUS_OVERFLOW        (1 << 3)
#define CORE_UART_STATUS_FRAMING_ERR     (1 << 4)

#endif /* __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_CORE_UART_H */
