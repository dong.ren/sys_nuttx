#ifndef __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_I2C_H
#define __ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_I2C_H
 
#include "chip.h"
#include <nuttx/config.h>
/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

#define  I2C0_IRQn                     20        /* I2C0 interrupt                          */
#define  I2C1_IRQn                     23        /* I2C1 interrupt                          */

/* Register Offsets *****************************************************************/

 /* R/W 0x00 Control register: Used to configure the I2C peripheral.*/
#define SF2_I2C_CTRL_OFFSET               0x00

 /*  R 0xF8 Status register: Read-only value which indicates the current state ofthe I2C peripheral.*/
#define SF2_I2C_STATUS_OFFSET             0x04

 /*  R/W 0x00 Data register: Read/write data to/from the serial interface.*/
#define SF2_I2C_DATA_OFFSET               0x08

 /*  R/W 0x00 Slave0 address register: Contains the primary programmable address of the I2C peripheral.*/
#define SF2_I2C_SLAVE0_ADR_OFFSET         0x0C

 /*  R/W 0b01X1X000 SMBus register: Configuration register for SMBus timeout reset condition and for the optional SMBus signals SMBALERT_N and SMSBUS_N.*/
#define SF2_I2C_SMBUS_OFFSET              0x10

/*  R/W 0x08 Frequency register: Necessary for configuring real-time timeout logic.Can be set to the PCLK frequency for 25 ms SMBus timeouts, or the timeout value maybe increased/decreased.*/
#define SF2_I2C_FREQ_OFFSET               0x14


 /*  R/W 0x03 Glitch Reg length register: Used to adjust the input glitch filter length. If  GLITCHREG_FIXED = 0, then the register can be set from 3 to 21.  */
#define SF2_I2C_GLITCHREG_OFFSET          0x18

    /*  R/W 0x00 Slave1 address register: Contains the secondary programmable address of the I2C peripheral */
#define SF2_I2C_SLAVE1_ADR_OFFSET         0x1C



#define  SOFT_RST_CR_OFFSET               0X48 


/******************************************************************************/
/*                         Peripheral memory map                              */
/******************************************************************************/


#define  I2C0_BASE                                   0x40002000u
#define  I2C1_BASE                                   0x40012000u
#define  SYSREG_BASE                                 0x40038000u


/* Register Addresses **********************************************************/

#define SOFT_RST_CR   (SYSREG_BASE+SOFT_RST_CR_OFFSET)    /* SYSTEM SOFT_RST_CR */

#define SF2_I2C0_CTRL     	 		 (I2C0_BASE + SF2_I2C_CTRL_OFFSET)         
#define SF2_I2C0_STATUS 			 (I2C0_BASE + SF2_I2C_STATUS_OFFSET)
#define SF2_I2C0_DATA     		     (I2C0_BASE + SF2_I2C_DATA_OFFSET) 
#define SF2_I2C0_SLAVE0  			 (I2C0_BASE + SF2_I2C_SLAVE0_ADR_OFFSET) 
#define SF2_I2C0_SMBUS  			 (I2C0_BASE + SF2_I2C_SMBUS_OFFSET ) 
#define SF2_I2C0_FREQ     			 (I2C0_BASE + SF2_I2C_FREQ_OFFSET)   
#define SF2_I2C0_GLITCHREG 	         (I2C0_BASE + SF2_I2C_GLITCHREG_OFFSET) 
#define SF2_I2C0_SLAVE1_ADR          (I2C0_BASE + SF2_I2C_SLAVE1_ADR_OFFSET)

#define SF2_I2C1_CTRL     	 		 (I2C1_BASE + SF2_I2C_CTRL_OFFSET)       
#define SF2_I2C1_STATUS 			 (I2C1_BASE + SF2_I2C_STATUS_OFFSET)
#define SF2_I2C1_DATA     		     (I2C1_BASE + SF2_I2C_DATA_OFFSET) 
#define SF2_I2C1_SLAVE0  			 (I2C1_BASE + SF2_I2C_SLAVE0_ADR_OFFSET) 
#define SF2_I2C1_SMBUS  			 (I2C1_BASE + SF2_I2C_SMBUS_OFFSET ) 
#define SF2_I2C1_FREQ     			 (I2C1_BASE + SF2_I2C_FREQ_OFFSET)   
#define SF2_I2C1_GLITCHREG 	         (I2C1_BASE + SF2_I2C_GLITCHREG_OFFSET) 
#define SF2_I2C1_SLAVE1_ADR          (I2C1_BASE + SF2_I2C_SLAVE1_ADR_OFFSET)








/* Register Bitfield Definitions */

/*SOFT_RESET_CR */

/* Bit 11: Controls reset input to I2C_0
 * 0: Release I2C_0 from reset  1: Keep I2C_0  in reset (reset value)
*/
#define I2C0_SOFTRESET            (1 << 11)

/* Bit 12: Controls reset input to I2C_1
0: Release I2C_1 from reset  1: Keep I2C_1 in reset (reset value)*/
#define I2C1_SOFTRESET            (1 << 12)  

#define SYSREG_I2C0_SOFTRESET_MASK          ( (uint32_t)( 0x01 << 11) )
#define SYSREG_I2C1_SOFTRESET_MASK          ( (uint32_t)( 0x01 << 12) )


/* I2C_CTRL register */
#	define 	I2C_CTRL_CR0     (1<<0)
#	define	I2C_CTRL_CR1     (1<<1)
#	define 	I2C_CTRL_AA      (1<<2)
#	define 	I2C_CTRL_SI      (1<<3)
#	define 	I2C_CTRL_STO     (1<<4)
#	define 	I2C_CTRL_STA     (1<<5)
#	define 	I2C_CTRL_ENS1    (1<<6)
#	define	I2C_CTRL_CR2     (1<<7)

/*I2C_DATA register */
#define	 I2C_DATA_DIR    (1<<0)    /*direction bit. 0 = Write; 1 = Read*/

/*I2C_SLAVE0  */
/*If the GC bit is set, the general call address is recognized; 
otherwise it is ignored.
*/
#define    I2C_SLAVE0_GC  (1<<0)   


#endif /*__ARCH_ARM_SRC_SMARTFUSION2_CHIP_SF2_I2C_H */

