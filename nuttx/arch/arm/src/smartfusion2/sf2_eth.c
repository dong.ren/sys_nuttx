
#include <nuttx/config.h>
#if defined(CONFIG_NET) && defined(CONFIG_SF2_ETHMAC)

#include <debug.h>
#include <errno.h>
#include <nuttx/wdog.h>
#include <queue.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
 
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/net/gmii.h>
#include <nuttx/net/mii.h>

#include <nuttx/net/arp.h>
#include <nuttx/net/netdev.h>
#if defined(CONFIG_NET_PKT)
#include <nuttx/net/pkt.h>
#endif

#include "up_arch.h"
#include "up_internal.h"

#include "chip.h"
#include "sf2_clock.h"
#include "sf2_eth.h"

#include <arch/board/board.h>

#if SF2_NETHERNET > 0


#define OPTIMAL_ETH_BUFSIZE ((CONFIG_NET_ETH_MTU + 4 + 15) & ~15)

#ifndef CONFIG_SF2_ETH_BUFSIZE
#define CONFIG_SF2_ETH_BUFSIZE OPTIMAL_ETH_BUFSIZE
#endif

#if CONFIG_SF2_ETH_BUFSIZE > ETH_DES_PKTSIZE_PS_MASK
#error "CONFIG_SF2_ETH_BUFSIZE is too large"
#endif

#if (CONFIG_SF2_ETH_BUFSIZE & 15) != 0
#error "CONFIG_SF2_ETH_BUFSIZE must be aligned"
#endif

#if CONFIG_SF2_ETH_BUFSIZE != OPTIMAL_ETH_BUFSIZE
#warning "You using an incomplete/untested configuration"
#endif
#ifdef CONFIG_ETH_HD
#define CONFIG_SF2_ETHMODE 0;
#endif
#ifdef CONFIG_ETH_FD
#define CONFIG_SF2_ETHMODE 1;
#endif
#ifdef CONFIG_ETH_MBPS10
#define CONFIG_SF2_ETHMBPS 0;
#endif
#ifdef CONFIG_ETH_MBPS100
#define CONFIG_SF2_ETHMBPS 1;
#endif
#ifdef CONFIG_ETH_MBPS1000
#define CONFIG_SF2_ETHMBPS 2;
#endif

#ifndef CONFIG_SF2_PHY_AUTONEG
#ifndef CONFIG_SF2_ETHMODE
#error "sf2 eth mode not define."
#endif
#ifndef CONFIG_SF2_ETHMBPS
#error "sf2 eth mbps not define."
#endif
#endif

#ifndef CONFIG_SF2_PHYADDR
#error "CONFIG_SF2_PHYADDR must be defined in the NuttX configuration"
#endif

#define PHY_RETRY_MAX 300000

#ifndef CONFIG_SF2_ETH_NRXDESC
#define CONFIG_SF2_ETH_NRXDESC 4
#endif
#ifndef CONFIG_SF2_ETH_NTXDESC
#define CONFIG_SF2_ETH_NTXDESC 4
#endif

#define SF2_ETH_NFREEBUFFERS (CONFIG_SF2_ETH_NTXDESC + 1)

/* TX poll delay = 1 seconds. CLK_TCK is the number of clock ticks per
 * second
 */
#define SF2_WDDELAY (1 * CLK_TCK)

/* TX timeout = 1 minute */
#define SF2_TXTIMEOUT (60 * CLK_TCK)

/* This is a helper pointer for accessing the contents of the Ethernet
 * header
 */
#define BUF ((struct eth_hdr_s *)priv->dev.d_buf)

struct sf2_ethmac_s {
	uint8_t ifup ;    /* true:ifup false:ifdown */
	uint8_t mbps ;    /* 0: 10M, 1: 100M, 2: 1000M */
	uint8_t fduplex ; /* Full (vs. half) duplex */
	WDOG_ID txpoll;      /* TX poll timer */
	WDOG_ID txtimeout;   /* TX timeout timer */

	/* This holds the information visible to uIP/NuttX */

	struct net_driver_s dev; /* Interface understood by uIP */

	/* Used to track transmit and receive descriptors */

	struct eth_txdesc_s *txhead; /* Next available TX descriptor */
	struct eth_rxdesc_s *rxhead; /* Next available RX descriptor */

	struct eth_txdesc_s *txtail; /* First "in_flight" TX descriptor */
	struct eth_rxdesc_s *rxcurr; /* First RX descriptor of the segment */
	uint16_t segments;           /* RX segment count */
	uint16_t inflight;           /* Number of TX transfers "in_flight" */
	sq_queue_t freeb;            /* The free buffer list */

	/* Descriptor allocations */

	struct eth_rxdesc_s rxtable[CONFIG_SF2_ETH_NRXDESC];
	struct eth_txdesc_s txtable[CONFIG_SF2_ETH_NTXDESC];

	/* Buffer allocations */

	uint8_t rxbuffer[CONFIG_SF2_ETH_NRXDESC * CONFIG_SF2_ETH_BUFSIZE];
	uint8_t alloc[SF2_ETH_NFREEBUFFERS * CONFIG_SF2_ETH_BUFSIZE];
};

/* Private Data ***************************************************************/
static struct sf2_ethmac_s g_sf2ethmac[SF2_NETHERNET];

/* Private Function Prototypes ************************************************/

/* Free buffer management */
static void sf2_initbuffer(FAR struct sf2_ethmac_s *priv);
static inline uint8_t *sf2_allocbuffer(FAR struct sf2_ethmac_s *priv);
static inline void sf2_freebuffer(FAR struct sf2_ethmac_s *priv,
		uint8_t *buffer);
static inline bool sf2_isfreebuffer(FAR struct sf2_ethmac_s *priv);

/* Common TX logic */
static int sf2_transmit(FAR struct sf2_ethmac_s *priv);
static int sf2_txpoll(struct net_driver_s *dev);
static void sf2_dopoll(FAR struct sf2_ethmac_s *priv);

/* Interrupt handling */
static void sf2_enableint(FAR struct sf2_ethmac_s *priv, uint32_t ierbit);
static void sf2_disableint(FAR struct sf2_ethmac_s *priv, uint32_t ierbit);

static int sf2_recvframe(FAR struct sf2_ethmac_s *priv);
static void sf2_receive(FAR struct sf2_ethmac_s *priv);
static void sf2_freeframe(FAR struct sf2_ethmac_s *priv);
static void sf2_txdone(FAR struct sf2_ethmac_s *priv);
static int sf2_interrupt(int irq, FAR void *context, void *arg);

/* Watchdog timer expirations */
static void sf2_polltimer(int argc, uint32_t arg, ...);
static void sf2_txtimeout(int argc, uint32_t arg, ...);

/* NuttX callback functions */
static int sf2_ifup(struct net_driver_s *dev);
static int sf2_ifdown(struct net_driver_s *dev);
static int sf2_txavail(struct net_driver_s *dev);

/* Descriptor Initialization */
static void sf2_txdescinit(FAR struct sf2_ethmac_s *priv);
static void sf2_rxdescinit(FAR struct sf2_ethmac_s *priv);

static void sf2_up_macconfig(FAR struct sf2_ethmac_s *priv);
static void sf2_ethreset(FAR struct sf2_ethmac_s *priv);
static void sf2_macconfig(FAR struct sf2_ethmac_s *priv);
static void sf2_macaddress(FAR struct sf2_ethmac_s *priv);
static int sf2_macenable(FAR struct sf2_ethmac_s *priv);
static int sf2_ethconfig(FAR struct sf2_ethmac_s *priv);

/* PHY config */
#ifdef CONFIG_SF2_PHY_CONFIG
static void sf2_phywrite(uint16_t phyaddr, uint16_t regaddr, uint16_t value);
static int sf2_phyread(uint16_t phyaddr, uint16_t regaddr, uint16_t *value);
static int sf2_phyreset(struct sf2_ethmac_s *priv);
#ifdef CONFIG_SF2_PHY_AUTONEG
static int sf2_autonegotiate(struct sf2_ethmac_s *priv);
#endif
static int sf2_phyconfig(struct sf2_ethmac_s *priv);
#endif

static void sf2_initbuffer(FAR struct sf2_ethmac_s *priv) {
	uint8_t *buffer;
	int i;

	/* Initialize the head of the free buffer list */
	sq_init(&priv->freeb);

	/* Add all of the pre-allocated buffers to the free buffer list */
	for (i = 0, buffer = priv->alloc; i < SF2_ETH_NFREEBUFFERS;
			i++, buffer += CONFIG_SF2_ETH_BUFSIZE) {
		sq_addlast((FAR sq_entry_t *)buffer, &priv->freeb);
	}
}

static inline uint8_t *sf2_allocbuffer(FAR struct sf2_ethmac_s *priv) {
	return (uint8_t *)sq_remfirst(&priv->freeb);
}

static inline void sf2_freebuffer(FAR struct sf2_ethmac_s *priv,
		uint8_t *buffer) {
	if (buffer >= priv->alloc) {
		sq_addlast((FAR sq_entry_t *)buffer, &priv->freeb);
	}
}

static inline bool sf2_isfreebuffer(FAR struct sf2_ethmac_s *priv) {
	return !sq_empty(&priv->freeb);
}

static int sf2_transmit(FAR struct sf2_ethmac_s *priv) {
	struct eth_txdesc_s *txdesc;
	struct eth_txdesc_s *txfirst;

	/* Verify that the hardware is ready to send another packet.  If we get
	 * here, then we are committed to sending a packet; Higher level logic
	 * must have assured that there is no transmission in progress.
	 */
	txdesc = priv->txhead;
	txfirst = txdesc;

	{
		/* Set the Buffer1 address pointer */
		txdesc->des_pktstartaddr = (uint32_t)priv->dev.d_buf;

		/* set length and empty flag */
		txdesc->des_pktsize = priv->dev.d_len;

		// update DMA_TX_DESC
		putreg32((uint32_t)txdesc, SF2_ETH_DMA_TX_DESC);

		/* Point to the next available TX descriptor */
		txdesc = (struct eth_txdesc_s *)txdesc->des_nextdes;
	}

	/* Remember where we left off in the TX descriptor chain */
	priv->txhead = txdesc;

	/* Detach the buffer from priv->dev structure.  That buffer is now
	 * "in-flight".
	 */
	priv->dev.d_buf = NULL;
	priv->dev.d_len = 0;
	/* If there is no other TX buffer, in flight, then remember the location
	 * of the TX descriptor.  This is the location to check for TX done events.
	 */
	if (!priv->txtail) {

		priv->txtail = txfirst;
	}

	/* Increment the number of TX transfer in-flight */
	priv->inflight++;

	if (priv->inflight >= CONFIG_SF2_ETH_NTXDESC) {

		sf2_disableint(priv, ETH_DMA_IRQ_MASK_RXPKTRECEIVED);
	}

	/* Enable TX interrupts */
	sf2_enableint(priv, ETH_DMA_IRQ_MASK_TXPKTSENT);

	// enable tx
	putreg32(0x01, SF2_ETH_DMA_TX_CTRL);

	/* Setup the TX timeout watchdog (perhaps restarting the timer) */
	(void)wd_start(priv->txtimeout, SF2_TXTIMEOUT, sf2_txtimeout, 1,
			(uint32_t)priv);
	return OK;
}

static int sf2_txpoll(struct net_driver_s *dev) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)dev->d_private;

	/* If the polling resulted in data that should be sent out on the network,
	 * the field d_len is set to a value > 0.
	 */
	if (priv->dev.d_len > 0) {

		/* Send the packet */
		arp_out(&priv->dev);
		sf2_transmit(priv);

		/* Check if the next TX descriptor is owned by the Ethernet DMA or CPU.  We
		 * cannot perform the TX poll if we are unable to accept another packet for
		 * transmission.
		 */
		if ((priv->txhead->des_pktsize & ETH_DES_PKTSIZE_EF) == 0 ||
				(priv->txhead->des_pktsize & ETH_DES_PKTSIZE_PS_MASK) != 0) {
			/* We have to terminate the poll if we have no more descriptors
			 * available for another transfer.
			 */
			return -EBUSY;
		}

		/* We have the descriptor, we can continue the poll. Allocate a new
		 * buffer for the poll.
		 */
		dev->d_buf = sf2_allocbuffer(priv);

		/* We can't continue the poll if we have no buffers */
		if (dev->d_buf == NULL) {

			/* Terminate the poll. */
			return -ENOMEM;
		}
	}

	/* If zero is returned, the polling will continue until all connections have
	 * been examined.
	 */
	return 0;
}

static void sf2_dopoll(FAR struct sf2_ethmac_s *priv) {
	FAR struct net_driver_s *dev = &priv->dev;

	/* Check if the next TX descriptor is owned by the Ethernet DMA or
	 * CPU.  We cannot perform the TX poll if we are unable to accept
	 * another packet for transmission.
	 */
	if ((priv->txhead->des_pktsize & ETH_DES_PKTSIZE_EF) &&
			((priv->txhead->des_pktsize & ETH_DES_PKTSIZE_PS_MASK) == 0)) {
		/* If we have the descriptor, then poll uIP for new XMIT data.
		 * Allocate a buffer for the poll.
		 */
		dev->d_buf = sf2_allocbuffer(priv);

		/* We can't poll if we have no buffers */
		if (dev->d_buf) {

			(void)devif_poll(dev, sf2_txpoll);

			/* We will, most likely end up with a buffer to be freed.  But it
			 * might not be the same one that we allocated above.
			 */
			if (dev->d_buf) {
				sf2_freebuffer(priv, dev->d_buf);
				dev->d_buf = NULL;
			}
		}
	}
}

static void sf2_enableint(FAR struct sf2_ethmac_s *priv, uint32_t ierbit) {
	uint32_t regval;

	regval = getreg32(SF2_ETH_DMA_IRQ_MASK);
	regval |= ierbit;
	putreg32(regval, SF2_ETH_DMA_IRQ_MASK);
}

static void sf2_disableint(FAR struct sf2_ethmac_s *priv, uint32_t ierbit) {
	uint32_t regval;

	regval = getreg32(SF2_ETH_DMA_IRQ_MASK);
	regval &= ~ierbit;
	putreg32(regval, SF2_ETH_DMA_IRQ_MASK);
}

static int sf2_recvframe(FAR struct sf2_ethmac_s *priv) {
	struct net_driver_s *dev = &priv->dev;
	struct eth_rxdesc_s *rxdesc;
	//	uint8_t *buffer;
	uint32_t regval;

	priv->segments++;

	// clear rx pending interrupt
	regval = getreg32(SF2_ETH_DMA_RX_STATUS);
	regval |= ETH_DMA_RX_STATUS_RXPKTRECEIVED;
	putreg32(regval, SF2_ETH_DMA_RX_STATUS);

	if (!sf2_isfreebuffer(priv)) {
		nerr("No free buffers\n");
		return -ENOMEM;
	}

	rxdesc = priv->rxhead;

	dev->d_len = (rxdesc->des_pktsize & ETH_DES_PKTSIZE_PS_MASK) - 4;
	dev->d_buf = (uint8_t *)rxdesc->des_pktstartaddr;

	rxdesc->des_pktsize = 0;
	rxdesc = (struct eth_rxdesc_s *)rxdesc->des_nextdes;
	priv->rxcurr = NULL;

	//	priv->rxhead = (struct eth_rxdesc_s*)rxdesc->des_nextdes;
	priv->rxhead = (struct eth_rxdesc_s *)rxdesc;

	priv->rxhead->des_pktsize |= ETH_DES_PKTSIZE_EF;
	putreg32((uint32_t)priv->rxhead, SF2_ETH_DMA_RX_DESC);

	regval = getreg32(SF2_ETH_DMA_RX_STATUS);
	regval |= ETH_DMA_RX_STATUS_RXOVERFLOW;
	putreg32(regval, SF2_ETH_DMA_RX_STATUS);

	putreg32(1, SF2_ETH_DMA_RX_CTRL);

	return OK;
}

static void sf2_receive(FAR struct sf2_ethmac_s *priv) {
	struct net_driver_s *dev = &priv->dev;

	priv->segments = 0;
	while ((sf2_recvframe(priv) == OK) && (priv->segments == 1)) {

#ifdef CONFIG_NET_PKT
		/* When packet sockets are enabled, feed the frame into the packet tap */
		pkt_input(&priv->dev);
#endif

		/* Check if the packet is a valid size for the uIP buffer configuration
		 * (this should not happen)
		 */
		if (dev->d_len > CONFIG_NET_ETH_MTU) {

			nerr("DROPPED: Too big: %d\n", dev->d_len);
		}

		/* We only accept IP packets of the configured type and ARP packets */

#ifdef CONFIG_NET_IPv6
		else if (BUF->type == HTONS(ETHTYPE_IP6))
#else
		else if (BUF->type == HTONS(ETHTYPE_IP))
#endif
		{
			ninfo("IP frame\n");

			/* Handle ARP on input then give the IP packet to uIP */
			arp_ipin(&priv->dev);
			// devif_input(&priv->dev);
			ipv4_input(&priv->dev);

			/* If the above function invocation resulted in data that should be
			 * sent out on the network, the field  d_len will set to a value > 0.
			 */
			if (priv->dev.d_len > 0) {
				arp_out(&priv->dev);
				sf2_transmit(priv);
			}
		} else if (BUF->type == htons(ETHTYPE_ARP)) {
			ninfo("ARP frame\n");

			/* Handle ARP packet */
			arp_arpin(&priv->dev);

			/* If the above function invocation resulted in data that should be
			 * sent out on the network, the field  d_len will set to a value > 0.
			 */
			if (priv->dev.d_len > 0) {

				sf2_transmit(priv);
			}
		} else {
			ninfo("DROPPED: Unknown type: %04x\n", BUF->type);
		}

		/* We are finished with the RX buffer.  NOTE:  If the buffer is
		 * re-used for transmission, the dev->d_buf field will have been
		 * nullified.
		 */
		if (dev->d_buf) {
			/* Free the receive packet buffer */
			sf2_freebuffer(priv, dev->d_buf);
			dev->d_buf = NULL;
			dev->d_len = 0;
		}
	}
}

static void sf2_freeframe(FAR struct sf2_ethmac_s *priv) {
	struct eth_txdesc_s *txdesc;

	/* Scan for "in-flight" descriptors owned by the CPU */
	txdesc = priv->txtail;
	if (txdesc) {
		DEBUGASSERT(priv->inflight > 0);

		while (1) {

			/* There should be a buffer assigned to all in-flight
			 * TX descriptors.
			 */
			DEBUGASSERT(txdesc->des_pktstartaddr != 0);

			sf2_freebuffer(priv, (uint8_t *)txdesc->des_pktstartaddr);

			txdesc->des_pktstartaddr = 0;

			priv->inflight--;

			/* If all of the TX descriptors were in-flight, then RX interrupts
			 * may have been disabled... we can re-enable them now.
			 */
			sf2_enableint(priv, ETH_DMA_IRQ_MASK_RXPKTRECEIVED);

			/* If there are no more frames in-flight, then bail. */
			if (priv->inflight <= 0) {
				priv->txtail = NULL;
				priv->inflight = 0;
				return;
			}

			/* Try the next descriptor in the TX chain */
			txdesc = (struct eth_txdesc_s *)txdesc->des_nextdes;

			if ((txdesc->des_pktsize & ETH_DES_PKTSIZE_EF) == 0) {
				priv->txtail = txdesc;
				return;
			}
		}
	}
}

static void sf2_txdone(FAR struct sf2_ethmac_s *priv) {
	uint32_t regval;

	/* Scan the TX desciptor change, returning buffers to free list */
	sf2_freeframe(priv);

	/* If no further xmits are pending, then cancel the TX timeout */
	if (priv->inflight <= 0) {

		wd_cancel(priv->txtimeout);

		/* And disable further TX interrupts. */
		sf2_disableint(priv, ETH_DMA_IRQ_MASK_TXPKTSENT);
	}

	regval = getreg32(SF2_ETH_DMA_TX_STATUS);
	regval |= ETH_DMA_TX_STATUS_TXPKTSENT;
	putreg32(regval, SF2_ETH_DMA_TX_STATUS);

	/* Then poll uIP for new XMIT data */
	sf2_dopoll(priv);
}

static int sf2_interrupt(int irq, FAR void *context, void *arg) {
	register FAR struct sf2_ethmac_s *priv = &g_sf2ethmac[0];
	uint32_t dma_irq;

	dma_irq = getreg32(SF2_ETH_DMA_IRQ);

	if (dma_irq & ETH_DMA_IRQ_RXPKTRECEIVED) {

		sf2_receive(priv);
	}

	if (dma_irq & ETH_DMA_IRQ_TXPKTSENT) {

		sf2_txdone(priv);
	}

	return OK;
}

static void sf2_txtimeout(int argc, uint32_t arg, ...) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)arg;

	/* Then reset the hardware.  Just take the interface down, then back
	 * up again.
	 */
	sf2_ifdown(&priv->dev);
	sf2_ifup(&priv->dev);

	/* Then poll uIP for new XMIT data */
	sf2_dopoll(priv);
}

static void sf2_polltimer(int argc, uint32_t arg, ...) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)arg;
	FAR struct net_driver_s *dev = &priv->dev;

	if ((priv->txhead->des_pktsize & ETH_DES_PKTSIZE_EF) &&
			((priv->txhead->des_pktsize & ETH_DES_PKTSIZE_PS_MASK) == 0)) {
		/* If we have the descriptor, then perform the timer poll.  Allocate a
		 * buffer for the poll.
		 */

		dev->d_buf = sf2_allocbuffer(priv);

		/* We can't poll if we have no buffers */

		if (dev->d_buf) {

			/* Update TCP timing states and poll uIP for new XMIT data.
			*/
			(void)devif_timer(dev, sf2_txpoll);

			/* We will, most likely end up with a buffer to be freed.  But it
			 * might not be the same one that we allocated above.
			 */

			if (dev->d_buf) {

				sf2_freebuffer(priv, dev->d_buf);
				dev->d_buf = NULL;
			}
		}
	}

	/* Setup the watchdog poll timer again */
	(void)wd_start(priv->txpoll, SF2_WDDELAY, sf2_polltimer, 1, arg);
}

static int sf2_ifup(struct net_driver_s *dev) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)dev->d_private;
	int ret;

	/* Configure the Ethernet interface for DMA operation. */
	ret = sf2_ethconfig(priv);
	if (ret < 0) {
		return ret;
	}

	/* Set and activate a timer process */
	(void)wd_start(priv->txpoll, SF2_WDDELAY, sf2_polltimer, 1, (uint32_t)priv);

	/* Enable the Ethernet interrupt */
	priv->ifup = true;
	up_enable_irq(SF2_IRQ_ETHERNETMAC);

	//	sf2_checksetup();
	return OK;
}

static int sf2_ifdown(struct net_driver_s *dev) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)dev->d_private;
	irqstate_t flags;

	/* Disable the Ethernet interrupt */
	flags = enter_critical_section();
	up_disable_irq(SF2_IRQ_ETHERNETMAC);

	/* Cancel the TX poll timer and TX timeout timers */
	wd_cancel(priv->txpoll);
	wd_cancel(priv->txtimeout);

	/* reset */
	sf2_ethreset(priv);

	/* Mark the device "down" */
	priv->ifup = false;
	leave_critical_section(flags);
	return OK;
}

static int sf2_txavail(struct net_driver_s *dev) {
	FAR struct sf2_ethmac_s *priv = (FAR struct sf2_ethmac_s *)dev->d_private;
	irqstate_t flags;

	/* Disable interrupts because this function may be called from interrupt
	 * level processing.
	 */
	flags = enter_critical_section();

	/* Ignore the notification if the interface is not yet up */
	if (priv->ifup) {
		/* Poll uIP for new XMIT data */
		sf2_dopoll(priv);
	}

	leave_critical_section(flags);
	return OK;
}

static void sf2_txdescinit(FAR struct sf2_ethmac_s *priv) {
	struct eth_txdesc_s *txdesc;
	int i;

	/* priv->txhead will point to the first, available TX descriptor in the chain.
	 * Set the priv->txhead pointer to the first descriptor in the table.
	 */
	priv->txhead = priv->txtable;

	/* priv->txtail will point to the first segment of the oldest pending
	 * "in-flight" TX transfer.  NULL means that there are no active TX
	 * transfers.
	 */
	priv->txtail = NULL;
	priv->inflight = 0;

	/* Initialize each TX descriptor */
	for (i = 0; i < CONFIG_SF2_ETH_NTXDESC; i++) {

		txdesc = &priv->txtable[i];

		txdesc->des_pktstartaddr = 0;
		txdesc->des_pktsize = ETH_DES_PKTSIZE_EF;

		if (i < (CONFIG_SF2_ETH_NTXDESC - 1)) {
			txdesc->des_nextdes = (uint32_t)&priv->txtable[i + 1];
		} else {
			txdesc->des_nextdes = (uint32_t)priv->txtable;
		}
	}

	putreg32((uint32_t)priv->txtable, SF2_ETH_DMA_TX_DESC);
}

static void sf2_rxdescinit(FAR struct sf2_ethmac_s *priv) {
	struct eth_rxdesc_s *rxdesc;
	int i;

	/* priv->rxhead will point to the first,  RX descriptor in the chain.
	 * This will be where we receive the first incomplete frame.
	 */
	priv->rxhead = priv->rxtable;

	/* If we accumulate the frame in segments, priv->rxcurr points to the
	 * RX descriptor of the first segment in the current TX frame.
	 */
	priv->rxcurr = NULL;
	priv->segments = 0;

	/* Initialize each TX descriptor */
	for (i = 0; i < CONFIG_SF2_ETH_NRXDESC; i++) {

		rxdesc = &priv->rxtable[i];

		/* Set empty flag and bufsize */
		rxdesc->des_pktsize = 0;

		/* Set Buffer1 address pointer */
		rxdesc->des_pktstartaddr =
			(uint32_t)&priv->rxbuffer[i * CONFIG_SF2_ETH_BUFSIZE];

		/* Initialize the next descriptor with the Next Descriptor Polling Enable */
		if (i < (CONFIG_SF2_ETH_NRXDESC - 1)) {
			/* Set next descriptor address register with next descriptor base
			 * address
			 */
			rxdesc->des_nextdes = (uint32_t)&priv->rxtable[i + 1];
		} else {
			/* For last descriptor, set next descriptor address register equal
			 * to the first descriptor base address
			 */
			rxdesc->des_nextdes = (uint32_t)priv->rxtable;
		}
	}

	priv->rxhead->des_pktsize |= ETH_DES_PKTSIZE_EF;
	/* Set Receive Descriptor List Address Register */
	putreg32((uint32_t)priv->rxtable, SF2_ETH_DMA_RX_DESC);
}

static void sf2_ethreset(FAR struct sf2_ethmac_s *priv) {
	uint32_t regval;
	/* Reset MAC*/
	regval = getreg32(SF2_SYSREG_SOFT_RST_CR);
	regval |= SYSREG_SOFTRESET_MAC;
	putreg32(regval, SF2_SYSREG_SOFT_RST_CR);
	regval &= ~SYSREG_SOFTRESET_MAC;
	putreg32(regval, SF2_SYSREG_SOFT_RST_CR);

	regval = getreg32(SF2_ETH_CFG1);
	regval |= (ETH_CFG1_TXFUNC_RESET | ETH_CFG1_RXFUNC_RESET |
			ETH_CFG1_TXMACCTRL_RESET | ETH_CFG1_RXMACCTRL_RESET);
	putreg32(regval, SF2_ETH_CFG1);

	regval = getreg32(SF2_ETH_INTERFACE_CTRL);
	regval |= ETH_INT_CTRL_RESET_INT;
	putreg32(regval, SF2_ETH_INTERFACE_CTRL);

	regval = getreg32(SF2_ETH_FIFO_CFG0);
	regval |= (ETH_FIFO_CFG0_HSTRSTWT | ETH_FIFO_CFG0_HSTRSTSR |
			ETH_FIFO_CFG0_HSTRSTFR | ETH_FIFO_CFG0_HSTRSTST |
			ETH_FIFO_CFG0_HSTRSTFT);
	putreg32(regval, SF2_ETH_FIFO_CFG0);

}

static void sf2_macconfig(FAR struct sf2_ethmac_s *priv) {
	uint32_t regval;

	/*Configure PHY related MII MGMT registers*/
	putreg32(0x7, SF2_ETH_MII_CONFIG);

	/* Clear Reset Bit*/
	regval = getreg32(SF2_ETH_CFG1);
	regval &= ~(ETH_CFG1_SOFT_RESET | ETH_CFG1_TXFUNC_RESET | ETH_CFG1_RXFUNC_RESET |
				ETH_CFG1_TXMACCTRL_RESET | ETH_CFG1_RXMACCTRL_RESET);
	putreg32(regval, SF2_ETH_CFG1);

	regval = getreg32(SF2_ETH_INTERFACE_CTRL);
	regval &= ~ETH_INT_CTRL_RESET_INT;
	putreg32(regval, SF2_ETH_INTERFACE_CTRL);

	regval = getreg32(SF2_ETH_FIFO_CFG0);
	regval &= ~(ETH_FIFO_CFG0_HSTRSTWT | ETH_FIFO_CFG0_HSTRSTSR |
			ETH_FIFO_CFG0_HSTRSTFR | ETH_FIFO_CFG0_HSTRSTST |
			ETH_FIFO_CFG0_HSTRSTFT);
	putreg32(regval, SF2_ETH_FIFO_CFG0);

#if defined CONFIG_SF2_LOOPBACK
	regval = 1;
#else 
	regval = 0;
#endif
	regval |= (ETH_CFG1_TXFLOWCTRL_EN | ETH_CFG1_RXFLOWCTRL_EN);
	putreg32(regval, SF2_ETH_CFG1);

	regval = ETH_CFG2_FULL_DUPLEX;
	regval |=
		(ETH_CFG2_CRC_ENABLE | ETH_CFG2_PADCRC_ENABLE | ETH_CFG2_LENGTH_CHECKING);
	regval &= ~ETH_CFG2_INT_MODE_MASK;
#if defined(CONFIG_SF2_MII) || defined(CONFIG_SF2_RMII)
	/* MII and RMII use nibble mode interface. */
	regval |= ETH_CFG2_INT_MODE_NIBBLE;
#else
	/* TBI and GMII use byte interface. */
	regval |= ETH_CFG2_INT_MODE_BYTE;
#endif
	// CFG2 PREAMBLE_LENGTH bits, default value
	regval &= ~ETH_CFG2_PREAMBLE_LENGTH_MASK;
	regval |= (0x7 << ETH_CFG2_PREAMBLE_LENGTH_SHIFT);
	putreg32(regval, SF2_ETH_CFG2);

	// IFG configure, default value

	// HALF_DUPLEX configure, the other bits default value
	regval = getreg32(SF2_ETH_HALF_DUPLEX);
	regval &= ~ETH_HALF_DUPLEX_ED;
	putreg32(regval, SF2_ETH_HALF_DUPLEX);

	// MAX_FRAME_LENGTH configure, default value

	regval = ETH_FIFO_CFG0_WTMENREQ | ETH_FIFO_CFG0_SRFENREQ |
		ETH_FIFO_CFG0_FRFENREQ | ETH_FIFO_CFG0_STFENREQ |
		ETH_FIFO_CFG0_FTFENREQ;
	putreg32(regval, SF2_ETH_FIFO_CFG0);

	regval = 0x0fff0000;
	putreg32(regval, SF2_ETH_FIFO_CFG1);

	regval = 0x04000180;
	putreg32(regval, SF2_ETH_FIFO_CFG2);

	regval = 0x0258ffff;
	putreg32(regval, SF2_ETH_FIFO_CFG3);

	regval = 0x0000ffff;
	putreg32(regval, SF2_ETH_FIFO_CFG4);

#if defined(CONFIG_SF2_MII) || defined(CONFIG_SF2_RMII)
	regval = 0x0007efef;
#else
	regval = 0x000fefe7;
#endif
	putreg32(regval, SF2_ETH_FIFO_CFG5);

#if defined(CONFIG_SF2_MII) 
	regval = SYSREG_MAR_CR_MODE_MII;
#elif defined(CONFIG_SF2_RMII)
	regval = SYSREG_MAR_CR_MODE_RMII;
#elif defined(CONFIG_SF2_GMII) || defined(CONFIG_SF2_RGMII)
	regval = SYSREG_MAR_CR_MODE_GMII;
#elif defined CONFIG_SF2_TBI
	regval = SYSREG_MAR_CR_MODE_TBI;
#endif
	putreg32(regval, SF2_SYSREG_MAC_CR);
}

static void sf2_up_macconfig(FAR struct sf2_ethmac_s *priv){
	uint32_t regval;

#if defined(CONFIG_SF2_RGMII) && defined(CONFIG_SF2_PHY_CONFIG)
	{                      
		/* config corergmii register 0h */
		uint16_t regval = 0; /* 10 */
		if (priv->mbps == 1)
			regval = GMII_MCR_SPEED100; /* 100 */
		else if (priv->mbps == 2)
			regval = GMII_MCR_SPEED1000; /* 1000 */

		sf2_phywrite(28, GMII_MCR, regval);
		sf2_phywrite(28, GMII_MCR, regval);
	}
#endif

	/* Reconfigure MAC based on PHY configuration. */
	regval = getreg32(SF2_ETH_INTERFACE_CTRL);
	if (priv->mbps == 1) 
		regval |= ETH_INT_CTRL_SPEED;
	else
		regval &= ~ETH_INT_CTRL_SPEED;
	putreg32(regval, SF2_ETH_INTERFACE_CTRL);

	/* config SYSREG MAR CR speed */
	regval = getreg32(SF2_SYSREG_MAC_CR);
	regval &= ~SYSREG_MAR_CR_SPEED_MASK;
	if (priv->mbps == 0)
		regval |= SYSREG_MAR_CR_SPEED_10;
	else if (priv->mbps == 1)
		regval |= SYSREG_MAR_CR_SPEED_100;
	else if (priv->mbps == 2)
		regval |= SYSREG_MAR_CR_SPEED_1000;
	putreg32(regval, SF2_SYSREG_MAC_CR);

	/* Configure duplex mode */
	uint32_t regval1;
	regval = getreg32(SF2_ETH_CFG2);
	regval1 = getreg32(SF2_ETH_FIFO_CFG5);
	if (priv->fduplex) 
	{
		regval |= ETH_CFG2_FULL_DUPLEX;
		regval1 &= ~ETH_FIFO_CFG5_CFGHDPLX;
	} 
	else 
	{
		regval &= ~ETH_CFG2_FULL_DUPLEX;
		regval1 |= ETH_FIFO_CFG5_CFGHDPLX;    
	}
	putreg32(regval, SF2_ETH_CFG2);
	putreg32(regval1, SF2_ETH_FIFO_CFG5);
}

static void sf2_macaddress(FAR struct sf2_ethmac_s *priv) {
	FAR struct net_driver_s *dev = &priv->dev;
	uint32_t regval;

	regval = ((uint32_t)dev->d_mac.ether.ether_addr_octet[1] << 24) |
		((uint32_t)dev->d_mac.ether.ether_addr_octet[0] << 16);
	putreg32(regval, SF2_ETH_STATION_ADDRESS2);

	regval = ((uint32_t)dev->d_mac.ether.ether_addr_octet[5] << 24) |
		((uint32_t)dev->d_mac.ether.ether_addr_octet[4] << 16) |
		((uint32_t)dev->d_mac.ether.ether_addr_octet[3] << 8) |
		(uint32_t)dev->d_mac.ether.ether_addr_octet[2];
	putreg32(regval, SF2_ETH_STATION_ADDRESS1);
}

static int sf2_macenable(FAR struct sf2_ethmac_s *priv) {
	uint32_t regval;

	putreg32(0, SF2_SYSREG_MAC_STAT_CLRONRD_CR);
	putreg32(1, SF2_SYSREG_MAC_STAT_CLR_CR);
	putreg32(0, SF2_SYSREG_MAC_STAT_CLR_CR);

	// enable mac-phy
	regval = getreg32(SF2_ETH_CFG1);
	regval |= (ETH_CFG1_TX_EN | ETH_CFG1_RX_EN);
	putreg32(regval, SF2_ETH_CFG1);

	regval = getreg32(SF2_ETH_DMA_IRQ_MASK);
	regval |= 1 << 0;
	regval |= 1 << 4;
	putreg32(regval, SF2_ETH_DMA_IRQ_MASK);


	// enable dma  rx interrupt,
	regval = ETH_DMA_RX_STATUS_RXOVERFLOW;   
	putreg32(regval, SF2_ETH_DMA_RX_STATUS); 

	// enable dma-mac
	putreg32(0x01, SF2_ETH_DMA_TX_CTRL);
	putreg32(0x01, SF2_ETH_DMA_RX_CTRL);

	// // enable mac-phy
	// regval = getreg32(SF2_ETH_CFG1);
	// regval |= (ETH_CFG1_TX_EN | ETH_CFG1_RX_EN);
	// putreg32(regval, SF2_ETH_CFG1);

	// // enable dma-mac
	// putreg32(0x01, SF2_ETH_DMA_TX_CTRL);
	// putreg32(0x01, SF2_ETH_DMA_RX_CTRL);

	// // clear tx/rxdma_status bits, clear pending interrupt
	// putreg32(0, SF2_ETH_DMA_IRQ);
	// putreg32(0, SF2_ETH_DMA_TX_STATUS);
	// putreg32(0, SF2_ETH_DMA_RX_STATUS);

	// // enable rx interrupt,
	// regval = ETH_DMA_RX_STATUS_RXOVERFLOW;   // ???
	// putreg32(regval, SF2_ETH_DMA_RX_STATUS); //
	// regval = ETH_DMA_IRQ_MASK_RXPKTRECEIVED;
	// putreg32(regval, SF2_ETH_DMA_IRQ_MASK);

	return OK;
}

#if defined(CONFIG_SF2_PHY_CONFIG) || defined(CONFIG_SF2_RGMII)
static void sf2_phywrite(uint16_t phyaddr, uint16_t regaddr, uint16_t value) {
	uint32_t regval;

	do {
		regval = getreg32(SF2_ETH_MII_INDICATORS);
	} while (regval & ETH_MII_IND_BUSY);

	regval = (regaddr << ETH_MII_ADDR_REGADDR_SHIFT) & ETH_MII_ADDR_REGADDR_MASK;
	regval |= (phyaddr << ETH_MII_ADDR_PHYADDR_SHIFT) & ETH_MII_ADDR_PHYADDR_MASK;
	putreg32(regval, SF2_ETH_MII_ADDRESS);

	putreg32(value, SF2_ETH_MII_CTRL);

	do {
		regval = getreg32(SF2_ETH_MII_INDICATORS);
	} while (regval & ETH_MII_IND_BUSY);
}

static int sf2_phyread(uint16_t phyaddr, uint16_t regaddr, uint16_t *value) {
	uint32_t regval;
	uint32_t timeout = 100000;

	do {
		regval = getreg32(SF2_ETH_MII_INDICATORS);
	} while (regval & ETH_MII_IND_BUSY);

	regval = (regaddr << ETH_MII_ADDR_REGADDR_SHIFT) & ETH_MII_ADDR_REGADDR_MASK;
	regval |= (phyaddr << ETH_MII_ADDR_PHYADDR_SHIFT) & ETH_MII_ADDR_PHYADDR_MASK;
	putreg32(regval, SF2_ETH_MII_ADDRESS);

	regval = getreg32(SF2_ETH_MII_COMMAND);
	regval |= ETH_MII_CMD_READ_CYCLE;
	putreg32(regval, SF2_ETH_MII_COMMAND);

	do {
		regval = getreg32(SF2_ETH_MII_INDICATORS);
		timeout--;
	} while (timeout && (regval & (ETH_MII_IND_BUSY | ETH_MII_IND_NOT_VALID)));

	regval = getreg32(SF2_ETH_MII_COMMAND);
	regval &= ~ETH_MII_CMD_READ_CYCLE;
	putreg32(regval, SF2_ETH_MII_COMMAND);

	if (timeout != 0) {
		*value = getreg32(SF2_ETH_MII_STATUS);
		return OK;
	}

	return -ETIMEDOUT;
}

// static void sf2_phydelay(void)
//{
// 	/* control data pad skew - devaddr = 0x02, register = 0x04 */
// 	sf2_phywrite_mmc(0x2, 0x4, 0x0000);
//
// 	/* rx data pad skew - devaddr = 0x02, register = 0x05 */
// 	sf2_phywrite_mmc(0x2, 0x5, 0x0000);
//
// 	/* tx data pad skew - devaddr = 0x02, register = 0x06 */
// 	sf2_phywrite_mmc(0x2, 0x6, 0x0000);
//
// 	/* gtx and rx clock pad skew - devaddr = 0x02, register = 0x08 */
// 	sf2_phywrite_mmc(0x2, 0x8, 0x03ff);
//}

static int sf2_phyreset(struct sf2_ethmac_s *priv) {
	int timeout;
	int ret;
	uint16_t regval = 0;

	/* Reset the PHY. */
	ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_MCR, &regval);
	if (ret < 0)
		return ret;
	sf2_phywrite(CONFIG_SF2_PHYADDR, MII_MCR, regval | MII_MCR_RESET);

	for (timeout = 0; timeout < 10; timeout++) {
		regval = 0;
		ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_MCR, &regval);
		if ((regval & MII_MCR_RESET) == 0) {
			ret = OK;
			break;
		}
	}
	regval = 0;
	/* Power up the PHY. */
	ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_MCR, &regval);
	if (ret < 0)
		return ret;
	sf2_phywrite(CONFIG_SF2_PHYADDR, MII_MCR, regval & (~MII_MCR_PDOWN));
	return ret;
}

#ifdef CONFIG_SF2_PHY_AUTONEG
static int sf2_autonegotiate(struct sf2_ethmac_s *priv) {
	uint16_t phyval;
	uint16_t advertise;
	uint16_t lpa;

	int timeout;
	int ret;

#ifdef CONFIG_ETH0_PHY_KSZ90x1
	{
		uint16_t id1, id2;

		sf2_phyread(CONFIG_SF2_PHYADDR, 2, &id1);
		sf2_phyread(CONFIG_SF2_PHYADDR, 3, &id2);

		if ((id1 != 0x0022) || ((id2 & 0xfff0) != 0x1620)) {
			/* err */
		}
	}
#endif

	advertise = MII_ADVERTISE_100BASETXFULL | MII_ADVERTISE_100BASETXHALF |
		MII_ADVERTISE_10BASETXFULL | MII_ADVERTISE_10BASETXHALF |
		MII_ADVERTISE_8023;
	sf2_phywrite(CONFIG_SF2_PHYADDR, MII_ADVERTISE, advertise);

#if defined CONFIG_SF2_GMII 
	uint16_t btcr;
	uint16_t btsr;
	ret = sf2_phyread(CONFIG_SF2_PHYADDR, GMII_1000BTCR, &btcr);
	if (ret < 0)
		goto errout;

	btcr |= GMII_1000BTCR_1000BASETFULL | GMII_1000BTCR_1000BASETHALF;
	sf2_phywrite(CONFIG_SF2_PHYADDR, GMII_1000BTCR, btcr);
#endif

	/* Restart Auto_negotiation */
	ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_MCR, &phyval);
	if (ret < 0)
		goto errout;

	phyval |= (MII_MCR_ANENABLE | MII_MCR_ANRESTART);
	phyval &= ~MII_MCR_ISOLATE;
	sf2_phywrite(CONFIG_SF2_PHYADDR, MII_MCR, phyval);

	/* Wait for autonegotion to complete */
	timeout = 0;
	for (;;) {
		ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_MSR, &phyval);
		if (ret < 0)
			goto errout;

		if ((phyval & MII_MSR_ANEGCOMPLETE) != 0)
			break;

		if (++timeout >= PHY_RETRY_MAX) {

			ret = -ETIMEDOUT;
			goto errout;
		}
	}

	priv->mbps = 0;    /* 10M */
	priv->fduplex = 0; /* Half */
	timeout = 0;

	for (;;) {
#if defined CONFIG_SF2_GMII 
		ret = sf2_phyread(CONFIG_SF2_PHYADDR, GMII_1000BTSR, &btsr);
		if (ret < 0)
			goto errout;

		if ((btsr & GMII_1000BTSR_LP1000BASETFULL) != 0 &&
				(btcr & GMII_1000BTCR_1000BASETHALF) != 0) {

			priv->mbps = 2;    /* 1000M */
			priv->fduplex = 1; /* Full */
			break;
		} else if ((btsr & GMII_1000BTSR_LP1000BASETHALF) != 0 &&
				(btcr & GMII_1000BTCR_1000BASETFULL) != 0) {

			priv->mbps = 2;    /* 1000M */
			priv->fduplex = 0; /* Half */
			break;
		}
#else
		ret = sf2_phyread(CONFIG_SF2_PHYADDR, MII_LPA, &lpa);
		if (ret < 0)
			goto errout;

		if ((advertise & MII_ADVERTISE_100BASETXFULL) != 0 &&
				(lpa & MII_LPA_100BASETXFULL) != 0) {
			priv->mbps = 1;
			priv->fduplex = 1;
			break;
		} else if ((advertise & MII_ADVERTISE_10BASETXFULL) != 0 &&
				(lpa & MII_LPA_10BASETXFULL) != 0) {
			priv->mbps = 0;
			priv->fduplex = 1;
			break;
		} else if ((advertise & MII_ADVERTISE_100BASETXHALF) != 0 &&
				(lpa & MII_LPA_100BASETXHALF) != 0) {
			priv->mbps = 1;
			priv->fduplex = 0;
			break;
		} else if ((advertise & MII_ADVERTISE_10BASETXHALF) != 0 &&
				(lpa & MII_LPA_10BASETXHALF) != 0) {
			break;
		}
#endif

		/* Check for a timeout */
		if (++timeout >= PHY_RETRY_MAX) {
			ret = -ETIMEDOUT;
			goto errout;
		}
	}

errout:
	return ret;
}
#endif

static int sf2_phyconfig(struct sf2_ethmac_s *priv) {
	int ret = OK;

	ret = sf2_phyreset(priv);
	if (ret < 0)
		return ret;

	//	sf2_phydelay();  /* pad skew */

#ifdef CONFIG_SF2_PHY_AUTONEG
	ret = sf2_autonegotiate(priv);
#else
	priv->fduplex = CONFIG_SF2_ETHMODE;
	priv->mbps = CONFIG_SF2_ETHMBPS;

	uint32_t phyval = 0;
	if (priv->fduplex)
		phyval |= MII_MCR_FULLDPLX;
	if (priv->fduplex == 1)
		phyval |= MII_MCR_SPEED100;
	sf2_phywrite(CONFIG_SF2_PHYADDR, MII_MCR, phyval);
#endif

	return ret;
}
#endif  //CONFIG_SF2_PHY_CONFIG

static int sf2_ethconfig(FAR struct sf2_ethmac_s *priv) {
	/* eth reset */
	sf2_ethreset(priv);

	/* Initialize the MAC and DMA */
	sf2_macconfig(priv);

	/* Set the MAC address */
	sf2_macaddress(priv);

	/* Initialize the free buffer list */
	sf2_initbuffer(priv);

	/* Initialize TX Descriptors list: Chain Mode */
	sf2_txdescinit(priv);

	/* Initialize RX Descriptors list: Chain Mode  */
	sf2_rxdescinit(priv);

	/* PHY config */
#ifdef CONFIG_SF2_PHY_CONFIG
	int ret;
	ret = sf2_phyconfig(priv);
	if (ret != OK)
		return ret;
	up_mdelay(50);
#else
	priv->fduplex = CONFIG_SF2_ETHMODE;
	priv->mbps = CONFIG_SF2_ETHMBPS;
#endif

	sf2_up_macconfig(priv);

	/* Enable normal MAC operation */
	return sf2_macenable(priv);
}

static inline int sf2_ethinitialize(int intf) {
	struct sf2_ethmac_s *priv;

	ninfo("intf: %d\n", intf);

	/* Get the interface structure associated with this interface number. */

	DEBUGASSERT(intf < SF2_NETHERNET);
	priv = &g_sf2ethmac[intf];

	/* Initialize the driver structure */

	memset(priv, 0, sizeof(struct sf2_ethmac_s));
	priv->dev.d_ifup = sf2_ifup;       /* I/F up (new IP address) callback */
	priv->dev.d_ifdown = sf2_ifdown;   /* I/F down callback */
	priv->dev.d_txavail = sf2_txavail; /* New TX data callback */

	priv->dev.d_private =
		(void *)g_sf2ethmac; /* Used to recover private state from dev */

	/* Create a watchdog for timing polling for and timing of transmisstions */

	priv->txpoll = wd_create();    /* Create periodic poll timer */
	priv->txtimeout = wd_create(); /* Create TX timeout timer */

	/* Attach the IRQ to the driver */
	if (irq_attach(SF2_IRQ_ETHERNETMAC, sf2_interrupt, NULL)) {

		/* We could not attach the ISR to the interrupt */
		return -EAGAIN;
	}

	/* Put the interface in the down state. */
	sf2_ifdown(&priv->dev);

	/* Register the device with the OS so that socket IOCTLs can be performed */
	(void)netdev_register(&priv->dev, NET_LL_ETHERNET);
	return OK;
}

/****************************************************************************
 * Function: up_netinitialize
 *
 * Description:
 *   This is the "standard" network initialization logic called from the
 *   low-level initialization logic in up_initialize.c.  If SF2_NETHERNET
 *   greater than one, then board specific logic will have to supply a
 *   version of up_netinitialize() that calls sf2_ethinitialize() with
 *   the appropriate interface number.
 *
 * Parameters:
 *   None.
 *
 * Returned Value:
 *   None.
 *
 * Assumptions:
 *
 ****************************************************************************/

#if SF2_NETHERNET == 1
void up_netinitialize(void) { (void)sf2_ethinitialize(0); }
#endif

#endif /* SF2_NETHERNET > 0 */
#else
void up_netinitialize(void) {}
#endif
