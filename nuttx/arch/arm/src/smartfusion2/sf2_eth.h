
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_ETH_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_ETH_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"

#if SF2_NETHERNET > 0


#include "chip/sf2_eth.h"



#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

/*
*/


#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* SF2_NETHERNET >0 */
#endif /* __ARCH_ARM_SRC_SMARTFUSION2_SF2_ETH_H */

