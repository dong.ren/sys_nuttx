
#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_CAN_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_CAN_H

#include <nuttx/config.h>

#include "chip.h"
#include "chip/sf2_can.h"

#include <nuttx/can/can.h>

#if SF2_NCAN < 1
#  undef CONFIG_SF2_CAN
#endif

#if defined(CONFIG_CAN) && defined(CONFIG_SF2_CAN)

/* CAN BAUD */

#if defined(CONFIG_SF2_CAN) && !defined(CONFIG_CAN_BAUD)
#  error "CONFIG_CAN_BAUD is not defined"
#endif

#ifndef CONFIG_CAN_TSEG1
#  define CONFIG_CAN_TSEG1 6
#endif

#if CONFIG_CAN_TSEG1 < 1 || CONFIG_CAN_TSEG1 > CAN_CFG_TSEG1_MAX
#  errror "CONFIG_CAN_TSEG1 is out of range"
#endif

#ifndef CONFIG_CAN_TSEG2
#  define CONFIG_CAN_TSEG2 7
#endif

#if CONFIG_CAN_TSEG2 < 1 || CONFIG_CAN_TSEG2 > CAN_CFG_TSEG2_MAX
#  errror "CONFIG_CAN_TSEG2 is out of range"
#endif

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

struct can_dev_s;
FAR struct can_dev_s *sf2_caninitialize(int port);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* CONFIG_CAN && CONFIG_SF2_CAN */
#endif /* __ARCH_ARM_SRC_SMARTFUSION_SF2_CAN_H */
