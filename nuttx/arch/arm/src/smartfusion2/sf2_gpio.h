#ifndef __ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H
#define __ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"
#include "chip/sf2_gpio.h"


#define SF2_GPIO_INPUT_MODE              0x0000000002uL
#define SF2_GPIO_OUTPUT_MODE             0x0000000005uL
#define SF2_GPIO_INOUT_MODE              0x0000000003uL


/* bit 0 - 2 */

#define GPIO_MODE_SHIFT    (0)

#define GPIO_MODE_MASK     (7 << GPIO_MODE_SHIFT)
#define GPIO_MODE_INPUT    (2 << GPIO_MODE_SHIFT)
#define GPIO_MODE_OUTPUT   (5 << GPIO_MODE_SHIFT)
#define GPIO_MODE_INOUT    (3 << GPIO_MODE_SHIFT)

/* bit 3 */

#define GPIO_INTERRUPT     (1 << 3)

/* bit 5 - 7 */



extern void sf2_gpio_config(uint8_t port_id, uint32_t config);
extern void sf2_gpio_write(uint8_t port_id, uint8_t value);
extern uint8_t sf2_gpio_read(uint32_t port_id);

#endif /*__ARCH_ARM_SRC_SMARTFUSION2_SF2_GPIO_H */
