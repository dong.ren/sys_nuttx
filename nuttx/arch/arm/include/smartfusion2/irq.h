

#ifndef __ARCH_ARM_INCLUDE_SMARTFUSION2_IRQ_H
#define __ARCH_ARM_INCLUDE_SMARTFUSION2_IRQ_H


#include <nuttx/config.h>
#include <nuttx/irq.h>
#include <arch/smartfusion2/chip.h>

#define NR_IRQS                  (97)

#define SF2_IRQ_RESERVED         (0)
#define SF2_IRQ_NMI              (2)
#define SF2_IRQ_HARDFAULT        (3)
#define SF2_IRQ_MEMFAULT         (4)
#define SF2_IRQ_BUSFAULT         (5)
#define SF2_IRQ_USAGEFAULT       (6)
#define SF2_IRQ_SVCALL           (11)
#define SF2_IRQ_DBGMONITOR       (12)
#define SF2_IRQ_PENDSV           (14)
#define SF2_IRQ_SYSTICK          (15)

#define SF2_IRQ_INTERRUPTS       (16)

#define SF2_IRQ_WWDG             (16)
#define SF2_IRQ_RTC              (17)
#define SF2_IRQ_SPI0             (18)
#define SF2_IRQ_SPI1             (19)
#define SF2_IRQ_I2C0             (20)
#define SF2_IRQ_I2C0_SMBALERT    (21)
#define SF2_IRQ_I2C0_SMBUS       (22)
#define SF2_IRQ_I2C1             (23)
#define SF2_IRQ_I2C1_SMBALERT    (24)
#define SF2_IRQ_I2C1_SMBUS       (25)
#define SF2_IRQ_UART0            (26)   /* UART0 interrupt */
#define SF2_IRQ_UART1            (27)   /* UART1 interrupt */
#define SF2_IRQ_ETHERNETMAC      (28)   /* ethernet mac */
#define SF2_IRQ_PDMA             (29)
#define SF2_IRQ_TIMER1           (30)
#define SF2_IRQ_TIMER2           (31)
#define SF2_IRQ_CAN              (32)
#define SF2_IRQ_ENVM0            (33)
#define SF2_IRQ_ENVM1            (34)
#define SF2_IRQ_COMBLK           (35)
#define SF2_IRQ_USB              (36)
#define SF2_IRQ_USB_DMA          (37)
#define SF2_IRQ_PLL_LOCK         (38)
#define SF2_IRQ_PLL_LOCKLOST     (39)
#define SF2_IRQ_COMMSWITCHERROR  (40)
#define SF2_IRQ_CACHEERR         (41)
#define SF2_IRQ_DDR              (42)
#define SF2_IRQ_HPDMA_COMPLETE   (43)
#define SF2_IRQ_HPDMA_ERROR      (44)
#define SF2_IRQ_ECC_ERROR        (45)
#define SF2_IRQ_MDDR_IOCALIB     (46)
#define SF2_IRQ_FAB_PLL_LOCK     (47)
#define SF2_IRQ_FAB_PLL_LOCKLOST (48)
#define SF2_IRQ_FIC64            (49)
#define SF2_IRQ_FABRICIRQ0       (50)   /* FPGA fabric interrupt 0                 */
#define SF2_IRQ_FABRICIRQ1       (51)   /* FPGA fabric interrupt 1                 */
#define SF2_IRQ_FABRICIRQ2       (52)   /* FPGA fabric interrupt 2                 */
#define SF2_IRQ_FABRICIRQ3       (53)   /* FPGA fabric interrupt 3                 */
#define SF2_IRQ_FABRICIRQ4       (54)   /* FPGA fabric interrupt 4                 */
#define SF2_IRQ_FABRICIRQ5       (55)   /* FPGA fabric interrupt 5                 */
#define SF2_IRQ_FABRICIRQ6       (56)   /* FPGA fabric interrupt 6                 */
#define SF2_IRQ_FABRICIRQ7       (57)   /* FPGA fabric interrupt 7                 */
#define SF2_IRQ_FABRICIRQ8       (58)   /* FPGA fabric interrupt 8                 */
#define SF2_IRQ_FABRICIRQ9       (59)   /* FPGA fabric interrupt 9                 */
#define SF2_IRQ_FABRICIRQ10      (60)   /* FPGA fabric interrupt 10                */
#define SF2_IRQ_FABRICIRQ11      (61)   /* FPGA fabric interrupt 11                */
#define SF2_IRQ_FABRICIRQ12      (62)   /* FPGA fabric interrupt 12                */
#define SF2_IRQ_FABRICIRQ13      (63)   /* FPGA fabric interrupt 13                */
#define SF2_IRQ_FABRICIRQ14      (64)   /* FPGA fabric interrupt 14                */
#define SF2_IRQ_FABRICIRQ15      (65)   /* FPGA fabric interrupt 15                */
#define SF2_IRQ_GPIO0            (66)   /* GPIO 0 interrupt                        */
#define SF2_IRQ_GPIO1            (67)   /* GPIO 1 interrupt                        */
#define SF2_IRQ_GPIO2            (68)   /* GPIO 2 interrupt                        */
#define SF2_IRQ_GPIO3            (69)   /* GPIO 3 interrupt                        */
#define SF2_IRQ_GPIO4            (70)   /* GPIO 4 interrupt                        */
#define SF2_IRQ_GPIO5            (71)   /* GPIO 5 interrupt                        */
#define SF2_IRQ_GPIO6            (72)   /* GPIO 6 interrupt                        */
#define SF2_IRQ_GPIO7            (73)   /* GPIO 7 interrupt                        */
#define SF2_IRQ_GPIO8            (74)   /* GPIO 8 interrupt                        */
#define SF2_IRQ_GPIO9            (75)   /* GPIO 9 interrupt                        */
#define SF2_IRQ_GPIO10           (76)   /* GPIO 10 interrupt                       */
#define SF2_IRQ_GPIO11           (77)   /* GPIO 11 interrupt                       */
#define SF2_IRQ_GPIO12           (78)   /* GPIO 12 interrupt                       */
#define SF2_IRQ_GPIO13           (79)   /* GPIO 13 interrupt                       */
#define SF2_IRQ_GPIO14           (80)   /* GPIO 14 interrupt                       */
#define SF2_IRQ_GPIO15           (81)   /* GPIO 15 interrupt                       */
#define SF2_IRQ_GPIO16           (82)   /* GPIO 16 interrupt                       */
#define SF2_IRQ_GPIO17           (83)   /* GPIO 17 interrupt                       */
#define SF2_IRQ_GPIO18           (84)   /* GPIO 18 interrupt                       */
#define SF2_IRQ_GPIO19           (85)   /* GPIO 19 interrupt                       */
#define SF2_IRQ_GPIO20           (86)   /* GPIO 20 interrupt                       */
#define SF2_IRQ_GPIO21           (87)   /* GPIO 21 interrupt                       */
#define SF2_IRQ_GPIO22           (88)   /* GPIO 22 interrupt                       */
#define SF2_IRQ_GPIO23           (89)   /* GPIO 23 interrupt                       */
#define SF2_IRQ_GPIO24           (90)   /* GPIO 24 interrupt                       */
#define SF2_IRQ_GPIO25           (91)   /* GPIO 25 interrupt                       */
#define SF2_IRQ_GPIO26           (92)   /* GPIO 26 interrupt                       */
#define SF2_IRQ_GPIO27           (93)   /* GPIO 27 interrupt                       */
#define SF2_IRQ_GPIO28           (94)   /* GPIO 28 interrupt                       */
#define SF2_IRQ_GPIO29           (95)   /* GPIO 29 interrupt                       */
#define SF2_IRQ_GPIO30           (96)   /* GPIO 30 interrupt                       */
#define SF2_IRQ_GPIO31           (97)   /* GPIO 31 interrupt                       */


#ifndef __ASSEMBLY__
#ifdef __cplusplus
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif




#undef EXTERN
#ifdef __cplusplus
}
#endif
#endif

#endif /* __ARCH_ARM_INCLUDE_SF2_IRQ_H */
