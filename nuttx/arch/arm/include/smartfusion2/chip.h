

#ifndef __ARCH_ARM_INCLUDE_SMARTFUSION2_CHIP_H
#define __ARCH_ARM_INCLUDE_SMARTFUSION2_CHIP_H

#include <nuttx/config.h>

#define SF2_NGPIO 32    /* GPIO0-GPIO31 */
#define SF2_NUART 2     /* UART0-1 */
#define SF2_NETHERNET 1 /* Ethernet MAC */
#define SF2_NCAN 1

#define ARMV7M_PERIPHERAL_INTERRUPTS 82
#define NR_VECTORS ARMV7M_PERIPHERAL_INTERRUPTS

/* NVIC priority levels */

#define NVIC_SYSH_PRIORITY_MIN 0xf0     /* All bits set in minimum priority */
#define NVIC_SYSH_PRIORITY_DEFAULT 0x80 /* Midpoint is the default */
#define NVIC_SYSH_PRIORITY_MAX 0x00     /* Zero is maximum priority */
#define NVIC_SYSH_PRIORITY_STEP                                                \
  0x10 /* Four bits of interrupt priority used                                 \
          */

#if defined(CONFIG_ARCH_HIPRI_INTERRUPT) && defined(CONFIG_ARCH_INT_DISABLEALL)
#define NVIC_SYSH_MAXNORMAL_PRIORITY                                           \
  (NVIC_SYSH_PRIORITY_MAX + 2 * NVIC_SYSH_PRIORITY_STEP)
#define NVIC_SYSH_HIGH_PRIORITY                                                \
  (NVIC_SYSH_PRIORITY_MAX + NVIC_SYSH_PRIORITY_STEP)
#define NVIC_SYSH_DISABLE_PRIORITY NVIC_SYSH_HIGH_PRIORITY
#define NVIC_SYSH_SVCALL_PRIORITY NVIC_SYSH_PRIORITY_MAX
#else
#define NVIC_SYSH_MAXNORMAL_PRIORITY                                           \
  (NVIC_SYSH_PRIORITY_MAX + NVIC_SYSH_PRIORITY_STEP)
#define NVIC_SYSH_HIGH_PRIORITY NVIC_SYSH_PRIORITY_MAX
#define NVIC_SYSH_DISABLE_PRIORITY NVIC_SYSH_MAXNORMAL_PRIORITY
#define NVIC_SYSH_SVCALL_PRIORITY NVIC_SYSH_PRIORITY_MAX
#endif

#endif /* __ARCH_ARM_INCLUDE_SMARTFUSION2_CHIP_H */
