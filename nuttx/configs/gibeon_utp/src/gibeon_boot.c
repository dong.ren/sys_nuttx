/************************************************************************************
 * configs/viewtool-stm32f107/src/stm32_boot.c
 *
 *   Copyright (C) 2013 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include <nuttx/compiler.h>
#include <stdio.h>
#include <fcntl.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <sys/ioctl.h>
#include <nuttx/mtd/mtd.h>

#include "stm32_adc.h"
#include "stm32_gpio.h"
/************************************************************************************
 * Public Functions
 ************************************************************************************/

const static uint32_t g_adc_pinlist[] = {GPIO_ADC1_IN0, GPIO_ADC1_IN1, GPIO_ADC1_IN2, GPIO_ADC1_IN3,
                                         GPIO_ADC1_IN4, GPIO_ADC1_IN5, GPIO_ADC1_IN6, GPIO_ADC1_IN7,
                                         GPIO_ADC1_IN8, GPIO_ADC1_IN9, GPIO_ADC1_IN10, GPIO_ADC1_IN11,
                                         GPIO_ADC1_IN12, GPIO_ADC1_IN13, GPIO_ADC1_IN14, GPIO_ADC1_IN15};

const static uint8_t g_adc_chnlist[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

static void adc_initialize(void)
{
    for (int i = 0; i < 16; i++)
    {
        stm32_configgpio(g_adc_pinlist[i]);
    }
    struct adc_dev_s *p_adc = stm32_adcinitialize(1, g_adc_chnlist, 16);
    adc_register("/dev/adc", p_adc);
}

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry
 *point
 *   is called early in the intitialization -- after all memory has been
 *configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
}

void board_initialize(void)
{
    adc_initialize();

    struct mtd_dev_s *pmtd = progmem_initialize();
    if (pmtd != NULL)
    {
        ftl_initialize(0, pmtd);
    }
}

int weak_function main(void)
{
    struct adc_msg_s buffer_adc[16];
    int fd = -1;
    printf("Hello from Dummy Main!\n");

    fd = open("/dev/adc", O_RDONLY);

    for (;;)
    {
        ioctl(fd, ANIOC_TRIGGER);
        int ret = read(fd, (void *)buffer_adc, 80);
        ret /= 5;
        for (int i = 0; i < ret; i++)
        {
            printf("Channel : %d,Voltage:%d\n", buffer_adc[i].am_channel, buffer_adc[i].am_data);
        }
        usleep(500000);
        printf("Testing ... \n");
    }

    return 0;
}
