#include <arch/board/board.h>
#include <debug.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <nuttx/board.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/fs/smart.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/spi/spi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stm32_spi.h>
#include <sys/mount.h>
#include <errno.h>

#define SPI_PORT 4

extern int mksmartfs(FAR const char *pathname, uint16_t sectorsize);

void w25_test(void)
{
  // flash_format();
  int ret = open("/mnt/smart/test",O_RDWR | O_CREAT);
  if(ret>0)
  {
    printf("open success\n");
  }else 
  {
    printf("open failed %d\n",errno);
  }
}

bool flash_format(void)
{
  int ret;

  (void)mksmartfs("/dev/smart0", 1024);
  ret = mount("/dev/smart0", "/mnt/smart", "smartfs", 0, NULL);
  if (ret < 0)
  {
    printf("ERROR: Failed to mount the SMART volume: %d\n", errno);
    fflush(stdout);
    return false;
  }else 
  {
    printf("format success\n");
    return true;
  }
}

int gd_w25initialize(void)
{
  struct spi_dev_s *spi;
  struct mtd_dev_s *mtd;
  int ret;

  spi = stm32_spibus_initialize(SPI_PORT);

  if (!spi)
  {
    printf("spi failed\n");
    return -1;
  }
  mtd = w25_initialize(spi);
  if (!mtd)
  {
    printf("w25 failed\n");
    return -1;
  }
  // MTD_IOCTL(mtd, MTDIOC_BULKERASE, 0);

  ret = smart_initialize(0, mtd, NULL);
  if (ret < 0)
  {
    printf("ERROR: SMART initialization failed: %d\n", -ret);
    return -1;
  }

  /* Mount the file system */
  ret = mount("/dev/smart0", "/mnt/smart", "smartfs", 0, NULL);
  if (ret < 0)
  {
    if (errno == 19) //no such device
    {
      (void)mksmartfs("/dev/smart0", 1024);
      ret = mount("/dev/smart0", "/mnt/smart", "smartfs", 0, NULL);
      if (ret < 0)
      {
        printf("ERROR: Failed to mount the SMART volume: %d\n", errno);
        fflush(stdout);
        return -1;
      }
    }
    else
    {
      printf("ERROR: Failed to mount the SMART volume: %d\n", errno);
      fflush(stdout);
      return -1;
    }
  }

  return OK;
}

int smart_directory(void)
{
  DIR *dirp;
  FAR struct dirent *entryp;
  int number;

  /* Open the directory */

  dirp = opendir("/mnt/smart");

  if (!dirp)
  {
    /* Failed to open the directory */

    printf("ERROR: Failed to open directory '%s': %d\n",
           "/mnt/smart", errno);
    return ERROR;
  }

  /* Read each directory entry */

  printf("Directory:\n");
  number = 1;
  do
  {
    entryp = readdir(dirp);
    if (entryp)
    {
      printf("%2d. Type[%d]: %s Name: %s\n",
             number, entryp->d_type,
             entryp->d_type == DTYPE_FILE ? "File " : "Error",
             entryp->d_name);
    }

    number++;
  } while (entryp != NULL);

  closedir(dirp);
  return OK;
}

void testpara(const char *filepath)
{
  printf("%s\n", filepath);
}

int32_t delfile(const char *filepath)
{
  return unlink(filepath);
}

static void smart_showmemusage(struct mallinfo *mmbe)
{
  printf("VARIABLE  BEFORE   AFTER\n");
  printf("======== ======== ========\n");
  printf("arena    %8x %8x\n", mmbe->arena);
  printf("ordblks  %8d %8d\n", mmbe->ordblks);
  printf("uordblks %8x %8x\n", mmbe->uordblks);
  printf("fordblks %8x %8x\n", mmbe->fordblks);
}

static void smart_memusage(void)
{
  struct mallinfo mm;

  mm = mallinfo();

  printf("\nFinal memory usage:\n");
  smart_showmemusage(&mm);
}
