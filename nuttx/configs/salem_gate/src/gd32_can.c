 #include <nuttx/config.h>
 
 #include <errno.h>
 #include <debug.h>
 
 #include <nuttx/can/can.h>
 #include <arch/board/board.h>
 
 #include <fcntl.h>
 #include "chip.h"
 #include "stm32.h"
 #include "stm32_can.h"
#include "stdio.h"


 #define CAN_PORT 1

 int gd32_can_setup(void)
 {

   struct can_dev_s *can;
   int ret;

   can = stm32_caninitialize(CAN_PORT);
   if (can == NULL)
     {
       canerr("ERROR:  Failed to get CAN interface\n");
       return -ENODEV;
     }

   ret = can_register("/dev/can0", can);
   if (ret < 0)
     {
       canerr("ERROR: can_register failed: %d\n", ret);
       return ret;
     }
 
   return OK;
 }
 
void can_test(void)
{
  struct can_msg_s txmsg,rxmsg;
  int msgsize,nbytes;

  txmsg.cm_hdr.ch_id     = 1234;
  txmsg.cm_hdr.ch_rtr    = false;
  txmsg.cm_hdr.ch_dlc    = 8;
  txmsg.cm_hdr.ch_unused = 0;

  for (int i = 0; i < 8; i++)
  {
    txmsg.cm_data[i] = i;
  }
  msgsize = CAN_MSGLEN(8);

  int canfd = open("/dev/can0",O_RDWR);
  if(canfd<0)
  {
      printf("can failed\n");
      close(canfd);
      return ;
  }

  msgsize = CAN_MSGLEN(8);
  nbytes = write(canfd, &txmsg, msgsize);
  printf("send %d\n",nbytes);

  msgsize = sizeof(struct can_msg_s);
  nbytes = read(canfd, &rxmsg, msgsize);
  printf("read %d,id %d\n",nbytes,rxmsg.cm_hdr.ch_id);
}