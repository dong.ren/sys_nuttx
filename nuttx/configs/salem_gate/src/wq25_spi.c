#include <arch/board/board.h>
#include <debug.h>
#include <nuttx/board.h>
#include <nuttx/spi/spi.h>
#include <stm32_spi.h>

void stm32_spi4select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    spiinfo("devid: %d CS: %s\n", (int)devid, selected ? "assert" : "de-assert");
     if (devid == SPIDEV_FLASH(0))
     {
        stm32_gpiowrite(GPIO_SPI4_CS, !selected);
    }
}

uint8_t stm32_spi4status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return 0;
}

int stm32_spi4cmddata(FAR struct spi_dev_s *dev, uint32_t devid, bool cmd)
{
    if (devid == SPIDEV_FLASH(0))
    {
        /* "This is the Data/Command control pad which determines whether the
       *  data bits are data or a command.
       *
       *  A0 = "H": the inputs at D0 to D7 are treated as display data.
       *  A0 = "L": the inputs at D0 to D7 are transferred to the command
       *       registers."
       */
        if (cmd)
        {
            // stm32_gpiowrite(GPIO_OLED_DC, false);
        }
        else
        {
            // stm32_gpiowrite(GPIO_OLED_DC, true);
        }
        return OK;
    }
    return -ENODEV;
}