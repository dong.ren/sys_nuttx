/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "stm32_gpio.h"
#include <arch/board/board.h>
#include <nuttx/clock.h>
#include <nuttx/config.h>
#include <stdbool.h>
#include <sys/types.h>

#define PIN_BUTTON_ESC (GPIO_PULLUP | GPIO_SPEED_50MHz | GPIO_INPUT | GPIO_PORTC | GPIO_PIN10)
#define PIN_BUTTON_MINUS (GPIO_PULLUP | GPIO_SPEED_50MHz | GPIO_INPUT | GPIO_PORTA | GPIO_PIN15)
#define PIN_BUTTON_PLUS (GPIO_PULLUP | GPIO_SPEED_50MHz | GPIO_INPUT | GPIO_PORTE | GPIO_PIN15)
#define PIN_BUTTON_SET (GPIO_PULLUP | GPIO_SPEED_50MHz | GPIO_INPUT | GPIO_PORTE | GPIO_PIN14)

struct buttons_s
{
    uint8_t pre_st_set; /* pre status: 0, low; 1, high  */
    uint8_t pre_st_esc;
    uint8_t pre_st_plus;
    uint8_t pre_st_minus;
    uint32_t ms_set; /* the value is the time in low, uint ms */
    uint32_t ms_esc;
    uint32_t ms_plus;
    uint32_t ms_minus;
    uint32_t ms_now;
};

static struct buttons_s p_buttons;

void buttons_initialize(void)
{
    stm32_configgpio(PIN_BUTTON_SET);
    stm32_configgpio(PIN_BUTTON_ESC);
    stm32_configgpio(PIN_BUTTON_PLUS);
    stm32_configgpio(PIN_BUTTON_MINUS);
    p_buttons.pre_st_set = stm32_gpioread(PIN_BUTTON_SET);
    p_buttons.pre_st_esc = stm32_gpioread(PIN_BUTTON_ESC);
    p_buttons.pre_st_plus = stm32_gpioread(PIN_BUTTON_PLUS);
    p_buttons.pre_st_minus = stm32_gpioread(PIN_BUTTON_MINUS);
}

void buttons_update(void)
{
    uint32_t time_past;
    uint32_t now;
    now = TICK2MSEC(clock_systimer());
    time_past = now - p_buttons.ms_now;
    p_buttons.ms_now = now;

    if (p_buttons.ms_set)
    {
        p_buttons.pre_st_set = 0;
    }
    else
    {
        p_buttons.pre_st_set = 1;
    }
    if (stm32_gpioread(PIN_BUTTON_SET))
    {
        p_buttons.ms_set = 0;
    }
    else
    {
        p_buttons.ms_set += time_past;
    }

    if (p_buttons.ms_esc)
    {
        p_buttons.pre_st_esc = 0;
    }
    else
    {
        p_buttons.pre_st_esc = 1;
    }
    if (stm32_gpioread(PIN_BUTTON_ESC))
    {
        p_buttons.ms_esc = 0;
    }
    else
    {
        p_buttons.ms_esc += time_past;
    }

    if (p_buttons.ms_plus)
    {
        p_buttons.pre_st_plus = 0;
    }
    else
    {
        p_buttons.pre_st_plus = 1;
    }
    if (stm32_gpioread(PIN_BUTTON_PLUS))
    {
        p_buttons.ms_plus = 0;
    }
    else
    {
        p_buttons.ms_plus += time_past;
    }

    if (p_buttons.ms_minus)
    {
        p_buttons.pre_st_minus = 0;
    }
    else
    {
        p_buttons.pre_st_minus = 1;
    }
    if (stm32_gpioread(PIN_BUTTON_MINUS))
    {
        p_buttons.ms_minus = 0;
    }
    else
    {
        p_buttons.ms_minus += time_past;
    }
}
bool button_any(void)
{
    if (p_buttons.ms_set || p_buttons.ms_esc ||
        p_buttons.ms_minus || p_buttons.ms_plus)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
bool button_any_change(void)
{
    if (button_set_up() || button_set_down() ||
        button_esc_up() || button_esc_down() ||
        button_minus_up() || button_minus_down() ||
        button_plus_up() || button_plus_down())
    {
        return TRUE;
    }
    else
        return FALSE;
}
bool button_any_up(void)
{
    if (button_set_up() || button_esc_up() || button_minus_up() || button_plus_up())
    {
        return TRUE;
    }
    else
        return FALSE;
}
bool button_any_down(void)
{
    if (button_set_down() || button_esc_down() ||
        button_minus_down() || button_plus_down())
    {
        return TRUE;
    }
    else
        return FALSE;
}

bool button_set_down(void)
{
    if (p_buttons.pre_st_set && p_buttons.ms_set)
        return TRUE;
    else
        return FALSE;
}
bool button_set_up(void)
{
    if (p_buttons.pre_st_set == 0 && p_buttons.ms_set == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_set_low(void)
{
    if (p_buttons.ms_set)
        return TRUE;
    else
        return FALSE;
}
bool button_set_hi(void)
{
    if (p_buttons.ms_set == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_set_long(uint32_t ms)
{
    if (p_buttons.ms_set >= ms)
        return TRUE;
    else
        return FALSE;
}

bool button_esc_down(void)
{
    if (p_buttons.pre_st_esc && p_buttons.ms_esc)
        return TRUE;
    else
        return FALSE;
}
bool button_esc_up(void)
{
    if (p_buttons.pre_st_esc == 0 && p_buttons.ms_esc == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_esc_low(void)
{
    if (p_buttons.ms_esc)
        return TRUE;
    else
        return FALSE;
}
bool button_esc_hi(void)
{
    if (p_buttons.ms_esc == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_esc_long(uint32_t ms)
{
    if (p_buttons.ms_esc >= ms)
        return TRUE;
    else
        return FALSE;
}

bool button_plus_down(void)
{
    if (p_buttons.pre_st_plus && p_buttons.ms_plus)
        return TRUE;
    else
        return FALSE;
}
bool button_plus_up(void)
{
    if (p_buttons.pre_st_plus == 0 && p_buttons.ms_plus == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_plus_low(void)
{
    if (p_buttons.ms_plus)
        return TRUE;
    else
        return FALSE;
}
bool button_plus_hi(void)
{
    if (p_buttons.ms_plus == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_plus_long(uint32_t ms)
{
    if (p_buttons.ms_plus >= ms)
        return TRUE;
    else
        return FALSE;
}

bool button_minus_down(void)
{
    if (p_buttons.pre_st_minus && p_buttons.ms_minus)
        return TRUE;
    else
        return FALSE;
}
bool button_minus_up(void)
{
    if (p_buttons.pre_st_minus == 0 && p_buttons.ms_minus == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_minus_low(void)
{
    if (p_buttons.ms_minus)
        return TRUE;
    else
        return FALSE;
}
bool button_minus_hi(void)
{
    if (p_buttons.ms_minus == 0)
        return TRUE;
    else
        return FALSE;
}
bool button_minus_long(uint32_t ms)
{
    if (p_buttons.ms_minus >= ms)
        return TRUE;
    else
        return FALSE;
}

/* device test use only */
uint8_t btns_status(void)
{
    uint8_t btn_status = 0;
    btn_status |= stm32_gpioread(PIN_BUTTON_ESC);
    btn_status |= stm32_gpioread(PIN_BUTTON_MINUS) << 1;
    btn_status |= stm32_gpioread(PIN_BUTTON_PLUS) << 2;
    btn_status |= stm32_gpioread(PIN_BUTTON_SET) << 3;
    return btn_status;
}
