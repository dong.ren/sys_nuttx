
#include <nuttx/config.h>
#include <nuttx/nx/nx.h>
#include <nuttx/nx/nxglib.h>
#include <nuttx/nx/nxfonts.h>

extern FAR int qxf_getbitmap(NXHANDLE handle, const uint16_t ch, struct nx_fontbitmap_s *font);

static int utf8_unicode(const uint8_t *ch, uint16_t *unicode)
{
  uint16_t e = 0;
  int n = 0;
  if (*ch >= 0xe0)
  {
    e = (ch[0] & 0x0f) << 12;
    e |= (ch[1] & 0x3f) << 6;
    e |= (ch[2] & 0x3f);
    n = 3;
  }
  else if (*ch >= 0xc0)
  {
    e = (ch[0] & 0x1f) << 6;
    e |= (ch[1] & 0x3f);
    n = 2;
  }
  else
  {
    e = ch[0];
    n = 1;
  }
  *unicode = e;

  return n;
}
static void clear_glyph(uint8_t *glyph, uint8_t fheight, uint8_t fwidth, uint8_t color)
{
  uint8_t row, col;

  for (row = 0; row < fheight; row++)
  {
    for (col = 0; col < fwidth; col++)
    {
      *glyph++ = color;
    }
  }
}
void ui_dis_str(NXHANDLE hbkgd, NXHANDLE hfont, const struct nxgl_point_s *p,
                        const uint8_t *str, uint8_t bcolor, uint8_t fcolor)
{

  const struct nx_font_s *fontmetrics;
  struct nx_fontbitmap_s fbm;
  static uint8_t glyph[32];
  const uint8_t *ptr;
  struct nxgl_rect_s dest;
  struct nxgl_point_s pos;
  uint16_t unicode;
  uint8_t fheight;
  uint8_t fwidth;
  uint8_t fstride;
  void *src[CONFIG_NX_NPLANES];

  fontmetrics = nxf_getfontset(hfont);

  pos.x = p->x;
  pos.y = p->y;
  for (ptr = str; *ptr;)
  {
    ptr += utf8_unicode(ptr, &unicode);
    if (unicode != 32)
    {
      qxf_getbitmap(hfont, unicode, &fbm);
      fwidth = fbm.metric.width + fbm.metric.xoffset;
      fheight = fbm.metric.height + fbm.metric.yoffset;
      fstride = (fwidth + 7) >> 3;
      clear_glyph(glyph, fheight, fwidth, bcolor);
      nxf_convert_1bpp(glyph, fheight, fwidth, fstride, &fbm, fcolor);
      dest.pt1.x = pos.x;
      dest.pt1.y = pos.y;
      dest.pt2.x = pos.x + fwidth - 1;
      dest.pt2.y = pos.y + fheight - 1;
      src[0] = &glyph;
      nx_bitmap(hbkgd, &dest, (const void **)src, &pos, fstride);
      pos.x += fwidth;
    }
    else
    {
      pos.x += fontmetrics->spwidth;
    }
  }
}