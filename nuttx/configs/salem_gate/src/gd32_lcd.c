#include <arch/board/board.h>
#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/lcd/st7565.h>
#include <stdint.h>

static void lcd_reset(FAR struct st7565_lcd_s *lcd, bool on);
static void lcd_select(FAR struct st7565_lcd_s *lcd);
static void lcd_deselect(FAR struct st7565_lcd_s *lcd);
static void lcd_cmddata(FAR struct st7565_lcd_s *lcd, FAR const uint8_t tcmd);
static int lcd_senddata(FAR struct st7565_lcd_s *lcd, FAR const uint8_t *data, int size);
static int lcd_backlight(FAR struct st7565_lcd_s *lcd, int level);

static void st7565r_send_one_data(uint8_t data);
static struct lcd_dev_s *pdev = NULL;

static struct st7565_lcd_s g_lcd = {
    lcd_reset,
    lcd_select,
    lcd_deselect,
    lcd_cmddata,
    lcd_senddata,
    lcd_backlight,
};

int board_lcd_initialize(void)
{
    return 0;
}

struct lcd_dev_s *board_lcd_getdev(int lcdno)
{
    if (NULL == pdev)
    {
        pdev = st7565_initialize(&g_lcd, lcdno);
    }
    return pdev;
}

struct lcd_dev_s *board_graphics_setup(uint32_t devno)
{
    if (NULL == pdev)
    {
        pdev = st7565_initialize(&g_lcd, devno);
    }
    return pdev;
}

void lcd_reset(FAR struct st7565_lcd_s *lcd, bool on)
{
    if (on)
    {
        stm32_gpiowrite(DIS_RST, false);
    }
    else
    {
        stm32_gpiowrite(DIS_RST, true);
    }
}
void lcd_select(FAR struct st7565_lcd_s *lcd)
{
    stm32_gpiowrite(DIS_CS, false);
}
void lcd_deselect(FAR struct st7565_lcd_s *lcd)
{
    stm32_gpiowrite(DIS_CS, true);
}
void lcd_cmddata(FAR struct st7565_lcd_s *lcd, FAR const uint8_t tcmd)
{
    if (tcmd)
    {
        stm32_gpiowrite(DIS_CMDATA, false);
    }
    else
    {
        stm32_gpiowrite(DIS_CMDATA, true);
    }
}
int lcd_senddata(FAR struct st7565_lcd_s *lcd, FAR const uint8_t *data, int size)
{
    for (int i = 0; i < size; i++)
    {
        st7565r_send_one_data(*data);
        data++;
    }
    return size;
}

#define LCD_CM_DISPLAY_ON 0xaf
#define LCD_CM_DISPLAY_OFF 0xae

int lcd_backlight(FAR struct st7565_lcd_s *lcd, int level)
{
    if (level <= 0)
    {
        stm32_gpiowrite(DIS_BK, false);
    }
    else
    {
        stm32_gpiowrite(DIS_BK, true);
    }
    return level;
}

static void st7565r_send_one_data(uint8_t data)
{
    char i = 0;
    for (; i < 8; i++)
    {
        stm32_gpiowrite(DIS_SCK, false);
        if (data & 0x80)
        {
            stm32_gpiowrite(DIS_SDO, true);
        }
        else
        {
            stm32_gpiowrite(DIS_SDO, false);
        }
        stm32_gpiowrite(DIS_SCK, true);
        data = data << 1;
    }
}

void lcd_setbacklight(bool state)
{
    if (state)
    {
        stm32_gpiowrite(DIS_BK, true);
    }
    else
    {
        stm32_gpiowrite(DIS_BK, false);
    }
}