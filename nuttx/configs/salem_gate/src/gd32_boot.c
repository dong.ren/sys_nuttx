
#include <arch/board/board.h>
#include <errno.h>
#include <fcntl.h>
#include <nuttx/board.h>
#include <nuttx/can/can.h>
#include <nuttx/config.h>
#include <stdio.h>
#include "stm32.h"

void stm32_boardinitialize(void)
{
    //LED
    stm32_configgpio(GPIO_LED_B0);
    stm32_configgpio(GPIO_LED_B1);
    //Display
    stm32_configgpio(DIS_RST);
    stm32_configgpio(DIS_CS);
    stm32_configgpio(DIS_CMDATA);
    stm32_configgpio(DIS_SCK);
    stm32_configgpio(DIS_SDO);
    stm32_configgpio(DIS_BK);

    //KEY
    stm32_configgpio(KEY_SET_PIN);
    stm32_configgpio(KEY_ADD_PIN);
    stm32_configgpio(KEY_SUB_PIN);
    stm32_configgpio(KEY_ESC_PIN);

    //Lora
    stm32_configgpio(LORA_STATUS);

    //spi
    stm32_configgpio(GPIO_SPI4_CS);

    //4G
    stm32_configgpio(_4G_STATUS_PIN);
    stm32_configgpio(POWERKEY_PIN);
    stm32_configgpio(NET_PIN);
    stm32_configgpio(STATUS_PIN);
    stm32_configgpio(RESET_4G_PIN);
    stm32_configgpio(RG_ON_OFF_PIN);

    //WIFI
    stm32_configgpio(WIFI_RESET_PIN);

    //CAN

    //
    stm32_configgpio(DI1_PIN);
    stm32_configgpio(DI2_PIN);

    //485
    // stm32_configgpio(TXE_0_PIN);
    // stm32_configgpio(TXE_1_PIN);

}

int board_app_initialize(uintptr_t arg){

}

void board_initialize(void)
{
    gd32_can_setup();
    led_setstate(1,true);
    int ret = gd_w25initialize();
    if(ret<0)
    {
        led_setstate(1,false);
    }else 
    {
        led_setstate(2,true);
    }
    printf("flash init %d\n",ret);
    gd32_adc_setup();
}

int weak_function main(int argc, char **argv)
{
    printf("in main\n");
    // w25_test();
    while (1)
    {
        printf("in main\n");
    }
}

uint32_t key_getstatus(void)
{
    uint32_t key = 0xFFFFFFFF;

    key = key << 1;
    key |= stm32_gpioread(KEY_SET_PIN);
    key = key << 1;
    key |= stm32_gpioread(KEY_ADD_PIN);
    key = key << 1;
    key |= stm32_gpioread(KEY_SUB_PIN);
    key = key << 1;
    key |= stm32_gpioread(KEY_ESC_PIN);

    return ~key;
}

void keytest(void)
{
    int key = 0;
    key = key_getstatus();
    // clean_screen(0);
    switch (key)
    {
    case 1:
        usleep(50000);
        stm32_gpiowrite(GPIO_LED_B0, false);
        stm32_gpiowrite(GPIO_LED_B1, false);
        usleep(50000);
        break;
    case 2:
        usleep(100000);
        stm32_gpiowrite(GPIO_LED_B0, false);
        stm32_gpiowrite(GPIO_LED_B1, false);
        usleep(100000);
        break;
    case 4:
        usleep(500000);
        stm32_gpiowrite(GPIO_LED_B0, false);
        stm32_gpiowrite(GPIO_LED_B1, false);
        usleep(500000);
        break;
    case 8:
        usleep(1000000);
        stm32_gpiowrite(GPIO_LED_B0, false);
        stm32_gpiowrite(GPIO_LED_B1, false);
        usleep(1000000);
        break;
    }
    stm32_gpiowrite(GPIO_LED_B0, true);
    stm32_gpiowrite(GPIO_LED_B1, true);
}

void led_setstate(uint8_t lednum, bool state)
{
    uint32_t led;
    switch (lednum)
    {
    case 1:
        led = GPIO_LED_B0;
        break;
    case 2:
        led = GPIO_LED_B1;
        break;
    default:
        return;
        break;
    }
    stm32_gpiowrite(led, state);
}

int8_t get_status(uint16_t sta)
{
    uint32_t pinx=0;
    switch (sta)
    {
    case STATUS1:
        pinx = STATUS1_PIN;
        break;
    case STATUS2:
        pinx = STATUS2_PIN;
        break;
    case LORA_STATUS:
        pinx = LORA_STATUS_PIN;
        break;
    case _4G_STATUS:
        pinx = _4G_STATUS_PIN;
        break;
    case STATUS: 
        pinx = STATUS_PIN;
    break;
    case NET: 
        pinx = NET_PIN;
    break;
    default:
        return -1;
        break;
    }
    return stm32_gpioread(pinx);
}

void set_status(uint16_t num,bool sta)
{
    uint32_t pinx;
    switch(num)
    {
        case POWERKEY_4G: 
            pinx = POWERKEY_PIN;
        break;
        case RESET_4G: 
            pinx = RESET_4G_PIN;
        break;
        case RG_ON_OFF: 
            pinx = RG_ON_OFF_PIN;
        break;
        case WIFI_RESET:
            pinx = WIFI_RESET_PIN;
        break;
        default: 
            return;
        break;
    }
     stm32_gpiowrite(pinx,sta);
}

uint32_t di_getstatus(void)
{
    uint32_t state = 0xFFFFFFFF;

    state = state << 1;
    state |= stm32_gpioread(DI2_PIN);
    state = state << 1;
    state |= stm32_gpioread(DI1_PIN);

    return ~state;
}