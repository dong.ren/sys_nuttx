#include <arch/board/board.h>
#include <chip.h>
#include <debug.h>
#include <errno.h>
#include <fcntl.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <nuttx/board.h>
#include <nuttx/config.h>
#include <string.h>

#include "stm32_adc.h"

#define ADC1_NCHANNELS 2

/************************************************************************************
 * Name: gd32_adc_setup
 *
 * Description:
 *   Initialize ADC and register the ADC driver.
 *
 ************************************************************************************/
void gd32_adc_setup(void)
{

    const static uint8_t g_chanlist[ADC1_NCHANNELS] = {5, 6};

    //Configurations of pins used by each ADC channel
    const static uint32_t g_pinlist[ADC1_NCHANNELS] = {
        GPIO_ADC1_IN5,
        GPIO_ADC1_IN6,
    };

    struct adc_dev_s *adc;
    int ret;
    int i;

    //configure the pins as analog inputs for the selected chanels
    for (i = 0; i < ADC1_NCHANNELS; i++)
    {
        stm32_configgpio(g_pinlist[i]);
    }

    //call stm32_adcinitialize() to get an instance of the ADC interface
    adc = stm32_adcinitialize(1, g_chanlist, ADC1_NCHANNELS);
    if (adc == NULL)
    {
        aerr("ERROR: Fail to get ADC interface\n");
        return;
    }

    //register the ADC driver at "/dev/adc0"
    ret = adc_register("/dev/adc0", adc);
    if (ret < 0)
    {
        aerr("ERROR: adc_register failed: %d\n", ret);
    }

    return;
}

int16_t gd32_adc_open(const char *dev)
{
    return open(dev, O_RDONLY);
}

int16_t gd32_adc_startconv(int16_t fd)
{
    return ioctl(fd, ANIOC_TRIGGER, 0);
}

int16_t gd32_adc_read(int16_t fd, uint16_t *buf)
{
    struct adc_msg_s sample[2];
    int16_t nbytes;

    nbytes = read(fd, sample, sizeof(sample));
    if (sizeof(sample) == nbytes)
    {
        buf[0] = sample[0].am_data;
        buf[1] = sample[1].am_data;
        return 2;
    }
    return 0;
}
