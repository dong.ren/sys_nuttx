#ifndef __CONFIGS_AI_GLORA_INCLUDE_EEPROM_H
#define __CONFIGS_AI_GLORA_INCLUDE_EEPROM_H

#include <stdint.h>
#include <nuttx/config.h>
#include <errno.h>
#include <debug.h>
#include <syslog.h>
#include <nuttx/arch.h>
#include <arch/chip/chip.h>

#define EE_SIZE	128				/* The max size is 255, but, that will lose life */

extern void ee_erase(void);
extern uint16_t ee_read(uint16_t virtaddress);
extern int ee_write(uint16_t virtaddress, uint16_t data);

#endif  /* __CONFIGS_AI_GLORA_INCLUDE_EEPROM_H */
