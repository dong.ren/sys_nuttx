#include <arch/board/eeprom_emulator.h>
#include <arch/chip/irq.h>
#include <nuttx/clock.h>
#include <stdbool.h>
#include <semaphore.h>
#include "chip.h"
#include "stm32_rcc.h"
#include "up_arch.h"
#include "stm32_flash.h"
#include "stm32_waste.h"

#define dmesg()	syslog(4, " ERR: %s - %d\n", __FUNCTION__, __LINE__)
#define mesg	syslog

//#define getreg16(a)       (*(volatile uint16_t *)(a)) 
//#define putreg16(v,a)       (*(volatile uint16_t *)(a) = (v)) 


#ifdef CONFIG_ARCH_CHIP_STM32F103RC

#		define EEPROM_END_ADDRESS		(0x08000000 + 256*1024)
#		define EEPROM_START_ADDRESS   (EEPROM_END_ADDRESS - 2*(STM32_FLASH_PAGESIZE))

#		define FLASH_KEY1      0x45670123
#		define FLASH_KEY2      0xCDEF89AB

#		define PAGE0_ADDRESS      (EEPROM_START_ADDRESS)
#		define PAGE1_ADDRESS      (EEPROM_START_ADDRESS + (STM32_FLASH_PAGESIZE))

#elif CONFIG_ARCH_CHIP_STM32F107RC
#	define EEPROM_END_ADDRESS	(0x08000000 + 256*1024)
#	define EEPROM_START_ADDRESS (EEPROM_END_ADDRESS - 2*(STM32_FLASH_PAGESIZE))
#	define FLASH_KEY1           0x45670123
#	define FLASH_KEY2           0xCDEF89AB
#	define PAGE0_ADDRESS        (EEPROM_START_ADDRESS)
#	define PAGE1_ADDRESS        (EEPROM_START_ADDRESS + (STM32_FLASH_PAGESIZE))

#elif defined CONFIG_ARCH_CHIP_STM32F105RB

#		define EEPROM_END_ADDRESS		(0x08000000 + 128*1024)
#		define EEPROM_START_ADDRESS   (EEPROM_END_ADDRESS - 2*(STM32_FLASH_PAGESIZE))

#		define FLASH_KEY1      0x45670123
#		define FLASH_KEY2      0xCDEF89AB

#		define PAGE0_ADDRESS      (EEPROM_START_ADDRESS)
#		define PAGE1_ADDRESS      (EEPROM_START_ADDRESS + (STM32_FLASH_PAGESIZE))

#else

#	error "eeprom_emualtor does not support this chip! "

#endif

#if EE_SIZE > 255
#		warning "EE_SIZE is better less than 255!"
#endif

#if EE_SIZE > 511
#		error "EE_SIZE can NOT great than 511!"
#endif

#define sum_add(data) ((((data) & 0xff) +0x55)<<8 | ((data) & 0xff))

static bool sum_check(uint16_t data){
	return (((((data >> 8) - 0x55)&0xff) == (data & 0xff)) ? TRUE : FALSE);
}

struct eeprom_emulator_s {
	uint16_t map[EE_SIZE];		/* addr offset of data, base on PAGE0_ADDRESS */
	uint32_t addr;
	sem_t sem;					/* to protect this data struct */
	bool pagefull;
	volatile bool inited;
};

struct eeprom_emulator_s p_ee;

static void flash_program(uint32_t addr, uint16_t data){
	uint32_t reg;
 	irqstate_t flag;

	while(getreg32(STM32_FLASH_SR) & FLASH_SR_BSY){
		usleep(1000);
	}
	flag = enter_critical_section();

	reg = getreg32(STM32_FLASH_CR);
	reg &= ~(FLASH_CR_STRT | FLASH_CR_MER | FLASH_CR_PER | FLASH_CR_PG);
	putreg32(reg, STM32_FLASH_CR);
	reg |= FLASH_CR_PG;
	putreg32(reg, STM32_FLASH_CR);

	putreg16(data, addr);

	while(getreg32(STM32_FLASH_SR) & FLASH_SR_BSY){
		continue;
	}	
	reg &= ~(FLASH_CR_STRT | FLASH_CR_MER | FLASH_CR_PER | FLASH_CR_PG);
	putreg32(reg, STM32_FLASH_CR);

	leave_critical_section(flag);
}

static void page_erase(uint32_t flashaddr){
	uint32_t reg;
 	irqstate_t flag;
	
	while(getreg32(STM32_FLASH_SR) & FLASH_SR_BSY){
		usleep(1000);
	}
	flag = enter_critical_section();

	reg = getreg32(STM32_FLASH_CR);
	reg &= ~(FLASH_CR_STRT | FLASH_CR_MER | FLASH_CR_PER | FLASH_CR_PG);
	putreg32(reg, STM32_FLASH_CR);
	reg |= FLASH_CR_PER;
	putreg32(reg, STM32_FLASH_CR);

	putreg32(flashaddr, STM32_FLASH_AR);

	reg |= FLASH_CR_STRT;
	putreg32(reg, STM32_FLASH_CR);
	/* the while loop should not have come up when the irq is saved why???? */
	while(getreg32(STM32_FLASH_SR) & FLASH_SR_BSY){
		continue;
	}	

	reg &= ~(FLASH_CR_STRT | FLASH_CR_MER | FLASH_CR_PER | FLASH_CR_PG);
	putreg32(reg, STM32_FLASH_CR);

	leave_critical_section(flag);
}

static int page_transfer(uint32_t addr_to){
	uint16_t i;
	uint32_t addr_from;
	int ret = OK;

	page_erase(addr_to);

	for(i = 0; i < EE_SIZE; i++){
		if(p_ee.map[i] && getreg16(PAGE0_ADDRESS + p_ee.map[i]) != 0xffff) {
			addr_to += 4;
		}
	}
	p_ee.addr = addr_to;
	for(i = 0; i < EE_SIZE; i++){
		if(p_ee.map[i] && getreg16(PAGE0_ADDRESS + p_ee.map[i]) != 0xffff) {
			addr_from = PAGE0_ADDRESS + p_ee.map[i];
			addr_to -= 2;
			flash_program(addr_to, getreg16(addr_from));
			if(getreg16(addr_to) != getreg16(addr_from))
				ret = ret? ret:-110;

			p_ee.map[i] = addr_to - PAGE0_ADDRESS;

			addr_to -= 2;
			flash_program(addr_to, sum_add(i));
			if(getreg16(addr_to) != sum_add(i))
				ret = ret? ret:-120;
		}else{
			p_ee.map[i] = 0;
		}
	}
	return ret;
}

static void page_scan(uint32_t pageaddr){
	int offset;
	uint16_t tag;

	p_ee.addr = pageaddr;

	for(offset = 0; offset < STM32_FLASH_PAGESIZE; offset += 4){
		tag = getreg16(p_ee.addr);
		if(tag == 0xffff)  return;

		p_ee.addr += 4;

		if(sum_check(tag) && (tag & 0xff) < EE_SIZE){
			p_ee.map[tag & 0xff] = p_ee.addr - PAGE0_ADDRESS -2;
		}else{
			continue;
		}
	}
	p_ee.pagefull = TRUE;
}

static void flash_init(void){
	irqstate_t flag;
	uint32_t reg;

	/* open HSI, that is required by flash controller */
	flag = enter_critical_section();

	reg = getreg32(STM32_RCC_CR);
	reg |= RCC_CR_HSION;
	putreg32(reg, STM32_RCC_CR);
	leave_critical_section(flag);

	if(getreg32(STM32_FLASH_CR) & FLASH_CR_LOCK){
        flag = enter_critical_section();
		putreg32(FLASH_KEY1, STM32_FLASH_KEYR);
		putreg32(FLASH_KEY2, STM32_FLASH_KEYR);
        leave_critical_section(flag);
	}
}

static int ee_init(void){
	uint16_t u16a, u16b;
	uint32_t reg;

	if(p_ee.inited) sem_wait(&p_ee.sem);
	else{ 
		sem_init(&p_ee.sem, 0, 1);
		p_ee.inited = 1;
	}

	flash_init();

	for(reg=0; reg<EE_SIZE; reg++){
		p_ee.map[reg] =0;
	}
	
	if(getreg32(STM32_FLASH_CR) & FLASH_CR_LOCK){/* if failed, return */
		dmesg();
		return ERROR;
	}
 
	u16a = getreg16(PAGE0_ADDRESS);
	if((u16a == 0xffff) || (u16a == 0) || (!(sum_check(u16a)))){
		u16b = getreg16(PAGE1_ADDRESS);
		if((u16b == 0xffff) || (u16b == 0) || (!(sum_check(u16b)))){	
			page_erase(PAGE0_ADDRESS);
			p_ee.addr = PAGE0_ADDRESS;
		}else{
			page_scan(PAGE1_ADDRESS);
		}
	}else{
		page_scan(PAGE0_ADDRESS);
	}
	sem_post(&p_ee.sem);
	return OK;
}

void ee_erase(void){
	flash_init();
	page_erase(PAGE0_ADDRESS); 
	page_erase(PAGE1_ADDRESS);
	ee_init();
}

uint16_t ee_read(uint16_t va){

	if(!p_ee.inited) ee_init();
	
	if(va >= EE_SIZE)
		return 0xffff;

	if(p_ee.map[va] == 0)
		return 0xffff;
	else
		return getreg16(PAGE0_ADDRESS + p_ee.map[va]);
}

int ee_write(uint16_t va, uint16_t data){
	int ret;
	uint32_t addr_from;
	static uint8_t err_times;

	if(err_times > 200)
		return -60;

	if(va >= EE_SIZE){
		if(data == 0xffff) return OK;
		else return -10;
	}
	if(ee_read(va) == data){
		return OK;
	}

	sem_wait(&p_ee.sem);
	ret = OK;

	if(p_ee.pagefull){
		if(p_ee.addr == PAGE1_ADDRESS){
			addr_from = PAGE0_ADDRESS;
		}else {
			addr_from = PAGE1_ADDRESS;
			p_ee.addr = PAGE0_ADDRESS;
		}
		ret = page_transfer(p_ee.addr);
		if(ret != OK){
			mesg(1, "Transfer err! \n");
		}

		flash_program(addr_from, 0);
		if(getreg16(addr_from) != 0)
			ret = ret? ret:-20;

		p_ee.pagefull = FALSE;
	}

	if(data == 0xffff && p_ee.map[va] == 0){
		goto ee_write_exit;
	}
	if(data == 0 && p_ee.map[va]){
		flash_program(PAGE0_ADDRESS + p_ee.map[va], 0);
		goto ee_write_exit;
	}
	flash_program(p_ee.addr+2, data);
	if(getreg16(p_ee.addr+2) != data){
		ret = ret? ret:-30;
		mesg(1, "-30 -- CR: 0x%X, SR: 0x%X \n", getreg32(STM32_FLASH_CR), getreg32(STM32_FLASH_SR));
	}
		
	flash_program(p_ee.addr, sum_add(va));
	if(getreg16(p_ee.addr) != sum_add(va))
		ret = ret? ret:-40;
		
	p_ee.addr += 4;
	p_ee.map[va] = p_ee.addr - PAGE0_ADDRESS - 2;
		
	if(p_ee.addr == PAGE1_ADDRESS || p_ee.addr == EEPROM_END_ADDRESS)
		p_ee.pagefull = TRUE;

ee_write_exit:
	sem_post(&p_ee.sem);

	if(ee_read(va) != data)
		ret = ret? ret:-50;

	if(ret < 0)
		err_times++;
	else
		err_times = 0;

	return ret;
}
