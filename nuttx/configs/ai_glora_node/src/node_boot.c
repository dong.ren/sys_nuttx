/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "stm32.h"
#include <arch/board/board.h>
#include <nuttx/compiler.h>
#include <nuttx/wireless/sx127x.h>
/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
}

/************************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_initialize().  board_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/

void board_initialize(void)
{
    stm32_configgpio(NODE_SX127X_CS);
    stm32_gpiowrite(NODE_SX127X_CS, true);
    struct spi_dev_s *p_spi = stm32_spibus_initialize(2);
    sx127x_register("/dev/sx0", p_spi, 0);
}

int weak_function main(void)
{
    return 0;
}
