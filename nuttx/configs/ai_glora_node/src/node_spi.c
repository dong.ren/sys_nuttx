#include "stm32.h"
#include <arch/board/board.h>
#include <nuttx/spi/spi.h>

void stm32_spi2select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = false;
    }
    else
    {
        selected = true;
    }

    switch (devid)
    {
    case SPIDEV_WIRELESS(0):
        stm32_gpiowrite(NODE_SX127X_CS, selected);
        break;
    default:
        break;
    }
}

uint8_t stm32_spi2status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}
