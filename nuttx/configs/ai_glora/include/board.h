
#ifndef __CONFIGS_AI_GLORA_INCLUDE_BOARD_H
#define __CONFIGS_AI_GLORA_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#ifndef __ASSEMBLY__
#include <stdint.h>
#endif
#include "stm32.h"
#include "stm32_rcc.h"
#include "stm32_sdio.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/

/* HSI - 8 MHz RC factory-trimmed
 * LSI - 40 KHz RC (30-60KHz, uncalibrated)
 * HSE - On-board crystal frequency is 25MHz
 * LSE - 32.768 kHz
 */

#define STM32_BOARD_XTAL 25000000ul

#define STM32_HSI_FREQUENCY 8000000ul
#define STM32_LSI_FREQUENCY 40000
#define STM32_HSE_FREQUENCY STM32_BOARD_XTAL
#define STM32_LSE_FREQUENCY 32768

/* PLL ouput is 72MHz */

#define STM32_PLL_PREDIV2 RCC_CFGR2_PREDIV2d5  /* 25MHz / 5 => 5MHz */
#define STM32_PLL_PLL2MUL RCC_CFGR2_PLL2MULx8  /* 5MHz * 8  => 40MHz */
#define STM32_PLL_PREDIV1 RCC_CFGR2_PREDIV1d5  /* 40MHz / 5 => 8MHz */
#define STM32_PLL_PLLMUL RCC_CFGR_PLLMUL_CLKx9 /* 8MHz * 9  => 72Mhz */
#define STM32_PLL_FREQUENCY (72000000)

/* SYCLLK and HCLK are the PLL frequency */

#define STM32_SYSCLK_FREQUENCY STM32_PLL_FREQUENCY
#define STM32_HCLK_FREQUENCY STM32_PLL_FREQUENCY
#define STM32_BOARD_HCLK STM32_HCLK_FREQUENCY /* same as above, to satisfy compiler */

/* APB2 clock (PCLK2) is HCLK (72MHz) */

#define STM32_RCC_CFGR_PPRE2 RCC_CFGR_PPRE2_HCLK
#define STM32_PCLK2_FREQUENCY STM32_HCLK_FREQUENCY
#define STM32_APB2_CLKIN (STM32_PCLK2_FREQUENCY) /* Timers 2-7, 12-14 */

/* APB2 timers 1 and 8 will receive PCLK2. */

#define STM32_APB2_TIM1_CLKIN (STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM8_CLKIN (STM32_PCLK2_FREQUENCY)

/* APB1 clock (PCLK1) is HCLK/2 (36MHz) */

#define STM32_RCC_CFGR_PPRE1 RCC_CFGR_PPRE1_HCLKd2
#define STM32_PCLK1_FREQUENCY (STM32_HCLK_FREQUENCY / 2)

/* APB1 timers 2-7 will be twice PCLK1 */

#define STM32_APB1_TIM2_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM3_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM4_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM5_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM6_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM7_CLKIN (2 * STM32_PCLK1_FREQUENCY)

/* MCO output driven by PLL3. From above, we already have PLL3 input frequency as:
 *
 *  STM32_PLL_PREDIV2 = 5, 25MHz / 5 => 5MHz
 */
#define ADC1_DMA_CHAN DMACHAN_ADC1

#if defined(CONFIG_STM32_MII_MCO) || defined(CONFIG_STM32_RMII_MCO)
#define BOARD_CFGR_MCO_SOURCE RCC_CFGR_PLL3CLK /* Source: PLL3 */
#define STM32_PLL_PLL3MUL RCC_CFGR2_PLL3MULx10 /* MCO 5MHz * 10 = 50MHz */
#endif

#define GLORA_KEY_SET (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTD | GPIO_PIN4)
#define GLORA_KEY_ADD (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTD | GPIO_PIN3)
#define GLORA_KEY_SUB (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTD | GPIO_PIN1)
#define GLORA_KEY_ESC (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTD | GPIO_PIN0)

#define GLORA_LED_1 (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTE | GPIO_PIN0)
#define GLORA_LED_2 (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTE | GPIO_PIN1)

#define GLORA_SX127X_INTE (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTA | GPIO_PIN4)
#define GLORA_SX127X_POOL (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTA | GPIO_PIN15)

#define GPIO_UART5_RS485_DIR (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTB | GPIO_PIN6)

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/
/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the intitialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

extern void stm32_boardinitialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_OLIMEX_STM32_P107_INCLUDE_BOARD_H */
