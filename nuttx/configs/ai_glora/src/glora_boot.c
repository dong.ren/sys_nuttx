/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "stm32.h"
#include <arch/board/board.h>
#include <nuttx/compiler.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
}

/************************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_initialize().  board_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/
struct buttons_s{
	uint8_t pre_st_set;				/* pre status: 0, low; 1, high  */
	uint8_t pre_st_esc;
	uint8_t pre_st_plus;
	uint8_t pre_st_minus;
	uint32_t ms_set;				/* the value is the time in low, uint ms */
	uint32_t ms_esc;
	uint32_t ms_plus;
	uint32_t ms_minus;
	uint32_t ms_now;
};

static struct buttons_s p_buttons;



void board_initialize(void)
{
    stm32_configgpio(GLORA_KEY_ADD);
    stm32_configgpio(GLORA_KEY_ESC);
    stm32_configgpio(GLORA_KEY_SET);
    stm32_configgpio(GLORA_KEY_SUB);

    p_buttons.pre_st_set = stm32_gpioread(GLORA_KEY_SET);
	p_buttons.pre_st_esc = stm32_gpioread(GLORA_KEY_ESC);
	p_buttons.pre_st_plus = stm32_gpioread(GLORA_KEY_ADD);
	p_buttons.pre_st_minus = stm32_gpioread(GLORA_KEY_SUB);


    stm32_configgpio(GLORA_LED_1);
    stm32_configgpio(GLORA_LED_2);
    stm32_configgpio(GLORA_SX127X_INTE);
    stm32_configgpio(GLORA_SX127X_POOL);

    struct spi_dev_s *p_spi = stm32_spibus_initialize(1);
    sx127x_register("/dev/sx0", p_spi, 0);
    p_spi = stm32_spibus_initialize(3);
    sx127x_register("/dev/sx1", p_spi, 1);
    struct mtd_dev_s *pmtd = progmem_initialize();
    ftl_initialize(0, pmtd);
}

void glora_set_led(uint8_t led)
{
    if (led & 1)
    {
        stm32_gpiowrite(GLORA_LED_1, true);
    }
    else
    {
        stm32_gpiowrite(GLORA_LED_1, false);
    }

    if (led & 0x02)
    {
        stm32_gpiowrite(GLORA_LED_2, true);
    }
    else
    {
        stm32_gpiowrite(GLORA_LED_2, false);
    }
}

void glora_set_bkg_light(uint8_t enable)
{
    if (enable)
    {
        stm32_gpiowrite((GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTA | GPIO_PIN12), true);
    }
    else
    {

        stm32_gpiowrite((GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTA | GPIO_PIN12), false);
    }
}

uint8_t glora_read_key(void)
{
    uint8_t ret = 0;

    if (stm32_gpioread(GLORA_KEY_ADD))
    {
        ret |= 0x01;
    }

    if (stm32_gpioread(GLORA_KEY_ESC))
    {
        ret |= 0x02;
    }

    if (stm32_gpioread(GLORA_KEY_SET))
    {
        ret |= 0x04;
    }

    if (stm32_gpioread(GLORA_KEY_SUB))
    {
        ret |= 0x08;
    }

    return ret;
}


void buttons_update(void){
	uint32_t time_past;
	uint32_t now;
	now = TICK2MSEC(clock_systimer());
	time_past = now - p_buttons.ms_now;
	p_buttons.ms_now = now;

	if(p_buttons.ms_set){
		p_buttons.pre_st_set = 0;
	}else{
		p_buttons.pre_st_set = 1;
	}
	if(stm32_gpioread(GLORA_KEY_SET)){
		p_buttons.ms_set = 0;
	}else{
		p_buttons.ms_set += time_past;
	}

	if(p_buttons.ms_esc){
		p_buttons.pre_st_esc = 0;
	}else{
		p_buttons.pre_st_esc = 1;
	}
	if(stm32_gpioread(GLORA_KEY_ESC)){
		p_buttons.ms_esc = 0;
	}else{
		p_buttons.ms_esc += time_past;
	}

	if(p_buttons.ms_plus){
		p_buttons.pre_st_plus = 0;
	}else{
		p_buttons.pre_st_plus = 1;
	}
	if(stm32_gpioread(GLORA_KEY_ADD)){
		p_buttons.ms_plus = 0;
	}else{
		p_buttons.ms_plus += time_past;
	}

	if(p_buttons.ms_minus){
		p_buttons.pre_st_minus = 0;
	}else{
		p_buttons.pre_st_minus = 1;
	}
	if(stm32_gpioread(GLORA_KEY_SUB)){
		p_buttons.ms_minus = 0;
	}else{
		p_buttons.ms_minus += time_past;
	}
}




bool button_set_down(void){
	if(p_buttons.pre_st_set && p_buttons.ms_set) return TRUE;
	else return FALSE;
}
bool button_set_up(void){
	if(p_buttons.pre_st_set ==0 && p_buttons.ms_set==0) return TRUE;
	else return FALSE;
}
bool button_set_low(void){
	if(p_buttons.ms_set) return TRUE;
	else return FALSE;
}
bool button_set_hi(void){
	if(p_buttons.ms_set==0) return TRUE;
	else return FALSE;
}
bool button_set_long(uint32_t ms){
	if(p_buttons.ms_set>=ms) return TRUE;
	else return FALSE;
}


bool button_esc_down(void){
	if(p_buttons.pre_st_esc && p_buttons.ms_esc) return TRUE;
	else return FALSE;
}
bool button_esc_up(void){
	if(p_buttons.pre_st_esc ==0 && p_buttons.ms_esc==0) return TRUE;
	else return FALSE;
}
bool button_esc_low(void){
	if(p_buttons.ms_esc) return TRUE;
	else return FALSE;
}
bool button_esc_hi(void){
	if(p_buttons.ms_esc==0) return TRUE;
	else return FALSE;
}
bool button_esc_long(uint32_t ms){
	if(p_buttons.ms_esc>=ms) return TRUE;
	else return FALSE;
}


bool button_plus_down(void){
	if(p_buttons.pre_st_plus && p_buttons.ms_plus) return TRUE;
	else return FALSE;
}
bool button_plus_up(void){
	if(p_buttons.pre_st_plus ==0 && p_buttons.ms_plus==0) return TRUE;
	else return FALSE;
}
bool button_plus_low(void){
	if(p_buttons.ms_plus) return TRUE;
	else return FALSE;
}
bool button_plus_hi(void){
	if(p_buttons.ms_plus==0) return TRUE;
	else return FALSE;
}
bool button_plus_long(uint32_t ms){
	if(p_buttons.ms_plus>=ms) return TRUE;
	else return FALSE;
}


bool button_minus_down(void){
	if(p_buttons.pre_st_minus && p_buttons.ms_minus) return TRUE;
	else return FALSE;
}
bool button_minus_up(void){
	if(p_buttons.pre_st_minus ==0 && p_buttons.ms_minus==0) return TRUE;
	else return FALSE;
}
bool button_minus_low(void){
	if(p_buttons.ms_minus) return TRUE;
	else return FALSE;
}
bool button_minus_hi(void){
	if(p_buttons.ms_minus==0) return TRUE;
	else return FALSE;
}
bool button_minus_long(uint32_t ms){
	if(p_buttons.ms_minus>=ms) return TRUE;
	else return FALSE;
}

bool button_any(void){
	if(p_buttons.ms_set || p_buttons.ms_esc ||
	   p_buttons.ms_minus || p_buttons.ms_plus){
		return TRUE;
	}else{
		return FALSE;
	}
}
bool button_any_change(void){
	if(button_set_up() || button_set_down() ||
	   button_esc_up() || button_esc_down() ||
	   button_minus_up() || button_minus_down() ||
	   button_plus_up() || button_plus_down()){
		return TRUE;
	}else
		return FALSE;
}
bool button_any_up(void){
	if(button_set_up() || button_esc_up() || button_minus_up() || button_plus_up()){
		return TRUE;
	}else
		return FALSE;
}
bool button_any_down(void){
	if(button_set_down() || button_esc_down() ||
	   button_minus_down() || button_plus_down()){
		return TRUE;
	}else
		return FALSE;
}


/* device test use only */
uint8_t btns_status(void){
	uint8_t btn_status = 0;
	btn_status |= stm32_gpioread(GLORA_KEY_ESC);
	btn_status |= stm32_gpioread(GLORA_KEY_SUB) << 1;
	btn_status |= stm32_gpioread(GLORA_KEY_ADD) << 2;
	btn_status |= stm32_gpioread(GLORA_KEY_SET) << 3;
	return btn_status;
}



int weak_function main(void)
{
    glora_set_bkg_light(0);
    glora_set_led(3);
    printf("main\n");
    int fd = open("/dev/sx0",O_RDWR);
    int fd1 = open("/dev/sx1",O_RDWR);

    int temp = 453000000;
    int ret = ioctl(fd,SX127X_IOC_SET_CARRIER_FREQ,&temp);
    temp = 453000000;
    int ret1 = ioctl(fd,SX127X_IOC_SET_CARRIER_FREQ,&temp);

    while(1)
    {
        printf("set %d\n",ret);
        int freq,freq1;
        ret = ioctl(fd,SX127X_IOC_GET_CARRIER_FREQ,&freq);
        ret1 = ioctl(fd1,SX127X_IOC_GET_CARRIER_FREQ,&freq1);
        printf("get %d\n",ret);
    }
    return 0;
}
