

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

void *port_mutex_create(void)
{
    pthread_mutex_t *mutex = malloc(sizeof(pthread_mutex_t));
    if (mutex == NULL)
    {
        return mutex;
    }
    pthread_mutex_init(mutex, NULL);
    return mutex;
}

int port_mutex_lock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_lock(mutex);
}

int port_mutex_try_lock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_trylock(mutex);
}
int port_mutex_unlock(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    return pthread_mutex_unlock(mutex);
}

int port_mutex_destroy(void *_mutex)
{
    pthread_mutex_t *mutex = _mutex;
    int ret = pthread_mutex_destroy(mutex);
    if (ret == 0)
    {
        free(mutex);
    }
    return ret;
}

pthread_attr_t *port_set_stack_size(int size)
{
    pthread_attr_t *attr = malloc(sizeof(pthread_attr_t));
    pthread_attr_init(attr);
    pthread_attr_setstacksize(attr, size);
    return attr;
}

void port_destory_stack_attr(pthread_attr_t *attr)
{
    pthread_attr_destroy(attr);
    free(attr);
}

int port_open_wr(const char *path) { return open(path, O_RDWR, 0666); }

int port_open_wr_c(const char *path)
{
    return open(path, O_RDWR | O_CREAT, 0666);
}

int port_open_r(const char *path) { return open(path, O_RDONLY, 0666); }

mqd_t port_mq_open(int siz, char *path_buf)
{
    static unsigned int seed = 666;
    int i;
    srand(seed++);
    i = rand();
    snprintf(path_buf, 11, "/mq_%d", i);
    struct mq_attr attr;
    attr.mq_curmsgs = 0;
    attr.mq_flags = O_RDWR | O_CREAT;
    attr.mq_maxmsg = 8;
    attr.mq_msgsize = siz;
    return mq_open(path_buf, O_RDWR | O_CREAT, 0666, &attr);
}

int port_mq_close(mqd_t id) { return mq_close(id); }

int port_mq_receive(mqd_t id, char *buf, size_t msg_len)
{
    return mq_receive(id, buf, msg_len, (void *)NULL);
}

int port_mq_unlink(const char *path) { return mq_unlink(path); }

int port_mq_send(mqd_t id, const char *buf, size_t msg_len)
{
    return mq_send(id, buf, msg_len, (unsigned int)0);
}

uint8_t *get_stdout(void) { return (uint8_t *)stdout; }

sem_t *port_sem_create(size_t n)
{
    sem_t *sig = malloc(sizeof(sem_t));
    sem_init(sig, 0, n);
    return sig;
}

// int port_open_stream(const char *addr, int port) {
//
//   int sock_cli = socket(AF_INET, SOCK_STREAM, 0);
//
//   ///定义sockaddr_in
//   struct sockaddr_in servaddr;
//   memset(&servaddr, 0, sizeof(servaddr));
//   servaddr.sin_family = AF_INET;
//   servaddr.sin_port = htons(port);            ///服务器端口
//   servaddr.sin_addr.s_addr = inet_addr(addr); ///服务器ip
//   int r = connect(sock_cli, (struct sockaddr *)&servaddr, sizeof(servaddr));
//   if (r == -1) {
//     return r;
//   }
//   return sock_cli;
// }
