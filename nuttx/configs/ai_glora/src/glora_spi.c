#include "stm32.h"
#include <arch/board/board.h>
#include <nuttx/spi/spi.h>

void stm32_spi1select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = false;
    }
    else
    {
        selected = true;
    }

    switch (devid)
    {
    case SPIDEV_WIRELESS(0):
        stm32_gpiowrite(GLORA_SX127X_INTE, selected);
        break;
    default:
        break;
    }
}

uint8_t stm32_spi1status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}

void stm32_spi3select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = false;
    }
    else
    {
        selected = true;
    }

    switch (devid)
    {
    case SPIDEV_WIRELESS(1):
        stm32_gpiowrite(GLORA_SX127X_POOL, selected);
        break;
    default:
        break;
    }
}

uint8_t stm32_spi3status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}
