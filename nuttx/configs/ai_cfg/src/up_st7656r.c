#include "stm32_gpio.h"
#include "stm32_waste.h"
#include <arch/board/board.h>
#include <arch/irq.h>
#include <errno.h>
#include <nuttx/arch.h>
#include <nuttx/config.h>
#include <nuttx/video/fb.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <syslog.h>

/* IO pins definition */
#define PIN_SPI_RST (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTC | GPIO_PIN6)
#define PIN_SPI_CS (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTC | GPIO_PIN7)
#define PIN_SPI_CMDATA (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTB | GPIO_PIN14)
#define PIN_SPI_SCK (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTB | GPIO_PIN13)
#define PIN_SPI_SDO (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTB | GPIO_PIN15)
#define PIN_BK (GPIO_OUTPUT | GPIO_CNF_OUTPP | GPIO_MODE_50MHz | GPIO_OUTPUT_SET | GPIO_PORTA | GPIO_PIN1)

/* Frame buffer configuration */
#define FB_FMT FB_FMT_Y1
#define FB_WIDTH (128 / 8)
#define FB_SIZE (FB_WIDTH * 64)

/* st7656r command definition */
#define LCD_CM_COL_HI 0X10
#define LCD_CM_COL_LOW 0x00
#define LCD_CM_PWR_SET_1 0x2c
#define LCD_CM_PWR_SET_2 0x2e
#define LCD_CM_PWR_SET_3 0x2f
#define LCD_CM_DISPLAY_START_LINE 0x40
#define LCD_CM_PAGE 0xb0
#define LCD_CM_VLCD_Resistor_Ratio 0x24 /* 0x20 ~ 0x27     0x22 current using 0x23                0x26test   24*/
#define LCD_CM_VOL_MODE 0x81
#define LCD_CM_VOL_VALUE 0x20 /* 0x28 original  0x34current using 0x29                  0x00test   1a*/
#define LCD_CM_DISPLAY_ALL 0xa5
#define LCD_CM_DISPLAY_ALL_CLOSE 0xa4
#define LCD_CM_DISPLAY_NORMAL 0xa6
#define LCD_CM_DISPLAY_REVERSE 0xa7
#define LCD_CM_DISPLAY_ON 0xaf
#define LCD_CM_DISPLAY_OFF 0xae
#define LCD_CM_COL_SEQUENCE 0xa0 /* 0xa0: left->right, 0xa1: reversed */
#define LCD_CM_ROW_SEQUENCE 0xc8 /* 0xc0: bottom->top, 0xc8: reversed */
#define LCD_CM_RESET 0xe2
#define LCD_CM_BIAS_7 0xa3
#define LCD_CM_BIAS_9 0xa2
#define LCD_CM_BOOST_RATIO 0xf8

#define ST7567_EXIT_SOFTRST 0xe2  /* 0xe2: Software RESET */
#define ST7567_BIAS_1_7 0xa2      /* 0xa3: Select BIAS setting 1/7 */
#define ST7567_DISPON 0xaf        /* 0xaf: Display ON in normal mode */
#define SSD1305_MAPCOL0 0xa0      /* 0xa0: Column address 0 is mapped to SEG0 */
#define ST7567_SETCOMREVERSE 0xc8 /* 0xc8: Set COM output direction, reverse mode */
#define ST7567_REG_RES_RR1 0x24   /* 0x22: 0x20~0x2f Regulation Resistior ratio */
#define ST7567_SETEV 0x81         /* 0x81: Set contrast control */
#define ST7567_POWERCTRL 0x2f     /* 0x2c: Control built-in power circuit */
#define ST7567_SETSTARTLINE 0x40  /* 0x40-7f: Set display start line */
#define ST7567_SETPAGESTART 0xb0  /* 0xb0-b7: Set page start address */
#define ST7567_SETCOLH 0x10       /* 0x10-0x1f: Set higher column address */
#define ST7567_SETCOLL 0x00       /* 0x00-0x0f: Set lower column address */
#define ST7567_DISPON 0xaf        /* 0xaf: Display ON in normal mode */
#define ST7567_DISPRAM 0xa4       /* 0xa4: Resume to RAM content display */


static const struct fb_videoinfo_s g_videoinfo = {
    .fmt = FB_FMT,
    .xres = 128,
    .yres = 64,
    .nplanes = 1,
};

static const struct fb_planeinfo_s g_planeinfo = {
    .fbmem = (void *)NULL,
    .fblen = FB_SIZE,
    .stride = FB_WIDTH,
    .bpp = 1,
};

static int up_getvideoinfo(FAR struct fb_vtable_s *vtable, FAR struct fb_videoinfo_s *vinfo);
static int up_getplaneinfo(FAR struct fb_vtable_s *vtable, int planeno, FAR struct fb_planeinfo_s *pinfo);

struct fb_vtable_s g_fbobject = {
    .getvideoinfo = up_getvideoinfo,
    .getplaneinfo = up_getplaneinfo,
};

static int up_getvideoinfo(FAR struct fb_vtable_s *vtable,
                           FAR struct fb_videoinfo_s *vinfo)
{
    if (vtable && vinfo)
    {
        memcpy(vinfo, &g_videoinfo, sizeof(struct fb_videoinfo_s));
        return OK;
    }
    return -EINVAL;
}

static int up_getplaneinfo(FAR struct fb_vtable_s *vtable, int planeno,
                           FAR struct fb_planeinfo_s *pinfo)
{
    if (vtable && planeno == 0 && pinfo)
    {
        memcpy(pinfo, &g_planeinfo, sizeof(struct fb_planeinfo_s));
        return OK;
    }
    return -EINVAL;
}

static void st7565r_send_cmd(uint8_t cmd)
{
    int i = 0;
    up_waste();
    stm32_gpiowrite(PIN_SPI_CS, false);
    stm32_gpiowrite(PIN_SPI_CMDATA, false);
    for (; i < 8; i++)
    {
        stm32_gpiowrite(PIN_SPI_SCK, false);
        if (cmd & 0x80)
        {
            stm32_gpiowrite(PIN_SPI_SDO, true);
        }
        else
        {
            stm32_gpiowrite(PIN_SPI_SDO, false);
        }
        stm32_gpiowrite(PIN_SPI_SCK, true);
        cmd = cmd << 1;
    }
    stm32_gpiowrite(PIN_SPI_CS, true);
    up_waste();
}

static int st7565r_setpower(int power)
{
    if (power <= 0)
    {
        st7565r_send_cmd(LCD_CM_DISPLAY_OFF);
    }
    else
    {
        st7565r_send_cmd(LCD_CM_DISPLAY_ON);
    }
    return OK;
}

static void st7565r_send_data(uint8_t data)
{
    char i = 0;
    up_waste();
    stm32_gpiowrite(PIN_SPI_CS, false);
    stm32_gpiowrite(PIN_SPI_CMDATA, true);
    for (; i < 8; i++)
    {
        stm32_gpiowrite(PIN_SPI_SCK, false);
        if (data & 0x80)
        {
            stm32_gpiowrite(PIN_SPI_SDO, true);
        }
        else
        {
            stm32_gpiowrite(PIN_SPI_SDO, false);
        }
        stm32_gpiowrite(PIN_SPI_SCK, true);
        data = data << 1;
    }
    stm32_gpiowrite(PIN_SPI_CS, true);
    up_waste();
}

void clean_screen(unsigned data)
{
    unsigned char page, col;
    for (page = 0; page < 8; page++)
    {
        st7565r_send_cmd(LCD_CM_PAGE + page);
        st7565r_send_cmd(LCD_CM_COL_HI);
        st7565r_send_cmd(LCD_CM_COL_LOW);
        for (col = 0; col < 128; col++)
        {
            st7565r_send_data(data);
        }
    }
}

#define st7565r_initialize up_fbinitialize

void st7565r_rst_up(void)
{
    stm32_configgpio(PIN_SPI_RST);
}

int up_fbinitialize(int display)
{
    stm32_configgpio(PIN_SPI_SCK);
    stm32_configgpio(PIN_SPI_RST);
    stm32_configgpio(PIN_SPI_SDO);
    stm32_configgpio(PIN_SPI_CMDATA);
    stm32_configgpio(PIN_SPI_CS);
    stm32_configgpio(PIN_BK);

    stm32_gpiowrite(PIN_BK, true);
    stm32_gpiowrite(PIN_SPI_RST, true);

    st7565r_send_cmd(ST7567_EXIT_SOFTRST);
    st7565r_send_cmd(ST7567_BIAS_1_7);
    st7565r_send_cmd(ST7567_DISPON);
    st7565r_send_cmd(SSD1305_MAPCOL0);
    st7565r_send_cmd(ST7567_SETCOMREVERSE);
    st7565r_send_cmd(ST7567_REG_RES_RR1);
    st7565r_send_cmd(ST7567_SETEV);
    st7565r_send_cmd(0x20);
    st7565r_send_cmd(ST7567_POWERCTRL);
    st7565r_send_cmd(ST7567_SETSTARTLINE);
    st7565r_send_cmd(ST7567_SETPAGESTART);
    st7565r_send_cmd(ST7567_SETCOLH);
    st7565r_send_cmd(ST7567_SETCOLL);
    st7565r_send_cmd(ST7567_DISPON);
    st7565r_send_cmd(ST7567_DISPRAM);

    st7565r_setpower(1);
    clean_screen(0x0);
    return OK;
}

/* redraw lcd:  write data from fb to ext. lcd chip */
void lcd_redraw(uint8_t *lcd_fb)
{

    uint8_t page, col, data, data_fb;
    uint16_t row, row_end;
    uint8_t const maskconst[] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
    uint8_t mask;

    st7565r_send_cmd(ST7567_EXIT_SOFTRST);
    st7565r_send_cmd(ST7567_SETCOMREVERSE);
    up_waste();
    up_waste();
    up_waste();

    for (page = 0; page < 8; page++)
    {
        st7565r_send_cmd(LCD_CM_PAGE + page);
        st7565r_send_cmd(LCD_CM_COL_HI);
        st7565r_send_cmd(LCD_CM_COL_LOW);
        for (col = 0; col < 128; col++)
        {
            row_end = page * 8 + 8;
            data = 0;
            mask = maskconst[col % 8];
            for (row = page * 8; row < row_end; row++)
            {
                data_fb = lcd_fb[FB_WIDTH * row + (col >> 3)];
                data >>= 1;
                if (data_fb & mask)
                    data |= 0x80;
            }
            st7565r_send_data(data);
        }
    }
}

struct fb_vtable_s *up_fbgetvplane(int disable, int vplane)
{
    if (vplane == 0)
    {
        return &g_fbobject;
    }
    else
    {
        return NULL;
    }
}

void fb_uninitialize(void)
{
    st7565r_setpower(0);
}
