/************************************************************************************
 * configs/stm32f103-minimum/src/stm32_boot.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           Laurent Latil <laurent@latil.nom.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "stm32_gpio.h"
#include <arch/board/board.h>
#include <fcntl.h>
#include <nuttx/compiler.h>
#include <nuttx/usb/usbdev.h>
#include <nuttx/config.h>
/************************************************************************************
 * Public Functions
 ************************************************************************************/
#include <stdio.h>
/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
}

int stm32_usbpullup(FAR struct usbdev_s *dev, bool enable)
{
  // usbtrace(TRACE_DEVPULLUP, (uint16_t)enable);
  // stm32_gpiowrite(GPIO_USB_PULLUP, !enable);
  return OK;
}

void stm32_usbsuspend(FAR struct usbdev_s *dev, bool resume)
{
  // uinfo("resume: %d\n", resume);
}

/************************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_initialize().  board_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/

void board_initialize(void)
{
  stm32_configgpio(CFG_KEY_ADD);
  stm32_configgpio(CFG_KEY_ESC);
  stm32_configgpio(CFG_KEY_SET);
  stm32_configgpio(CFG_KEY_SUB);
  stm32_configgpio(USB_OTG);
}

int weak_function main(void)
{
  printf("\nDummy Main\n");
  return 0;
}

uint8_t cfg_read_key(void)
{
  uint8_t ret = 0;

  if (stm32_gpioread(CFG_KEY_ADD))
  {
    ret |= 0x01;
  }

  if (stm32_gpioread(CFG_KEY_ESC))
  {
    ret |= 0x02;
  }

  if (stm32_gpioread(CFG_KEY_SET))
  {
    ret |= 0x04;
  }

  if (stm32_gpioread(CFG_KEY_SUB))
  {
    ret |= 0x08;
  }

  return ret;
}
