#ifndef __INCLUDE_APPS_FRB_FRB_MASTER_H
#define __INCLUDE_APPS_FRB_FRB_MASTER_H

#include <sys/types.h>
#include <stdbool.h>
#include <poll.h>

#define FRB_TIMEOUT		10		/* unit: mS */
#define FRB_D_LEN	  32	 /* NOTE: unit is nibble, NOT byte*/

/* FRB master api return values */
#define FRB_ACCESS_OVERRUN -10
#define FRB_ACCESS_EOS_AFTER_ESC -12
#define FRB_ACCESS_COM_ERR -13
#define FRB_ACCESS_NO_REPLY -14
#define FRB_ACCESS_EOF_EXP -15
#define FRB_ACCESS_RS_ESC_ERR -16
#define FRB_READ_TG_ERR -20
#define FRB_READ_EXP -21
#define FRB_READ_EXP_TIMEOUT -27
#define FRB_READ_EXP_ERR -28
#define FRB_EXTRACT_NO_ADDR -30
#define FRB_EXTRACT_LACK_LEN -31
#define FRB_EXTRACT_DATA_ERR -32
#define FRB_WRITE_TG_ERR -40
#define FRB_WRITE_DATA_ERR -41
#define FRB_WRITE_ACK_ERR -42
#define FRB_WRITE_ACK_TIMEOUT -43
#define FRB_WRITE_ACK -44

#define FRB_TG_SINGLE 14

typedef struct frb_master_s {
	uint8_t		tg_qty;
	uint8_t		*tg_map[14]; 		/* use similar data struct as frb.py */
	uint8_t		tg_seq_max;			/* max length of tg sequence */

	uint8_t		acks[256];

	uint8_t		(*rs)[FRB_D_LEN];
	uint8_t		*rs_len;
	uint8_t		*rs_addr;
	uint8_t		rs_qty;

	uint16_t	v;		/* self.v in frb.py, can used as int16_t */

	uint8_t		com_err;
	uint8_t		com_warn;

	struct pollfd	serial;

} frb_master_t;

#define frb_master_read_int16 frb_master_read_word
#define frb_master_read_uint16 frb_master_read_word

extern frb_master_t * frb_master_init(const char *port, uint8_t tg_qty, uint8_t tg_seq_max);
extern int frb_master_ping(frb_master_t *self, uint8_t addr, uint8_t tg);
extern int frb_master_read(frb_master_t *self, uint8_t addr, uint8_t tag, uint8_t tg);
extern int frb_master_extract_word(frb_master_t *self, uint8_t addr, uint8_t pos);
extern int frb_master_read_word(frb_master_t *self, uint8_t addr, uint8_t tag);
extern int frb_master_write(frb_master_t *self, uint8_t addr, uint8_t tag,
						  uint8_t *data, int data_len, uint8_t tg);
extern int frb_master_write_uint16(frb_master_t *self, uint8_t addr, uint8_t tag,
						  uint16_t uint16, uint8_t tg);
extern int frb_master_write_int16(frb_master_t *self, uint8_t addr, uint8_t tag,
						  int16_t int16, uint8_t tg);
extern void frb_master_sync(frb_master_t *self);
extern void	frb_master_scan(frb_master_t *self, int start, int end);
extern void	frb_master_close(frb_master_t *self);
extern uint16_t frb_master_get_val(frb_master_t *self);


#endif  /* __INCLUDE_APPS_FRB_FRB_MASTER_H */







