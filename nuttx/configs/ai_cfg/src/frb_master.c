#include <errno.h>
#include <debug.h>
#include <syslog.h>
#include <sys/types.h>
#include <stdbool.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <poll.h>

#include "frb_master.h"

#define FRB_ESC			13
#define FRB_EOS			14
#define FRB_EOF			15

#define FRB_FC_CONFIG	0
#define FRB_FC_READ		3
#define FRB_FC_WRITE	6
#define FRB_FC_PING		14

/* ACK defines recommended, only can use 5, 8, 11, 14, do not use other */
#define FRB_ACK_TIMEOUT			2 /* hi app does not start send data in time */
#define FRB_ACK_ERR				5 /* unsuport fc, no tag, rq err etc. */
#define FRB_ACK_APP1			8 /* reserved for app usage */
#define FRB_ACK_APP2			11 /* reserved for app usage */
#define FRB_ACK_OK				14 /* normal, succeed */

#define FRB_ACK_none		0xff

/* RS exception definition, only the below can by used */
#define FRB_EXP_TIMEOUT		(FRB_ACK_TIMEOUT) 
#define FRB_EXP_ERR			(FRB_ACK_ERR)
#define FRB_EXP_APP1			(FRB_ACK_APP1)
#define FRB_EXP_APP2			(FRB_ACK_APP2)

#ifdef CONFIG_FRB_MASTER_DEBUG
#		define dmesg()	syslog(3, "%s - %d\n", __FUNCTION__, __LINE__)
#		define mesg syslog
#else
#		define dmesg()	
#		define mesg(x...)
#endif

#ifdef CONFIG_FRB_RECV_SELF
#	define rx_self(self) _rx(self)
#else
#	define rx_self(self)
#endif

#ifdef CONFIG_FRB_RECV_SELF
/* defined in up_frb_master.c */
extern void frb_txe_init(void);
extern void frb_txe_out(bool txe);
#else
static void frb_txe_init(void) {}
static void frb_txe_out(bool txe) {}
#endif

static const uint8_t ecc_tab[16] = {0xF, 0xA, 0x9, 0xC, 0x6, 0x3, 0x0, 0x5,
								 0x5, 0x0, 0x3, 0x6, 0xC, 0x9, 0xA, 0xF};

#define FRB_RX_TIMOUT 0x40
#define FRB_RX_READ_ERR 0x30
#define FRB_RX_ECC_ERR 0x20

//0x00-0x0f cerrect content, 0x10~0xf: revised value; 0x20: ecc err
static uint8_t ecc(uint8_t in)
{
	uint8_t ct, ck;
	ct = in;
	ct >>=4;
	ck = ecc_tab[ct];
	ck ^= (in & 0xF);
	switch(ck) {
	case 0x5: ct ^= 0x11; break;
	case 0x6: ct ^= 0x12; break;
	case 0x9: ct ^= 0x14; break;
	case 0xA: ct ^= 0x18; break;
	case 0x3: 
	case 0x7: 
	case 0xB: 
	case 0xC: 
	case 0xD: 
	case 0xE: 
	case 0xF: ct = 0x20;
	}
	return ct;
}

// return 0~0xf, read corrent, 0x10~0x1f, ecc revised value, 0x20, ecc err.
// 0x30, read err, 0x40: timeout
static uint8_t _rx(frb_master_t *self)
{
	int poll_ret;
	uint8_t data;

	poll_ret = poll(&self->serial, 1, FRB_TIMEOUT);

	if(poll_ret == 0){
		mesg(2, "FRB_RX_TIMOUT\n");
		return FRB_RX_TIMOUT;
	}else if(poll_ret < 0){
		mesg(2, "FRB_RX_READ_ERR\n");
		return FRB_RX_READ_ERR;
	}else {
		if(read(self->serial.fd, &data, 1) != 1){
			mesg(2, "re\n");
			return FRB_RX_READ_ERR;
		}else{
			mesg(2, "r %d\n", ecc(~data));
			return ecc(~data);
		}
	}
}

static int _tx(frb_master_t *self, uint8_t data)
{
	frb_txe_out(1);
	if(data > 0xf){
		dmesg();
		mesg(1, "data must be 0~16, but you input %d!\n", data);
		return ERROR;
	}
	mesg(1, "t %d\n", data);
	data = ~(ecc_tab[data] + (data<<4));
	if(write(self->serial.fd, &data, 1) != 1){
		dmesg();
		mesg(1, "Serial write error %d!\n", data);
		return ERROR;
	}
	return OK;
}


static int frb_master_access(frb_master_t *self, uint8_t tg, uint8_t addr,
							 uint8_t fc, uint8_t *rq, uint8_t rq_len)
{
	uint8_t rs_no, rx, tx, tu8a;
	int rx_st;
	
	if(tg != FRB_TG_SINGLE && tg >= self->tg_qty){
		dmesg();
		mesg(2, "err: tg is larger than tg_qty.\n");
		return ERROR;
	}
	
	if(fc > 14){
		dmesg();
		mesg(2, "err: fc is too large.\n");
		return ERROR;
	}
	
#ifdef CONFIG_SERIAL_TERMIOS
	tcflush(self->serial.fd, TCIOFLUSH);
#endif
	_tx(self, tg);
	if(tg == FRB_TG_SINGLE){
		tx = addr>>4;
		if(tx == FRB_ESC || tx == FRB_EOS || tx == FRB_EOF){
			_tx(self, FRB_ESC);
			tx -= 3;
		}
		_tx(self, tx);
		tx = addr & 0xf;
		if(tx == FRB_ESC || tx == FRB_EOS || tx == FRB_EOF){
			_tx(self, FRB_ESC);
			tx -= 3;
		}
		_tx(self, tx);
		rx_self(self);	
	}
	if(fc != FRB_FC_PING){
		_tx(self, fc);
	}
	for(tu8a=0; tu8a < rq_len; tu8a++){
		tx = rq[tu8a];
		if(tx == FRB_ESC || tx == FRB_EOS || tx == FRB_EOF){
			_tx(self, FRB_ESC);
			tx -= 3;
		}
		_tx(self, tx);
		rx_self(self);
	}

	_tx(self, FRB_EOS);
#ifdef CONFIG_FRB_RECV_SELF
	do{//poll send finish
		rx = _rx(self);
		if(rx < 0x20){
			rx &= 0xf;
			if(rx == FRB_EOS) break;
		}
	}while(rx != FRB_RX_TIMOUT);
	frb_txe_out(0);

	if(rx != FRB_EOS){
		dmesg();
		mesg(3, "something error\n");
		frb_master_sync(self);
		return ERROR;
	}
#endif
	rs_no = 1;
	rx_st = OK;
	rx = _rx(self);
	if((fc&1) == 0){ //ack response
		if(tg == FRB_TG_SINGLE){
			self->acks[addr] = FRB_ACK_none;
		}else{
			uint8_t i;
			for(i=0; i <= self->tg_seq_max; i++){
				self->acks[self->tg_map[tg][i]] = FRB_ACK_none;
			}
		}
		while(rx != FRB_RX_TIMOUT){
			if(rx_st == OK){
				if(tg == FRB_TG_SINGLE){
					self->acks[addr] = rx;
				}else{
					if(self->tg_map[tg][rs_no] == 0xff){
						rx_st = FRB_ACCESS_OVERRUN;
					}
					self->acks[self->tg_map[tg][rs_no]] = rx;
				}
			}

			if(rx >= 0x20){
				self->com_err = 255;
				if(rx_st == OK)
					rx_st = FRB_ACCESS_COM_ERR;
			}else if(self->com_err)
				self->com_err--;
			if(rx > 0x10)
				self->com_warn = 255;
			else if(self->com_warn)
				self->com_warn --;
				
			rs_no++;
			if(tg == FRB_TG_SINGLE || self->tg_map[tg][rs_no] == 0xff){
				break;
			}
			if(tg != FRB_TG_SINGLE && rs_no > self->tg_seq_max){
				dmesg();
				mesg(2, "Err, rs_no is larger than tg_seq_max\n");
				break;
			}
			rx = _rx(self);
		}
	}else{// rs response
		if(tg == FRB_TG_SINGLE){
			self->rs_len[0] = 0;
			self->rs_addr[0] = addr;
		}else{
			int i;
			for(i=0; self->tg_map[tg][i+1] != 0xff && i< self->rs_qty; i++){
				self->rs_len[i] = 0;
				self->rs_addr[i] = self->tg_map[tg][i+1];
			}
		}
		uint8_t esc = 0;
		while(rx != FRB_RX_TIMOUT){
			if((rx&0xf) == FRB_EOS && esc && rx_st == OK){
				rx_st = FRB_ACCESS_EOS_AFTER_ESC;
			}
			if(rx >= 0x20){
				self->com_err = 255;
				if(rx_st == OK)
					rx_st = FRB_ACCESS_COM_ERR;
			}else if(self->com_err)
				self->com_err--;
			if(rx > 0x10)
				self->com_warn = 255;
			else if(self->com_warn)
				self->com_warn --;

			if((rx&0xf) == FRB_EOS){
				if(rx_st == OK && self->tg_map[tg][rs_no] == 0xff){
					rx_st = FRB_ACCESS_OVERRUN;
				}
				if(esc){
					rx_st = FRB_ACCESS_RS_ESC_ERR;
					esc = 0;
				}
				rs_no++;
				if(tg == FRB_TG_SINGLE || self->tg_map[tg][rs_no] == 0xff)
					break;
				if(tg != FRB_TG_SINGLE && rs_no > self->tg_seq_max){
					dmesg();
					mesg(2, "Err, rs_no is larger than tg_seq_max\n");
					break;
				}
			}else if((rx&0xf) == FRB_ESC){
				esc = 1;
			}else{
				if(esc){
					esc = 0;
					if(rx < 0x20)
						self->rs[rs_no-1][self->rs_len[rs_no-1]++] = rx+3;
					else
						self->rs[rs_no-1][self->rs_len[rs_no-1]++] = rx;
				}else{
					self->rs[rs_no-1][self->rs_len[rs_no-1]++] = rx;
				}
			}

			rx = _rx(self);
		}	
	}
	if(rs_no == 1 || (tg != FRB_TG_SINGLE && self->tg_map[tg][rs_no] != 0xff)){
		if(rx_st == OK)
			rx_st = FRB_ACCESS_NO_REPLY;
	}
	
	_tx(self, FRB_EOF);
#ifdef CONFIG_FRB_RECV_SELF
	do{//poll send finish
		rx = _rx(self);
		if(rx < 0x20){
			rx &= 0xf;
			if(rx == FRB_EOF) break;
		}
	}while(rx != FRB_RX_TIMOUT);
	frb_txe_out(0);

	if(rx != FRB_EOF){
		if(rx_st == OK) rx_st = FRB_ACCESS_EOF_EXP;
		frb_master_sync(self);
		dmesg();
		mesg(2, "something error\n");
	}
#endif
	return rx_st;
}

frb_master_t *frb_master_init(const char *port, uint8_t tg_qty, uint8_t tg_seq_max)
{
	frb_master_t *self;
	self = malloc(sizeof(frb_master_t));
	if(self == NULL){
		dmesg();
		mesg(2, "Can allocate memory for frb_master!\n");
		return NULL;
	}

	self->serial.fd = open(port, (O_RDWR|O_NOCTTY));
	if(self->serial.fd < 0){
		dmesg();
		mesg(2, "Can not open serial port %s!\n", port);
		return NULL;
	}
	self->serial.events = POLLRDNORM;

	if(tg_qty > 14){
		dmesg();
		mesg(2, "tg_qty must by small than 14!\n");
		return NULL;
	}
	self->tg_qty = tg_qty;
	for(; tg_qty>0; tg_qty--){
		self->tg_map[tg_qty-1] = malloc(tg_seq_max+1);
		if(self->tg_map[tg_qty-1] == NULL){
			dmesg();
			mesg(2, "tg_map malloc err!\n");
			return NULL;
		}
		uint8_t i;
		for(i=0; i<= tg_seq_max; i++){
			self->tg_map[tg_qty-1][i] = 255;
		}
	}
	self->tg_seq_max = tg_seq_max;
	int i;
	for(i=0; i<256; i++)
		self->acks[i] = FRB_ACK_none;

	if(self->tg_qty){
		self->rs_qty = tg_seq_max;
	}else{
		self->rs_qty = 1;
	}
	self->rs = malloc(self->rs_qty * FRB_D_LEN);
	self->rs_len = malloc(self->rs_qty);
	self->rs_addr = malloc(self->rs_qty);
	if(self->rs == NULL || self->rs_len == NULL || self->rs_addr == NULL){
		dmesg();
		mesg(2, "rs malloc err!\n");
		return NULL;
	}

	self->com_err = 0;
	self->com_warn = 0;
	
	frb_txe_init();

	return self;
}

int frb_master_ping(frb_master_t *self, uint8_t addr, uint8_t tg){
	return frb_master_access(self, tg, addr, FRB_FC_PING, NULL, 0);
}

int frb_master_read(frb_master_t *self, uint8_t addr, uint8_t tag, uint8_t tg){
	int access_ret;
	uint8_t rq[2];

	rq[0] = tag>>4;
	rq[1] = tag&0xf;

	if(tg > 14){
		return FRB_READ_TG_ERR;
	}
	access_ret = frb_master_access(self, tg, addr, FRB_FC_READ, rq, 2);
	if(access_ret == OK){
		if(tg == FRB_TG_SINGLE && self->rs_len[0] == 1){
			if((self->rs[0][0] & 0xf) == FRB_EXP_ERR)
				access_ret = FRB_READ_EXP_ERR;
			else if((self->rs[0][0] & 0xf) == FRB_EXP_TIMEOUT)
				access_ret = FRB_READ_EXP_TIMEOUT;
			else access_ret = FRB_READ_EXP;
		}
		if(tg != FRB_TG_SINGLE){
			int i;
			for(i=0; i < self->tg_seq_max; i++){
				if(self->tg_map[tg][i+1] != 0xff && self->rs_len[i] == 1){
					if((self->rs[i][0] & 0xf) == FRB_EXP_ERR)
						access_ret = FRB_READ_EXP_ERR;
					else if((self->rs[i][0] & 0xf) == FRB_EXP_TIMEOUT)
						access_ret = FRB_READ_EXP_TIMEOUT;
					else access_ret = FRB_READ_EXP;
					break;
				}
			}
		}
	}
	return access_ret;
}

int frb_master_extract_word(frb_master_t *self, uint8_t addr, uint8_t pos){
	int i;

	for(i=0; i < self->rs_qty && self->rs_addr[i] != addr; i++){;}

	if(i == self->rs_qty)
		return FRB_EXTRACT_NO_ADDR;

	if(self->rs_len[i] < pos+4)
		return FRB_EXTRACT_LACK_LEN;

	self->v = 0;
	int j;
	for(j=0; j<4; j++){
		if(self->rs[i][pos] >= 0x20)
			return FRB_EXTRACT_DATA_ERR;
		self->v <<= 4;
		self->v |= (self->rs[i][pos++] & 0xf);
	}
	return OK;
}

int frb_master_read_word(frb_master_t *self, uint8_t addr, uint8_t tag){
	int access_ret = frb_master_read(self, addr, tag, FRB_TG_SINGLE);
	if(access_ret == OK){
		return frb_master_extract_word(self, addr, 0);
	}
	return access_ret;
}

int frb_master_write(frb_master_t *self, uint8_t addr, uint8_t tag,
							uint8_t *data, int data_len, uint8_t tg){
	int access_ret;
	uint8_t rq[data_len+2];

	if(tg > 14){
		return FRB_WRITE_TG_ERR;
	}

	rq[0] = tag>>4;
	rq[1] = tag&0xf;
	int i;
	for(i=0; i<data_len; i++){
		if(data[i] > 0xf)
			return FRB_WRITE_DATA_ERR;
		rq[i+2] = data[i];
	}

	access_ret = frb_master_access(self, tg, addr, FRB_FC_WRITE, rq, data_len+2);
	if(access_ret == OK){
		if(tg == FRB_TG_SINGLE && (self->acks[addr]&0xf) < FRB_ACK_OK-2){
			if((self->acks[addr]&0xf) <= FRB_ACK_TIMEOUT)
				access_ret = FRB_WRITE_ACK_TIMEOUT;
			else if((self->acks[addr]&0xf) <= FRB_ACK_ERR)
				access_ret = FRB_WRITE_ACK_ERR;
			else
				access_ret = FRB_WRITE_ACK;
		}
		if(tg != FRB_TG_SINGLE){
			for(i=0; i < self->tg_seq_max; i++){
				if(self->tg_map[tg][i+1] != 0xff && (self->acks[self->rs_addr[i]]&0xf) <= FRB_ACK_APP2){
					if((self->acks[self->rs_addr[i]]&0xf) <= FRB_ACK_TIMEOUT)
						access_ret = FRB_WRITE_ACK_TIMEOUT;
					else if((self->acks[self->rs_addr[i]]&0xf) <= FRB_ACK_ERR)
						access_ret = FRB_WRITE_ACK_ERR;
					else
						access_ret = FRB_WRITE_ACK;
					break;
				}
			}
		}
	}
	return access_ret;
}

int frb_master_write_uint16(frb_master_t *self, uint8_t addr, uint8_t tag,
							uint16_t uint16, uint8_t tg){
	uint8_t rq[4];
	rq[0] = uint16 >> 12;
	rq[1] = (uint16 >> 8) & 0xf;
	rq[2] = (uint16 >> 4) & 0xf;
	rq[3] = uint16 & 0xf;
	return frb_master_write(self, addr, tag, rq, 4, tg);
}

int frb_master_write_int16(frb_master_t *self, uint8_t addr, uint8_t tag,
							int16_t int16, uint8_t tg){
	return frb_master_write_uint16(self, addr, tag, int16, tg);
}

void frb_master_sync(frb_master_t *self){
	uint8_t data;

	while(_rx(self) != FRB_RX_TIMOUT){;}

	_tx(self, FRB_EOF);

	do{//poll send finish
		data = _rx(self);
		if(data < 0x20){
			data &= 0xf;
			if(data == FRB_EOF) break;
		}
	}while(data != FRB_RX_TIMOUT);

	frb_txe_out(0);
}

void frb_master_scan(frb_master_t *self, int start, int end){
	int i;
	for(i=start; i<end; i++){
		frb_master_ping(self, i, FRB_TG_SINGLE);
	}
}

void frb_master_close(frb_master_t *self){
	free(self->rs_addr);
	free(self->rs_len);
	free(self->rs);
	int i;
	for(i=0; i<self->tg_qty; i++){
		free(self->tg_map[i]);
	}
	free(self);
}

uint16_t frb_master_get_val(frb_master_t *self)
{
	return self->v;
}
