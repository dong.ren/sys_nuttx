#include <arch/board/board.h>
#include <debug.h>
#include <nuttx/fs/nxffs.h>
#include <nuttx/fs/smart.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/ssd1306.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/spi/spi.h>
#include <stm32.h>
#include <sys/mount.h>

void stm32_spi1select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
	if (selected)
	{
		selected = false;
	}
	else
	{
		selected = true;
	}

	switch (devid)
	{
	case SPIDEV_WIRELESS(0):
		stm32_gpiowrite(SX1278_CS_POLL, selected);
		break;
	default:
		break;
	}
}

uint8_t stm32_spi1status(FAR struct spi_dev_s *dev, uint32_t devid)
{
	return SPI_STATUS_PRESENT;
}

void stm32_spi2select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
	if (selected)
	{
		selected = false;
	}
	else
	{
		selected = true;
	}

	switch (devid)
	{
	case SPIDEV_WIRELESS(1):
		stm32_gpiowrite(SX1278_CS_INTE, selected);
		break;
	default:
		break;
	}
}
uint8_t stm32_spi2status(FAR struct spi_dev_s *dev, uint32_t devid)
{
	return SPI_STATUS_PRESENT;
}

int stm32_spi2cmddata(FAR struct spi_dev_s *dev, uint32_t devid, bool cmd)
{
	return OK;
}
