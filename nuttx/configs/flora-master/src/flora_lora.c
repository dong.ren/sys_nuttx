#include "stm32.h"
#include <arch/board/board.h>
#include <arch/board/board.h>
#include <debug.h>
#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/config.h>
// #include <nuttx/fs/mkfatfs.h>
#include <nuttx/mmcsd.h>
#include <nuttx/spi/spi.h>
#include <nuttx/spi/spi_transfer.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>
#include <sys/mount.h>

#include "stm32.h"

void stm32_loreinitialize(void)
{
	struct spi_dev_s *p_spi = stm32_spibus_initialize(1);

	stm32_configgpio(SX1278_CS_INTE);
	stm32_configgpio(SX1278_CS_POLL);
	stm32_configgpio(SX1278_RST_INTE);
	stm32_configgpio(SX1278_RST_POLL);

	stm32_gpiowrite(SX1278_CS_INTE, true);
	stm32_gpiowrite(SX1278_CS_POLL, true);

	stm32_gpiowrite(SX1278_RST_INTE, false);
	stm32_gpiowrite(SX1278_RST_POLL, false);

	up_mdelay(10);
	stm32_gpiowrite(SX1278_RST_INTE, true);
	stm32_gpiowrite(SX1278_RST_POLL, true);

	sx127x_register("/dev/sx0", p_spi, 0);
	p_spi = stm32_spibus_initialize(2);
	sx127x_register("/dev/sx1", p_spi, 1);
}
