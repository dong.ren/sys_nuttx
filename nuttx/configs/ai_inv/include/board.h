
#ifndef __CONFIG_AI_EXB_INCLUDE_BOARD_H
#define __CONFIG_AI_EXB_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#ifndef __ASSEMBLY__
#include <stdbool.h>
#include <stdint.h>
#endif

#ifdef __KERNEL__
#endif

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry
 *point
 *   is called early in the initialization -- after all memory has been
 *configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/
extern void sf2_boardinitialize();
#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIG_STM32F4DISCOVERY_INCLUDE_BOARD_H */
