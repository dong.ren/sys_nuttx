/************************************************************************************
 * configs/viewtool-stm32f107/src/stm32_boot.c
 * 
 *   Copyright (C) 2013 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <arch/board/board.h>
#include <debug.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <nuttx/compiler.h>
#include <nuttx/config.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/drivers/drivers.h>
#include <stdio.h>
#include "up_arch.h"
#include "sf2.h"

/************************************************************************************
 * Public Functions
 ************************************************************************************/
 

// void task1(void)
// {
//     clear_tim1_irq();
//     printf("task1\n");
// }




int weak_function main(void)
{
    printf("weak main!\n");
    sleep(1);
  /****************************************************************/
    // int fd = open("/dev/ttyS2", O_RDWR);
    // if(fd < 0)
    //     printf("open failed\n");
    // write(fd,"hello", 5);
    // set_tim1_us(1000000);
    // tim1_start((void*)task1);

    return 0; 
}

void sf2_boardinitialize(void) 
{
    /* Nothing to do*/
}

/************************************************************
*   Function Name: bsp_init
*   This is bsp initialize.
*   It executed before main function and after OS_Start task
*   Using it, you need to add a  BSP entry into nuttx system configuration
*
*************************************************************/
void bsp_init(void) 
{
    sf2_timebus_initialize();
    
    // struct spi_dev_s *pspi_dev = sf2_spibus_initialize(0);
    // struct mtd_dev_s *pmtd_flash = w25_initialize(pspi_dev);
    // ftl_initialize(0, pmtd_flash);
    // bchdev_register("/dev/mtdblock0", "/dev/mtd0", false);




/*******************************************************************/
    sleep(1);        
    /*main function entry*/
    main();  
/*******************************************************************/    
}


