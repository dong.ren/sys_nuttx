/************************************************************************
  @ File Name:          configs/ai_inv/src/sf2_spi.c
  @ Author:             Li Meng
  @ Mail:               meng.li@fonrich.com
  @ Created Time:       07-12-2017
************************************************************************/

#include "sf2.h"
#include <arch/board/board.h>
#include <nuttx/spi/spi.h>
#include <nuttx/config.h>
#include "../common/up_arch.h"

void sf2_spi0select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = true;
    }
    else
    {
        selected = false;
    }

    switch (devid)
    {
    case SPIDEV_FLASH(0):
        putreg32(selected, SF2_SPI0_SLAVE_SELECT);
        break;
    default:
        break;
    }
}

uint8_t sf2_spi0status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}
