#include "stm32.h"
#include <arch/board/board.h>
#include <debug.h>
#include <nuttx/board.h>
#include <nuttx/config.h>
// #include <nuttx/fs/mkfatfs.h>
#include <nuttx/mmcsd.h>
#include <nuttx/spi/spi.h>
#include <nuttx/spi/spi_transfer.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>
#include <sys/mount.h>

int stm32_loreinitialize(int minor)
{
	struct spi_dev_s *p_spi = stm32_spibus_initialize(3);
	sx127x_register("/dev/sx0", p_spi, 0);
	return OK;
}
