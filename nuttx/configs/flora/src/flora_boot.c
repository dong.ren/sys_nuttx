/************************************************************************************
 * configs/stm32f103-minimum/src/stm32_boot.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           Laurent Latil <laurent@latil.nom.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <debug.h>
#include <nuttx/config.h>
#include <nuttx/spi/spi.h>

#include "stm32.h"
#include "stm32_adc.h"
#include "stm32_gpio.h"
#include "stm32_gpio.h"
#include "stm32_spi.h"
#include "stm32_spi.h"
#include <arch/board/board.h>
#include <arch/board/board.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <nuttx/board.h>
#include <nuttx/board.h>
#include <nuttx/drivers/pwm.h>
// #include <nuttx/fs/mkfatfs.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/ssd1306.h>
#include <nuttx/mmcsd.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/spi/spi_transfer.h>
#include <nuttx/wireless/sx127x.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

void stm32_boardinitialize(void)
{
}

void board_initialize(void)
{
	stm32_spi_dev_initialize();
	stm32_loreinitialize(0);
	stm32_sdinitialize(0);
	flora_adc_setup();
	struct mtd_dev_s *pmtd = progmem_initialize();
	ftl_initialize(0, pmtd);
}

int weak_function main(void)
{
	struct sx127x_modem_s modem;
	int fd = open("/dev/sx0", O_RDWR);
	int freq = 424000000;
	bool optimize = true;
	uint8_t buf[32];

	modem.band_width = SX127X_MODEM_BANDWITH_62_5K;
	modem.coding_rate = SX127X_MODEM_CODING_RATE_4_5;
	modem.implicit_header = SX127X_MODEM_IMPLICIT_HEADER_OFF;
	modem.rx_payload_crc = SX127X_MODEM_RX_PAYLOAD_CRC_ON;
	modem.rx_timeout_msb = 1;
	modem.spreading_factor = SX127X_MODEM_SPREADING_FACTOR_10;
	modem.tx_continus = SX127X_MODEM_TX_CONTINUS_OFF;
	ioctl(fd, SX127X_IOC_SET_MODEM, &modem);
	ioctl(fd, SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ, &optimize);
	ioctl(fd, SX127X_IOC_SET_CARRIER_FREQ, &freq);

	printf("Freq %d\n", freq);
	freq = write(fd, buf, 32);
	// printf("Wet %d %d\n", freq, errno);
	for (;;)
	{
		freq = read(fd, buf, 32);
		printf("Ret %d %d\n", freq, errno);
	}
	return 0;
}
