#include <debug.h>
#include <nuttx/config.h>
#include <nuttx/mmcsd.h>
#include <nuttx/spi/spi.h>
#include <stdio.h>

#include "stm32_spi.h"

//SPI2 connects to the SD CARD
#define STM32_MMCSDSPIPORTNO 2 //port is spi2
#define STM32_MMCSDSLOTNO 0	//there is only one slot

/*
 * Name : stm32_sdinitialize
 * Description :
 * Initialize the SPI-based SD card, requies CONFIG_DISABLE_MOUNTPOINT = n
 * and CONFIG_STM32_SPI2 = y
 */
int stm32_sdinitialize(int minor)
{
	FAR struct spi_dev_s *spi;
	int ret;

	//get the spi port
	finfo("Initializing SPI port %d\n", STM32_MMCSDSPIPORTNO);

	spi = stm32_spibus_initialize(STM32_MMCSDSPIPORTNO);
	if (!spi)
	{
		ferr("ERROR : Failed to initialize SPI%d\n", STM32_MMCSDSPIPORTNO);
		return -ENODEV;
	}
	finfo("Successfully initialized SPI port %d\n", STM32_MMCSDSPIPORTNO);

	//bind the spi port to the slot
	ret = mmcsd_spislotinitialize(STM32_MMCSDSLOTNO, STM32_MMCSDSLOTNO, spi);
	if (ret < 0)
	{
		ferr("ERROR : Fail to bind spi port%d to mmc/sd slot %d : %d\n",
			 STM32_MMCSDSPIPORTNO, STM32_MMCSDSLOTNO000);
		return ret;
	}
	finfo("Successfuly bound spi %d to mmc/sd slot %d\n",
		  STM32_MMCSDSPIPORTNO, STM32_MMCSDSLOTNO);

	return OK;
}
