#include <arch/board/board.h>
#include <chip.h>
#include <debug.h>
#include <errno.h>
#include <nuttx/analog/adc.h>
#include <nuttx/board.h>
#include <nuttx/config.h>

#include "stm32_adc.h"

#define ADC1_NCHANNELS 3

/************************************************************************************
 * Name: stm32_adc_setup
 *
 * Description:
 *   Initialize ADC and register the ADC driver.
 *
 ************************************************************************************/
void flora_adc_setup(void)
{

	const static uint8_t g_chanlist[ADC1_NCHANNELS] = {11, 12, 13};

	//Configurations of pins used by each ADC channel
	const static uint32_t g_pinlist[ADC1_NCHANNELS] = {
		GPIO_ADC123_IN11,
		GPIO_ADC123_IN12,
		GPIO_ADC123_IN13};

	struct adc_dev_s *adc;
	int ret;
	int i;

	//configure the pins as analog inputs for the selected chanels
	for (i = 0; i < ADC1_NCHANNELS; i++)
	{
		stm32_configgpio(g_pinlist[i]);
	}

	//call stm32_adcinitialize() to get an instance of the ADC interface
	adc = stm32_adcinitialize(1, g_chanlist, ADC1_NCHANNELS);
	if (adc == NULL)
	{
		aerr("ERROR: Fail to get ADC interface\n");
		return;
	}

	//register the ADC driver at "/dev/adc0"
	ret = adc_register("/dev/adc0", adc);
	if (ret < 0)
	{
		aerr("ERROR: adc_register failed: %d\n", ret);
	}

	return;
}
