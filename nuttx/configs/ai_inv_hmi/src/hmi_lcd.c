#include <debug.h>
#include <nuttx/fs/nxffs.h>
#include <nuttx/fs/smart.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/ssd1306.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/spi/spi.h>
#include <sys/mount.h>

#include "../include/board.h"
#include "stm32_gpio.h"
#include "stm32_spi.h"
static struct spi_dev_s *g_spidev = NULL;
static struct lcd_dev_s *g_lcddev = NULL;

int board_lcd_initialize(void)
{
	stm32_configgpio(GPIO_OLED_CS);
	stm32_configgpio(GPIO_OLED_DC);
	stm32_configgpio(GPIO_OLED_RESET);

	stm32_gpiowrite(GPIO_OLED_RESET, false);
	stm32_gpiowrite(GPIO_OLED_CS, true);

	g_spidev = stm32_spibus_initialize(1);

	stm32_gpiowrite(GPIO_OLED_RESET, false);
	up_mdelay(20);
	stm32_gpiowrite(GPIO_OLED_RESET, true);
	up_mdelay(20);
	return 1;
}

struct lcd_dev_s *board_lcd_getdev(int lcddev)
{
	g_lcddev = ssd1306_initialize(g_spidev, (const struct ssd1306_priv_s *)lcddev,0);
	/* And turn the LCD on (CONFIG_LCD_MAXPOWER should be 1) */
	(void)g_lcddev->setpower(g_lcddev, CONFIG_LCD_MAXPOWER);
	/* Set contrast to right value, otherwise background too dark */
	(void)g_lcddev->setcontrast(g_lcddev, CONFIG_LCD_MAXCONTRAST);

	return g_lcddev;
}

struct lcd_dev_s *board_graphics_setup(uint32_t devno)
{
	static struct lcd_dev_s *dev = NULL;
	static struct spi_dev_s *spi1 = NULL;

	if (dev == NULL)
	{
		stm32_configgpio(GPIO_OLED_CS);
		stm32_configgpio(GPIO_OLED_DC);
		stm32_configgpio(GPIO_OLED_RESET);

		stm32_gpiowrite(GPIO_OLED_RESET, false);
		stm32_gpiowrite(GPIO_OLED_CS, true);
		//wait a bit then release the  OLED from the reset state
		spi1 = stm32_spibus_initialize(1);
		if (!spi1)
		{
			lcdinfo("init spi1 failed\n");
		}
		stm32_gpiowrite(GPIO_OLED_RESET, false);
		up_mdelay(20);
		stm32_gpiowrite(GPIO_OLED_RESET, true);
		up_mdelay(20);

		if (!spi1)
		{
			lcderr("ERROR:Failed to initialize SPI port1\n");
		}
		else
		{
			//bind the spi1 port to oled
			dev = ssd1306_initialize(spi1, (const struct ssd1306_priv_s *)devno,0);
			if (!dev)
			{
				lcderr("ERROR:Failed to bind spi port 1 to oled");
			}
			else
			{
				lcdinfo("bind spi port 1 to oled %d\n", devno);
				//and turn the oled on
				(void)dev->setpower(dev, CONFIG_LCD_MAXPOWER);
			}
		}
	}
	return dev;
}
