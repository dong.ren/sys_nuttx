#include "../include/board.h"
#include "chip.h"
#include "stm32_gpio.h"
#include "stm32_spi.h"
#include <debug.h>
#include <nuttx/fs/nxffs.h>
#include <nuttx/fs/smart.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/ssd1306.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/spi/spi.h>
#include <sys/mount.h>

void hmi_spi_dev_initialize(void)
{
	stm32_configgpio(GPIO_OLED_CS);
	stm32_configgpio(GPIO_OLED_DC);
	stm32_configgpio(GPIO_OLED_RESET);
}

void stm32_spi1select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
	spiinfo("devid: %d CS: %s\n", (int)devid, selected ? "assert" : "de-assert");
	if (devid == SPIDEV_DISPLAY(0))
	{
		stm32_gpiowrite(GPIO_OLED_CS, !selected);
	}
}

uint8_t stm32_spi1status(FAR struct spi_dev_s *dev, uint32_t devid)
{
	return 0;
}

int stm32_spi1cmddata(FAR struct spi_dev_s *dev, uint32_t devid, bool cmd)
{
	if (devid == SPIDEV_DISPLAY(0))
	{
		/* "This is the Data/Command control pad which determines whether the
       *  data bits are data or a command.
       *
       *  A0 = "H": the inputs at D0 to D7 are treated as display data.
       *  A0 = "L": the inputs at D0 to D7 are transferred to the command
       *       registers."
       */
		if (cmd)
		{
			stm32_gpiowrite(GPIO_OLED_DC, false);
		}
		else
		{
			stm32_gpiowrite(GPIO_OLED_DC, true);
		}
		return OK;
	}
	return -ENODEV;
}


void board_app_initialize(void)
{
}


