/************************************************************************************
 * configs/stm32f103-minimum/src/stm32_boot.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *           Laurent Latil <laurent@latil.nom.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <debug.h>
#include <nuttx/config.h>
#include <nuttx/spi/spi.h>

#include "stm32.h"
#include "stm32_adc.h"
#include "stm32_gpio.h"
#include "stm32_spi.h"
#include "../include/board.h"
#include <arch/board/board.h>
#include <errno.h>
#include <nuttx/analog/adc.h>
#include <nuttx/board.h>
#include <nuttx/fs/mkfatfs.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/spi/spi_transfer.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
}

/************************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_initialize().  board_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/
extern void hmi_adc_setup(void);
extern void hmi_spi_dev_initialize(void);
extern void hmi_gpio_init(void);
// extern void hmi_gpio_mode(uint8_t mode);

void board_initialize(void)
{
	hmi_gpio_init();
	hmi_adc_setup();
	hmi_spi_dev_initialize();
}

// void *test(void *arg)
// {
// 	while (1)
// 	{
// 		printf("test\n");
// 		usleep(1000);
// 		int i, j, k, h, s;
// 		for (i = 0; i < 100; i++)
// 			for (j = 0; j < 1000; j++)
// 			{
// 				s = 0;
// 				s = 1;
// 			}
// 	}
// }

int weak_function main(void)
{
	/* this main fun apply to test boot code */
	// pthread_t pth1;
	bool flag = true;
	printf("Test Main\n");

	// pthread_create(&pth1, NULL, test, NULL);

	while (1)
	{
		sleep(1);
		if (flag)
		{
			printf("on\n");
			flag = false;
			hmi_gpio_mode(0);
		}
		else
		{
			printf("off\n");
			flag = true;
			hmi_gpio_mode(1);
		}
	}
	printf("dumpy main!");
	return 0;
}
