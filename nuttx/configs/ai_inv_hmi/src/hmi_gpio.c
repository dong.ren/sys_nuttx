#include <debug.h>
#include <nuttx/config.h>
#include <nuttx/spi/spi.h>

#include "stm32.h"
#include "stm32_gpio.h"
#include "../include/board.h"
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#define ALL_OFF 0
#define ALL_ON 1
#define BULE_ON 2
#define BULE_OFF 3
#define RED_ON 4
#define RED_OFF 5
#define YELLO_ON 6
#define YELLO_OFF 7
#define STATUS_ON 8
#define STATUS_OFF 9
#define WIFI_RUN 10
#define WIFI_DOWNLOAD 11
#define FPGA_HMI1_H 12
#define FPGA_HMI1_L 13
#define FPGA_HMI2_H 14
#define FPGA_HMI2_L 15
#define I2C_SDA_H 16
#define I2C_SDA_L 17
#define I2C_SCL_H 18
#define I2C_SCL_L 19

void hmi_gpio_init(void)
{
	stm32_configgpio(GPIO_B1);
	stm32_configgpio(GPIO_B2);
	stm32_configgpio(GPIO_B3);
	stm32_configgpio(GPIO_B4);
	stm32_configgpio(GPIO_R1);
	stm32_configgpio(GPIO_R2);
	stm32_configgpio(GPIO_R3);
	stm32_configgpio(GPIO_R4);
	stm32_configgpio(GPIO_Y1);
	stm32_configgpio(GPIO_Y2);
	stm32_configgpio(GPIO_Y3);
	stm32_configgpio(GPIO_Y4);
	stm32_configgpio(GPIO_STATUS);
	stm32_configgpio(GPIO_WIFI_MOD);
	stm32_configgpio(GPIO_FPGA_HMI1);
	stm32_configgpio(GPIO_FPGA_HMI2);
	stm32_configgpio(GPIO_I2C_SDA);
	stm32_configgpio(GPIO_I2C_SCL);
	stm32_gpiowrite(GPIO_B1, 0);
	stm32_gpiowrite(GPIO_B2, 0);
	stm32_gpiowrite(GPIO_B3, 0);
	stm32_gpiowrite(GPIO_B4, 0);
	stm32_gpiowrite(GPIO_R1, 0);
	stm32_gpiowrite(GPIO_R2, 0);
	stm32_gpiowrite(GPIO_R3, 0);
	stm32_gpiowrite(GPIO_R4, 0);
	stm32_gpiowrite(GPIO_Y1, 0);
	stm32_gpiowrite(GPIO_Y2, 0);
	stm32_gpiowrite(GPIO_Y3, 0);
	stm32_gpiowrite(GPIO_Y4, 0);
	stm32_gpiowrite(GPIO_STATUS, 0);
	stm32_gpiowrite(GPIO_WIFI_MOD, 1);
	stm32_gpiowrite(GPIO_FPGA_HMI1, 0);
	stm32_gpiowrite(GPIO_FPGA_HMI2, 0);
	stm32_gpiowrite(GPIO_I2C_SDA, 0);
	stm32_gpiowrite(GPIO_I2C_SCL, 0);
}

void hmi_gpio_mode(uint8_t mode)
{
	switch (mode)
	{
	case ALL_OFF:
		{
			stm32_gpiowrite(GPIO_B1, 0);
			stm32_gpiowrite(GPIO_B2, 0);
			stm32_gpiowrite(GPIO_B3, 0);
			stm32_gpiowrite(GPIO_B4, 0);
			stm32_gpiowrite(GPIO_R1, 0);
			stm32_gpiowrite(GPIO_R2, 0);
			stm32_gpiowrite(GPIO_R3, 0);
			stm32_gpiowrite(GPIO_R4, 0);
			stm32_gpiowrite(GPIO_Y1, 0);
			stm32_gpiowrite(GPIO_Y2, 0);
			stm32_gpiowrite(GPIO_Y3, 0);
			stm32_gpiowrite(GPIO_Y4, 0);
		}
		break;
	case ALL_ON:
		{
			stm32_gpiowrite(GPIO_B1, 1);
			stm32_gpiowrite(GPIO_B2, 1);
			stm32_gpiowrite(GPIO_B3, 1);
			stm32_gpiowrite(GPIO_B4, 1);
			stm32_gpiowrite(GPIO_R1, 1);
			stm32_gpiowrite(GPIO_R2, 1);
			stm32_gpiowrite(GPIO_R3, 1);
			stm32_gpiowrite(GPIO_R4, 1);
			stm32_gpiowrite(GPIO_Y1, 1);
			stm32_gpiowrite(GPIO_Y2, 1);
			stm32_gpiowrite(GPIO_Y3, 1);
			stm32_gpiowrite(GPIO_Y4, 1);
		}
		break;
	case BULE_ON:
		{
			stm32_gpiowrite(GPIO_B1, 1);
			stm32_gpiowrite(GPIO_B2, 1);
			stm32_gpiowrite(GPIO_B3, 1);
			stm32_gpiowrite(GPIO_B4, 1);
		}
		break;
	case BULE_OFF:
		{
			stm32_gpiowrite(GPIO_B1, 0);
			stm32_gpiowrite(GPIO_B2, 0);
			stm32_gpiowrite(GPIO_B3, 0);
			stm32_gpiowrite(GPIO_B4, 0);
		}
		break;
	case RED_ON:
		{
			stm32_gpiowrite(GPIO_R1, 1);
			stm32_gpiowrite(GPIO_R2, 1);
			stm32_gpiowrite(GPIO_R3, 1);
			stm32_gpiowrite(GPIO_R4, 1);
		}
		break;

	case RED_OFF:
		{
			stm32_gpiowrite(GPIO_R1, 0);
			stm32_gpiowrite(GPIO_R2, 0);
			stm32_gpiowrite(GPIO_R3, 0);
			stm32_gpiowrite(GPIO_R4, 0);
		}
		break;
	case YELLO_ON:
		{
			stm32_gpiowrite(GPIO_Y1, 1);
			stm32_gpiowrite(GPIO_Y2, 1);
			stm32_gpiowrite(GPIO_Y3, 1);
			stm32_gpiowrite(GPIO_Y4, 1);
		}
		break;
	case YELLO_OFF:
		{
			stm32_gpiowrite(GPIO_Y1, 0);
			stm32_gpiowrite(GPIO_Y2, 0);
			stm32_gpiowrite(GPIO_Y3, 0);
			stm32_gpiowrite(GPIO_Y4, 0);
		}
		break;
	case STATUS_ON:
		stm32_gpiowrite(GPIO_STATUS, 1);
		break;
	case STATUS_OFF:
		stm32_gpiowrite(GPIO_STATUS, 0);
		break;
	case WIFI_RUN:
		stm32_gpiowrite(GPIO_WIFI_MOD, 1);
		break;
	case WIFI_DOWNLOAD:
		stm32_gpiowrite(GPIO_WIFI_MOD, 0);
		break;
	case FPGA_HMI1_H:
		stm32_gpiowrite(GPIO_FPGA_HMI1, 1);
		break;
	case FPGA_HMI1_L:
		stm32_gpiowrite(GPIO_FPGA_HMI1, 0);
		break;
	case FPGA_HMI2_H:
		stm32_gpiowrite(GPIO_FPGA_HMI2, 1);
		break;
	case FPGA_HMI2_L:
		stm32_gpiowrite(GPIO_FPGA_HMI2, 0);
		break;
	case I2C_SDA_H:
		stm32_gpiowrite(GPIO_I2C_SDA, 1);
		break;
	case I2C_SDA_L:
		stm32_gpiowrite(GPIO_I2C_SDA, 0);
		break;
	case I2C_SCL_H:
		stm32_gpiowrite(GPIO_I2C_SCL, 1);
		break;
	case I2C_SCL_L:
		stm32_gpiowrite(GPIO_I2C_SCL, 0);
		break;
	default:
		{
			stm32_gpiowrite(GPIO_B1, 0);
			stm32_gpiowrite(GPIO_B2, 0);
			stm32_gpiowrite(GPIO_B3, 0);
			stm32_gpiowrite(GPIO_B4, 0);
			stm32_gpiowrite(GPIO_R1, 0);
			stm32_gpiowrite(GPIO_R2, 0);
			stm32_gpiowrite(GPIO_R3, 0);
			stm32_gpiowrite(GPIO_R4, 0);
			stm32_gpiowrite(GPIO_Y1, 0);
			stm32_gpiowrite(GPIO_Y2, 0);
			stm32_gpiowrite(GPIO_Y3, 0);
			stm32_gpiowrite(GPIO_Y4, 0);
		}
		break;
	}
}
