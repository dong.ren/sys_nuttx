#include <arch/board/board.h>
#include <chip.h>
#include <debug.h>
#include <errno.h>
#include <nuttx/analog/adc.h>
#include <nuttx/board.h>
#include <nuttx/config.h>

#include "stm32_adc.h"

#define ADC1_NCHANNELS 2

/************************************************************************************
 * Name: stm32_adc_setup
 *
 * Description:
 *   Initialize ADC and register the ADC driver.
 *
 ************************************************************************************/
void hmi_adc_setup(void)
{
	const static uint8_t g_chanlist[ADC1_NCHANNELS] = {14, 15};
	struct adc_dev_s *adc;
	int ret;

	//configure the pins as analog inputs for the selected chanels
	stm32_configgpio(GPIO_ADC12_IN14);
	stm32_configgpio(GPIO_ADC12_IN15);

	//call stm32_adcinitialize() to get an instance of the ADC interface
	adc = stm32_adcinitialize(1, g_chanlist, ADC1_NCHANNELS);
	if (adc == NULL)
	{
		aerr("ERROR: Fail to get ADC interface\n");
		return;
	}

	//register the ADC driver at "/dev/adc1"
	ret = adc_register("/dev/adc1", adc);
	if (ret < 0)
	{
		aerr("ERROR: adc_register failed: %d\n", ret);
	}
}
