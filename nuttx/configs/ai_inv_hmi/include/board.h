/************************************************************************************
 * configs/stm32f103-minimum/include/board.h
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Laurent Latil <laurent@latil.nom.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __CONFIGS_AI_CFG_BOARD_H
#define __CONFIGS_AI_CFG_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#ifndef __ASSEMBLY__
#include <stdint.h>
#endif
#include "stm32.h"
#include "stm32_rcc.h"
#include "stm32_sdio.h"
#ifdef __KERNEL__
#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/

/* On-board crystal frequency is 8MHz (HSE) */

#define STM32_BOARD_XTAL 8000000ul

/* PLL source is HSE/1, PLL multipler is 9: PLL frequency is 8MHz (XTAL) x 9 = 72MHz */

#define STM32_CFGR_PLLSRC RCC_CFGR_PLLSRC
#define STM32_CFGR_PLLXTPRE 0
#define STM32_CFGR_PLLMUL RCC_CFGR_PLLMUL_CLKx9
#define STM32_PLL_FREQUENCY (9 * STM32_BOARD_XTAL)

/* Use the PLL and set the SYSCLK source to be the PLL */

#define STM32_SYSCLK_SW RCC_CFGR_SW_PLL
#define STM32_SYSCLK_SWS RCC_CFGR_SWS_PLL
#define STM32_SYSCLK_FREQUENCY STM32_PLL_FREQUENCY

/* AHB clock (HCLK) is SYSCLK (72MHz) */

#define STM32_RCC_CFGR_HPRE RCC_CFGR_HPRE_SYSCLK
#define STM32_HCLK_FREQUENCY STM32_PLL_FREQUENCY
#define STM32_BOARD_HCLK STM32_HCLK_FREQUENCY /* same as above, to satisfy compiler */

/* APB2 clock (PCLK2) is HCLK (72MHz) */

#define STM32_RCC_CFGR_PPRE2 RCC_CFGR_PPRE2_HCLK
#define STM32_PCLK2_FREQUENCY STM32_HCLK_FREQUENCY

/* APB2 timers 1 and 8 will receive PCLK2. */

#define STM32_APB2_TIM1_CLKIN (STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM8_CLKIN (STM32_PCLK2_FREQUENCY)

/* APB1 clock (PCLK1) is HCLK/2 (36MHz) */

#define STM32_RCC_CFGR_PPRE1 RCC_CFGR_PPRE1_HCLKd2
#define STM32_PCLK1_FREQUENCY (STM32_HCLK_FREQUENCY / 2)

/* APB1 timers 2-7 will be twice PCLK1 */

#define STM32_APB1_TIM2_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM3_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM4_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM5_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM6_CLKIN (2 * STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM7_CLKIN (2 * STM32_PCLK1_FREQUENCY)

/* USB divider -- Divide PLL clock by 1.5 */

#define STM32_CFGR_USBPRE 0

/* Timer Frequencies, if APBx is set to 1, frequency is same to APBx
 * otherwise frequency is 2xAPBx.
 * Note: TIM1,8 are on APB2, others on APB1 */

#define BOARD_TIM1_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM2_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM3_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM4_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM5_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM6_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM7_FREQUENCY STM32_HCLK_FREQUENCY
#define BOARD_TIM8_FREQUENCY STM32_HCLK_FREQUENCY

#define GPIO_UART4_TX (GPIO_ALT | GPIO_CNF_AFPP | GPIO_MODE_50MHz | GPIO_PORTC | GPIO_PIN10)
#define GPIO_UART4_RX (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTC | GPIO_PIN11)

#define GPIO_UART5_TX (GPIO_ALT | GPIO_CNF_AFPP | GPIO_MODE_50MHz | GPIO_PORTC | GPIO_PIN12)
#define GPIO_UART5_RX (GPIO_INPUT | GPIO_CNF_INFLOAT | GPIO_MODE_INPUT | GPIO_PORTD | GPIO_PIN2)

#define GPIO_OLED_DC (GPIO_PORTA | GPIO_PIN2 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_OLED_RESET (GPIO_PORTA | GPIO_PIN3 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_OLED_CS (GPIO_PORTB | GPIO_PIN10 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_STATUS (GPIO_PORTB | GPIO_PIN11 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_WIFI_MOD (GPIO_PORTC | GPIO_PIN12 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_FPGA_HMI1 (GPIO_PORTB | GPIO_PIN5 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_FPGA_HMI2 (GPIO_PORTB | GPIO_PIN4 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_I2C_SDA (GPIO_PORTB | GPIO_PIN9 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_I2C_SCL (GPIO_PORTB | GPIO_PIN8 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

#define GPIO_B1 (GPIO_PORTA | GPIO_PIN9 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_B2 (GPIO_PORTC | GPIO_PIN8 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_B3 (GPIO_PORTB | GPIO_PIN15 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_B4 (GPIO_PORTB | GPIO_PIN12 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_R1 (GPIO_PORTA | GPIO_PIN11 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_R2 (GPIO_PORTA | GPIO_PIN8 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_R3 (GPIO_PORTC | GPIO_PIN7 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_R4 (GPIO_PORTB | GPIO_PIN14 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_Y1 (GPIO_PORTA | GPIO_PIN10 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_Y2 (GPIO_PORTC | GPIO_PIN9 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_Y3 (GPIO_PORTC | GPIO_PIN6 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)
#define GPIO_Y4 (GPIO_PORTB | GPIO_PIN13 | GPIO_CNF_OUTPP | GPIO_MODE_50MHz)

#define ADC1_DMA_CHAN DMACHAN_ADC1

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_STM32F103_MINIMUM_BOARD_H */
