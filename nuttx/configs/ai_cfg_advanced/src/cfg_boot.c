/************************************************************************************
 * configs/viewtool-stm32f107/src/stm32_boot.c
 *
 *   Copyright (C) 2013 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include <nuttx/compiler.h>
#include <stdio.h>
#include <fcntl.h>
#include <nuttx/usb/usbdev.h>
#include <nuttx/usb/usbdev_trace.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <sys/ioctl.h>
#include <nuttx/mtd/mtd.h>
#include <arch/board/board.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <errno.h>

#include "stm32.h"
#include "stm32_gpio.h"
#include "stm32_adc.h"
#include "stm32_spi.h"

#include "../../../../apps/include/fsutils/mkfatfs.h"
#include "../../../../apps/include/fsutils/mksmartfs.h"

/************************************************************************************
 * Public Functions
 ************************************************************************************/
static const uint32_t g_adc_pinlist[] = {GPIO_ADC1_IN0, GPIO_ADC1_IN1, GPIO_ADC1_IN2, GPIO_ADC1_IN3};
static const uint8_t g_adc_chnlist[] = {0, 1, 2, 3};

#define CFG_FLASH_CS (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | GPIO_PORTA | GPIO_PIN4)

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry
 *point
 *   is called early in the intitialization -- after all memory has been
 *configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/
void board_app_initialize(void)
{
}

void stm32_boardinitialize(void)
{
    for (int i = 0; i < 4; i++)
    {
        stm32_configgpio(g_adc_pinlist[i]);
    }
    stm32_configgpio(CFG_FLASH_CS);
    stm32_gpiowrite(CFG_FLASH_CS, true);
    // stm32_configgpio(DIS_BK);
    // stm32_gpiowrite(DIS_BK, true);
}

void board_initialize(void)
{
    struct adc_dev_s *padc = stm32_adcinitialize(1, g_adc_chnlist, 4);
    adc_register("/dev/adc", padc);
    struct spi_dev_s *pspi = stm32_spibus_initialize(1);
    struct mtd_dev_s *pmtd_flash = w25_initialize(pspi);
    if (pmtd_flash)
    {
        smart_initialize(0, pmtd_flash, 0);
        if (mount("/dev/smart0", "/mnt/flash", "smartfs", 0, NULL) < 0)
        {
            mksmartfs("/dev/smart0", 256);
            if (mount("/dev/smart0", "/mnt/flash", "smartfs", 0, NULL) == 0)
            {
                pmtd_flash = filemtd_initialize("/mnt/flash/emu", 0, 512, 512);
                ftl_initialize(0, pmtd_flash);
            }
        }
        else
        {
            int fd = open("/mnt/flash/emu", O_RDWR);
            if (fd < 0)
            {
                fd = open("/mnt/flash/emu", O_RDWR | O_CREAT);
            }

            if (fd > 0)
            {
                struct stat sb;
                stat("/mnt/flash/emu", &sb);
                if (sb.st_size != 1024 * 512)
                {
                    uint8_t dummy[512] = {0};
                    for (int i = 0; i < 1024; i++)
                    {
                        write(fd, dummy, 512);
                    }
                }

                close(fd);
            }
            pmtd_flash = filemtd_initialize("/mnt/flash/emu", 0, 512, 4096);
            if (pmtd_flash)
            {
                ftl_initialize(0, pmtd_flash);
                if (mount("/dev/mtdblock0", "/mnt/storage", "vfat", 0, 0) < 0)
                {
                    struct fat_format_s format = FAT_FORMAT_INITIALIZER;
                    mkfatfs("/dev/mtdblock0", &format);
                    mount("/dev/mtdblock0", "/mnt/storage", "vfat", 0, 0);
                }
                // fd = open("/mnt/storage/emu", O_RDWR | O_CREAT);
                // printf("FDD %d\n", fd);
                // close(fd);
                // bchdev_register("/dev/mtdblock0", "/dev/mtd0", false);
            }
        }
    }
}

int weak_function main(void)
{
    // struct adc_msg_s buffer_adc[16];
    // struct fat_format_s format = FAT_FORMAT_INITIALIZER;
    // int fd = -1, ret;
    // printf("Hello from Dummy Main!\n");
    // // format.ff_fattype = 16;
    // // format.ff_nfats = 1;
    // ret = mkfatfs("/dev/mtdblock0", &format);
    // printf("make fs return %d,errno %d\n", ret, errno);
    // ret = mount("/dev/mtdblock0", "/mnt/flash", "vfat", 0, NULL);
    // printf("mount fs return %d,errno %d\n", ret, errno);

    // fd = open("/dev/adc", O_RDONLY);
    // for (;;)
    //     ;
    // for (;;)
    // {
    //     ioctl(fd, ANIOC_TRIGGER);
    //     ret = read(fd, (void *)buffer_adc, 80);
    //     ret /= 5;
    //     for (int i = 0; i < ret; i++)
    //     {
    //         printf("Channel : %d,Voltage:%d\n", buffer_adc[i].am_channel, buffer_adc[i].am_data);
    //     }
    //     usleep(500000);
    //     printf("Testing ... \n");
    // }

    return 0;
}

void stm32_usbsuspend(FAR struct usbdev_s *dev, bool resume)
{
}

uint8_t stm32_spi1status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}

uint8_t stm32_spi4status(FAR struct spi_dev_s *dev, uint32_t devid)
{
    return SPI_STATUS_PRESENT;
}

void stm32_spi1select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
    if (selected)
    {
        selected = false;
    }
    else
    {
        selected = true;
    }

    switch (devid)
    {
    case SPIDEV_FLASH(0):
        stm32_gpiowrite(CFG_FLASH_CS, selected);
        break;

    default:
        break;
    }
}

void stm32_spi4select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
}
