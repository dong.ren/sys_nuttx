#include "gw_lcd.h"
#include "../../../arch/arm/src/stm32/chip/stm32f40xxx_gpio.h"
#include "../../../arch/arm/src/stm32/chip/stm32f40xxx_rcc.h"

void ili9327_init_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_configgpio(LCD_GPIO_CS);
    stm32_configgpio(LCD_GPIO_RS);
    stm32_configgpio(LCD_GPIO_WR);
    stm32_configgpio(LCD_GPIO_RD);
}

void ili9327_destory_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_unconfiggpio(LCD_GPIO_CS);
    stm32_unconfiggpio(LCD_GPIO_RS);
    stm32_unconfiggpio(LCD_GPIO_WR);
    stm32_unconfiggpio(LCD_GPIO_RD);
}

void ili9327_init_output_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_configgpio(LCD_GPIO_OUTPUT_D0);
    stm32_configgpio(LCD_GPIO_OUTPUT_D1);
    stm32_configgpio(LCD_GPIO_OUTPUT_D2);
    stm32_configgpio(LCD_GPIO_OUTPUT_D3);
    stm32_configgpio(LCD_GPIO_OUTPUT_D4);
    stm32_configgpio(LCD_GPIO_OUTPUT_D5);
    stm32_configgpio(LCD_GPIO_OUTPUT_D6);
    stm32_configgpio(LCD_GPIO_OUTPUT_D7);
    stm32_configgpio(LCD_GPIO_OUTPUT_D8);
    stm32_configgpio(LCD_GPIO_OUTPUT_D9);
    stm32_configgpio(LCD_GPIO_OUTPUT_D10);
    stm32_configgpio(LCD_GPIO_OUTPUT_D11);
    stm32_configgpio(LCD_GPIO_OUTPUT_D12);
    stm32_configgpio(LCD_GPIO_OUTPUT_D13);
    stm32_configgpio(LCD_GPIO_OUTPUT_D14);
    stm32_configgpio(LCD_GPIO_OUTPUT_D15);
}

void ili9327_destory_output_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D0);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D1);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D2);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D3);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D4);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D5);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D6);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D7);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D8);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D9);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D10);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D11);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D12);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D13);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D14);
    stm32_unconfiggpio(LCD_GPIO_OUTPUT_D15);
}
void ili9327_init_input_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_configgpio(LCD_GPIO_INPUT_D0);
    stm32_configgpio(LCD_GPIO_INPUT_D1);
    stm32_configgpio(LCD_GPIO_INPUT_D2);
    stm32_configgpio(LCD_GPIO_INPUT_D3);
    stm32_configgpio(LCD_GPIO_INPUT_D4);
    stm32_configgpio(LCD_GPIO_INPUT_D5);
    stm32_configgpio(LCD_GPIO_INPUT_D6);
    stm32_configgpio(LCD_GPIO_INPUT_D7);
    stm32_configgpio(LCD_GPIO_INPUT_D8);
    stm32_configgpio(LCD_GPIO_INPUT_D9);
    stm32_configgpio(LCD_GPIO_INPUT_D10);
    stm32_configgpio(LCD_GPIO_INPUT_D11);
    stm32_configgpio(LCD_GPIO_INPUT_D12);
    stm32_configgpio(LCD_GPIO_INPUT_D13);
    stm32_configgpio(LCD_GPIO_INPUT_D14);
    stm32_configgpio(LCD_GPIO_INPUT_D15);
}
void ili9327_destory_input_gpio(struct ili9327_lcd_s *lcd)
{
    stm32_unconfiggpio(LCD_GPIO_INPUT_D0);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D1);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D2);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D3);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D4);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D5);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D6);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D7);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D8);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D9);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D10);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D11);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D12);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D13);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D14);
    stm32_unconfiggpio(LCD_GPIO_INPUT_D15);
}

void ili9327_select(struct ili9327_lcd_s *lcd)
{
    stm32_gpiowrite(LCD_GPIO_CS, false);
}

void ili9327_deselect(struct ili9327_lcd_s *lcd)
{
    stm32_gpiowrite(LCD_GPIO_CS, true);
}

int ili9327_sendcmd(struct ili9327_lcd_s *lcd, const uint8_t cmd)
{
    stm32_gpiowrite(LCD_GPIO_WR, false);
    stm32_gpiowrite(LCD_GPIO_RS, false);
    stm32_gpiowrite(LCD_GPIO_RD, true);
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D7, (bool)(cmd & (1 << 7)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D6, (bool)(cmd & (1 << 6)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D5, (bool)(cmd & (1 << 5)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D4, (bool)(cmd & (1 << 4)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D3, (bool)(cmd & (1 << 3)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D2, (bool)(cmd & (1 << 2)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D1, (bool)(cmd & (1 << 1)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D0, (bool)(cmd & (1 << 0)));
    stm32_gpiowrite(LCD_GPIO_WR, true);
    return 0;
}

int ili9327_sendparam(struct ili9327_lcd_s *lcd, const uint8_t param)
{
    stm32_gpiowrite(LCD_GPIO_RD, true);
    stm32_gpiowrite(LCD_GPIO_RS, true);
    stm32_gpiowrite(LCD_GPIO_WR, false);
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D7, (bool)(param & (1 << 7)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D6, (bool)(param & (1 << 6)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D5, (bool)(param & (1 << 5)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D4, (bool)(param & (1 << 4)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D3, (bool)(param & (1 << 3)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D2, (bool)(param & (1 << 2)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D1, (bool)(param & (1 << 1)));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D0, (bool)(param & (1 << 0)));
    stm32_gpiowrite(LCD_GPIO_WR, true);
    return 0;
}

int ili9327_recvparam(struct ili9327_lcd_s *lcd, uint8_t *param)
{
    int i = 0;
    bool buf[8] = {false};
    stm32_gpiowrite(LCD_GPIO_WR, true);
    stm32_gpiowrite(LCD_GPIO_RD, false);
    stm32_gpiowrite(LCD_GPIO_RS, true);
    stm32_gpiowrite(LCD_GPIO_RD, true);
    buf[0] = stm32_gpioread(LCD_GPIO_INPUT_D0);
    buf[1] = stm32_gpioread(LCD_GPIO_INPUT_D1);
    buf[2] = stm32_gpioread(LCD_GPIO_INPUT_D2);
    buf[3] = stm32_gpioread(LCD_GPIO_INPUT_D3);
    buf[4] = stm32_gpioread(LCD_GPIO_INPUT_D4);
    buf[5] = stm32_gpioread(LCD_GPIO_INPUT_D5);
    buf[6] = stm32_gpioread(LCD_GPIO_INPUT_D6);
    buf[7] = stm32_gpioread(LCD_GPIO_INPUT_D7);
    stm32_gpiowrite(LCD_GPIO_RD, false);
    int res = buf[7];
    res = res * 2 + buf[6];
    res = res * 2 + buf[5];
    res = res * 2 + buf[4];
    res = res * 2 + buf[3];
    res = res * 2 + buf[2];
    res = res * 2 + buf[1];
    res = res * 2 + buf[0];
    *param = res;
    return 0;
}

int ili9327_recvgram(struct ili9327_lcd_s *lcd, uint16_t *wd, uint32_t nwords)
{
    int i = 0, j = 0;
    uint16_t res;
    bool buf[16] = {false};
    stm32_gpiowrite(LCD_GPIO_RS, true);
    stm32_gpiowrite(LCD_GPIO_WR, true);
    stm32_gpiowrite(LCD_GPIO_RD, false);
    for (i = 0; i < nwords; i++)
    {
        stm32_gpiowrite(LCD_GPIO_RD, true);
        buf[15] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D15);
        buf[14] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D14);
        buf[13] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D13);
        buf[12] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D12);
        buf[11] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D11);
        buf[10] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D10);
        buf[9] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D9);
        buf[8] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D8);
        buf[7] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D7);
        buf[6] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D6);
        buf[5] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D5);
        buf[4] = stm32_gpioread_in_regester_pe(LCD_GPIO_INPUT_D4);
        buf[3] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D3);
        buf[2] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D2);
        buf[1] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D1);
        buf[0] = stm32_gpioread_in_regester_pd(LCD_GPIO_INPUT_D0);
        stm32_gpiowrite(LCD_GPIO_RD, false);
        res = buf[15];
        res = res * 2 + buf[14];
        res = res * 2 + buf[13];
        res = res * 2 + buf[12];
        res = res * 2 + buf[11];
        res = res * 2 + buf[10];
        res = res * 2 + buf[9];
        res = res * 2 + buf[8];
        res = res * 2 + buf[7];
        res = res * 2 + buf[6];
        res = res * 2 + buf[5];
        res = res * 2 + buf[4];
        res = res * 2 + buf[3];
        res = res * 2 + buf[2];
        res = res * 2 + buf[1];
        res = res * 2 + buf[0];
        wd[i] = res;
    }
    return 0;
}

int ili9327_sendgram(struct ili9327_lcd_s *lcd, const uint16_t *wd, uint32_t nwords)
{
    int i = 0, j = 0;
    uint16_t cmd = 0;
    stm32_gpiowrite_in_regester_pd(LCD_GPIO_RS, true);
    stm32_gpiowrite_in_regester_pd(LCD_GPIO_RD, true);
    for (i = 0; i < nwords; i++)
    {
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_WR, false);
        cmd = wd[i];
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D0, cmd & (1 << 0));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D1, cmd & (1 << 1));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D2, cmd & (1 << 2));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D3, cmd & (1 << 3));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D4, cmd & (1 << 4));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D5, cmd & (1 << 5));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D6, cmd & (1 << 6));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D7, cmd & (1 << 7));
        cmd = cmd / 256;
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D8, cmd & (1 << 0));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D9, cmd & (1 << 1));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D10, cmd & (1 << 2));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D11, cmd & (1 << 3));
        stm32_gpiowrite_in_regester_pe(LCD_GPIO_OUTPUT_D12, cmd & (1 << 4));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D13, cmd & (1 << 5));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D14, cmd & (1 << 6));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_OUTPUT_D15, cmd & (1 << 7));
        stm32_gpiowrite_in_regester_pd(LCD_GPIO_WR, true);
    }
    return 0;
}

int ili9327_backlight(struct ili9327_lcd_s *lcd, int level)
{
    return 0;
}

int ili9327_setparam(struct ili9327_lcd_s *lcd, const uint8_t cmd, const uint8_t *param, int noword)
{
    int i = 0;
    const uint8_t *p = param;
    lcd->init_gpio(lcd);
    stm32_gpiowrite(LCD_GPIO_RS, false);
    stm32_gpiowrite(LCD_GPIO_CS, false);
    stm32_gpiowrite(LCD_GPIO_WR, false);
    lcd->init_output_gpio(lcd);
    stm32_gpiowrite(LCD_GPIO_RD, true);
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D7, cmd & (1 << 7));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D6, cmd & (1 << 6));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D5, cmd & (1 << 5));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D4, cmd & (1 << 4));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D3, cmd & (1 << 3));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D2, cmd & (1 << 2));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D1, cmd & (1 << 1));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D0, cmd & (1 << 0));
    stm32_gpiowrite(LCD_GPIO_WR, true);
    stm32_gpiowrite(LCD_GPIO_RS, true);
    for (i = 0; i < noword; i++)
    {
        uint8_t temp = *p;
        stm32_gpiowrite(LCD_GPIO_WR, false);
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D7, temp & (1 << 7));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D6, temp & (1 << 6));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D5, temp & (1 << 5));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D4, temp & (1 << 4));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D3, temp & (1 << 3));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D2, temp & (1 << 2));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D1, temp & (1 << 1));
        stm32_gpiowrite(LCD_GPIO_OUTPUT_D0, temp & (1 << 0));
        stm32_gpiowrite(LCD_GPIO_WR, true);
        p++;
    }
    stm32_gpiowrite(LCD_GPIO_CS, true);
    lcd->destory_output_gpio(lcd);
    lcd->destory_gpio(lcd);
    return 0;
}

int ili9327_getparam(struct ili9327_lcd_s *lcd, const uint8_t cmd, uint8_t *param, int nowords)
{
    bool read_buf1[8] = {false};
    int i = 0, j = 0, res = 0;
    uint8_t *p = param;
    lcd->init_gpio(lcd);
    stm32_gpiowrite(LCD_GPIO_WR, false);
    stm32_gpiowrite(LCD_GPIO_CS, false);
    lcd->init_output_gpio(lcd);
    stm32_gpiowrite(LCD_GPIO_RS, false);
    stm32_gpiowrite(LCD_GPIO_RD, true);
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D7, cmd & (1 << 7));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D6, cmd & (1 << 6));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D5, cmd & (1 << 5));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D4, cmd & (1 << 4));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D3, cmd & (1 << 3));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D2, cmd & (1 << 2));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D1, cmd & (1 << 1));
    stm32_gpiowrite(LCD_GPIO_OUTPUT_D0, cmd & (1 << 0));
    stm32_gpiowrite(LCD_GPIO_WR, true);
    lcd->destory_output_gpio(lcd);
    lcd->init_input_gpio(lcd);
    stm32_gpiowrite(LCD_GPIO_RD, false);
    stm32_gpiowrite(LCD_GPIO_RS, true);
    for (i = 0; i < nowords; i++)
    {
        stm32_gpiowrite(LCD_GPIO_RD, true);
        read_buf1[0] = stm32_gpioread(LCD_GPIO_INPUT_D0);
        read_buf1[1] = stm32_gpioread(LCD_GPIO_INPUT_D1);
        read_buf1[2] = stm32_gpioread(LCD_GPIO_INPUT_D2);
        read_buf1[3] = stm32_gpioread(LCD_GPIO_INPUT_D3);
        read_buf1[4] = stm32_gpioread(LCD_GPIO_INPUT_D4);
        read_buf1[5] = stm32_gpioread(LCD_GPIO_INPUT_D5);
        read_buf1[6] = stm32_gpioread(LCD_GPIO_INPUT_D6);
        read_buf1[7] = stm32_gpioread(LCD_GPIO_INPUT_D7);
        stm32_gpiowrite(LCD_GPIO_RD, false);
        for (res = 0, j = 0; j < 8; j++)
        {
            res = res * 2 + (int)read_buf1[7 - j];
        }
        *p = res;
        p++;
    }
    stm32_gpiowrite(LCD_GPIO_CS, true);
    lcd->destory_input_gpio(lcd);
    lcd->destory_gpio(lcd);
    return 0;
}

struct ili9327_lcd_s *gw_lcd_init(void)
{
    static struct ili9327_lcd_s lcd;
    lcd.destory_gpio = ili9327_destory_gpio;
    lcd.destory_input_gpio = ili9327_destory_input_gpio;
    lcd.destory_output_gpio = ili9327_destory_output_gpio;
    lcd.init_gpio = ili9327_init_gpio;
    lcd.init_input_gpio = ili9327_init_input_gpio;
    lcd.init_output_gpio = ili9327_init_output_gpio;
    lcd.backlight = ili9327_backlight;
    lcd.deselect = ili9327_deselect;
    lcd.select = ili9327_select;
    lcd.recvgram = ili9327_recvgram;
    lcd.sendcmd = ili9327_sendcmd;
    lcd.sendgram = ili9327_sendgram;
    lcd.sendparam = ili9327_sendparam;
    lcd.setparam = ili9327_setparam;
    lcd.getparam = ili9327_getparam;
    return &lcd;
}

int board_lcd_initialize(void)
{
    return 0;
}

struct lcd_dev_s *board_lcd_getdev(int devno)
{
    UNUSED(devno);
    struct ili9327_lcd_s *plcd = gw_lcd_init();

    struct lcd_dev_s *pdev = ili9327_initialize(plcd);
    stm32_gpiowrite(LCD_GPIO_RESET, false);
    usleep(100);
    stm32_gpiowrite(LCD_GPIO_RESET, true);
    usleep(100);
    plcd->init_gpio(plcd);
    plcd->init_output_gpio(plcd);
    plcd->select(plcd);
    ili9327_config(plcd);
    plcd->deselect(plcd);
    return pdev;
}

void stm32_gpiowrite_in_regester_pd(uint32_t pinset, bool value)
{
#if defined(CONFIG_STM32_STM32L15XX) || defined(CONFIG_STM32_STM32F20XX) || \
    defined(CONFIG_STM32_STM32F30XX) || defined(CONFIG_STM32_STM32F33XX) || \
    defined(CONFIG_STM32_STM32F37XX) || defined(CONFIG_STM32_STM32F4XXX)
    uint32_t bit, pin, base = 0X40020C00;
    uint32_t port = (pinset & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    if (port < STM32_NGPIO_PORTS)
    {
        pin = (pinset & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;
        if (value)
        {
            bit = (1 << (pin));
        }
        else
        {
            bit = (1 << ((pin) + 16));
        }
        (*(volatile uint32_t *)(base + STM32_GPIO_BSRR_OFFSET) = (bit));
    }
#else
#error "Unsupported STM32 chip"
    return;
#endif
}

void stm32_gpiowrite_in_regester_pe(uint32_t pinset, bool value)
{
#if defined(CONFIG_STM32_STM32L15XX) || defined(CONFIG_STM32_STM32F20XX) || \
    defined(CONFIG_STM32_STM32F30XX) || defined(CONFIG_STM32_STM32F33XX) || \
    defined(CONFIG_STM32_STM32F37XX) || defined(CONFIG_STM32_STM32F4XXX)
    uint32_t bit, pin, base = 0x40021000;
    uint32_t port = (pinset & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    if (port < STM32_NGPIO_PORTS)
    {
        pin = (pinset & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;
        if (value)
        {
            bit = (1 << (pin));
        }
        else
        {
            bit = (1 << ((pin) + 16));
        }
        (*(volatile uint32_t *)(base + STM32_GPIO_BSRR_OFFSET) = (bit));
    }
#else
#error "Unsupported STM32 chip"
    return;
#endif
}

bool stm32_gpioread_in_regester_pe(uint32_t pinset)
{
    uint32_t base = 0x40021000, port, pin;
    port = (pinset & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    if (port < STM32_NGPIO_PORTS)
    {
        pin = (pinset & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;
        return ((getreg32(base + STM32_GPIO_IDR_OFFSET) & (1 << pin)) != 0);
    }
    return 0;
}

bool stm32_gpioread_in_regester_pd(uint32_t pinset)
{
    uint32_t base = 0X40020C00, port, pin;
    port = (pinset & GPIO_PORT_MASK) >> GPIO_PORT_SHIFT;
    if (port < STM32_NGPIO_PORTS)
    {
        pin = (pinset & GPIO_PIN_MASK) >> GPIO_PIN_SHIFT;
        return ((getreg32(base + STM32_GPIO_IDR_OFFSET) & (1 << pin)) != 0);
    }
    return 0;
}