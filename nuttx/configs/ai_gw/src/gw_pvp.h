#ifndef __CBM_PVP_H__
#define __CBM_PVP_H__

#define FUD_IN_NOTSTART			0x00
#define FUD_IN_NORMAL			0x01
#define FUD_IN_TIMEOUT			0x02

#define FUD_IN_ERR_PARITY		0x40
#define FUD_IN_ERR_SUM			0x41
#define FUD_IN_ERR_BYTE_SHORT	0x42
#define FUD_IN_ERR_BYTE_LONG	0x43
#define FUD_IN_ERR_FRAME_SHORT	0x44
#define FUD_IN_ERR_FRAME_LONG	0x45
#define FUD_IN_ERR_GAP2			0x46

#define CBM_PVP_TOOSHORT		0x80

#ifdef CONFIG_DEBUG_MVS
typedef struct cbm_pvp_s{
	uint8_t     pvp_data_len;
	uint16_t	hw_version;		/* the version of PVP hardware */
	uint16_t	sw_version;		/* the version of PVP software */
	uint16_t	voltage;
	uint16_t	cb_info;	    /* calibaration info */
	uint8_t		status;		    /* see above CMB_PVP_xxx definitions */
}cbm_pvp_t;
#else
typedef struct cbm_pvp_s{
	uint8_t     pvp_data_len;
	uint16_t	hw_version;		/* the version of PVP hardware */
	uint16_t	sw_version;		/* the version of PVP software */
	uint16_t	voltage;
	uint16_t	cb_info;	    /* calibaration info */
	uint8_t		status;		    /* see above CMB_PVP_xxx definitions */
}cbm_pvp_t;
#endif

extern int cbm_get_pvp(cbm_pvp_t *pvp);
extern int cbm_pvp_start(void);


#endif /*__CBM_PVP_H__*/
