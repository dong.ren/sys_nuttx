#include <errno.h>
#include <nuttx/config.h>
#include <nuttx/fs/fs.h>
#include <nuttx/kmalloc.h>
#include <nuttx/spi/spi.h>
#include <nuttx/wireless/sx127x.h>
#include <semaphore.h>

#ifndef CONFIG_SPI
#error "SPI driver required here"
#endif

#include <stdio.h>

#define SX127X_REG_OP_MODE_LORA_STDBY 0b10001001
#define SX127X_REG_OP_MODE_LORA_FSTXD 0b10001010
#define SX127X_REG_OP_MODE_LORA_TRANS 0b10001011
#define SX127X_REG_OP_MODE_LORA_FSRXD 0b10001100
#define SX127X_REG_OP_MODE_LORA_RXCON 0b10001101
#define SX127X_REG_OP_MODE_LORA_RXSIG 0b10001110

#define SX127X_REG_FIFO 0x00
#define SX127X_REG_OP_MODE 0x01
#define SX127X_REG_FR_MSB 0x06
#define SX127X_REG_FR_MID 0x07
#define SX127X_REG_FR_LSB 0x08
#define SX127X_REG_PA_CONFIG 0x09
#define SX127X_REG_PA_RAMP 0x0A
#define SX127X_REG_OCP 0x0B
#define SX127X_REG_LNA 0x0C
#define SX127X_REG_FIFO_ADDR_PTR 0x0D
#define SX127X_REG_FIFO_TX_BASE_ADDR 0x0E
#define SX127X_REG_FIFO_RX_BASE_ADDR 0x0F
#define SX127X_REG_FIFO_RX_CURRENT_ADDR 0x10
#define SX127X_REG_IRQ_FLAGS_MASK 0x11
#define SX127X_REG_IRQ_FLAGS 0x12
#define SX127X_REG_RX_NB_BYTES 0x13
#define SX127X_REG_RX_HEADER_CNT_MSB 0x14
#define SX127X_REG_RX_HEADER_CNT_LSB 0x15
#define SX127X_REG_RX_PACKET_CNT_MSB 0x16
#define SX127X_REG_RX_PACKET_CNT_LSB 0x17
#define SX127X_REG_MODEM_STAT 0x18
#define SX127X_REG_PACKET_SNR 0x19
#define SX127X_REG_PACKET_RSSI 0x1A
#define SX127X_REG_RSSI 0x1B
#define SX127X_REG_HOP_CHANNEL 0x1C
#define SX127X_REG_MODEM_CONFIG 0x1D
#define SX127X_REG_MODEM_CONFIG_2 0x1E
#define SX127X_REG_PAYLOAD_LENGTH 0x22
#define SX127X_REG_PAYLOAD_MAX_LENGTH 0x23
#define SX127X_REG_MODEM_CONFIG_3 0x26

#define SX127X_IRQ_RX_TIMEOUT 0x80
#define SX127X_IRQ_RX_DONE 0x40
#define SX127X_IRQ_PAYLOAD_CRC_ERROR 0x20
#define SX127X_IRQ_VALID_HEADER 0x10
#define SX127X_IRQ_TX_DONE 0x08
#define SX127X_IRQ_CAD_DONE 0x04
#define SX127X_IRQ_FHSS_CHANG 0x02
#define SX127X_IRQ_CAD_DETECTED 0x01

#define SX127X_OPMODE_LF 0b00001000
#define SX127X_OPMODE_SLEEP 0b10000000
#define SX127X_OPMODE_STDBY 0b10000001
#define SX127X_OPMODE_FSTX 0b10000010
#define SX127X_OPMODE_TX 0b10000011
#define SX127X_OPMODE_FSRX 0b10000100
#define SX127X_OPMODE_RXCON 0b10000101
#define SX127X_OPMODE_RXSIG 0b10000110
#define SX127X_OPMODE_CAD 0b10000111

struct sx127x_dev_s
{
    int dev_number;
    bool rx_single_on;
    struct spi_dev_s *p_spi;
    sem_t dev_sem;
};

static int sx127x_open(FAR struct file *p_file);
static int sx127x_close(FAR struct file *p_file);
static ssize_t sx127x_read(FAR struct file *p_file, FAR char *buffer,
                           size_t buflen);
static ssize_t sx127x_write(FAR struct file *p_file, FAR const char *buffer,
                            size_t buflen);
static int sx127x_ioctl(FAR struct file *p_file, int cmd, unsigned long arg);

static const struct file_operations g_sx127x_fops =
    {
        sx127x_open,
        sx127x_close,
        sx127x_read,
        sx127x_write,
        NULL,
        sx127x_ioctl,
#ifndef CONFIG_DISABLE_POLL
        NULL,
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
        NULL
#endif
};

static uint8_t sx127x_read_reg(struct sx127x_dev_s *p_sx127x, uint8_t reg)
{
    uint8_t p_tx[2];
    uint8_t p_rx[2];
    p_tx[0] = reg;
    SPI_LOCK(p_sx127x->p_spi, true);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), true);
    SPI_EXCHANGE(p_sx127x->p_spi, p_tx, p_rx, 2);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), false);
    SPI_LOCK(p_sx127x->p_spi, false);

    return p_rx[1];
}

static void sx127x_write_reg(struct sx127x_dev_s *p_sx127x, uint8_t reg, uint8_t data)
{
    uint8_t p_tx[2];
    p_tx[0] = reg | 0x80;
    p_tx[1] = data;

    SPI_LOCK(p_sx127x->p_spi, true);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), true);
    SPI_SNDBLOCK(p_sx127x->p_spi, p_tx, 2);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), false);
    SPI_LOCK(p_sx127x->p_spi, false);
}

static void sx127x_read_fifo(struct sx127x_dev_s *p_sx127x, uint8_t *p_buf, uint8_t len)
{
    SPI_LOCK(p_sx127x->p_spi, true);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), true);
    SPI_SEND(p_sx127x->p_spi, SX127X_REG_FIFO);
    SPI_RECVBLOCK(p_sx127x->p_spi, p_buf, len);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), false);
    SPI_LOCK(p_sx127x->p_spi, false);
}

static void sx127x_write_fifo(struct sx127x_dev_s *p_sx127x, uint8_t *p_buf, uint8_t len)
{
    SPI_LOCK(p_sx127x->p_spi, true);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), true);
    SPI_SEND(p_sx127x->p_spi, SX127X_REG_FIFO | 0x80);
    SPI_SNDBLOCK(p_sx127x->p_spi, p_buf, len);
    SPI_SELECT(p_sx127x->p_spi, SPIDEV_WIRELESS(p_sx127x->dev_number), false);
    SPI_LOCK(p_sx127x->p_spi, false);
}

static int sx127x_open(FAR struct file *p_file)
{
    struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;
    DEBUGASSERT(NULL != p_sx127x);

    if (sem_trywait(&p_sx127x->dev_sem) == 0)
    {
        p_sx127x->rx_single_on = true;
        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_SLEEP | SX127X_OPMODE_LF);
        // 20 dBm,high power
        sx127x_write_reg(p_sx127x, 0x4d, 0x87);
        // Set max power,refer to datasheet
        sx127x_write_reg(p_sx127x, SX127X_REG_PA_CONFIG, 0xff);
        sx127x_write_reg(p_sx127x, SX127X_REG_PAYLOAD_MAX_LENGTH, 0xff);
    }
    else
    {
        return -EBUSY;
    }
    return OK;
}
static int sx127x_close(FAR struct file *p_file)
{
    struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;
    sem_post(&p_sx127x->dev_sem);
    sem_destroy(&p_sx127x->dev_sem);
    return OK;
}

static ssize_t sx127x_read(FAR struct file *p_file, FAR char *buffer,
                               size_t buflen)
{
    struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;

    for (;;)
    {
        uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
        if (SX127X_IRQ_RX_TIMEOUT & reg)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
            sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
            return -ETIME;
        }

        if (SX127X_IRQ_RX_DONE & reg)
        {
            if (SX127X_IRQ_PAYLOAD_CRC_ERROR & reg)
            {
                sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
                if(p_sx127x->rx_single_on)
                {
                    sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
                    return -EBADE;
                }
            }else 
            {
                uint8_t len = sx127x_read_reg(p_sx127x, SX127X_REG_RX_NB_BYTES);
                uint8_t addr = sx127x_read_reg(p_sx127x, SX127X_REG_FIFO_RX_CURRENT_ADDR);
    
                sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_ADDR_PTR, addr);
                if (len > buflen)
                {
                    char buf[255]={0};
                    sx127x_read_fifo(p_sx127x, (void *)buf, len);
                    memcpy(buffer,buf,buflen);
                }else 
                {
                    sx127x_read_fifo(p_sx127x, (void *)buffer, len);
                }

                if(p_sx127x->rx_single_on)
                {
                    sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
                }
                sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
                return len;
            }
        }
        usleep(5000);
    }

}

// static ssize_t sx127x_read(FAR struct file *p_file, FAR char *buffer,
//                            size_t buflen)
// {
//     struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;
//     sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
//     while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
//     {
//         sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
//         usleep(1000);
//     };
//     sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_RX_BASE_ADDR, 0x00);
//     sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS_MASK, 0x0f);
//     while (1)
//     {
//         sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
//         uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
//         if ((reg & SX127X_IRQ_RX_DONE) == 0)
//         {
//             if ((reg & SX127X_IRQ_RX_TIMEOUT) == 0)
//             {
//                 if ((reg & SX127X_IRQ_PAYLOAD_CRC_ERROR) == 0)
//                 {
//                     break;
//                 }
//             }
//         }
//         usleep(2000);
//     }

//     if (p_sx127x->rx_single_on)//Rx SIG
//     {
//         sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_RXSIG | SX127X_OPMODE_LF);
//     }
//     else
//     {
//         sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_RXCON | SX127X_OPMODE_LF);
//     }

//     for (;;)
//     {
//         uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
//         if (SX127X_IRQ_RX_TIMEOUT & reg)
//         {
//             sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
//             sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
//             return -ETIME;
//         }

//         if (SX127X_IRQ_RX_DONE & reg)
//         {
//             if (SX127X_IRQ_PAYLOAD_CRC_ERROR & reg)
//             {
//                 sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
//                 sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
//                 return -EBADE;
//             }

//             uint8_t len = sx127x_read_reg(p_sx127x, SX127X_REG_RX_NB_BYTES);
//             uint8_t addr = sx127x_read_reg(p_sx127x, SX127X_REG_FIFO_RX_CURRENT_ADDR);

//             sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_ADDR_PTR, addr);
//             if (len > buflen)
//             {
//                 len = buflen;
//             }
//             sx127x_read_fifo(p_sx127x, (void *)buffer, len);
//             sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
//             sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
//             return len;
//         }
//         usleep(5000);
//     }
//     return OK;
// }

static ssize_t sx127x_write(FAR struct file *p_file, FAR const char *buffer,
                            size_t buflen)
{
    struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;
    sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
    while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
    {
        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
        usleep(1000);
    };
    sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS_MASK, 0xF7);
    while (1)
    {
        sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
        uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
        if ((reg & SX127X_IRQ_TX_DONE) == 0)
        {
            break;
        }
        usleep(2000);
    }
    sx127x_write_reg(p_sx127x, SX127X_REG_PAYLOAD_LENGTH, buflen);
    sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_TX_BASE_ADDR, 0x00);
    sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_ADDR_PTR, 0x00);
    while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
        ;
    sx127x_write_fifo(p_sx127x, (void *)buffer, buflen);
    sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_TX | SX127X_OPMODE_LF);

    for (;;)
    {
        uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
        if (SX127X_IRQ_TX_DONE & reg)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
            return buflen;
        }
        usleep(5000);
    }

    return OK;
}
static int sx127x_ioctl(FAR struct file *p_file, int cmd, unsigned long arg)
{
    struct sx127x_dev_s *p_sx127x = p_file->f_inode->i_private;
    if (NULL == (void *)arg)
    {
        return -EINVAL;
    }

    switch (cmd)
    {
    case SX127X_IOC_GET_CARRIER_FREQ:
    {
        uint32_t *p_freq = (uint32_t *)arg;
        *p_freq = 0;
        *p_freq |= (sx127x_read_reg(p_sx127x, SX127X_REG_FR_MSB) << 16);
        *p_freq |= (sx127x_read_reg(p_sx127x, SX127X_REG_FR_MID) << 8);
        *p_freq |= (sx127x_read_reg(p_sx127x, SX127X_REG_FR_LSB) << 0);
        *p_freq *= 61.035f;
        break;
    }
    case SX127X_IOC_SET_CARRIER_FREQ:
    {
        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
        while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
        {
        }

        uint32_t freq = *(uint32_t *)arg;
        uint8_t data;
        freq /= 61.035f;
        data = (freq >> 16) & 0xFF;
        sx127x_write_reg(p_sx127x, SX127X_REG_FR_MSB, data);
        data = (freq >> 8) & 0xFF;
        sx127x_write_reg(p_sx127x, SX127X_REG_FR_MID, data);
        data = (freq >> 0) & 0xFF;
        sx127x_write_reg(p_sx127x, SX127X_REG_FR_LSB, data);
        break;
    }
    case SX127X_IOC_SET_MODEM:
    {

        uint8_t *p_modem = (uint8_t *)arg;

        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
        while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
        {
        }

        sx127x_write_reg(p_sx127x, SX127X_REG_MODEM_CONFIG, p_modem[0]);
        sx127x_write_reg(p_sx127x, SX127X_REG_MODEM_CONFIG_2, p_modem[1]);
        break;
    }
    case SX127X_IOC_SET_RX_MODE:
        p_sx127x->rx_single_on = *(bool *)arg;

        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
        while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
            usleep(1000);
        };
        sx127x_write_reg(p_sx127x, SX127X_REG_FIFO_RX_BASE_ADDR, 0x00);
        sx127x_write_reg(p_sx127x,SX127X_REG_FIFO_ADDR_PTR,0x00);
        sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS_MASK, 0x0f);

        while (1)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);
            uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
            if ((reg & SX127X_IRQ_RX_DONE) == 0)
            {
                if ((reg & SX127X_IRQ_RX_TIMEOUT) == 0)
                {
                    if ((reg & SX127X_IRQ_PAYLOAD_CRC_ERROR) == 0)
                    {
                        break;
                    }
                }
            }
            usleep(2000);
        }
        if (p_sx127x->rx_single_on)//Rx SIG
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_RXSIG | SX127X_OPMODE_LF);
        }
        else
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_RXCON | SX127X_OPMODE_LF);
        }
        break;
    case SX127X_IOC_SET_CAD:
        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
        while ((sx127x_read_reg(p_sx127x, SX127X_REG_OP_MODE) & 0x01) == 0)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_STDBY | SX127X_OPMODE_LF);
            usleep(1000);
        };
        sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS_MASK, 0xfa);
        sx127x_write_reg(p_sx127x, SX127X_REG_IRQ_FLAGS, 0xff);//clr irq
        sx127x_write_reg(p_sx127x, SX127X_REG_OP_MODE, SX127X_OPMODE_CAD | SX127X_OPMODE_LF);

        break;
    case SX127X_IOC_GET_CAD:
        {
            uint8_t *ret = (uint8_t *)arg;
            uint8_t reg = sx127x_read_reg(p_sx127x,SX127X_REG_IRQ_FLAGS);
            if(reg & SX127X_IRQ_CAD_DONE)
            {
                sx127x_write_reg(p_sx127x,SX127X_REG_IRQ_FLAGS,0x04);
                if(reg & SX127X_IRQ_CAD_DETECTED)
                {
                    sx127x_write_reg(p_sx127x,SX127X_REG_IRQ_FLAGS,0x01);
                    *ret = SX127X_CAD_CHANNEL_DETECTED;
                }else 
                {
                    *ret = SX127X_CAD_CHANNEL_EMPTY;
                }
            }else 
            {
                *ret = SX127x_CAD_NO_INTERRUPT;
            }

        }
        break;
    case SX127X_IOC_GET_MODEM:
    {
        uint8_t *p_modem = (uint8_t *)arg;
        p_modem[0] = sx127x_read_reg(p_sx127x, SX127X_REG_MODEM_CONFIG);
        p_modem[1] = sx127x_read_reg(p_sx127x, SX127X_REG_MODEM_CONFIG_2);
        break;
    }
    case SX127X_IOC_GET_STATUS:
    {
        uint32_t *p_status = (uint32_t *)arg;
        *p_status = sx127x_read_reg(p_sx127x, SX127X_REG_IRQ_FLAGS);
        break;
    }
    case SX127X_IOC_GET_RSSI:
    {
        uint32_t *p_rssi = (uint32_t *)arg;
        uint8_t packet_rssi = sx127x_read_reg(p_sx127x, SX127X_REG_PACKET_RSSI);
        *p_rssi = packet_rssi - 164;
        break;
    }
    case SX127X_IOC_SET_LOW_DATA_RATE_OPTIMIZ:
    {
        bool *p_optimize = (bool *)arg;
        if (*p_optimize)
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_MODEM_CONFIG_3, 0x00);
        }
        else
        {
            sx127x_write_reg(p_sx127x, SX127X_REG_MODEM_CONFIG_3, 0x80);
        }
        break;
    }
    case SX127X_IOC_GET_LOW_DATA_RATE_OPTIMIZ:
    {
        bool *p_optimize = (bool *)arg;
        uint8_t reg = sx127x_read_reg(p_sx127x, SX127X_REG_MODEM_CONFIG_3);
        if (reg & 0x08)
        {
            *p_optimize = true;
        }
        else
        {
            *p_optimize = false;
        }
    }
    default:
        return -ENOTTY;
    }
    return OK;
}

int sx127x_register(const char *p_dev_path, FAR struct spi_dev_s *p_spi, int dev_num)
{
    FAR struct sx127x_dev_s *p_sx127x = NULL;
    DEBUGASSERT(NULL != p_dev_path);
    DEBUGASSERT(NULL != p_spi);

    p_sx127x = kmm_malloc(sizeof(struct sx127x_dev_s));

    if (NULL == p_sx127x)
    {
        return -ENOMEM;
    }

    p_sx127x->dev_number = dev_num;
    p_sx127x->p_spi = p_spi;

    SPI_SETMODE(p_sx127x->p_spi, SPIDEV_MODE0);
    SPI_SETFREQUENCY(p_sx127x->p_spi, 10000000);
    sem_init(&p_sx127x->dev_sem, 0, 1);

    return register_driver(p_dev_path, &g_sx127x_fops, 0666, p_sx127x);
}