/****************************************************************************
 * drivers/lcd/ili9327.c
 *
 * LCD driver for the ILI9327 LCD Single Chip Driver
 *
 *   Copyright (C) 2017 Huang Qi. All rights reserved.
 *   Author: Huang Qi <qi.huang@fonrich.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior writen permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <debug.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>

#include <nuttx/arch.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/ili9327.h>

#include <arch/irq.h>

/****************************************************************************
 * Private Type Definition
 ****************************************************************************/

#define ILI9327_XRES 400
#define ILI9327_YRES 240

/* Each single connected ili9327 LCD driver needs an own driver instance
 * to provide a unique getrun and putrun method. Also store fundamental
 * parameter in driver internal structure. This minimal overhead should be
 * acceptable.
 */

struct ili9327_dev_s
{
  /* Publically visible device structure */

  struct lcd_dev_s dev;

  /* Private driver-specific information follows */

  FAR struct ili9327_lcd_s *lcd;

  /* Driver specific putrun function */

  int (*putrun)(fb_coord_t row, fb_coord_t col,
                FAR const uint8_t *buffer, size_t npixels);
#ifndef CONFIG_LCD_NOGETRUN
  /* Driver specific getrun function */

  int (*getrun)(fb_coord_t row, fb_coord_t col,
                FAR uint8_t *buffer, size_t npixels);
#endif
  /* Run buffer for the device */

  uint16_t *runbuffer;

  /* Display orientation, e.g. Landscape, Portrait */

  uint8_t orient;

  /* LCD driver pixel format */

  uint8_t pxfmt;

  /* LCD driver color depth */

  uint8_t bpp;

  /* Current power state of the device */

  uint8_t power;
};

/****************************************************************************
 * Private Function Protototypes
 ****************************************************************************/

/* Internal low level helpers */

static inline uint16_t ili9327_getxres(FAR struct ili9327_dev_s *dev);
static inline uint16_t ili9327_getyres(FAR struct ili9327_dev_s *dev);

/* lcd data transfer methods */

int ili9327_putrun(fb_coord_t row, fb_coord_t col,
                   FAR const uint8_t *buffer, size_t npixels);
#ifndef CONFIG_LCD_NOGETRUN
int ili9327_getrun(fb_coord_t row, fb_coord_t col,
                   FAR uint8_t *buffer, size_t npixels);
#endif

/* lcd configuration */

static int ili9327_getvideoinfo(FAR struct lcd_dev_s *dev,
                                FAR struct fb_videoinfo_s *vinfo);
static int ili9327_getplaneinfo(FAR struct lcd_dev_s *dev, unsigned int planeno,
                                FAR struct lcd_planeinfo_s *pinfo);

/* lcd specific controls */

static int ili9327_getpower(struct lcd_dev_s *dev);
static int ili9327_setpower(struct lcd_dev_s *dev, int power);
static int ili9327_getcontrast(struct lcd_dev_s *dev);
static int ili9327_setcontrast(struct lcd_dev_s *dev, unsigned int contrast);

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* Initialize driver instance 1 < LCD_ILI9327_NINTERFACES */

static uint16_t g_runbuffer[ILI9327_XRES];

static struct ili9327_dev_s g_lcddev =
    {
        .lcd = 0,
        .putrun = ili9327_putrun,
#ifndef CONFIG_LCD_NOGETRUN
        .getrun = ili9327_getrun,
#endif
        .runbuffer = g_runbuffer,
        .orient = 0,
        .pxfmt = FB_FMT_RGB16_565,
        .bpp = 16,
        .power = 0,
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name:  ili9327_getxres
 *
 * Description:
 *   Get horicontal resolution of the connected LCD driver depending on the
 *   configured display orientation.
 *
 * Parameters:
 *   dev   - Reference to private driver structure
 *
 * Return Value:
 *
 *   Horicontal resolution
 *
 ****************************************************************************/

static inline uint16_t ili9327_getxres(FAR struct ili9327_dev_s *dev)
{
  return ILI9327_XRES;
}

/****************************************************************************
 * Name:  ili9327_getyres
 *
 * Description:
 *   Get vertical resolution of the connected LCD driver depending on the
 *   configured display orientation.
 *
 * Parameter:
 *   dev   - Reference to private driver structure
 *
 * Return Value:
 *
 *   Vertical resolution
 *
 ****************************************************************************/

static inline uint16_t ili9327_getyres(FAR struct ili9327_dev_s *dev)
{
  return ILI9327_YRES;
}

/****************************************************************************
 * Name:  ili9327_selectarea
 *
 * Description:
 *   Select the active area for displaying pixel
 *
 * Parameter:
 *   lcd       - Reference to private driver structure
 *   x0        - Start x position
 *   y0        - Start y position
 *   x1        - End x position
 *   y1        - End y position
 *
 ****************************************************************************/

static void ili9327_selectarea(struct ili9327_lcd_s *lcd,
                               uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
  /* Select column */

  lcd->sendcmd(lcd, ILI9327_COLUMN_ADDRESS_SET);
  lcd->sendparam(lcd, (x0 >> 8));
  lcd->sendparam(lcd, (x0 & 0xff));
  lcd->sendparam(lcd, (x1 >> 8));
  lcd->sendparam(lcd, (x1 & 0xff));

  /* Select page */

  lcd->sendcmd(lcd, ILI9327_PAGE_ADDRESS_SET);
  lcd->sendparam(lcd, (y0 >> 8));
  lcd->sendparam(lcd, (y0 & 0xff));
  lcd->sendparam(lcd, (y1 >> 8));
  lcd->sendparam(lcd, (y1 & 0xff));
}

/****************************************************************************
 * Name:  ili9327_putrun
 *
 * Description:
 *   Write a partial raster line to the LCD.
 *
 * Parameters:
 *   devno   - Number of lcd device
 *   row     - Starting row to write to (range: 0 <= row < yres)
 *   col     - Starting column to write to (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer containing the run to be writen to the LCD
 *   npixels - The number of pixels to write to the
 *             (range: 0 < npixels <= xres-col)
 *
 * Returned Value:
 *
 *   On success - OK
 *   On error   - -EINVAL
 *
 ****************************************************************************/

int ili9327_putrun(fb_coord_t row, fb_coord_t col,
                   FAR const uint8_t *buffer, size_t npixels)
{
  FAR struct ili9327_dev_s *dev = &g_lcddev;
  FAR struct ili9327_lcd_s *lcd = dev->lcd;
  FAR const uint16_t *src = (FAR const uint16_t *)buffer;

  DEBUGASSERT(buffer && ((uintptr_t)buffer & 1) == 0);

  /* Check if position outside of area */
  if (col + npixels > ili9327_getxres(dev) || row > ili9327_getyres(dev))
  {
    return -EINVAL;
  }

  lcd->init_gpio(lcd);
  lcd->init_output_gpio(lcd);

  /* Select lcd driver */

  lcd->select(lcd);
  //ili9327_config(lcd);
  /* Select column and area similar to the partial raster line */
  ili9327_selectarea(lcd, col, row, col + npixels - 1, row);
  /* Send memory write cmd */

  lcd->sendcmd(lcd, ILI9327_MEMORY_WRITE);
  /* Send pixel to gram */

  lcd->sendgram(lcd, src, npixels);
  /* Deselect the lcd driver */

  lcd->deselect(lcd);
  lcd->destory_output_gpio(lcd);
  lcd->destory_gpio(lcd);
  return OK;
}

/****************************************************************************
 * Name:  ili9327_getrun
 *
 * Description:
 *   Read a partial raster line from the LCD.
 *
 * Parameter:
 *   devno   - Number of the lcd device
 *   row     - Starting row to read from (range: 0 <= row < yres)
 *   col     - Starting column to read read (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer in which to return the run read from the LCD
 *   npixels - The number of pixels to read from the LCD
 *            (range: 0 < npixels <= xres-col)
 *
 * Returned Value:
 *
 *   On success - OK
 *   On error   - -EINVAL
 *
 ****************************************************************************/

#ifndef CONFIG_LCD_NOGETRUN
int ili9327_getrun(fb_coord_t row, fb_coord_t col,
                   FAR uint8_t *buffer, size_t npixels)
{
  FAR struct ili9327_dev_s *dev = &g_lcddev;
  FAR struct ili9327_lcd_s *lcd = dev->lcd;
  FAR uint16_t *dest = (FAR uint16_t *)buffer;

  DEBUGASSERT(buffer && ((uintptr_t)buffer & 1) == 0);

  /* Check if position outside of area */
  if (col + npixels > ili9327_getxres(dev) || row > ili9327_getyres(dev))
  {
    return -EINVAL;
  }

  /* Select lcd driver */
  lcd->init_gpio(lcd);
  lcd->init_output_gpio(lcd);
  lcd->select(lcd);

  /* Select column and area similar to the partial raster line */

  ili9327_selectarea(lcd, col, row, col + npixels - 1, row);

  /* Send memory read cmd */

  lcd->sendcmd(lcd, ILI9327_MEMORY_READ);
  lcd->destory_output_gpio(lcd);
  lcd->init_input_gpio(lcd);
  /* Receive pixel to gram */

  lcd->recvgram(lcd, dest, npixels);

  /* Deselect the lcd driver */

  lcd->deselect(lcd);
  lcd->destory_input_gpio(lcd);
  lcd->destory_gpio(lcd);
  return OK;
}
#endif

/****************************************************************************
 * Name:  ili9327_hwinitialize
 *
 * Description:
 *   Initialize and configure the ILI9327 LCD driver hardware.
 *
 * Parameter:
 *   dev - A reference to the driver specific structure
 *
 * Returned Value:
 *
 *   On success - OK
 *   On error - EINVAL
 *
 ****************************************************************************/

static int ili9327_hwinitialize(FAR struct ili9327_dev_s *dev)
{
#ifdef CONFIG_DEBUG_LCD_INFO
  uint8_t param;
#endif
  FAR struct ili9327_lcd_s *lcd = dev->lcd;

  /* Select spi device */

  lcdinfo("Initialize lcd driver\n");
  lcd->init_gpio(lcd);
  lcd->select(lcd);
  lcd->init_output_gpio(lcd);

  /* Reset the lcd display to the default state */

  lcdinfo("ili9327 LCD driver: Software Reset\n");
  lcd->sendcmd(lcd, ILI9327_SOFTWARE_RESET);
  up_mdelay(5);

  lcdinfo("ili9327 LCD driver: set Memory Access Control: %04x\n", dev->orient);
  lcd->sendcmd(lcd, ILI9327_MEMORY_ACCESS_CONTROL);
  lcd->sendparam(lcd, dev->orient);

  /* Select column and area */

  ili9327_selectarea(lcd, 0, 0, ILI9327_XRES, ILI9327_YRES);

  /* Pixel Format set */

  lcd->sendcmd(lcd, ILI9327_PIXEL_FORMAT_SET);
  lcd->sendparam(lcd, 0x05);
  /* 16 bit RGB565 */

  // lcdinfo("ili9327 LCD driver: Set Pixel Format: %04x\n",
  //         ILI9327_PIXSET_16BITMCU_PARAM1);
  // lcd->sendparam(lcd, ILI9327_PIXSET_16BITMCU_PARAM1);

  /* 18 bit RGB666, add settings here */

  // lcdinfo("ili9327 LCD driver: Set Interface control\n");
  // lcd->sendcmd(lcd, ILI9327_INTERFACE_CONTROL);
  // lcd->sendparam(lcd, ILI9327_IFCTL_PARAM1);
  // lcd->sendparam(lcd, ILI9327_IFCTL_PARAM2);
  // lcd->sendparam(lcd, ILI9327_IFCTL_PARAM3);

  /* Sleep out */

  lcdinfo("ili9327 LCD driver: Sleep Out\n");
  lcd->sendcmd(lcd, ILI9327_SLEEP_OUT);
  up_mdelay(120);

  /* Deselect the device */

  lcd->deselect(lcd);
  lcd->destory_output_gpio(lcd);
  lcd->destory_gpio(lcd);

  /* Switch display off */

  ili9327_setpower(&dev->dev, 0);

  return OK;
}

int ili9327_config(FAR struct ili9327_lcd_s *lcd)
{
  lcd->init_gpio(lcd);
  lcd->select(lcd);
  lcd->init_output_gpio(lcd);
  lcd->sendcmd(lcd, 0x36);
  lcd->sendparam(lcd, 0x28);
  lcd->sendcmd(lcd, 0x3A);
  lcd->sendparam(lcd, 0x05);
  lcd->sendcmd(lcd, 0xB1);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0x1A);
  lcd->sendcmd(lcd, 0xB6); // Display Function Control
  lcd->sendparam(lcd, 0x0A);
  lcd->sendparam(lcd, 0xA2);
  lcd->sendcmd(lcd, 0x2B);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0x01);
  lcd->sendparam(lcd, 0x8f);
  lcd->sendcmd(lcd, 0x2A);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0x00);
  lcd->sendparam(lcd, 0xef);
  lcd->sendcmd(lcd, 0x11); //Exit Sleep
  usleep(100);
  lcd->sendcmd(lcd, 0x29); //display on
                           //LCD_SetParam(true);      //设置LCD参数
  lcd->deselect(lcd);
  lcd->destory_output_gpio(lcd);
  lcd->destory_gpio(lcd);
  return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name:  ili9327_getrunx
 *
 * Description:
 *   Read a partial raster line from the LCD.
 *
 * Parameter:
 *   row     - Starting row to read from (range: 0 <= row < yres)
 *   col     - Starting column to read from (range: 0 <= col <= xres-npixels)
 *   buffer  - The buffer containing the run to be writen to the LCD
 *   npixels - The number of pixels to read from the
 *             (range: 0 < npixels <= xres-col)
 *
 * Returned Value:
 *
 *   On success - OK
 *   On error   - -EINVAL
 *
 ****************************************************************************/

/****************************************************************************
 * Name:  ili9327_getvideoinfo
 *
 * Description:
 *   Get information about the LCD video controller configuration.
 *
 * Parameter:
 *   dev - A reference to the driver specific structure
 *   vinfo - A reference to the videoinfo structure
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -EINVAL
 *
 ****************************************************************************/

static int ili9327_getvideoinfo(FAR struct lcd_dev_s *dev,
                                FAR struct fb_videoinfo_s *vinfo)
{
  if (dev && vinfo)
  {
    FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;

    vinfo->fmt = priv->pxfmt;
    vinfo->xres = ili9327_getxres(priv);
    vinfo->yres = ili9327_getyres(priv);
    vinfo->nplanes = 1;

    lcdinfo("fmt: %d xres: %d yres: %d nplanes: %d\n",
            vinfo->fmt, vinfo->xres, vinfo->yres, vinfo->nplanes);

    return OK;
  }

  return -EINVAL;
}

/****************************************************************************
 * Name:  ili9327_getplaneinfo
 *
 * Description:
 *   Get information about the configuration of each LCD color plane.
 *
 * Parameter:
 *   dev     - A reference to the driver specific structure
 *   planeno - The plane number
 *   pinfo   - A reference to the planeinfo structure
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -EINVAL
 *
 ****************************************************************************/

static int ili9327_getplaneinfo(FAR struct lcd_dev_s *dev, unsigned int planeno,
                                FAR struct lcd_planeinfo_s *pinfo)
{
  if (dev && pinfo && planeno == 0)
  {
    FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;

    pinfo->putrun = priv->putrun;
#ifndef CONFIG_LCD_NOGETRUN
    pinfo->getrun = priv->getrun;
#endif
    pinfo->bpp = priv->bpp;
    pinfo->buffer = (FAR uint8_t *)priv->runbuffer; /* Run scratch buffer */

    lcdinfo("planeno: %d bpp: %d\n", planeno, pinfo->bpp);

    return OK;
  }

  return -EINVAL;
}

/****************************************************************************
 * Name:  ili9327_getpower
 *
 * Description:
 *   Get the LCD panel power status (0: full off - CONFIG_LCD_MAXPOWER: full on.
 *   On backlit LCDs, this setting may correspond to the backlight setting.
 *
 * Parameter:
 *   dev     - A reference to the driver specific structure
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -EINVAL
 *
 ****************************************************************************/

static int ili9327_getpower(FAR struct lcd_dev_s *dev)
{
  FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;

  if (priv)
  {
    lcdinfo("%d\n", priv->power);

    return priv->power;
  }

  return -EINVAL;
}

/****************************************************************************
 * Name:  ili9327_setpower
 *
 * Description:
 *   Enable/disable LCD panel power (0: full off - CONFIG_LCD_MAXPOWER: full on).
 *   On backlight LCDs, this setting may correspond to the backlight setting.
 *
 * Parameter:
 *   dev   - A reference to the driver specific structure
 *   power - Value of the power
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -EINVAL
 *
 ****************************************************************************/

static int ili9327_setpower(FAR struct lcd_dev_s *dev, int power)
{
  FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;
  FAR struct ili9327_lcd_s *lcd = priv->lcd;

  if (dev)
  {
    lcdinfo("%d\n", power);
    lcd->init_gpio(lcd);
    lcd->select(lcd);
    lcd->init_output_gpio(lcd);

    if (power > 0)
    {
      /* Set backlight level */

      lcd->backlight(lcd, power);

      /* And switch LCD on */

      lcd->sendcmd(lcd, ILI9327_DISPLAY_ON);
      up_mdelay(120);
    }
    else
    {
      /* Switch LCD off */

      lcd->sendcmd(lcd, ILI9327_DISPLAY_OFF);
    }

    lcd->deselect(lcd);
    lcd->destory_output_gpio(lcd);
    lcd->destory_gpio(lcd);

    priv->power = power;

    return OK;
  }

  return -EINVAL;
}

/****************************************************************************
 * Name:  ili9327_getcontrast
 *
 * Description:
 *   Get the current contrast setting (0-CONFIG_LCD_MAXCONTRAST).
 *
 * Parameter:
 *   dev   - A reference to the lcd driver structure
 *
 * Returned Value:
 *
 *  On success - current contrast value
 *  On error   - -ENOSYS, not supported by the ili9327.
 *
 ****************************************************************************/

static int ili9327_getcontrast(struct lcd_dev_s *dev)
{
  lcdinfo("Not implemented\n");
  return -ENOSYS;
}

/****************************************************************************
 * Name:  ili9327_setcontrast
 *
 * Description:
 *   Set LCD panel contrast (0-CONFIG_LCD_MAXCONTRAST).
 *
 * Parameter:
 *   dev   - A reference to the lcd driver structure
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -ENOSYS, not supported by the ili9327.
 *
 ****************************************************************************/

static int ili9327_setcontrast(struct lcd_dev_s *dev, unsigned int contrast)
{
  lcdinfo("contrast: %d\n", contrast);
  return -ENOSYS;
}

/****************************************************************************
 * Name:  ili9327_initialize
 *
 * Description:
 *   Initialize the LCD video driver internal sturcture. Also initialize the
 *   lcd hardware if not done. The control of the LCD driver is depend on the
 *   selected MCU interface and part of the platform specific subdriver (see
 *   config/stm32f429i-disco/src/stm32_ili93274ws.c)
 *
 * Input Parameters:
 *
 *   lcd - A reference to the platform specific driver instance to control the
 *     ili9327 display driver.
 *   devno - A value in the range of 0 through CONFIG_ILI9327_NINTERFACES-1.
 *     This allows support for multiple LCD devices.
 *
 * Returned Value:
 *
 *   On success, this function returns a reference to the LCD driver object for
 *   the specified LCD driver. NULL is returned on any failure.
 *
 ****************************************************************************/

FAR struct lcd_dev_s *ili9327_initialize(FAR struct ili9327_lcd_s *lcd)
{
  FAR struct ili9327_dev_s *priv = &g_lcddev;
  /* Check if initialized */

  if (!priv->lcd)
  {
    FAR struct lcd_dev_s *dev = &priv->dev;
    int ret;

    /* Initialize internal structure */

    dev->getvideoinfo = ili9327_getvideoinfo;
    dev->getplaneinfo = ili9327_getplaneinfo;
    dev->getpower = ili9327_getpower;
    dev->setpower = ili9327_setpower;
    dev->getcontrast = ili9327_getcontrast;
    dev->setcontrast = ili9327_setcontrast;
    priv->lcd = lcd;
    //ili9327_config(lcd);
    /* Initialze the LCD driver */

    // ret = ili9327_hwinitialize(priv);
    ret = OK;

    if (ret == OK)
    {
      return &priv->dev;
    }

    errno = EINVAL;
  }

  return NULL;
}

/****************************************************************************
 * Name:  ili9327_clear
 *
 * Description:
 *   This is a non-standard LCD interface.  Because of the various rotations,
 *   clearing the display in the normal way by writing a sequences of runs that
 *   covers the entire display can be very slow. Here the display is cleared by
 *   simply setting all GRAM memory to the specified color.
 *
 * Parameter:
 *   dev   - A reference to the lcd driver structure
 *   color - The background color
 *
 * Returned Value:
 *
 *  On success - OK
 *  On error   - -EINVAL
 *
 ****************************************************************************/

int ili9327_clear(FAR struct lcd_dev_s *dev, uint16_t *color)
{
  FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;
  FAR struct ili9327_lcd_s *lcd = priv->lcd;
  uint16_t xres = 432; //ili9327_getxres(priv);
  uint16_t yres = 240; //ili9327_getyres(priv);
  uint32_t n;

  if (!lcd)
  {
    return -EINVAL;
  }
  /* Select lcd driver */
  lcd->init_gpio(lcd);
  lcd->select(lcd);
  lcd->init_output_gpio(lcd);
  /* Select column and area similar to the visible area */

  ili9327_selectarea(lcd, 0, 0, xres - 1, yres - 1);

  /* Send memory write cmd */

  lcd->sendcmd(lcd, ILI9327_MEMORY_WRITE);

  /* clear the visible area */

  for (n = 0; n < xres * yres; n++)
  {
    /* Send pixel to gram */

    lcd->sendgram(lcd, color, 1);
  }

  /* Deselect the lcd driver */

  lcd->deselect(lcd);
  lcd->destory_output_gpio(lcd);
  lcd->destory_gpio(lcd);
  return OK;
}

// int ili9327_test(FAR struct lcd_dev_s *dev)
// {

//   FAR struct ili9327_dev_s *priv = (FAR struct ili9327_dev_s *)dev;
//   FAR struct ili9327_lcd_s *plcd = priv->lcd;
//   int i;
//   uint16_t test[240];
//   for (i = 0; i < 240; i++)
//   {
//     test[i] = 0x1200 + i;
//   }

//   plcd->init_gpio(plcd);
//   plcd->init_output_gpio(plcd);
//   ili9327_config(plcd);

//   plcd->select(plcd);
//   plcd->sendcmd(plcd, ILI9327_COLUMN_ADDRESS_SET);
//   plcd->sendparam(plcd, (60 >> 8));
//   plcd->sendparam(plcd, (60 & 0xff));
//   plcd->sendparam(plcd, (75 >> 8));
//   plcd->sendparam(plcd, (75 & 0xff));
//   plcd->sendcmd(plcd, ILI9327_PAGE_ADDRESS_SET);
//   plcd->sendparam(plcd, (200 >> 8));
//   plcd->sendparam(plcd, (200 & 0xff));
//   plcd->sendparam(plcd, (214 >> 8));
//   plcd->sendparam(plcd, (214 & 0xff));
//   plcd->sendcmd(plcd, ILI9327_WRITE_MEMORY_CONTINUE);
//   plcd->sendgram(plcd, test, 240);
//   plcd->deselect(plcd);

//   plcd->select(plcd);
//   plcd->sendcmd(plcd, ILI9327_COLUMN_ADDRESS_SET);
//   plcd->sendparam(plcd, (60 >> 8));
//   plcd->sendparam(plcd, (60 & 0xff));
//   plcd->sendparam(plcd, (75 >> 8));
//   plcd->sendparam(plcd, (75 & 0xff));
//   plcd->sendcmd(plcd, ILI9327_PAGE_ADDRESS_SET);
//   plcd->sendparam(plcd, (200 >> 8));
//   plcd->sendparam(plcd, (200 & 0xff));
//   plcd->sendparam(plcd, (214 >> 8));
//   plcd->sendparam(plcd, (214 & 0xff));
//   plcd->sendcmd(plcd, ILI9327_READ_MEMORY_CONTINUE);
//   plcd->destory_output_gpio(plcd);
//   plcd->init_input_gpio(plcd);
//   plcd->recvgram(plcd, test, 240);
//   plcd->deselect(plcd);

//   for (i = 0; i < 240; i++)
//   {

//     printf("idx %d:%d\n", i, test[i]);
//   }
// }
